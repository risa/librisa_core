<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf

SPDX-License-Identifier: CC-PDDC
-->

# libRISA_Core - basic image stream processing algorithms

### Check [Configuration.md](Configuration.md) for a description of implemented stages.

### To be used with [RISA](https://codebase.helmholtz.cloud/risa/RISA).

### Prerequisites:
- cmake (>= 3.19)
- CUDA (>= 11.2) and compatible host compiler (e.g. gcc >= 9)
- Boost (>= 1.71)
- glados (included as gitmodule)
- libtiff
- (optional) HDF5

### Installation on Ubuntu (&geq; 20.04):
- install cmake (if only an older version is installed by default, get the newest one from [here](https://cmake.org/download/)): `sudo apt install cmake`
- install boost: `sudo apt install libboost-all-dev`
- install CUDA (download from NVIDIA homepage: https://developer.nvidia.com/cuda-toolkit and follow the  instructions)
- in order to build the documentation, `doxygen` and `graphviz` need to be installed: `sudo apt install doxygen graphviz`

### Building the software:
```
git clone https://codebase.helmholtz.cloud/risa/librisa_core 
cd librisa_core
git submodule update --init --recursive
mkdir ../build && cd ../build
cmake ../librisa_core
make
```
- By default, the software is built for CUDA compute capability 6.1. To add more, change the second to last line of `CMakeLists.txt` in `/libRISA_Core`:  
e.g. `set_property(TARGET RISA_Core PROPERTY CUDA_ARCHITECTURES 50)`  
- if build was successful, there is a shared library `libRISA_Core.so` in the `build/lib` directory
- the doxygen documentation can be found in the `docs/html` folder

### Running the example
Check the [example configuration](example/example_config.json) in the example directory. Therein, adapt the file paths for 
- `libraryPaths` ([Line 4](https://codebase.helmholtz.cloud/risa/librisa_core/-/blob/main/example/example_config.json?ref_type=heads#L4)) : the compiled library file (.dll or .so),
- `pipeline.runtimeConfigPath`([Line 7](https://codebase.helmholtz.cloud/risa/librisa_core/-/blob/main/example/example_config.json?ref_type=heads#L7)): any writable temporary path for runtime configuration files,
- `imSeqLoader.filePath` ([Line 74](https://codebase.helmholtz.cloud/risa/librisa_core/-/blob/main/example/example_config.json?ref_type=heads#L74)): the example images from the [example/data](example/data) directory.

Run `./risa C:\dev\librisa_core\example\example_config.json` to start the example. It auto-terminates after a few seconds but can also be cancelled at any point using `Ctrl + C`.
