// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Convert/ushortToFloat.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

ushortToFloat::ushortToFloat(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Convert: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&ushortToFloat::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert: Running " << m_numberOfDevices << " thread(s).";
}

ushortToFloat::~ushortToFloat()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto ushortToFloat::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert: Finished.";
	}
}

auto ushortToFloat::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto ushortToFloat::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	float factor = 1.0;
	if(m_normalize)
		factor = 1.0 / ((float)m_normalizeMaxValue);

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert: Received sentinel, finishing.";
			break;
		}

		// if necessary, request memory from MemoryPool here
		auto ret =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

		convert<<<grids, blocks, 0, m_streams[deviceID]>>>(
			img.data(), ret.data(), m_imageWidth, m_imageHeight, factor);

		CHECK(cudaPeekAtLastError());

		ret.setDevice(deviceID);
		ret.setProperties(img.properties());

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(auto i = 1; i < m_numberOfOutputs; i++)
		{
			auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
				m_memoryPoolIdxs[deviceID]);

			cudaMemcpyAsync(imgSplit.data(), ret.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);

			imgSplit.setDevice(deviceID);
			imgSplit.setProperties(ret.properties());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			m_results[i].push(std::move(imgSplit));
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Convert:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << ret.index() << " processed.";
		m_results[0].push(std::move(ret));
	}
}

auto ushortToFloat::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".normalize", m_normalize))
	{

		if(m_normalize
			&& !configReader.lookupValue(
				parameterSet + ".normalizeMaxValue", m_normalizeMaxValue))
		{
			BOOST_LOG_TRIVIAL(error)
				<< "risa::cuda::Convert:: Normalization enabled but normalizeMaxValue not provided.";
			return false;
		}

		return true;
	}
	else
		return false;
}

__global__ void convert(const unsigned short* __restrict__ in, float* __restrict__ out, const int imageWidth,
	const int imageHeight, const float factor)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	out[y * imageWidth + x] = (float)in[y * imageWidth + x] * factor;
}

}
}
