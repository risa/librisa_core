// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Average/Average.h>
#include <risaCore/ConfigReader/ConfigReader.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Average::Average(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Average: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Average::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average: Running " << m_numberOfDevices << " thread(s).";
}

Average::~Average()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Average::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average: Finished.";
	}
}

auto Average::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Average::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	int averagedImageSize;
	if(m_averagingType == detail::AveragingType::averageColumns)
	{
		averagedImageSize = m_imageHeight * m_numberOfPlanes;
	}
	else if(m_averagingType == detail::AveragingType::averageRows)
	{
		averagedImageSize = m_imageWidth * m_numberOfPlanes;
	}
	else if(m_averagingType == detail::AveragingType::averagePixelwise)
	{
		averagedImageSize = m_imageHeight * m_imageWidth * m_numberOfPlanes;
	}
	else
	{
		// we should never reach this point but it stops the compiler from throwing warnings
		BOOST_LOG_TRIVIAL(error) << "risa::cuda::Average: Thread stared with unknown average type.";
		return;
	}

	unsigned int receivedImages = 0;

	std::vector<float> averagedImage_h(averagedImageSize, 0.0);
	auto averagedImage_d =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(averagedImageSize);
	CHECK(cudaMemcpyAsync(averagedImage_d.get(), averagedImage_h.data(),
		sizeof(float) * averagedImage_h.size(), cudaMemcpyHostToDevice, m_streams[deviceID]));

	std::vector<float> accumulatedImages_h(m_imageWidth * m_imageHeight * m_numberOfPlanes, 0.0);
	auto accumulatedImages_d =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(accumulatedImages_h.size());
	CHECK(cudaMemcpyAsync(accumulatedImages_d.get(), accumulatedImages_h.data(),
		sizeof(float) * accumulatedImages_h.size(), cudaMemcpyHostToDevice, m_streams[deviceID]));

	std::vector<std::map<std::string, std::any>> latestProperties(m_numberOfPlanes);

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::Average: Received final image. Pushing average results.";

			float weight;

			receivedImages /=
				m_numberOfPlanes; // normalize number of received images to normalize accordingly per plane

			if(m_averagingType == detail::AveragingType::averageColumns)
			{
				weight = 1.0 / ((float)receivedImages * (float)m_imageWidth);
			}
			else if(m_averagingType == detail::AveragingType::averageRows)
			{
				weight = 1.0 / ((float)receivedImages * (float)m_imageHeight);
			}
			else if(m_averagingType == detail::AveragingType::averagePixelwise)
			{
				weight = 1.0 / ((float)receivedImages);
			}
			else
			{
				// we should never reach this point but it stops the compiler from throwing warnings
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Average: Thread stared with unknown average type.";
				return;
			}

			// input stream done, we now send the images to the next stage
			// we need one output image per plane
			for(int plane = 0; plane < m_numberOfPlanes; plane++)
			{
				auto ret = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				const int planeOffset = m_imageWidth * m_imageHeight * plane;

				if(m_averagingType == detail::AveragingType::averagePixelwise)
				{
					averagePixelwise<<<grids, blocks, 0, m_streams[deviceID]>>>(
						&accumulatedImages_d.get()[planeOffset], averagedImage_d.get(), m_imageWidth,
						m_imageHeight, plane, weight);
				}
				else if(m_averagingType == detail::AveragingType::averageRows)
				{
					averageRows<<<grids, blocks, 0, m_streams[deviceID]>>>(
						&accumulatedImages_d.get()[planeOffset], averagedImage_d.get(), m_imageWidth,
						m_imageHeight, plane, weight);
				}
				else if(m_averagingType == detail::AveragingType::averageColumns)
				{
					averageColumns<<<grids, blocks, 0, m_streams[deviceID]>>>(
						&accumulatedImages_d.get()[planeOffset], averagedImage_d.get(), m_imageWidth,
						m_imageHeight, plane, weight);
				}

				cudaMemcpyAsync(ret.data(),
					&averagedImage_d.get()[plane * averagedImageSize / m_numberOfPlanes],
					m_imageWidth * m_imageHeight * sizeof(float), cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				ret.setDevice(deviceID);
				ret.setProperties(latestProperties[plane]);
				ret.setPlane(plane);

				CHECK(cudaStreamSynchronize(m_streams[deviceID]));

				for(int i = 1; i < m_numberOfOutputs; i++)
				{
					auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
						m_memoryPoolIdxs[deviceID]);

					cudaMemcpyAsync(imgSplit.data(), ret.data(), m_imageWidth * m_imageHeight * sizeof(float),
						cudaMemcpyDeviceToDevice, m_streams[deviceID]);

					imgSplit.setIdx(ret.index());
					imgSplit.setDevice(deviceID);
					imgSplit.setPlane(ret.plane());
					imgSplit.setStart(ret.start());
					imgSplit.setProperties(ret.properties());

					// wait until work on device is finished
					CHECK(cudaStreamSynchronize(m_streams[deviceID]));
					BOOST_LOG_TRIVIAL(debug)
						<< "risa::cuda::Average:        (Device " << deviceID << " | Output " << i
						<< "): Image " << imgSplit.index() << " processed.";
					m_results[i].push(std::move(imgSplit));
				}

				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Average:        (Device " << deviceID << " | Output "
										 << 0 << "): Image " << ret.index() << " processed.";
				m_results[0].push(std::move(ret));
			}
			break;
		}

		receivedImages++;

		// using average with weight 1.0 sums all pixel values
		averagePixelwise<<<grids, blocks, 0, m_streams[deviceID]>>>(
			img.data(), accumulatedImages_d.get(), m_imageWidth, m_imageHeight, img.plane(), 1.0);

		CHECK(cudaPeekAtLastError());

		latestProperties[img.plane()] = img.properties();

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
	}
}

auto Average::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string averagingType;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".averagingType", averagingType))
	{

		if(averagingType == "averagePixelwise")
			m_averagingType = detail::AveragingType::averagePixelwise;
		else if(averagingType == "averageRows")
			m_averagingType = detail::AveragingType::averageRows;
		else if(averagingType == "averageColumns")
			m_averagingType = detail::AveragingType::averageColumns;
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Average: Unknown averaging type '" << averagingType
									 << "'.";
			return false;
		}

		// generic streams may not set a "number of planes"
		if(!configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
		{
			m_numberOfPlanes = 1;
		}
		if(m_numberOfPlanes < 1)
		{
			m_numberOfPlanes = 1;
		}


		return true;
	}
	else
		return false;
}

__global__ void averageRows(const float* __restrict__ in, float* out, const int imageWidth,
	const int imageHeight, const int planeId, const float weightPerRow)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	// use 1 x n_det threads
	if(x >= imageWidth || y > 0)
		return;

	// loop over all rows then update output
	const int planeOffset = planeId * imageWidth;
	float sumOverRows = 0.0;
	for(auto row = 0; row < imageHeight; row++)
		sumOverRows += (float)in[row * imageWidth + x];

	out[planeOffset + x] += sumOverRows * weightPerRow;
}

__global__ void averageColumns(const float* __restrict__ in, float* out, const int imageWidth,
	const int imageHeight, const int planeId, const float weightPerColumn)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	// use 1 x n_det threads
	if(x > 0 || y >= imageHeight)
		return;

	// loop over all rows then update output
	const int planeOffset = planeId * imageHeight;
	float sumOverColumns = 0.0;
	for(auto col = 0; col < imageWidth; col++)
		sumOverColumns += (float)in[y * imageWidth + col];

	out[planeOffset + y] += sumOverColumns * weightPerColumn;
}

__global__ void averagePixelwise(const float* __restrict__ in, float* out, const int imageWidth,
	const int imageHeight, const int planeId, const float weightPerImage)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	// use 1 x n_det threads
	if(x >= imageWidth || y >= imageHeight)
		return;

	const int planeOffset = planeId * imageHeight * imageWidth;

	out[planeOffset + y * imageWidth + x] += (float)in[y * imageWidth + x] * weightPerImage;
}

}
}
