// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Flip/Flip.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Flip::Flip(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Flip: Configuration file could not be loaded successfully.");
	}
	
	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::Flip: Number of input ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Flip::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip: Running " << m_numberOfDevices << " thread(s).";
}

Flip::~Flip()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Flip::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip: Finished.";
	}
}

auto Flip::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

//per device, one thread executes this function
auto Flip::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	using imageType = glados::Image<deviceManagerType>;

	std::vector<imageType> copiesOfFlippedImage;
	copiesOfFlippedImage.reserve(m_numberOfOutputs - 1);//make space

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip: Received sentinel, finishing.";
			break;
		}

		// if necessary, request memory from MemoryPool here

		if(m_flipHorizontally)
		{
			flipHorizontally<<<grids, blocks, 0, m_streams[deviceID]>>>(img.data(), m_imageWidth, m_imageHeight);
		}
		else
		{
			flipVertically<<<grids, blocks, 0, m_streams[deviceID]>>>(img.data(), m_imageWidth, m_imageHeight);
		}
		

		CHECK(cudaPeekAtLastError());

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(int i = 1; i < m_numberOfOutputs; i++)
		{
			auto imgSplit =
				glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

			cudaMemcpyAsync(imgSplit.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);

			imgSplit.setIdx(img.index());
			imgSplit.setDevice(deviceID);
			imgSplit.setPlane(img.plane());
			imgSplit.setStart(img.start());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip:        (Device " << deviceID << " | Output "
									 << i << "): Image " << imgSplit.index() << " processed.";
			m_results[i].push(std::move(imgSplit));
		}

		// wait until work on device is finished

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Flip:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << img.index() << " processed.";
		m_results[0].push(std::move(img));
	}
}

auto Flip::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string flipMode;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".flipMode", flipMode))
	{
		if (flipMode == "horizontal") {
			m_flipHorizontally = true;
		}
		else if (flipMode == "vertical") {
			m_flipHorizontally = false;
		}
		else {
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::Flip: flipMode '" << flipMode << "' is not supported.";
			return false;
		}
		return true;
	}
	return false;
}

__global__ void flipHorizontally(float* __restrict__ data, const int imageWidth,const int imageHeight)
{
	auto x = glados::cuda::getX();
	
	if(x > imageWidth / 2)
		return;
	
	auto y = glados::cuda::getY();
	if(y >= imageHeight)
		return;

	const auto flipWithThisX = imageWidth - x - 1;
	const auto idx1 = y * imageWidth + x;
	const auto idx2 = y * imageWidth + flipWithThisX;

	float tmp = data[idx1];
	data[idx1] = data[idx2];
	data[idx2] = tmp;
}

__global__ void flipVertically(float* __restrict__ data, const int imageWidth, const int imageHeight)
{
	auto y = glados::cuda::getY();
	if(y > imageHeight / 2)
		return;

	auto x = glados::cuda::getX();
	if(x >= imageWidth)
		return;

	const auto flipWithThisY = imageHeight - y - 1;
	const auto idx1 = y * imageWidth + x;
	const auto idx2 = flipWithThisY * imageWidth + x;

	float tmp = data[idx1];
	data[idx1] = data[idx2];
	data[idx2] = tmp;
}

}
}
