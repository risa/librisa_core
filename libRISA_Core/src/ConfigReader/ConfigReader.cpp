// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "../include/risaCore/ConfigReader/ConfigReader.h"

namespace risa
{

ConfigReader::ConfigReader(const char* configFile, bool allowedToFail) : m_allowedToFail{allowedToFail}
{
	parseFile(configFile);
}

ConfigReader::ConfigReader(bool allowedToFail) : m_allowedToFail{allowedToFail} {}

auto ConfigReader::parseFile(const std::string& configPath) -> bool
{
	try
	{
		std::ifstream fstream(configPath);
		cfg = nlohmann::json::parse(fstream);
	}
	catch(nlohmann::json::parse_error& err)
	{
		// output exception information
		std::cerr << "message: " << err.what() << '\n'
				  << "exception id: " << err.id << '\n'
				  << "byte position of error: " << err.byte << std::endl;
		exit(1);
	}

	m_isInitialized = true;

	return true;
}

auto ConfigReader::parseString(const std::string& configString) -> bool
{
	try
	{
		std::stringstream sstream;
		sstream << configString;
		cfg = nlohmann::json::parse(sstream);
	}
	catch(nlohmann::json::parse_error& err)
	{
		// output exception information
		std::cerr << "message: " << err.what() << '\n'
				  << "exception id: " << err.id << '\n'
				  << "byte position of error: " << err.byte << std::endl;
		exit(1);
	}

	m_isInitialized = true;

	return true;
}

}
