// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Masking/Masking.h>

#include <risaCore/Basics/UtilityKernels.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

#ifdef __linux__
#define FLT_MIN __FLT_MIN__
#define FLT_MAX __FLT_MAX__
#endif

#define GET_MAX(A, B) A > B ? A : B
#define GET_MIN(A, B) A < B ? A : B
// if both are masked, retrun FLT_MIN/MAX. if only one value is masked, return the other one.  if none is
// masked, return GET_MAX(A,B):
#define GET_MAX_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? -FLT_MAX : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : (GET_MAX(A, B))))
#define GET_MIN_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? FLT_MAX : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : (GET_MIN(A, B))))

namespace risa
{
namespace cuda
{

Masking::Masking(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Masking: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	// populate results vector with an empty queue for each output
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 1));
		m_streams[i] = stream;
	}

	// precalculate mask
	if(!createMask())
	{
		throw std::runtime_error("risa::cuda::Masking: Unable to create mask with given parameters.");
	}

	m_doConfig = true;

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Masking::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: Running " << m_numberOfDevices << " thread(s).";
}

Masking::~Masking()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: Destroyed.";
}

auto Masking::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: Finished.";
	}
}

auto Masking::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto Masking::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid(
		(int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: Running thread for device " << deviceID;

	while(m_doConfig)
	{
		auto imageMask_d =
			glados::cuda::make_device_ptr<bool, glados::cuda::async_copy_policy>(m_imageMask.size());
		auto imageMaskData = std::make_unique<bool[]>(
			m_imageMask.size()); // workaround because std::vector<bool> doesn't have .data method
		std::copy(m_imageMask.begin(), m_imageMask.end(), imageMaskData.get());
		CHECK(cudaMemcpyAsync(imageMask_d.get(), imageMaskData.get(), sizeof(bool) * m_imageMask.size(),
			cudaMemcpyHostToDevice, m_streams[deviceID]));
		m_doConfig = false;

		const auto sharedMemBytes = detail::ReduceProperty::getSharedMemSize(m_blockSize2D);
		const unsigned int numThreads = m_blockSize2D * m_blockSize2D;
		const unsigned int numBlocks = (m_imageWidth * m_imageHeight + numThreads - 1) / numThreads
			/ 2; // div by 2 because each block reads data from an area twice its size

		auto minMaxVals_d =
			glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(2 * numBlocks);
		std::vector<float> minMaxVals_h(2 * numBlocks, 0.0);

		bool doAutoNormalize = m_doAutoNormalize;
		float fixedMinValue = m_fixedMinValue;
		float fixedMaxValue = m_fixedMaxValue;

		// accumulating min/max values (until reconfig)
		std::vector<float> imageMinVal(m_numberOfPlanes, FLT_MAX);
		std::vector<float> imageMaxVal(m_numberOfPlanes, FLT_MIN);

		if(!doAutoNormalize)
		{
			for(int i = 0; i < m_numberOfPlanes; i++)
			{
				imageMinVal[i] = fixedMinValue;
				imageMaxVal[i] = fixedMaxValue;
			}
		}

		while(true)
		{
			auto img = m_imgs[deviceID][0].take();
			if(!img.valid())
				break;

			auto plane = img.plane();

			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			if(doAutoNormalize)
			{
				switch(m_blockSize2D * m_blockSize2D)
				{
				case 256:
					reduceKernel<256, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum, true>
						<<<numBlocks, numThreads, sharedMemBytes, m_streams[deviceID]>>>(
							img.data(), m_imageHeight * m_imageWidth, minMaxVals_d.get(), imageMask_d.get());
					break;
				case 1024:
					reduceKernel<1024, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum,
						true><<<numBlocks, numThreads, sharedMemBytes, m_streams[deviceID]>>>(
						img.data(), m_imageHeight * m_imageWidth, minMaxVals_d.get(), imageMask_d.get());
					break;
				}
				CHECK(cudaPeekAtLastError());
				CHECK(cudaMemcpyAsync(minMaxVals_h.data(), minMaxVals_d.get(), 2 * numBlocks * sizeof(float),
					cudaMemcpyDeviceToHost, m_streams[deviceID]));
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));

				for(auto i = 0; i < numBlocks; i++)
				{
					const auto minIdx = detail::ReduceProperty::getNthMinIdx(numBlocks, i);
					imageMinVal[plane] = GET_MIN(imageMinVal[plane], minMaxVals_h[minIdx]);

					const auto maxIdx = detail::ReduceProperty::getNthMaxIdx(numBlocks, i);
					imageMaxVal[plane] = GET_MAX(imageMaxVal[plane], minMaxVals_h[maxIdx]);
				}
			}

			normalizeImage<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
				img.data(), m_imageWidth, m_imageHeight, imageMaxVal[plane], imageMinVal[plane], 1.0, 0.0);
			CHECK(cudaPeekAtLastError());

			applyImageMask<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
				img.data(), m_maskingValue, m_imageWidth, m_imageHeight, imageMask_d.get());
			CHECK(cudaPeekAtLastError());

			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(img.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking:        (Device " << deviceID << " | Output "
										 << i << "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			// wait until work on device is finished

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking:        (Device " << deviceID << " | Output "
									 << 0 << "): Image " << img.index() << " processed.";
			m_results[0].push(std::move(img));

			if(m_doConfig)
			{ // trigger for runtime reconfiguration
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Masking:        Reconfiguration triggered.";
				break; // breaks inner while, outer will keep running after reconfiguration
			}
		}
	}
}

auto Masking::createMask() -> bool
{

	m_imageMask = std::vector<bool>(
		m_imageWidth * m_imageHeight, false); //!< where true, image will be set to m_maskingValue

	if(m_maskingType == detail::MaskingType::circleMaskInside)
		return createCircleMask(false);
	else if(m_maskingType == detail::MaskingType::circleMaskOutside)
		return createCircleMask(true);
	else if(m_maskingType == detail::MaskingType::rectangleMaskInside)
		return createRectangularMask(false);
	else if(m_maskingType == detail::MaskingType::rectangleMaskOutside)
		return createRectangularMask(true);
	else if(m_maskingType == detail::MaskingType::lowerPart)
	{
		m_rectangleMaskLowerLeftX = 0;
		m_rectangleMaskWidth = m_imageWidth;
		m_rectangleMaskLowerLeftY = (float)m_imageHeight * (1.0 - m_maskingFraction);
		m_rectangleMaskHeight = m_imageHeight - m_rectangleMaskLowerLeftY;
		return createRectangularMask(false);
	}
	else if(m_maskingType == detail::MaskingType::upperPart)
	{
		m_rectangleMaskLowerLeftX = 0;
		m_rectangleMaskWidth = m_imageWidth;
		m_rectangleMaskLowerLeftY = 0;
		m_rectangleMaskHeight = (float)m_imageHeight * m_maskingFraction;
		return createRectangularMask(false);
	}
	else if(m_maskingType == detail::MaskingType::leftPart)
	{
		m_rectangleMaskLowerLeftX = 0;
		m_rectangleMaskWidth = (float)m_imageWidth * m_maskingFraction;
		m_rectangleMaskLowerLeftY = 0;
		m_rectangleMaskHeight = m_imageHeight;
		return createRectangularMask(false);
	}
	else if(m_maskingType == detail::MaskingType::rightPart)
	{
		m_rectangleMaskLowerLeftX = (float)m_imageWidth * (1.0 - m_maskingFraction);
		;
		m_rectangleMaskWidth = m_imageWidth - m_rectangleMaskLowerLeftX;
		m_rectangleMaskLowerLeftY = 0;
		m_rectangleMaskHeight = m_imageHeight;
		return createRectangularMask(false);
	}
	else if(m_maskingType == detail::MaskingType::maskingTypeNoMask)
	{
		// image mask is allready instantiated with `false` values, just return
		return false;
	}

	return true;
}

auto Masking::createCircleMask(bool maskOuterRegion) -> bool
{
	float rSquared = m_circleMaskRadius * m_circleMaskRadius;

	for(int y = 0; y < m_imageHeight; y++)
	{
		float dy = y - m_circleMaskCenterY;
		for(int x = 0; x < m_imageWidth; x++)
		{
			float dx = x - m_circleMaskCenterX;
			float dist = dy * dy + dx * dx;

			if(maskOuterRegion && dist >= rSquared)
				m_imageMask[y * m_imageWidth + x] = true;

			if(!maskOuterRegion && dist <= rSquared)
				m_imageMask[y * m_imageWidth + x] = true;
		}
	}

	return true;
}

auto Masking::createRectangularMask(bool maskOuterRegion) -> bool
{
	if(!maskOuterRegion)
	{
		// mask everything inside the given rectangle
		if(m_rectangleMaskLowerLeftX >= m_imageWidth || m_rectangleMaskLowerLeftY >= m_imageHeight
			|| m_rectangleMaskWidth == 0 || m_rectangleMaskHeight == 0)
			return true;

		auto xMax = (m_rectangleMaskLowerLeftX + m_rectangleMaskWidth >= m_imageWidth
				? m_imageWidth
				: m_rectangleMaskLowerLeftX + m_rectangleMaskWidth);
		auto yMax = (m_rectangleMaskLowerLeftY + m_rectangleMaskHeight >= m_imageHeight
				? m_imageHeight
				: m_rectangleMaskLowerLeftY + m_rectangleMaskHeight);

		for(auto y = m_rectangleMaskLowerLeftY; y < yMax; y++)
			for(auto x = m_rectangleMaskLowerLeftX; x < xMax; x++)
				m_imageMask[y * m_imageWidth + x] = true;
	}
	else
	{
		// mask everything outside the given rectangle
		if(m_rectangleMaskLowerLeftX >= m_imageWidth || m_rectangleMaskLowerLeftY >= m_imageHeight
			|| m_rectangleMaskWidth == 0 || m_rectangleMaskHeight == 0)
			return false; // doesn't make sense to mask everything outside of an out-of-bounds
						  // rectangle

		for(auto y = 0; y < m_imageHeight; y++)
		{

			if(y < m_rectangleMaskLowerLeftY || y > m_rectangleMaskLowerLeftY + m_rectangleMaskHeight)
			{
				// row out of mask -> mask all x values
				for(auto x = 0; x < m_imageWidth; x++)
					m_imageMask[y * m_imageWidth + x] = true;
				continue;
			}

			for(auto x = 0; x < m_imageWidth; x++)
			{
				// row inside mask range -> check x
				if(x < m_rectangleMaskLowerLeftX || x > m_rectangleMaskLowerLeftX + m_rectangleMaskWidth)
					m_imageMask[y * m_imageWidth + x] = true;
			}
		}
	}

	return true;
}

auto Masking::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			std::string maskingType;

			hasValidSetting = configReader.lookupValue("maskingValue", m_maskingValue);
			hasValidSetting =
				configReader.lookupValue("doAutoNormalize", m_doAutoNormalize) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("fixedMinValue", m_fixedMinValue) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("fixedMaxValue", m_fixedMaxValue) || hasValidSetting;

			if(configReader.lookupValue("maskingType", maskingType))
			{
				m_maskingType = getMaskingTypeFromString(maskingType);
				hasValidSetting = true;
			}

			if(m_maskingType == detail::MaskingType::circleMaskInside
				|| m_maskingType == detail::MaskingType::circleMaskOutside)
			{
				hasValidSetting =
					configReader.lookupValue("circleMaskCenterX", m_circleMaskCenterX) || hasValidSetting;
				hasValidSetting =
					configReader.lookupValue("circleMaskCenterY", m_circleMaskCenterY) || hasValidSetting;
				hasValidSetting =
					configReader.lookupValue("circleMaskRadius", m_circleMaskRadius) || hasValidSetting;
			}

			if(m_maskingType == detail::MaskingType::rectangleMaskInside
				|| m_maskingType == detail::MaskingType::rectangleMaskOutside)
			{
				hasValidSetting =
					configReader.lookupValue("rectangleMaskLowerLeftX", m_rectangleMaskLowerLeftX)
					|| hasValidSetting;
				hasValidSetting =
					configReader.lookupValue("rectangleMaskLowerLeftY", m_rectangleMaskLowerLeftY)
					|| hasValidSetting;
				hasValidSetting =
					configReader.lookupValue("rectangleMaskWidth", m_rectangleMaskWidth) || hasValidSetting;
				hasValidSetting =
					configReader.lookupValue("rectangleMaskHeight", m_rectangleMaskHeight) || hasValidSetting;
			}

			if(m_maskingType == detail::MaskingType::upperPart
				|| m_maskingType == detail::MaskingType::lowerPart
				|| m_maskingType == detail::MaskingType::leftPart
				|| m_maskingType == detail::MaskingType::rightPart)
			{
				hasValidSetting =
					configReader.lookupValue("maskingFraction", m_maskingFraction) || hasValidSetting;
			}
			if(m_maskingFraction < 0.0 || m_maskingFraction >= 1.0)
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Masking: Masking fraction" << m_maskingFraction
										 << "out of range. Use value 0 <= maskingFraction < 1.0.";
				hasValidSetting = false;
			}

			if(hasValidSetting)
			{
				if(createMask())
				{
					std::unique_lock<std::mutex> lock(m_mutex);
					m_doConfig = true; // trigger reconfig
				}
				else
				{
					BOOST_LOG_TRIVIAL(warning)
						<< "risa::cuda::Masking: Unable to create mask with given values.";
				}
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Masking: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto Masking::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string maskingType;

	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".maskingValue", m_maskingValue)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".maskingType", maskingType)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes)
		&& configReader.lookupValue(parameterSet + ".doAutoNormalize", m_doAutoNormalize))
	{

		m_maskingType = getMaskingTypeFromString(maskingType);

		if(m_maskingType == detail::MaskingType::UNDEFINED)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::Masking: Unknown masking type '" << maskingType << "'.";
			return false;
		}

		if(m_maskingType == detail::MaskingType::circleMaskInside
			|| m_maskingType == detail::MaskingType::circleMaskOutside)
		{
			if(!configReader.lookupValue(parameterSet + ".circleMaskCenterX", m_circleMaskCenterX)
				|| !configReader.lookupValue(parameterSet + ".circleMaskCenterY", m_circleMaskCenterY)
				|| !configReader.lookupValue(parameterSet + ".circleMaskRadius", m_circleMaskRadius))
			{

				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Masking: Missing parameter for masking type '" << maskingType << "'.";
				return false;
			}
		}

		if(m_maskingType == detail::MaskingType::rectangleMaskInside
			|| m_maskingType == detail::MaskingType::rectangleMaskOutside)
		{
			if(!configReader.lookupValue(parameterSet + ".rectangleMaskLowerLeftX", m_rectangleMaskLowerLeftX)
				|| !configReader.lookupValue(
					parameterSet + ".rectangleMaskLowerLeftY", m_rectangleMaskLowerLeftY)
				|| !configReader.lookupValue(parameterSet + ".rectangleMaskWidth", m_rectangleMaskWidth)
				|| !configReader.lookupValue(parameterSet + ".rectangleMaskHeight", m_rectangleMaskHeight))
			{

				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Masking: Missing parameter for masking type '" << maskingType << "'.";
				return false;
			}
		}

		if(m_maskingType == detail::MaskingType::upperPart || m_maskingType == detail::MaskingType::lowerPart
			|| m_maskingType == detail::MaskingType::leftPart
			|| m_maskingType == detail::MaskingType::rightPart)
		{
			if(!configReader.lookupValue(parameterSet + ".maskingFraction", m_maskingFraction))
			{

				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Masking: Missing parameter for masking type '" << maskingType << "'.";
				return false;
			}

			if(m_maskingFraction < 0.0 || m_maskingFraction >= 1.0)
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Masking: Masking fraction" << m_maskingFraction
										 << "out of range. Use value 0 <= maskingFraction < 1.0.";
				return false;
			}
		}

		if(!m_doAutoNormalize)
		{
			if(!configReader.lookupValue(parameterSet + ".fixedMinValue", m_fixedMinValue)
				|| !configReader.lookupValue(parameterSet + ".fixedMaxValue", m_fixedMaxValue))
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Masking: Auto-normalization is disabled, but fixed "
											"min and max values not provided.";
				return false;
			}
		}

		return true;
	}
	return false;
}

__global__ void applyImageMask(float* __restrict__ img, const float value, const int imageWidth,
	const int imageHeight, const bool* const __restrict__ imageMask)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	if(imageMask[y * imageWidth + x])
	{
		img[y * imageWidth + x] = value;
	}
}

__global__ void normalizeImage(float* __restrict__ img, const int imageWidth, const int imageHeight,
	const float maxValue, const float minValue, const float normalizeToValueMax,
	const float normalizeToValueMin)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;

	const float inputDynamic = maxValue - minValue;
	const float outputDynamic = normalizeToValueMax - normalizeToValueMin;

	img[idx] = normalizeToValueMin + ((img[idx] - minValue) / inputDynamic) * outputDynamic;
}

}
}
