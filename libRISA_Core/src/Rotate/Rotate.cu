// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Rotate/Rotate.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

namespace risa
{
namespace cuda
{

Rotate::Rotate(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Rotate: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_rotatedWidth * m_rotatedHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Rotate::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate: Running " << m_numberOfDevices << " thread(s).";
}

Rotate::~Rotate()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Rotate::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate: Finished.";
	}
}

auto Rotate::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Rotate::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(
		std::ceil(m_rotatedWidth / (float)m_blockSize2D), std::ceil(m_rotatedHeight / (float)m_blockSize2D));

	auto weights_d = glados::cuda::make_device_ptr<float>(m_rotatedHeight * m_rotatedWidth * 4);
	auto indices_d = glados::cuda::make_device_ptr<ssize_t>(m_rotatedHeight * m_rotatedWidth * 4);

	while(m_doConfig)
	{
		const float rotationAngle_rad = m_rotationAngle / 180.0f * M_PI;
		const float scalingFactor = (m_rotationScale == detail::RotationScale::KeepSize) ? 1.0f
			: (m_rotationScale == detail::RotationScale::ShrinkToFit)					 ? 1.0f
																						 : 0.0f;
		setupRotation<<<grids, blocks, 0, m_streams[deviceID]>>>(m_imageWidth, m_imageHeight, m_rotatedWidth,
			m_rotatedHeight, indices_d.get(), weights_d.get(), rotationAngle_rad, scalingFactor);

		m_doConfig = false;

		while(true)
		{
			auto img = m_imgs[deviceID][0].take();
			if(!img.valid())
			{
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate: Received sentinel, finishing.";
				break;
			}

			auto ret = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
				m_memoryPoolIdxs[deviceID]);

			performRotation<<<grids, blocks, 0, m_streams[deviceID]>>>(
				img.data(), ret.data(), m_rotatedWidth, m_rotatedHeight, indices_d.get(), weights_d.get());

			CHECK(cudaPeekAtLastError());

			ret.setDevice(deviceID);
			ret.setProperties(img.properties());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(auto i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), ret.data(), m_rotatedWidth * m_rotatedHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(ret.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				m_results[i].push(std::move(imgSplit));
			}

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Rotate:        (Device " << deviceID << " | Output " << 0
									 << "): Image " << ret.index() << " processed.";
			m_results[0].push(std::move(ret));

			if(m_doConfig)
			{
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Rotate:        Reconfiguration triggered.";
				break;
			}
		}
	}
}

auto Rotate::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			std::string maskingType;

			hasValidSetting = configReader.lookupValue("rotationAngleDeg", m_rotationAngle);
			if(hasValidSetting)
			{
				m_doConfig = true;
			}
		}
	}
}

auto Rotate::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".rotatedWidth", m_rotatedWidth)
		&& configReader.lookupValue(parameterSet + ".rotatedHeight", m_rotatedHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".rotationAngleDeg", m_rotationAngle))
	{
		std::string rotationScale;
		if(!configReader.lookupValue(parameterSet + ".scaleMode", rotationScale))
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "risa::cuda::Rotate: Rotation scale mode not defined. Default to 'ShrinkToFit'";
			m_rotationScale = detail::RotationScale::ShrinkToFit;
		}
		else
		{
			if(rotationScale == "ShrinkToFit")
			{
				m_rotationScale = detail::RotationScale::ShrinkToFit;
			}
			else if(rotationScale == "KeepSize")
			{
				m_rotationScale = detail::RotationScale::KeepSize;
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Rotate: Unknown rotation scale mode '"
										 << rotationScale << "'. Use 'ShrinkToFit' or 'KeepSize'";
				return false;
			}
		}
		return true;
	}
	else
		return false;
}

__global__ void performRotation(const float* __restrict__ input, float* rotated, const int rotatedWidth,
	const int rotatedHeight, const ssize_t* indices, const float* weights)
{
	auto rotX = glados::cuda::getX();
	auto rotY = glados::cuda::getY();

	if(rotX >= rotatedWidth || rotY >= rotatedHeight)
		return;

	const auto idxRotated = (rotY * rotatedWidth + rotX);

	if(indices[idxRotated * 4] < 0)
	{
		rotated[idxRotated] = 0.0f;
		return;
	}

	rotated[idxRotated] = input[indices[idxRotated * 4 + 0]] * weights[idxRotated * 4 + 0]
		+ input[indices[idxRotated * 4 + 1]] * weights[idxRotated * 4 + 1]
		+ input[indices[idxRotated * 4 + 2]] * weights[idxRotated * 4 + 2]
		+ input[indices[idxRotated * 4 + 3]] * weights[idxRotated * 4 + 3];
}

__global__ void setupRotation(const int inputWidth, const int inputHeight, const int rotatedWidth,
	const int rotatedHeight, ssize_t* indices, float* weights, const float rotationAngle_rad,
	const float scalingFactor)
{
	auto rotX = glados::cuda::getX();
	auto rotY = glados::cuda::getY();

	if(rotX >= rotatedWidth || rotY >= rotatedHeight)
		return;

	const auto idxRotated =
		(rotY * rotatedWidth + rotX) * 4; // 4 entries per pixel (neighbouring indices and weights)

	const float cenRotX = 0.5f * (rotatedWidth - 1);
	const float cenRotY = 0.5f * (rotatedHeight - 1);
	const float lenRot = sqrtf((rotX - cenRotX) * (rotX - cenRotX) + (rotY - cenRotY) * (rotY - cenRotY));
	const float angRot = atan2f(rotY - cenRotY, rotX - cenRotX);

	const float angIn = angRot + rotationAngle_rad;
	const float lenIn = lenRot * scalingFactor;
	const float cenInX = 0.5f * (inputWidth - 1);
	const float cenInY = 0.5f * (inputHeight - 1);
	const float inX = lenIn * cosf(angIn) + cenInX;
	const float inY = lenIn * sinf(angIn) + cenInY;

	const int inX0 = floorf(inX);
	const int inX1 = ceilf(inX);
	const int inY0 = floorf(inY);
	const int inY1 = ceilf(inY);

	const size_t idxIn00 = inY0 * inputWidth + inX0;
	const size_t idxIn10 = inY0 * inputWidth + inX1;
	const size_t idxIn01 = inY1 * inputWidth + inX0;
	const size_t idxIn11 = inY1 * inputWidth + inX1;

	if(inX < 0 || inX >= inputWidth || inY < 0 || inY >= inputHeight)
	{
		// no input pixel corresponding to current output pixel
		indices[idxRotated + 0] = -1;
		indices[idxRotated + 1] = -1;
		indices[idxRotated + 2] = -1;
		indices[idxRotated + 3] = -1;
		weights[idxRotated + 0] = 0.0f;
		weights[idxRotated + 1] = 0.0f;
		weights[idxRotated + 2] = 0.0f;
		weights[idxRotated + 3] = 0.0f;
		return;
	}

	if(inX0 == inX1 && inY0 == inY1)
	{
		// single perfect match
		indices[idxRotated + 0] = idxIn00;
		indices[idxRotated + 1] = idxIn00;
		indices[idxRotated + 2] = idxIn00;
		indices[idxRotated + 3] = idxIn00;
		weights[idxRotated + 0] = 1.0f;
		weights[idxRotated + 1] = 0.0f;
		weights[idxRotated + 2] = 0.0f;
		weights[idxRotated + 3] = 0.0f;
		return;
	}

	if(inX0 == inX1)
	{
		// perfect match in x, need to interpolate y
		indices[idxRotated + 0] = idxIn00;
		indices[idxRotated + 1] = idxIn01;
		indices[idxRotated + 2] = idxIn00;
		indices[idxRotated + 3] = idxIn00;
		weights[idxRotated + 0] = (inY1 - inY);
		weights[idxRotated + 1] = (inY - inY0);
		weights[idxRotated + 2] = 0.0f;
		weights[idxRotated + 3] = 0.0f;
		return;
	}

	if(inY0 == inY1)
	{
		// perfect match in y, need to interpolate x
		indices[idxRotated + 0] = idxIn00;
		indices[idxRotated + 1] = idxIn10;
		indices[idxRotated + 2] = idxIn00;
		indices[idxRotated + 3] = idxIn00;
		weights[idxRotated + 0] = (inX1 - inX);
		weights[idxRotated + 1] = (inX - inX0);
		weights[idxRotated + 2] = 0.0f;
		weights[idxRotated + 3] = 0.0f;
		return;
	}

	// need bilinear interpolation
	indices[idxRotated + 0] = idxIn00;
	indices[idxRotated + 1] = idxIn01;
	indices[idxRotated + 2] = idxIn10;
	indices[idxRotated + 3] = idxIn11;
	weights[idxRotated + 0] = 0.25f * (inX1 - inX + inY1 - inY);
	weights[idxRotated + 1] = 0.25f * (inX1 - inX + inY - inY0);
	weights[idxRotated + 2] = 0.25f * (inX - inX0 + inY1 - inY);
	weights[idxRotated + 3] = 0.25f * (inX - inX0 + inY - inY0);
	return;
}
}
}
