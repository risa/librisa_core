// SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Statistics/HistogramPerPixel.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

HistogramPerPixel::HistogramPerPixel(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::HistogramPerPixel: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&HistogramPerPixel::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HistogramPerPixel: Running " << m_numberOfDevices
							 << " thread(s).";
}

HistogramPerPixel::~HistogramPerPixel()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto HistogramPerPixel::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HistogramPerPixel:        (Device " << img.device()
								 << " |  Input " << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HistogramPerPixel: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HistogramPerPixel: Finished.";
	}
}

auto HistogramPerPixel::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto HistogramPerPixel::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HistogramPerPixel: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(
		std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	size_t histogramSize = m_imageHeight * m_imageWidth * m_numberOfHistogramBins;
	std::vector<float> histogram_h(histogramSize * m_numberOfPlanes, 0);
	auto histogram_d = glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(histogramSize * m_numberOfPlanes);
	CHECK(cudaMemcpyAsync(histogram_d.get(), histogram_h.data(), sizeof(float) * histogram_h.size(),
		cudaMemcpyHostToDevice, m_streams[deviceID]));

	std::vector<std::map<std::string, std::any>> latestProperties(m_numberOfPlanes);

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(info)
				<< "risa::cuda::HistogramPerPixel: Received final image. Pushing HistogramPerPixel results.";

			// input stream done, we now send the images to the next stage
			// we need one output image per plane
			for(int plane = 0; plane < m_numberOfPlanes; plane++)
			{
				const int planeOffset = m_imageWidth * m_imageHeight * m_numberOfHistogramBins * plane;
				const float incPerBin =
					(m_maxValue - m_minValue) / static_cast<float>(m_numberOfHistogramBins);
				for(int bin = 0; bin < m_numberOfHistogramBins; bin++)
				{
					auto ret = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
						m_memoryPoolIdxs[deviceID]);
					auto offs = planeOffset + bin * m_imageHeight * m_imageWidth;

					cudaMemcpyAsync(ret.data(), &histogram_d.get()[offs],
						m_imageWidth * m_imageHeight * sizeof(float), cudaMemcpyDeviceToDevice,
						m_streams[deviceID]);

					ret.setDevice(deviceID);
					ret.setProperties(latestProperties[plane]);
					ret.setPlane(plane);
					float binCenter = m_minValue + (0.5f + static_cast<float>(bin)) * incPerBin;

					std::ignore = ret.setProperty("histogramBinCenter", binCenter); // just a convenience property, not relevant for processing

					CHECK(cudaStreamSynchronize(m_streams[deviceID]));

					for(int i = 1; i < m_numberOfOutputs; i++)
					{
						auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
							m_memoryPoolIdxs[deviceID]);

						cudaMemcpyAsync(imgSplit.data(), ret.data(),
							m_imageWidth * m_imageHeight * sizeof(float), cudaMemcpyDeviceToDevice,
							m_streams[deviceID]);

						imgSplit.setIdx(ret.index());
						imgSplit.setDevice(deviceID);
						imgSplit.setPlane(ret.plane());
						imgSplit.setStart(ret.start());
						imgSplit.setProperties(ret.properties());

						// wait until work on device is finished
						CHECK(cudaStreamSynchronize(m_streams[deviceID]));
						BOOST_LOG_TRIVIAL(debug)
							<< "risa::cuda::HistogramPerPixel:        (Device " << deviceID << " | Output "
							<< i << "): Image " << imgSplit.index() << " processed.";
						m_results[i].push(std::move(imgSplit));
					}

					BOOST_LOG_TRIVIAL(debug)
						<< "risa::cuda::HistogramPerPixel:        (Device " << deviceID << " | Output " << 0
						<< "): Image " << ret.index() << " processed.";
					m_results[0].push(std::move(ret));
				}
			}
			break;
		}

		fillHistogram<<<grids, blocks, 0, m_streams[deviceID]>>>(img.data() /*input image*/,
			m_imageWidth /*imageWidth*/, m_imageHeight /*imageHeight*/,
			&histogram_d.get()[histogramSize * img.plane()] /* relevant histogram for this plane */,
			m_numberOfHistogramBins /* number of histogram bins*/, m_minValue /* lower histogram bound */,
			m_maxValue /*upper histogram bound*/);

		CHECK(cudaPeekAtLastError());

		latestProperties[img.plane()] = img.properties();

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
	}
}

auto HistogramPerPixel::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string averagingType;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".numberOfHistogramBins", m_numberOfHistogramBins)
		&& configReader.lookupValue(parameterSet + ".histogramMinValue", m_minValue)
		&& configReader.lookupValue(parameterSet + ".histogramMaxValue", m_maxValue))
	{
		if(m_numberOfHistogramBins > 256)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::HistogramPerPixel: Max supported number of bins is 256. Requested "
				<< m_numberOfHistogramBins;
			return false;
		}

		// generic streams may not set a "number of planes"
		if(!configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
		{
			m_numberOfPlanes = 1;
		}
		if(m_numberOfPlanes < 1)
		{
			m_numberOfPlanes = 1;
		}

		return true;
	}
	else
		return false;
}

__global__ void fillHistogram(const float* __restrict__ image, const int imageWidth, const int imageHeight,
	float* histogram, const int numBins, const float minValue, const float maxValue)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	// use 1 x n_det threads
	if(x >= imageWidth || y >= imageHeight)
		return;

	const size_t imIdx = y * imageWidth + x;
	int bin = (image[imIdx] - minValue) / (maxValue - minValue) * static_cast<float>(numBins);

	bin = bin * (bin >= 0); // set to zero if below zero without branching
	bin = numBins - 1
		- (numBins - bin - 1)
			* (bin < numBins); // set to numBins - 1 if >= numBins. Works because we ensured that bin>=0

	const size_t histIdx = bin * imageHeight * imageWidth + imIdx;

	histogram[histIdx]++;
}

}
}
