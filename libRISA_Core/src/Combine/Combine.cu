// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/Combine/Combine.h>
#include <risaCore/ConfigReader/ConfigReader.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>

namespace risa
{
namespace cuda
{

Combine::Combine(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Combine: Configuration file could not be loaded successfully.");
	}

	if((m_combineSource == detail::combineSource::combineSourceStream || m_combineSource == detail::combineSource::combineSourceStreamSingle) && numberOfInputs != 2)
	{
		BOOST_LOG_TRIVIAL(error)
			<< "risa::cuda::Combine: Using combineSourceStream expects 2 input streams. Got: "
			<< numberOfInputs;
		throw std::runtime_error("risa::cuda::Combine: Wrong number of input streams.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));

		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Combine::processor, this, i};
	}

	m_doConfig = true;

	// initialize input stream counter
	m_activeInputStreams = m_numberOfInputs;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Running " << m_numberOfDevices << " thread(s).";
}

Combine::~Combine()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Destroyed.";
}

auto Combine::process(input_type&& image, int inputIdx) -> void
{

	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine:        (Device " << image.device() << " |  Input "
								 << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else
	{
		if(m_combineSource == detail::combineSource::combineSourceStream
			|| m_combineSource == detail::combineSource::combineSourceStreamSingle)
		{
			std::unique_lock<std::mutex> lock(
				m_mutex); // we have to lock here because the threads per input stream may try to access
						 // m_activeInputStreams simultaneously

			m_activeInputStreams--;
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: " << m_numberOfInputs - m_activeInputStreams
									 << " / " << m_numberOfInputs << " input streams finished.";

			if(m_activeInputStreams == 0)
			{
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Received sentinel, finishing.";

				// send sentinel to all processor threads and wait 'til they're finished
				for(auto i = 0; i < m_numberOfDevices; i++)
				{
					m_images[i][0].push(input_type());
				}

				for(auto i = 0; i < m_numberOfDevices; i++)
				{
					m_processorThreads[i].join();
				}
				// push sentinel to results for next stage
				for(int i = 0; i < m_numberOfOutputs; i++)
					m_results[i].push(output_type());

				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Finished.";
			}
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine: Received sentinel, finishing.";

			// send sentinel to all processor threads and wait 'til they're finished
			for(auto i = 0; i < m_numberOfDevices; i++)
			{
				m_images[i][0].push(input_type());
			}

			for(auto i = 0; i < m_numberOfDevices; i++)
			{
				m_processorThreads[i].join();
			}
			// push sentinel to results for next stage
			for(int i = 0; i < m_numberOfOutputs; i++)
				m_results[i].push(output_type());

			BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine: Finished.";
			return;
		}
	}
}

auto Combine::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto Combine::processor(const int deviceID) -> void
{
	// nvtxNameOsThreadA(pthread_self(), "Attenuation");
	CHECK(cudaSetDevice(deviceID));
	auto imageB_d = glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * m_numberOfPlanes);

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));
	CHECK(cudaStreamSynchronize(m_streams[deviceID]));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Running thread for device " << deviceID;
	size_t filledStaticImagesFromStreamB{0}; // buffer one image per plane
	std::vector<float> imageB_h(m_imageWidth * m_imageHeight * m_numberOfPlanes);

	while(m_doConfig)
	{

		detail::combineSource combineSource = m_combineSource;
		detail::combineMethod combineMethod = m_combineMethod;
		float sourceStaticValue = m_sourceStaticValue;
		float weight = m_combineWeightOfMainImage;

		if(combineSource == detail::combineSource::combineSourceFile)
		{
			auto ifStream = std::ifstream(m_sourceFile, std::ios::in | std::ios::binary);
			auto expectedFileSize = imageB_h.size() * sizeof(float);
			if(ifStream)
			{
				ifStream.seekg(0, std::ios::end);
				auto fileSize = ifStream.tellg();
				if(fileSize >= expectedFileSize)
				{
					ifStream.seekg(0, std::ios::beg);
					ifStream.read((char*)&imageB_h[0], expectedFileSize);
				}
				else
				{
					BOOST_LOG_TRIVIAL(error) << "risa::cuda::Combine: " << m_sourceFile
											 << " is too small. Fall back to static value 0.";
					combineSource = detail::combineSource::combineSourceStaticValue;
					sourceStaticValue = 0.0;
				}
			}
			else
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Combine: Unable to load \"" << m_sourceFile
										 << "\". Fall back to static value 0.";
				combineSource = detail::combineSource::combineSourceStaticValue;
				sourceStaticValue = 0.0;
			}
		}

		if(combineSource == detail::combineSource::combineSourceStaticValue)
		{
			std::fill(imageB_h.begin(), imageB_h.end(), sourceStaticValue);
		}

		if(combineSource == detail::combineSource::combineSourceStaticValue
			|| combineSource == detail::combineSource::combineSourceFile)
		{
			CHECK(cudaMemcpyAsync(imageB_d.get(), &imageB_h[0], sizeof(float) * imageB_h.size(),
				cudaMemcpyHostToDevice, m_streams[deviceID]));
		}

		m_doConfig = false;

		while(true)
		{

			auto image = m_images[deviceID][0].take();
			if(!image.valid())
			{
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: Invalid image, terminating.";
				m_doConfig = false;
				break;
			}

			if(combineSource == detail::combineSource::combineSourceStreamSingle
				&& filledStaticImagesFromStreamB != m_numberOfPlanes)
			{
				// take the image from the second stream for the current plane and store it
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine: Waiting for single image on second input...";
				auto imageB = m_images[deviceID][1].take();
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine: Got second image.";
				const auto plane = imageB.plane();
				const auto elemsPerFrame = m_imageWidth * m_imageHeight;
				CHECK(cudaMemcpyAsync(&imageB_d.get()[elemsPerFrame * plane], imageB.data(),
					sizeof(float) * elemsPerFrame, cudaMemcpyDeviceToDevice, m_streams[deviceID]));
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				filledStaticImagesFromStreamB++;
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine: Processed " << filledStaticImagesFromStreamB
										<< "/" << m_numberOfPlanes << " input images.";
			}

			if(combineSource == detail::combineSource::combineSourceStream)
			{
				auto imageB = m_images[deviceID][1].take();
				switch(combineMethod)
				{
				case detail::combineMethod::combineAdd:
				{
					combineImagesAdd<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineSubtract:
				{
					combineImagesSubtract<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineDifference:
				{
					combineImagesDifference<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMultiply:
				{
					combineImagesMultiply<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineRaiseToPower:
				{
					combineImagesRaiseToPower<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineDivide:
				{
					combineImagesDivide<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMax:
				{
					combineImagesMax<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMin:
				{
					combineImagesMin<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineWeighted:
				{
					combineImagesWeighted<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight, weight);
					break;
				}
				case detail::combineMethod::combineSoftThresholdGreaterThan:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight, true, true);
					break;
				}
				case detail::combineMethod::combineSoftThresholdLowerThan:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight, true, false);
					break;
				}
				case detail::combineMethod::combineHardThreshold:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB.data(), m_imageWidth, m_imageHeight, false, false);
					break;
				}
				case detail::combineMethod::doNothing:
				{
					// literally do nothing with the image
					break;
				}
				}
			}
			else
			{
				switch(combineMethod)
				{
				case detail::combineMethod::combineAdd:
				{
					combineImagesAdd<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineSubtract:
				{
					combineImagesSubtract<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineDifference:
				{
					combineImagesDifference<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMultiply:
				{
					combineImagesMultiply<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineRaiseToPower:
				{
					combineImagesRaiseToPower<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineDivide:
				{
					combineImagesDivide<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMax:
				{
					combineImagesMax<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineMin:
				{
					combineImagesMin<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight);
					break;
				}
				case detail::combineMethod::combineWeighted:
				{
					combineImagesWeighted<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight, weight);
					break;
				}
				case detail::combineMethod::combineSoftThresholdGreaterThan:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight, true, true);
					break;
				}
				case detail::combineMethod::combineSoftThresholdLowerThan:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight, true, false);
					break;
				}
				case detail::combineMethod::combineHardThreshold:
				{
					combineImagesThreshold<<<grids, blocks, 0, m_streams[deviceID]>>>(
						image.data(), imageB_d.get(), m_imageWidth, m_imageHeight, false, false);
					break;
				}
				case detail::combineMethod::doNothing:
				{
					// literally do nothing with the image
					break;
				}
				}
			}

			CHECK(cudaPeekAtLastError());
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), image.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(image.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine:        (Device " << deviceID << " | Output "
										 << i << "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine:        (Device " << deviceID << " | Output "
									 << 0 << "): Image " << image.index() << " processed.";
			m_results[0].push(std::move(image));

			if(m_doConfig)
			{
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Combine:        Reconfiguration triggered.";
				break;
			}
		}
	}
}

auto Combine::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Combine: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			std::string combineMethod, combineSource;

			hasValidSetting = configReader.lookupValue("combineSource", combineSource);
			detail::combineSource tmpSource = getCombineSourceFromString(combineSource);
			if(tmpSource == detail::combineSource::combineSourceNotDefined)
			{
				hasValidSetting = false;
			}
			else if(tmpSource == detail::combineSource::combineSourceStream
				&& m_combineSource != detail::combineSource::combineSourceStream)
			{
				hasValidSetting = false;
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Combine: Cannot change combineSource to stream during runtime.";
			}
			else if(tmpSource != detail::combineSource::combineSourceStream
				&& m_combineSource == detail::combineSource::combineSourceStream)
			{
				hasValidSetting = false;
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Combine: Switching from combineSource stream during "
											"runtime may result in blocking of preceeding stages.";
			}
			else
			{
				m_combineSource = tmpSource;
			}

			bool changeCombineMethod = configReader.lookupValue("combineMethod", combineMethod);
			detail::combineMethod tmpMethod = getCombineMethodFromString(combineMethod);
			if(changeCombineMethod && tmpMethod != detail::combineMethod::combineMethodNotDefined)
			{
				m_combineMethod = tmpMethod;
				hasValidSetting = true;
			}

			hasValidSetting = configReader.lookupValue("combineWeightOfMainImage", m_combineWeightOfMainImage)
				|| hasValidSetting;
			hasValidSetting = configReader.lookupValue("sourceFile", m_sourceFile) || hasValidSetting;
			hasValidSetting =
				configReader.lookupValue("sourceStaticValue", m_sourceStaticValue) || hasValidSetting;
			if(m_combineMethod == detail::combineMethod::combineMean)
			{
				m_combineMethod = detail::combineMethod::combineWeighted;
				m_combineWeightOfMainImage = 0.5f;
			}

			if(hasValidSetting)
			{
				std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Combine: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto Combine::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string combineMethod, combineSource;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".combineMethod", combineMethod)
		&& configReader.lookupValue(parameterSet + ".combineSource", combineSource)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize))
	{

		m_combineMethod = getCombineMethodFromString(combineMethod);
		if(m_combineMethod == detail::combineMethod::combineMethodNotDefined)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: combineMethod \"" << combineMethod
									 << "\" is not defined.";
			return false;
		}

		if(m_combineMethod == detail::combineMethod::combineWeighted
			&& (!configReader.lookupValue(
				parameterSet + ".combineWeightOfMainImage", m_combineWeightOfMainImage)))
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: combineWeightOfMainImage not defined for "
										"combineMethod \"combineMethodWeighted\".";
			return false;
		}

		if(m_combineMethod == detail::combineMethod::combineMean)
		{
			m_combineMethod = detail::combineMethod::combineWeighted;
			m_combineWeightOfMainImage = 0.5;
		}

		if(m_combineMethod == detail::combineMethod::combineWeighted
			&& (m_combineWeightOfMainImage < 0.0 || m_combineWeightOfMainImage > 1.0))
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: combineWeightOfMainImage out of range. "
										"Expected 0.0 <= value <= 1.0. Got: "
									 << m_combineWeightOfMainImage << ".";
			return false;
		}

		m_combineSource = getCombineSourceFromString(combineSource);
		if(m_combineSource == detail::combineSource::combineSourceNotDefined)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: combineSource \"" << combineSource
									 << "\" is not defined.";
			return false;
		}

		if(m_combineSource == detail::combineSource::combineSourceFile
			&& (!configReader.lookupValue(parameterSet + ".sourceFile", m_sourceFile)))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::Combine: sourceFile not defined for combineSource \"combineSourceFile\".";
			return false;
		}

		if(m_combineSource == detail::combineSource::combineSourceStaticValue
			&& (!configReader.lookupValue(parameterSet + ".sourceStaticValue", m_sourceStaticValue)))
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: sourceStaticValue not defined for "
										"combineSource \"combineSourceStaticValue\".";
			return false;
		}

		if(m_combineSource == detail::combineSource::combineSourceFile)
		{
			auto ifStream = std::ifstream(m_sourceFile, std::ios::in | std::ios::binary);

			if(ifStream.fail())
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::Combine: Source file '" << m_sourceFile << "'could not be loaded.";
				return false;
			}

			std::streampos fileSize;
			ifStream.seekg(0, std::ios::end);
			fileSize = ifStream.tellg();
			auto expectedFileSize = m_imageHeight * m_imageWidth * m_numberOfPlanes * sizeof(float);

			if(fileSize < expectedFileSize)
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Combine: Source file too small. Expected "
										 << expectedFileSize << "B, got: " << fileSize << "B.";
				return false;
			}
			ifStream.close();
		}

		return true;
	}

	return false;
}

__global__ void combineImagesAdd(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] += image_B[idx];
}

__global__ void combineImagesSubtract(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] -= image_B[idx];
}

__global__ void combineImagesDifference(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] = abs(image_A[idx] - image_B[idx]);
}

__global__ void combineImagesMultiply(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] *= image_B[idx];
}

__global__ void combineImagesRaiseToPower(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] = pow(image_A[idx], image_B[idx]);
}

__global__ void combineImagesDivide(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	if(image_B[idx] != 0.0f)
		image_A[idx] /= image_B[idx];
	else
		image_A[idx] = 0.0f;
}

__global__ void combineImagesMax(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] = image_A[idx] > image_B[idx] ? image_A[idx] : image_B[idx];
}

__global__ void combineImagesMin(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] = image_A[idx] < image_B[idx] ? image_A[idx] : image_B[idx];
}

__global__ void combineImagesWeighted(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight, const float weight)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	image_A[idx] = image_A[idx] * weight + image_B[idx] * (1.0f - weight);
}

__global__ void combineImagesThreshold(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight, const bool useSoftThreshold,
	const bool isUpperThreshold)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	if(useSoftThreshold)
	{
		if(isUpperThreshold)
		{
			image_A[idx] = image_A[idx] > image_B[idx] ? 1.0 : image_A[idx];
		}
		else
		{
			image_A[idx] = image_A[idx] < image_B[idx] ? 0.0 : image_A[idx];
		}
	}
	else
	{
		image_A[idx] = image_A[idx] > image_B[idx] ? 1.0 : 0.0;
	}
}

}
}
