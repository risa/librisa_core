// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <iostream>
#include <string>
#include <vector>

#include <risaCore/RISAModuleInterface.h>
#include <risaCore/libRISA_Core.h>

//**********************
// LIBRARY INFORMATION *
//**********************
// clang-format off

// LIBRARY NAME
const std::string libName = "RISA_Core";

// LIBRARY VERSION
#ifndef NDEBUG
const std::string libVers = "DEBUG    " + std::string(__DATE__) + "|" + std::string(__TIME__);
#else
const std::string libVers = "RELEASE  " + std::string(__DATE__) + "|" + std::string(__TIME__);
#endif

// PROCESSOR STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libProcessorStages = {
	{"Average", "cfloat", "cfloat"},
	{"Combine", "cfloat", "cfloat"}, 
	{"Convert", "cuint16_t", "cfloat"}, 
	{"D2H", "cfloat", "hfloat"},
	{"DistanceTransform","cfloat","cfloat"},
	{"FFT2D", "cfloat", "cfloat"}, 
	{"Filter1D", "cfloat", "cfloat"}, 
	{"Filter2D", "cfloat", "cfloat"},
	{"FilterSequence", "cfloat", "cfloat"},
	{"FilterStatistical", "cfloat", "cfloat"},
	{"Flip", "cfloat", "cfloat"}, 
	{"GetProperty", "cfloat", "cfloat"}, 
	{"HistogramPerPixel", "cfloat", "cfloat"}, 
	{"IFFT2D", "cfloat", "cfloat"},
	{"Interleave", "cfloat", "cfloat"}, 
	{"Masking", "cfloat", "cfloat"}, 
	{"Reduce", "cfloat", "cfloat"},
	{"Reframe", "cfloat", "cfloat"},
	{"Rotate", "cfloat", "cfloat"}, 
	{"Scale", "cfloat", "cfloat"}, 
	{"SetProperty", "cfloat", "cfloat"},
	{"StartAfterNImages", "cfloat", "cfloat"},
	{"Split", "cfloat", "cfloat"},
	{"StopAfterNImages", "cfloat", "cfloat"},
	{"Transform2D", "cfloat", "cfloat"},
	{"Watershed", "cfloat", "cfloat"}};

// SOURCE STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libSourceStages = {
#ifdef LIBRISACORE_HAS_HDF5
	{"HDF5Loader", "cfloat"},
#endif
	{"ImageSequenceLoader", "cfloat"}};

// SINK STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libSinkStages = {
#ifdef LIBRISACORE_HAS_HDF5
	{"HDF5Saver", "cfloat"},
#endif
#ifdef LIBRISACORE_HAS_OPENCV
	{"OpenCVViewer", "cfloat"},
#endif
#ifdef __linux__
	{"SharedHostMemSaver", "hfloat"},
#endif	
	
	{"OfflineSaver", "hfloat"},
#ifdef LIBRISACORE_HAS_TIFF
	{"TIFFStackSaver", "hfloat"},
#endif
#ifdef LIBRISACORE_HAS_JPEG_ENCODER
	{"WebCompatibleSaver", "cfloat"},
#endif
	{"DeviceDiscarder", "cfloat"}
};

// LIST OF DISABLED STAGES DUE TO MISSING PREREQUISITS
const std::vector<std::vector<std::string>> libDisabledStages = {
#ifndef LIBRISACORE_HAS_HDF5
	{"HDF5Loader", "HDF5 library not found"},	
	{"HDF5Saver", "HDF5 library not found"},
#endif

#ifndef LIBRISACORE_HAS_OPENCV
	{"OpenCVViewer", "OpenCV library not found"},
#endif

#ifndef LIBRISACORE_HAS_JPEG_ENCODER
	{"WebCompatibleSaver", "nvJPEG library not found"},
#endif

#ifndef __linux
	{"SharedHostMemSaver", "Only supported in Linux OS"},		  
#endif
	{"FilterSpecial", "Renamed to 'FilterStatistical'"}
};

EXPORT auto get_library_name(char* ptr) -> uint32_t { return get_str(ptr, libName); }
EXPORT auto get_library_version(char* ptr) -> uint32_t  { return get_str(ptr, libVers); }
EXPORT auto get_implemented_processors(char* ptr, int32_t n, int32_t part) -> uint32_t { return get_str(ptr, n, part, libProcessorStages); }
EXPORT auto get_implemented_sources(char* ptr, int32_t n, int32_t part) -> uint32_t { return get_str(ptr, n, part, libSourceStages); }
EXPORT auto get_implemented_sinks(char* ptr, int32_t n, int32_t part) -> uint32_t {	return get_str(ptr, n, part, libSinkStages); }
EXPORT auto get_disabled_stages(char* ptr, int32_t n, int32_t part) -> uint32_t {	return get_str(ptr, n, part, libDisabledStages); }

// clang-format on

//*****************************
// STAGE CREATORS / DESTROYERS
//*****************************

// NOLINTBEGIN(cppcoreguidelines-owning-memory,readability-non-const-parameter)

//***************
// SOURCE STAGES
//***************

#ifdef LIBRISACORE_HAS_HDF5
// HDF5Loader
EXPORT auto create_HDF5Loader(const char* configFile, const char* parameterSet)
	-> RISAModule_Source<cuda_array<float>>*
{
	return new risa::cuda::HDF5Loader(configFile, parameterSet);
}
EXPORT void destroy_HDF5Loader(RISAModule_Source<cuda_array<float>>* ptr) { delete ptr; }
#endif

// ImageSequenceLoader
EXPORT auto create_ImageSequenceLoader(char* configFile, char* parameterSet)
	-> RISAModule_Source<cuda_array<float>>*
{
	return new risa::cuda::ImageSequenceLoader(configFile, parameterSet);
}
EXPORT void destroy_ImageSequenceLoader(RISAModule_Source<cuda_array<float>>* ptr) { delete ptr; }

//******************
// PROCESSOR STAGES
//******************

// Average
EXPORT auto create_Average(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Average(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Average(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Combine
EXPORT auto create_Combine(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Combine(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Combine(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Convert
EXPORT auto create_Convert(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<uint16_t>, cuda_array<float>>*
{
	return new risa::cuda::ushortToFloat(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Convert(RISAModule_Processor<cuda_array<uint16_t>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// D2H
EXPORT auto create_D2H(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, host_array<float>>*
{
	return new risa::cuda::D2H(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_D2H(RISAModule_Processor<cuda_array<float>, host_array<float>>* ptr) { delete ptr; }

// FFT2D
EXPORT auto create_DistanceTransform(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::DistanceTransform(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_DistanceTransform(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// FFT2D
EXPORT auto create_FFT2D(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::FFT2D(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_FFT2D(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Filter1D
EXPORT auto create_Filter1D(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Filter1D(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Filter1D(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Filter2D
EXPORT auto create_Filter2D(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Filter2D(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Filter2D(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// FilterSequence
EXPORT auto create_FilterSequence(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::FilterSequence(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_FilterSequence(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// FilterStatistical
EXPORT auto create_FilterStatistical(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::FilterStatistical(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_FilterStatistical(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Flip
EXPORT auto create_Flip(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Flip(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Flip(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// GetProperty
EXPORT auto create_GetProperty(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::GetProperty(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_GetProperty(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// HistogramPerPixel
EXPORT auto create_HistogramPerPixel(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::HistogramPerPixel(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_HistogramPerPixel(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// IFFT2D
EXPORT auto create_IFFT2D(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::IFFT2D(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_IFFT2D(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Interleave
EXPORT auto create_Interleave(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Interleave(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Interleave(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Masking
EXPORT auto create_Masking(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Masking(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Masking(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Reduce
EXPORT auto create_Reduce(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Reduce(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Reduce(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Reframe
EXPORT auto create_Reframe(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Reframe(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Reframe(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Rotate
EXPORT auto create_Rotate(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Rotate(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Rotate(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// Scale
EXPORT auto create_Scale(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Scale(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Scale(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// SetProperty
EXPORT auto create_SetProperty(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::SetProperty(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_SetProperty(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Split
EXPORT auto create_Split(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Split(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Split(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// StartAfterNImages
EXPORT auto create_StartAfterNImages(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::StartAfterNImages(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_StartAfterNImages(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Stitch
EXPORT auto create_Stitch(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Stitch(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Stitch(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

// StopAfterNImages
EXPORT auto create_StopAfterNImages(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::StopAfterNImages(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_StopAfterNImages(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Transform2D
EXPORT auto create_Transform2D(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Transform2D(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Transform2D(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
{
	delete ptr;
}

// Watershed
EXPORT auto create_Watershed(const char* configFile, const char* parameterSet, const int numberOfOutputs,
	const int numberOfInputs) -> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Watershed(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
EXPORT void destroy_Watershed(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr) { delete ptr; }

//*************
// SINK STAGES
//*************

// DeviceDiscarder
EXPORT auto create_DeviceDiscarder(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<cuda_array<float>>*
{
	return new risa::cuda::DeviceDiscarder(configFile, parameterSet);
}
EXPORT void destroy_DeviceDiscarder(RISAModule_Sink<cuda_array<float>>* ptr) { delete ptr; }

#ifdef LIBRISACORE_HAS_HDF5
// HDF5Saver
EXPORT auto create_HDF5Saver(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<cuda_array<float>>*
{
	return new risa::cuda::HDF5Saver(configFile, parameterSet);
}
EXPORT void destroy_HDF5Saver(RISAModule_Sink<cuda_array<float>>* ptr) { delete ptr; }
#endif

// --DEPRECATED--
#ifdef LIBRISACORE_HAS_TIFF
// OfflineSaver
EXPORT auto create_OfflineSaver(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<host_array<float>>*
{
	std::cout << "!!! Usage of 'OfflineSaver' is deprecated. Use 'TIFFStackSaver' instead. !!!\n";
	return new risa::TIFFStackSaver(configFile, parameterSet);
}
EXPORT void destroy_OfflineSaver(RISAModule_Sink<host_array<float>>* ptr) { delete ptr; }
#endif

#ifdef LIBRISACORE_HAS_OPENCV 
// OpenCVViewer
EXPORT auto create_OpenCVViewer(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<cuda_array<float>>*
{
	return new risa::cuda::OpenCVViewer(configFile, parameterSet);
}
EXPORT void destroy_OpenCVViewer(RISAModule_Sink<cuda_array<float>>* ptr) { delete ptr; }
#endif

#ifdef __linux__
// SharedHostMemSaver
EXPORT auto create_SharedHostMemSaver(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<host_array<float>>*
{
	return new risa::SharedHostMemSaver(configFile, parameterSet);
}
EXPORT void destroy_SharedHostMemSaver(RISAModule_Sink<host_array<float>>* ptr) { delete ptr; }
#endif

// TIFFStackSaver
#ifdef LIBRISACORE_HAS_TIFF
EXPORT auto create_TIFFStackSaver(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<host_array<float>>*
{
	return new risa::TIFFStackSaver(configFile, parameterSet);
}
EXPORT void destroy_TIFFStackSaver(RISAModule_Sink<host_array<float>>* ptr) { delete ptr; }
#endif

// WebCompatibleSaver
#ifdef LIBRISACORE_HAS_JPEG_ENCODER
EXPORT auto create_WebCompatibleSaver(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<cuda_array<float>>*
{
	return new risa::cuda::WebCompatibleSaver(configFile, parameterSet);
}
EXPORT void destroy_WebCompatibleSaver(RISAModule_Sink<cuda_array<float>>* ptr) { delete ptr; }
#endif
// NOLINTEND(cppcoreguidelines-owning-memory,readability-non-const-parameter)