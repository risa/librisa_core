// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Resize/Reframe.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Reframe::Reframe(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Reframe: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::Reframe: Expected exactly 1 input port");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_outputImageWidth * m_outputImageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Reframe::processor, this, i};
	}

	m_doConfig = true;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: Running " << m_numberOfDevices << " thread(s).";
}

Reframe::~Reframe()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Reframe::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: Finished.";
	}
}

auto Reframe::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Reframe::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{

			bool hasValidSetting = false;
			std::string filterType;

			hasValidSetting = configReader.lookupValue("coordinateBaseOfOriginalImageInFinalImageX",
								  m_coordinateBaseOfOriginalImageInFinalImageX)
				|| hasValidSetting;

			hasValidSetting = configReader.lookupValue("coordinateBaseOfOriginalImageInFinalImageY",
								  m_coordinateBaseOfOriginalImageInFinalImageY)
				|| hasValidSetting;

			hasValidSetting =
				configReader.lookupValue("fillerPixelValue", m_fillerPixelValue) || hasValidSetting;

			if(hasValidSetting)
			{
				m_doConfig = true;
			}
		}
	}
}

auto Reframe::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: Running thread for device " << deviceID;

	dim3 blocks(m_blockSize2D, m_blockSize2D);
	dim3 grids(std::ceil(m_outputImageWidth / (float)m_blockSize2D),
		std::ceil(m_outputImageHeight / (float)m_blockSize2D));

	using imageType = glados::Image<deviceManagerType>;

	while(m_doConfig)
	{
		auto coordX = m_coordinateBaseOfOriginalImageInFinalImageX;
		auto coordY = m_coordinateBaseOfOriginalImageInFinalImageY;
		auto fillerPixelValue = m_fillerPixelValue;
		m_doConfig = false;
		while(true)
		{

			auto img = m_imgs[deviceID][0].take();
			if(!img.valid())
			{
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe: Received sentinel, finishing.";
				m_doConfig = false;
				break;
			}

			// if necessary, request memory from MemoryPool here
			auto outputImg =
				glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

			reframe<<<grids, blocks, 0, m_streams[deviceID]>>>(img.data(), outputImg.data(), m_outputImageWidth,
				m_outputImageHeight, m_inputImageWidth, m_inputImageHeight, coordX, coordY, fillerPixelValue);

			CHECK(cudaPeekAtLastError());

			outputImg.setDevice(deviceID);
			outputImg.setProperties(img.properties());
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), outputImg.data(),
					m_outputImageWidth * m_outputImageHeight * sizeof(float), cudaMemcpyDeviceToDevice,
					m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(outputImg.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe:        (Device " << deviceID << " | Output "
										 << i << "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			// use the img that we have for the first port(0)
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reframe:        (Device " << deviceID << " | Output "
									 << 0 << "): Image " << outputImg.index() << " processed.";
			m_results[0].push(std::move(outputImg));

			if(m_doConfig)
			{ // trigger for runtime reconfiguration
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Reframe:      Reconfiguration triggered.";
				break; // breaks inner while, outer will keep running after reconfiguration
			}
		}
	}
}

auto Reframe::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	return (configReader.lookupValue(parameterSet + ".outputImageWidth", m_outputImageWidth)
		&& configReader.lookupValue(parameterSet + ".outputImageHeight", m_outputImageHeight)
		&& configReader.lookupValue(parameterSet + ".inputImageWidth", m_inputImageWidth)
		&& configReader.lookupValue(parameterSet + ".inputImageHeight", m_inputImageHeight)
		&& configReader.lookupValue(parameterSet + ".coordinateBaseOfOriginalImageInFinalImageX",
			m_coordinateBaseOfOriginalImageInFinalImageX)
		&& configReader.lookupValue(parameterSet + ".coordinateBaseOfOriginalImageInFinalImageY",
			m_coordinateBaseOfOriginalImageInFinalImageY)
		&& configReader.lookupValue(parameterSet + ".fillerPixelValue", m_fillerPixelValue)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D));
}

__global__ void reframe(float* __restrict__ inputImg, float* __restrict__ outputImg,
	const unsigned int outputImageWidth, const unsigned int outputImageHeight,
	const unsigned int inputImageWidth, const unsigned int inputImageHeight, const int posInOutputImageX,
	const int posInOutputImageY, const float fillerPixelValue)
{
	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	if(x >= outputImageWidth || y >= outputImageHeight)
		return;

	const auto idxInOutputImg = y * outputImageWidth + x;
	const int xIn = x - posInOutputImageX;
	const int yIn = y - posInOutputImageY;
	if(xIn >= 0 && xIn < inputImageWidth && yIn >= 0 && yIn < inputImageHeight)
	{
		const auto idxInInputImg = yIn * inputImageWidth + xIn;
		outputImg[idxInOutputImg] = inputImg[idxInInputImg];
	}
	else
	{
		outputImg[idxInOutputImg] = fillerPixelValue;
	}
}

}
}
