// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Reduce/Reduce.h>

#include <risaCore/Basics/UtilityKernels.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <float.h> //FLT_MIN, FLT_MAX

#include <exception>

namespace risa
{
namespace cuda
{

Reduce::Reduce(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Reduce: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Reduce::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce: Running " << m_numberOfDevices << " thread(s).";
}

Reduce::~Reduce()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		CHECK(cudaSetDevice(idx.first));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx.second);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Reduce::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
		{
			m_results[i].push(output_type());
		}
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce: Finished.";
	}
}

auto Reduce::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

//! Call the fitting reduce kernel template
#define CALL_REDUCE_KERNEL(blockSz) \
	{ \
		switch(m_reduceProperty) \
		{ \
			using namespace detail::ReduceProperty; \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Minimum | Product | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Minimum | Product); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Minimum | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Product | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Minimum | Product | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Minimum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Product); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Minimum | Product); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Minimum | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Product | Sum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Maximum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Minimum); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Product); \
			CASE_CALL_REDUCE_KERNEL(blockSz, Sum); \
		} \
	}

#define CASE_CALL_REDUCE_KERNEL(blockSz, props) \
	{case props : \
			{reduceKernel<blockSz, props><<<numBlocks, numThreads, sharedMemBytes, m_streams[deviceID]>>>( \
				img.data(), numElems, reducedProps_d.get()); \
	break; \
	} \
	}

auto Reduce::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce: Running thread for device " << deviceID;

	const unsigned int numThreads = m_blockSize2D * m_blockSize2D;
	const unsigned int numBlocks =
		std::ceil(((float)m_imageWidth * (float)m_imageHeight + (float)numThreads - 1.0f) / numThreads
			/ 2.0f); // div by 2 because each thread handles 2 pixels

	const auto sharedMemBytes = detail::ReduceProperty::getSharedMemSize(m_blockSize2D);
	const auto reducePropertyArraySize = detail::ReduceProperty::getReducePropertyArraySize(numBlocks);
	std::vector<float> reducedProps_h(reducePropertyArraySize, 0.0);
	auto reducedProps_d =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(reducePropertyArraySize);
	CHECK(cudaMemcpyAsync(reducedProps_d.get(), reducedProps_h.data(), sizeof(float) * reducedProps_h.size(),
		cudaMemcpyHostToDevice, m_streams[deviceID]));
	CHECK(cudaStreamSynchronize(m_streams[deviceID]));

	const auto numElems = m_imageWidth * m_imageHeight;

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			return;
		}
		switch(m_blockSize2D * m_blockSize2D)
		{
			using namespace detail;
		case 256:
		{
			CALL_REDUCE_KERNEL(256);
			break;
		}
		case 1024:
		{
			CALL_REDUCE_KERNEL(1024);
			break;
		}
		default:
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce: Using unsupported blockSize2D. Use 16 or 32.";
			return;
		}
		}

		CHECK(cudaPeekAtLastError());
		CHECK(cudaMemcpyAsync(reducedProps_h.data(), reducedProps_d.get(),
			reducedProps_h.size() * sizeof(float), cudaMemcpyDeviceToHost, m_streams[deviceID]));

		float maxVal = reducedProps_h[numBlocks * 0];
		float minVal = reducedProps_h[numBlocks * 1];
		float prodVal = 1;
		float sumVal = 0;

		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(auto i = 0; i < numBlocks; i++)
		{
			if(m_reduceProperty & detail::ReduceProperty::Maximum)
			{
				const auto maxIdx = detail::ReduceProperty::getNthMaxIdx(numBlocks, i);
				maxVal = GET_MAX(maxVal, reducedProps_h[maxIdx]);
			}
			if(m_reduceProperty & detail::ReduceProperty::Minimum)
			{
				const auto minIdx = detail::ReduceProperty::getNthMinIdx(numBlocks, i);
				minVal = GET_MIN(minVal, reducedProps_h[minIdx]);
			}
			if(m_reduceProperty & detail::ReduceProperty::Product)
			{
				const auto prodIdx = detail::ReduceProperty::getNthProdIdx(numBlocks, i);
				prodVal = GET_PROD(prodVal, reducedProps_h[prodIdx]);
			}
			if(m_reduceProperty & detail::ReduceProperty::Sum)
			{
				const auto sumIdx = detail::ReduceProperty::getNthSumIdx(numBlocks, i);
				sumVal = GET_SUM(sumVal, reducedProps_h[sumIdx]);
			}
		}

		// append as props
		if(m_reduceProperty & detail::ReduceProperty::Maximum)
		{
			std::ignore = img.setProperty(
				"maxValue", maxVal, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		}
		if(m_propMeanValue)
		{
			float meanVal = sumVal / (float)(numElems);
			std::ignore = img.setProperty(
				"meanValue", meanVal, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		}
		if(m_reduceProperty & detail::ReduceProperty::Minimum)
		{
			std::ignore = img.setProperty(
				"minValue", minVal, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		}
		if(m_reduceProperty & detail::ReduceProperty::Product)
		{
			std::ignore = img.setProperty(
				"productValue", prodVal, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		}
		if(m_reduceProperty & detail::ReduceProperty::Sum)
		{
			std::ignore = img.setProperty(
				"sumValue", sumVal, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		}

		for(int i = 1; i < m_numberOfOutputs; i++)
		{
			auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
				m_memoryPoolIdxs[deviceID]);

			cudaMemcpyAsync(imgSplit.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);

			imgSplit.setProperties(img.properties());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce:        (Device " << deviceID << " | Output " << i
									 << "): Image " << imgSplit.index() << " processed.";
			m_results[i].push(std::move(imgSplit));
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Reduce:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << img.index() << " processed.";
		m_results[0].push(std::move(img));
	}
}

auto Reduce::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D))
	{

		std::vector<std::string> reduceToProps;
		configReader.lookupValue(parameterSet + ".reduceToProperties", reduceToProps);

		m_reduceProperty = 0;
		for(const auto& prop : reduceToProps)
		{
			using namespace detail::ReduceProperty;
			if(prop == "all")
			{
				m_propMeanValue = true;
				m_reduceProperty |= (Minimum | Maximum | Sum | Product);
			}
			else if(prop == "maximum")
			{
				m_reduceProperty |= Maximum;
			}
			else if(prop == "mean")
			{
				m_propMeanValue = true;
				m_reduceProperty |= Sum;
			}
			else if(prop == "minimum")
			{
				m_reduceProperty |= Minimum;
			}
			else if(prop == "product")
			{
				m_reduceProperty |= Product;
			}
			else if(prop == "sum")
			{
				m_reduceProperty |= Sum;
			}
			else
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Reduce: Unknown reduction property '" << prop << "'.";
				return false;
			}
		}
		if(m_reduceProperty == 0)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::Reduce: No valid properties to reduce to provided.";
			return false;
		}

		return true;
	}
	else
		return false;
}

}
}
