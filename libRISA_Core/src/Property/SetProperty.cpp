// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Property/SetProperty.h>

#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

#if defined(_MSC_VER)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

namespace risa::cuda
{

SetProperty::SetProperty(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs((size_t)numberOfOutputs), m_numberOfInputs((size_t)numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::SetProperty: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::SetProperty: Number of input ports must be 1.");
	}

	if(numberOfOutputs != 1)
	{
		throw std::runtime_error("risa::cuda::SetProperty: Number of output ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&SetProperty::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: Running " << m_numberOfDevices << " thread(s).";
}

auto SetProperty::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][(size_t)inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(size_t i = 0; i < m_numberOfOutputs; i++)
		{
			m_results[i].push(output_type());
		}
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: Finished.";
	}
}

auto SetProperty::update(glados::Subject* subject) -> void
{
	auto* fileObserver = dynamic_cast<glados::FileObserver*>(subject);
	std::string fileContentStr(fileObserver->m_fileContents.begin(), fileObserver->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: ###### UPDATE ###### -> " << fileObserver->m_fileName
							 << ": " << fileObserver->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: ###### CONTENT ##### -> " << fileContentStr;
	if(fileObserver->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			// get prop tree
			bool clearProperties{false};
			bool hasValidSetting = configReader.lookupValue("clearProperties", clearProperties);
			auto propDescriptorArray = configReader.lookup("propertyDescriptors");
			if(!propDescriptorArray.empty())
			{
				hasValidSetting = true;
			}

			if(hasValidSetting)
			{
				std::lock_guard<std::mutex> lck(m_mutex);
				if(clearProperties)
				{
					m_propDescriptors.clear();
				}
				if(parsePropDescriptorArray(propDescriptorArray))
				{
					BOOST_LOG_TRIVIAL(info) << "risa::cuda::SetProperty: Dynamic reconfiguration triggered.";
				}
				else
				{
					BOOST_LOG_TRIVIAL(error) << "risa::cuda::SetProperty: Dynamic reconfiguration failed.";
				}
			}
			else
			{
				BOOST_LOG_TRIVIAL(info)
					<< "risa::cuda::SetProperty: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto SetProperty::wait(int outputIdx) -> output_type { return m_results[(size_t)outputIdx].take(); }

// per device, one thread executes this function
auto SetProperty::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: Running thread for device " << deviceID;

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: Received sentinel, finishing.";
			break;
		}
		{
			std::lock_guard<std::mutex> lck(m_mutex);
			for(auto& [k, pd] : m_propDescriptors)
			{
				auto m_propType = pd.m_propType;
				auto m_propKey = pd.m_propKey;
				auto m_propValue = pd.m_propValue;

				if(pd.m_isFile)
				{
					m_propValue = pd.nextValue();
				}

				switch(m_propType)
				{
				case detail::SetPropertyType::floatingPoint:
				case detail::SetPropertyType::integer:
				case detail::SetPropertyType::signed_integer:
				case detail::SetPropertyType::text:
				{
					std::ignore = img.setProperty(m_propKey, m_propValue,
						glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
					break;
				}
				case detail::SetPropertyType::increment:
				{
					ssize_t counterVal{0};
					auto ret = img.getProperty(m_propKey, counterVal);
					if(ret == glados::details::PropertyReturnCode::NotFound)
					{
						BOOST_LOG_TRIVIAL(fatal)
							<< "risa::cuda::SetProperty: Cannot increment value for key '" << m_propKey
							<< "': Key not found.";
						throw std::runtime_error(
							"risa::cuda::SetProperty: Cannot increment non-existing key");
					}
					if(ret == glados::details::PropertyReturnCode::TypeMismatch)
					{
						BOOST_LOG_TRIVIAL(fatal)
							<< "risa::cuda::SetProperty: Cannot increment value for key '" << m_propKey
							<< "': Wrong type.";
						throw std::runtime_error(
							"risa::cuda::SetProperty: Cannot increment type miss-matched key");
					}
					counterVal++;

					// we made sure that key exists and is of the same type, so the setProperty call cannot
					// fail
					std::ignore = img.setProperty(
						m_propKey, counterVal, glados::details::PropertyOverwritePolicy::OverwriteValue);
					break;
				}
				case detail::SetPropertyType::timestamp:
				{

					std::ignore = img.setProperty(m_propKey, std::chrono::high_resolution_clock::now(),
						glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
					break;
				}
				case detail::SetPropertyType::file:
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::SetProperty: Property of type 'file' was not initialized before use.";
					throw std::runtime_error(
						"risa::cuda::SetProperty: Property of type 'file' not initialized before use.");
				}
				}
			}
		}
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty:        (Device " << deviceID << " | Output "
								 << 0 << "): Image " << img.index() << " processed.";
		m_results[0].push(std::move(img));
	}
}

template <typename T>
auto SetProperty::getPropValueFromTreeChild(nlohmann::json tree) -> T
{
	T val;
	try
	{
		val = tree["propertyValue"].get<T>();
	}
	catch(...)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: No value provided of requested type.";
		throw std::runtime_error("risa::cuda::SetProperty: No value provided of requested type.");
	}
	return val;
}

auto SetProperty::parsePropDescriptorArray(nlohmann::json &propDescriptorArray) -> bool
{
	std::string propTypeStr;
	for(const auto& grp : propDescriptorArray)
	{
		detail::SetPropertyDescriptor property;
		try
		{
			property.m_propKey = grp["propertyKey"].get<std::string>();
			propTypeStr = grp["propertyType"].get<std::string>();
		}
		catch(...)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: PropertyDescriptor does not have "
										"'propertyKey' and 'propertyValue' fields.";
			return false;
		}

		if(propTypeStr == "file")
		{
			try
			{
				property.m_filePath = grp["filePath"].get<std::string>();
			}
			catch(...)
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: PropertyDescriptor '"
										 << property.m_propKey
										 << "'of type 'file' has no field 'filePath'";
				return false;
			}

			property.m_isFile = true;
			if(!property.init())
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: Cannot initialize file for property '"
										 << property.m_propKey << "'";
				return false;
			}
		}
		else
		{
			property.m_isFile = false;

			if(propTypeStr == "increment")
			{
				property.m_propType = detail::SetPropertyType::increment;
			}
			else if(propTypeStr == "floatingPoint")
			{
				property.m_propType = detail::SetPropertyType::floatingPoint;
				auto val = getPropValueFromTreeChild<float>(grp);
				property.m_propValue = val;
			}
			else if(propTypeStr == "integer")
			{
				property.m_propType = detail::SetPropertyType::integer;
				auto val = getPropValueFromTreeChild<size_t>(grp);
				property.m_propValue = val;
			}
			else if(propTypeStr == "signed_integer")
			{
				property.m_propType = detail::SetPropertyType::signed_integer;
				auto val = getPropValueFromTreeChild<ssize_t>(grp);
				property.m_propValue = val;
			}
			else if(propTypeStr == "text")
			{
				property.m_propType = detail::SetPropertyType::text;
				auto val = getPropValueFromTreeChild<std::string>(grp);
				property.m_propValue = val;
			}
			else if(propTypeStr == "timestamp")
			{
				property.m_propType = detail::SetPropertyType::timestamp;
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::SetProperty: propertyType '" << propTypeStr << "' not supported.";
				return false;
			}
		}
		m_propDescriptors.emplace(property.m_propKey, std::move(property));
	}

	return true;
}

auto SetProperty::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	auto propDescriptorArray = configReader.lookup(parameterSet + ".propertyDescriptors");

	if(propDescriptorArray.empty())
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: No property descriptors provided to set.";
		return false;
	}

	return parsePropDescriptorArray(propDescriptorArray);
}

auto detail::SetPropertyDescriptor::nextValue() -> std::any
{
	std::string line;
	if(!std::getline(m_fstream, line))
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::SetProperty: End of file reached ('" << m_filePath
								 << "'). Reset to reading from the beginning.";
		// file end reached, reset to second line:
		m_fstream.clear(); // reset fail bit
		m_fstream.seekg(0, std::ios::beg);
		std::getline(m_fstream, line); // re-read type
		if(!std::getline(m_fstream, line)) // re-try reading a value
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: Cannot read value from property file.";
			throw std::runtime_error("risa::cuda::SetProperty: Cannot read value from property file.");
		}
	}
	if(m_propType == SetPropertyType::floatingPoint)
	{
		float res = 0.0F;
		try
		{
			res = std::stof(line);
		}
		catch(std::invalid_argument&)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::SetProperty: Entry '" << line
									 << "' cannot be converted to a float value. Setting to 0.0.";
		}
		return res;
	}
	if(m_propType == SetPropertyType::integer)
	{
		size_t res = 0;
		try
		{
			res = std::stoul(line);
		}
		catch(std::invalid_argument&)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::SetProperty: Entry '" << line
									 << "' cannot be converted to an integer value. Setting to 0.";
		}
		return res;
	}
	if(m_propType == SetPropertyType::signed_integer)
	{
		ssize_t res = 0;
		try
		{
			res = std::stol(line);
		}
		catch(std::invalid_argument&)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::SetProperty: Entry '" << line
									 << "' cannot be converted to an integer value. Setting to 0.";
		}
		return res;
	}
	return line; // return text as-is
}

auto detail::SetPropertyDescriptor::init() -> bool
{
	m_fstream = std::ifstream(m_filePath);
	if(m_fstream.good())
	{
		std::string line;
		if(std::getline(m_fstream, line))
		{
			if(line == "floatingPoint")
			{
				m_propType = SetPropertyType::floatingPoint;
				return true;
			}
			if(line == "integer")
			{
				m_propType = SetPropertyType::integer;
				return true;
			}
			if(line == "text")
			{
				m_propType = SetPropertyType::text;
				return true;
			}
		}
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: Property file holds invalid property type '"
								 << line << "'";
		return false;
	}

	BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::SetProperty: Opening file '" << m_filePath << "' failed.";
	return false;
}

} // namespace risa::cuda

