// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Property/GetProperty.h>

#include <glados/Utils.h>

#include <boost/log/trivial.hpp>

#include <ctime>
#include <exception>

#if defined(_MSC_VER)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

namespace risa::cuda
{

GetProperty::GetProperty(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs((size_t)numberOfOutputs), m_numberOfInputs((size_t)numberOfInputs)
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::GetProperty: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::GetProperty: Number of input ports must be 1.");
	}

	if(numberOfOutputs != 1)
	{
		throw std::runtime_error("risa::cuda::GetProperty: Number of output ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&GetProperty::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty: Running " << m_numberOfDevices << " thread(s).";
}

GetProperty::~GetProperty()
{
	if(m_outputType == detail::GetPropertyOutput::writeAllToFile
		|| m_outputType == detail::GetPropertyOutput::writeLatestToFile)
	{
		if(m_bufferedLines > 0)
		{
			std::ofstream fileOut;
			if(m_outputType == detail::GetPropertyOutput::writeAllToFile)
			{
				fileOut.open(m_outputBufferFile, std::ios::app);
			}
			else if(m_outputType == detail::GetPropertyOutput::writeLatestToFile)
			{
				fileOut.open(m_outputBufferFile, std::ios::trunc);
			}
			if(fileOut.is_open())
			{
				fileOut << m_outputBufferStr;
			}
		}

		// copy buffer to final location
		std::filesystem::copy(
			m_outputBufferFile, m_outputFilePath, std::filesystem::copy_options::overwrite_existing);
	}
}

auto GetProperty::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][(size_t)inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(size_t i = 0; i < m_numberOfOutputs; i++)
		{
			m_results[i].push(output_type());
		}
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty: Finished.";
	}
}

auto GetProperty::wait(int outputIdx) -> output_type { return m_results[(size_t)outputIdx].take(); }

// per device, one thread executes this function
auto GetProperty::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty: Running thread for device " << deviceID;

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty: Received sentinel, finishing.";
			break;
		}
		m_numPropertiesToProcess = m_propDescriptors.size();
		for(const auto& propDescr : m_propDescriptors)
		{
			auto m_propType = propDescr.m_propType;
			auto m_propKey = propDescr.m_propKey;
			if(m_propType == detail::GetPropertyType::all)
			{
				// try to retrieve and convert all available properties
				const auto keys = img.getPropertyKeys();
				m_numPropertiesToProcess = keys.size();
				for(const auto& key : keys)
				{

					std::string val_s;
					if(img.getProperty(key, val_s) == glados::details::PropertyReturnCode::Success)
					{
						handleOutput(img.index(), key, val_s, false);
						continue;
					}

					float val_f{0.0};
					if(img.getProperty(key, val_f) == glados::details::PropertyReturnCode::Success)
					{
						handleOutput(img.index(), key, std::to_string(val_f), false);
						continue;
					}

					ssize_t val_i{0};
					if(img.getProperty(key, val_i) == glados::details::PropertyReturnCode::Success)
					{
						handleOutput(img.index(), key, std::to_string(val_i), false);
						continue;
					}

					size_t val_l{0};
					if(img.getProperty(key, val_l) == glados::details::PropertyReturnCode::Success)
					{
						handleOutput(img.index(), key, std::to_string(val_l), false);
						continue;
					}

					std::chrono::time_point<m_clock> val_t;
					if(img.getProperty(m_propKey, val_t) == glados::details::PropertyReturnCode::Success)
					{
						handleOutput(img.index(), key, timePointToStr(val_t), false);
						continue;
					}
				}

				// additionally, get duration
				handleOutput(
					img.index(), "duration_from_start", std::to_string(img.duration()) + " ms", m_forceFlush);
			}
			else if(m_propType == detail::GetPropertyType::duration)
			{
				// double check the prop is a timestamp
				std::chrono::time_point<m_clock> val;
				if(img.getProperty(m_propKey, val) == glados::details::PropertyReturnCode::Success)
				{
					handleOutput(img.index(), "duration_from_" + m_propKey,
						std::to_string(img.duration(m_propKey)) + " ms", m_forceFlush);
				}
				else
				{
					handleOutput(img.index(), m_propKey, "TYPE_MISMATCH (is NOT timestamp)", m_forceFlush);
				}
			}
			else if(img.hasProperty(m_propKey))
			{
				std::string valStr{"TYPE_MISMATCH"};
				switch(m_propType)
				{
				case detail::GetPropertyType::floatingPoint:
				{
					float val{0.0};
					if(img.getProperty(m_propKey, val) == glados::details::PropertyReturnCode::Success)
					{
						valStr = std::to_string(val);
					}
					else
					{
						valStr += " (is NOT floatingPoint)";
					}
					break;
				}
				case detail::GetPropertyType::integer:
				{
					size_t val{0};
					if(img.getProperty(m_propKey, val) == glados::details::PropertyReturnCode::Success)
					{
						valStr = std::to_string(val);
					}
					else
					{
						valStr += " (is NOT integer)";
					}
					break;
				}
				case detail::GetPropertyType::signed_integer:
				{
					ssize_t val{0};
					if(img.getProperty(m_propKey, val) == glados::details::PropertyReturnCode::Success)
					{
						valStr = std::to_string(val);
					}
					else
					{
						valStr += " (is NOT signed_integer)";
					}
					break;
				}
				case detail::GetPropertyType::text:
				{
					if(img.getProperty(m_propKey, valStr) != glados::details::PropertyReturnCode::Success)
					{
						valStr += " (is NOT text)";
					}
					break;
				}
				case detail::GetPropertyType::timestamp:
				{
					std::chrono::time_point<m_clock> val;
					if(img.getProperty(m_propKey, val) == glados::details::PropertyReturnCode::Success)
					{
						valStr = timePointToStr(val);
					}
					else
					{
						valStr += " (is NOT timestamp)";
					}
					break;
				}
				case detail::GetPropertyType::all:
				case detail::GetPropertyType::duration:
				{
					; // already handled, cannot be reached
				}
				}
				handleOutput(img.index(), m_propKey, valStr, m_forceFlush);
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::GetProperty: image has no property '" << m_propKey << "'";
				const auto keys = img.getPropertyKeys();
				std::string availableKeys{"risa::cuda::GetProperty: image has these properties: '"};
				for(const auto& key : keys)
				{
					availableKeys += key + "', '";
				}
				for(auto i = 0; i < 3; i++)
				{
					availableKeys.pop_back();
				}

				BOOST_LOG_TRIVIAL(warning) << availableKeys;

				throw std::runtime_error("risa::cuda::GetProperty: property does not exist.");
			}
		}
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::GetProperty:        (Device " << deviceID << " | Output "
								 << 0 << "): Image " << img.index() << " processed.";
		m_results[0].push(std::move(img));
	}
}

auto GetProperty::timePointToStr(std::chrono::time_point<m_clock> val) -> std::string
{

	std::time_t time = m_clock::to_time_t(val);
	constexpr const size_t timeStrLen{21};
	std::array<char, timeStrLen> buf{};
#ifdef __linux__
	strftime(buf.data(), timeStrLen, "%Y-%b-%d %H:%M:%S", localtime(&time));
#elif _WIN32
	struct tm tm_buf;
	if(localtime_s(&tm_buf, &time) == 0)
	{
		strftime(buf.data(), timeStrLen, "%Y-%b-%d %H:%M:%S", &tm_buf);
	}
#endif
	const std::string baseTimeStr(buf.data(), buf.size());
	auto t_s = std::chrono::time_point_cast<std::chrono::seconds>(val).time_since_epoch().count();
	auto t_us = std::chrono::time_point_cast<std::chrono::microseconds>(val).time_since_epoch().count();
	constexpr const size_t US_PER_S{1000000};
	return (baseTimeStr + "." + std::to_string((unsigned long)t_us - US_PER_S * (unsigned long)t_s));
}

auto GetProperty::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	std::string propTypeStr;
	std::string propOutputStr;
	std::string propOutputFormatStr;

	auto propDescriptorArray = configReader.lookup(parameterSet + ".propertyDescriptors");

	if(propDescriptorArray.empty())
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: No property descriptors provided to get.";
		return false;
	}

	bool requestedAllProps = false;
	for(const auto& grp : propDescriptorArray)
	{
		detail::GetPropertyDescriptor property;
		if(!grp.contains("propertyKey"))
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::GetProperty: Malformed property descriptor object. "
										"Expected key 'propertyKey'.";
			return false;
		}
		property.m_propKey = grp["propertyKey"].get<std::string>();
		if(!grp.contains("propertyType"))
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::GetProperty: Malformed property descriptor object. "
										"Expected key 'propertyType'.";
			return false;
		}
		propTypeStr = grp["propertyType"].get<std::string>();

		if(requestedAllProps)
		{
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::GetProperty: Allready requested to get 'all' "
										  "properties, ignoring further requests.";
			property.m_propType = detail::GetPropertyType::all;
			m_propDescriptors.clear();
			m_propDescriptors.push_back(property);
			break;
		}

		if(propTypeStr == "all")
		{
			property.m_propType = detail::GetPropertyType::all;
			requestedAllProps = true;
		}
		else if(propTypeStr == "duration")
		{
			property.m_propType = detail::GetPropertyType::duration;
		}
		else if(propTypeStr == "integer")
		{
			property.m_propType = detail::GetPropertyType::integer;
		}
		else if(propTypeStr == "signed_integer")
		{
			property.m_propType = detail::GetPropertyType::signed_integer;
		}
		else if(propTypeStr == "floatingPoint")
		{
			property.m_propType = detail::GetPropertyType::floatingPoint;
		}
		else if(propTypeStr == "text")
		{
			property.m_propType = detail::GetPropertyType::text;
		}
		else if(propTypeStr == "timestamp")
		{
			property.m_propType = detail::GetPropertyType::timestamp;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: propertyType '" << propTypeStr
									 << "' not supported.";
			return false;
		}
		m_propDescriptors.push_back(property);
	}

	if(configReader.lookupValue(parameterSet + ".outputFormat", propOutputFormatStr))
	{
		if(propOutputFormatStr == "csv")
		{
			m_outputFormat = detail::GetPropertyOutputFormat::csv;
		}
		else if(propOutputFormatStr == "keyValue")
		{
			m_outputFormat = detail::GetPropertyOutputFormat::keyValue;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: Output format " << propOutputFormatStr
									 << " is not supported.";
		}
	}
	else
	{
		return false;
	}

	float maxOutputRate{0.0F};
	if(configReader.lookupValue(parameterSet + ".propertyOutput", propOutputStr)
		&& configReader.lookupValue(parameterSet + ".outputOnStateChangeOnly", m_outputOnStateChangeOnly)
		&& configReader.lookupValue(parameterSet + ".maxOutputRate", maxOutputRate))
	{
		if(propOutputStr == "print")
		{
			m_outputType = detail::GetPropertyOutput::print; // only print for now
			m_forceFlush = true; //?
		}
		else if(propOutputStr == "writeAllToFile")
		{
			m_outputType = detail::GetPropertyOutput::writeAllToFile;
			m_forceFlush = false;
		}
		else if(propOutputStr == "writeLatestToFile")
		{
			m_outputType = detail::GetPropertyOutput::writeLatestToFile;
			m_forceFlush = true;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: property output method '" << propOutputStr
									 << "' not supported.";
			return false;
		}

		if(m_outputType == detail::GetPropertyOutput::writeAllToFile
			|| m_outputType == detail::GetPropertyOutput::writeLatestToFile)
		{
			if(!configReader.lookupValue(parameterSet + ".propertyOutputFile", m_outputFilePath))
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: writing property to file requested but "
											"no file name provided.";
				return false;
			}

			if(m_outputFormat == detail::GetPropertyOutputFormat::csv && !m_outputFilePath.ends_with(".csv"))
			{
				m_outputFilePath += ".csv";
			}
			else if(m_outputFormat != detail::GetPropertyOutputFormat::csv
				&& !m_outputFilePath.ends_with(".txt"))
			{
				m_outputFilePath += ".txt";
			}

			std::filesystem::path outputFilePath(m_outputFilePath);
			auto parentDir = outputFilePath.parent_path().string();
			if(!glados::utils::FileSystemHandle::createDirectory(parentDir))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::GetProperty: Cannot create '" << parentDir << "'.";
				return false;
			}

			if(!glados::utils::FileSystemHandle::createEmptyFile(m_outputFilePath))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::GetProperty: Cannot create '" << m_outputFilePath << "'.";
				return false;
			}

			constexpr const size_t ASSUMED_LINE_LENGTH{100};
			m_outputBufferStr.reserve(
				m_maxBufferedLines * ASSUMED_LINE_LENGTH); // assume max average line width of 100 chars

			// NOLINTNEXTLINE(bugprone-unused-raii)
			std::ofstream(m_outputFilePath, std::ios::trunc); // clear file contents (if there are any)

			m_outputBufferFile = "/tmp/" + parameterSet + "_propBuffer.tmp";
			// NOLINTNEXTLINE(bugprone-unused-raii)
			std::ofstream(m_outputBufferFile, std::ios::trunc); // clear file contents (if there are any)
		}

		pauseBetweenOutputs_s = (maxOutputRate == 0 ? 0.0 : 1.0 / maxOutputRate);

		return true;
	}

	return false;
}

auto GetProperty::handleOutput(
	const size_t imgIndex, const std::string& key, const std::string& valStr, bool flush) -> void
{
	const auto secondsSinceLastOutput =
		std::chrono::duration_cast<m_second>(m_clock::now() - m_lastOutputTime);
	if(secondsSinceLastOutput.count() < pauseBetweenOutputs_s)
	{
		return;
	}

	// helper functor
	auto numToPaddedStr = [](const size_t num, const size_t totalLen)
	{
		std::string str = std::to_string(num);
		const auto len = str.size();
		if(len >= totalLen)
		{
			return str;
		}
		return (std::string(totalLen - len, '0') + str);
	};

	// buffer values
	bool valueChanged = false;
	if(m_propertyBuffer.contains(key))
	{
		if(m_propertyBuffer.at(key) != valStr)
		{
			// new value
			valueChanged = true;
		}
	}
	else
	{
		// new key, i.e. must also be a new value
		valueChanged = true;
	}
	m_propertyBuffer[key] = valStr;

	if(!m_outputOnStateChangeOnly || (m_outputOnStateChangeOnly && valueChanged))
	{
		switch(m_outputFormat)
		{
		case detail::GetPropertyOutputFormat::keyValue:
		{
			constexpr const size_t PADDED_STR_LEN{10};
			if(m_outputBufferStr.empty())
			{
				m_outputBufferStr = numToPaddedStr(imgIndex, PADDED_STR_LEN) + ": [" + key + "] = " + valStr;
			}
			else
			{
				m_outputBufferStr +=
					"\n" + numToPaddedStr(imgIndex, PADDED_STR_LEN) + ": [" + key + "] = " + valStr;
			}
			m_bufferedLines++;
			break;
		}
		case detail::GetPropertyOutputFormat::csv:
		{
			m_numPropertiesToProcess--;

			if(!m_csvHeaderDone)
			{
				m_csvHeaderBuf += "\"" + key + "\"" + (m_numPropertiesToProcess == 0 ? ";" : ",");
				if(m_numPropertiesToProcess == 0)
				{
					m_outputBufferStr = m_csvHeaderBuf + m_outputBufferStr + "\n";
					m_csvHeaderDone = true;
				}
				
			}

			m_outputBufferStr += "\"" + valStr + "\"" + (m_numPropertiesToProcess == 0 ? ";" : ",");

			if(m_numPropertiesToProcess == 0)
			{
				// processed all current keys, may flush now
				m_bufferedLines++;

				if(m_outputType != detail::GetPropertyOutput::print)
				{
					m_outputBufferStr += "\n";
				}
			}

			break;
		}
		}
	}
	else
	{
		// m_outputOnStateChangeOnly and value has not changed
		flush = false;
	}

	if(m_bufferedLines >= m_maxBufferedLines)
	{
		m_outputBufferStr += "\n";
		flush = true;
	}

	// perform output action with buffered contents
	if(flush)
	{
		m_lastOutputTime = m_clock::now();
		std::ofstream fileOut;
		switch(m_outputType)
		{
		case detail::GetPropertyOutput::print:
		{
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::GetProperty: " << m_outputBufferStr;
			m_outputBufferStr.clear();
			m_bufferedLines = 0;
			return;
		}
		case detail::GetPropertyOutput::writeAllToFile:
		{
			fileOut.open(m_outputBufferFile, std::ios::app);
			break;
		}
		case detail::GetPropertyOutput::writeLatestToFile:
		{
			fileOut.open(m_outputBufferFile, std::ios::trunc);
			break;
		}
		}
		// only file writing outputs should reach this point anyways
		if(fileOut.is_open())
		{
			fileOut << m_outputBufferStr;
			m_outputBufferStr.clear();
			m_bufferedLines = 0;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::GetProperty: Cannot open '" << m_outputBufferFile
									 << "' to write.";
			throw std::runtime_error("risa::cuda::GetProperty: Unable to open output file for writing.");
		}
		if(m_outputType == detail::GetPropertyOutput::writeLatestToFile)
		{
			std::filesystem::copy(
				m_outputBufferFile, m_outputFilePath, std::filesystem::copy_options::overwrite_existing);
		}
	}
}

} // namespace risa::cuda
