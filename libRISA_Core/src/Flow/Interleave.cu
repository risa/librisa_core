// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Flow/Interleave.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Interleave::Interleave(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Interleave: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Interleave::processor, this, i};
	}

	m_activeInputStreams = m_numberOfInputs;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Running " << m_numberOfDevices << " thread(s).";
}

auto Interleave::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else 
	{
		std::unique_lock<std::mutex> lock(
			m_mutex); // we have to lock here because the threads per input stream may try to access
					 // m_activeInputStreams simultaneously

		m_activeInputStreams--;

		BOOST_LOG_TRIVIAL(info) << "risa::cuda::Interleave: " << inputIdx << " finished. " << m_activeInputStreams
								 << " active input streams remaining.";

		if(m_activeInputStreams == 0)
		{
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::Interleave: Received sentinel, finishing.";

			// send sentinel to processor threads and wait 'til they're finished
			for(auto i = 0; i < m_numberOfDevices; i++)
			{
				for(auto j = 0; j < m_numberOfInputs; j++)
				{
					m_imgs[i][j].push(input_type());
				}
			}

			for(auto i = 0; i < m_numberOfDevices; i++)
			{
				m_processorThreads[i].join();
			}

			// push sentinel to results for next stage
			for(auto i = 0; i < m_numberOfOutputs; i++)
				m_results[i].push(output_type());
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Finished.";
		}
	}
}

auto Interleave::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Interleave::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Running thread for device " << deviceID;

	std::vector<input_type> buffImages;
	buffImages.reserve(m_numberOfInputs);
	size_t imgIdx;
	while(true)
	{
		//take one image from each input queue
		for(auto idx = 0; idx < m_numberOfInputs; idx++)
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Buffering image from input " << idx;
			buffImages[idx] = m_imgs[deviceID][idx].take();
		}
		
		for(auto idx = 0; idx < m_numberOfInputs; idx++)
		{
			if(!buffImages[idx].valid())
			{
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Received sentinel, finishing.";
				return;
			}
			imgIdx = buffImages[idx].index();
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave: Sending buffered image from input " << idx;
			m_results[0].push(std::move(buffImages[idx]));
		}
		
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Interleave:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << imgIdx << " processed.";
	}
}

auto Interleave::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	//ConfigReader configReader = ConfigReader(configFile.data());

	return true;//configReader.lookupValue("pipeline.interleave." + parameterSet + ".imageWidth", imageWidth);

}

}
}
