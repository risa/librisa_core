// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Flow/StartAfterNImages.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

StartAfterNImages::StartAfterNImages(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfInputs{numberOfInputs}, m_numberOfOutputs{numberOfOutputs}
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::StartAfterNImages: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::StartAfterNImages: Number of input ports must be 1.");
	}

	if(numberOfOutputs != 1)
	{
		throw std::runtime_error("risa::cuda::StartAfterNImages: Number of output ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(1);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&StartAfterNImages::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Skip " << m_waitNumImages
							<< " images before forwarding";
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Running " << m_numberOfDevices
							 << " thread(s).";
}

auto StartAfterNImages::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages:        (Device " << img.device()
								 << " |  Input " << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Finished.";
	}
}

auto StartAfterNImages::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto StartAfterNImages::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Running thread for device " << deviceID;

	CHECK(cudaSetDevice(deviceID));
	size_t skippedImages{0};

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();

		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Received sentinel, finishing.";
			return;
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages:        Handling image " << img.index();
		auto imgIdx = img.index();
		if(skippedImages > m_waitNumImages)
		{
			m_results[0].push(std::move(img));
		}
		else if(skippedImages == m_waitNumImages)
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages: Start forwarding images.";
			m_results[0].push(std::move(img));
		}
		else
		{
			// discard the input buffer, do not fill up the output queue though.
		}
		skippedImages++;
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StartAfterNImages:        (Device " << deviceID
								 << " | Output " << 0 << "): Image " << imgIdx << " processed.";
	}
}

auto StartAfterNImages::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	return configReader.lookupValue(parameterSet + ".waitNumImages", m_waitNumImages);
}
}
}
