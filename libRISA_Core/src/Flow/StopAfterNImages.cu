// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Flow/StopAfterNImages.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

StopAfterNImages::StopAfterNImages(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfInputs{numberOfInputs}, m_numberOfOutputs{numberOfOutputs}
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::StopAfterNImages: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::StopAfterNImages: Number of input ports must be 1.");
	}

	if(numberOfOutputs != 1)
	{
		throw std::runtime_error("risa::cuda::StopAfterNImages: Number of output ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(1);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&StopAfterNImages::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(info) << "risa::cuda::StopAfterNImages: Forwarding up to " << m_maxNumImages
							<< " images";
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages: Running " << m_numberOfDevices << " thread(s).";
}

auto StopAfterNImages::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages: Finished.";
	}
}

auto StopAfterNImages::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto StopAfterNImages::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages: Running thread for device " << deviceID;

	CHECK(cudaSetDevice(deviceID));
	size_t forwardedImages{0};

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();

		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages: Received sentinel, finishing.";
			return;
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages:        Handling image " << img.index();
		auto imgIdx = img.index();
		if(forwardedImages < m_maxNumImages)
		{
			m_results[0].push(std::move(img));
		}
		else if(forwardedImages == m_maxNumImages)
		{
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::StopAfterNImages: Forwarded last image.";
			m_results[0].push(output_type()); // indicate to next stage that we're done with this stream. Don't shut down yet!
		}
		else
		{
			// discard the input buffer, do not fill up the output queue though.
		}
		forwardedImages++;
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::StopAfterNImages:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << imgIdx << " processed.";
	}
}

auto StopAfterNImages::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	return configReader.lookupValue(parameterSet + ".maxNumImages", m_maxNumImages);
}
}
}
