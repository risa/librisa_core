// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Flow/Split.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Split::Split(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::Split: Configuration file could not be loaded successfully.");
	}

	if(numberOfInputs != 1)
	{
		throw std::runtime_error("risa::cuda::Split: Number of input ports must be 1.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	if(m_splitMode == detail::SplitMode::copyToAll)
	{
		m_memoryPoolIdxs.reserve(m_numberOfDevices);
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			CHECK(cudaSetDevice(i));
			m_memoryPoolIdxs[i] = glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
				m_memPoolSize, m_imageWidth * m_imageHeight);
			cudaStream_t stream;
			CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 5));
			m_streams[i] = stream;
		}
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Split::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Running " << m_numberOfDevices << " thread(s).";
}

Split::~Split() {
	if(m_splitMode == detail::SplitMode::copyToAll)
	{
		for(auto idx : m_memoryPoolIdxs)
		{
			glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
		}
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			CHECK(cudaSetDevice(i));
			CHECK(cudaStreamDestroy(m_streams[i]));
		}
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Destroyed.";
}

auto Split::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(auto i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Finished.";
	}
}

auto Split::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Split::processor(const int deviceID) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Running thread for device " << deviceID;

	unsigned int currentIndex = 0;

	CHECK(cudaSetDevice(deviceID));

	while(true)
	{
		auto img = m_imgs[deviceID][0].take();

		if(!img.valid())
		{
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split: Received sentinel, finishing.";
			return;
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split:        Handling image " << img.index();

		const auto imgIdx = img.index();

		switch(m_splitMode)
		{
		case detail::SplitMode::copyToAll:
			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::Split:        Copying image " << img.index() << " to output " << i;
				
				auto memPoolIdx = m_memoryPoolIdxs[deviceID];
				auto imgSplit =
					glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(img.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				m_results[i].push(std::move(imgSplit));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split:        (Device " << deviceID << " | Output "
										 << i << "): Image " << imgIdx << " processed.";
			}

			// wait until work on device is finished
			//CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			m_results[0].push(std::move(img));

			break;

		case detail::SplitMode::roundRobin:
			m_results[currentIndex].push(std::move(img));

			currentIndex = (currentIndex + 1) % m_numberOfOutputs;
			break;

		case detail::SplitMode::splitByPlaneIdx:

			currentIndex = img.plane();
			if(currentIndex < m_numberOfOutputs)
			{
				m_results[currentIndex].push(std::move(img));
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::Split: Cannot move image to output " << currentIndex + 1
					<< " because there are only " << m_numberOfOutputs << " output ports configured.";
				throw std::runtime_error("Trying to split stream to a planeId outside the configured range.");
			}

			break;
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Split:        (Device " << deviceID << " | Output " << 0
								 << "): Image " << imgIdx << " processed.";
	}
}

auto Split::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	std::string splitModeStr;
	if(configReader.lookupValue(parameterSet + ".splitMode", splitModeStr))
	{

		if(splitModeStr == "copyToAll")
		{
			m_splitMode = detail::SplitMode::copyToAll;
			if(!configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cude::Split: imageWidth not configured for splitMode 'copyToAll'";
				return false;
			}
			if(!configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cude::Split: imageHeight not configured for splitMode 'copyToAll'";
				return false;
			}
			if(!configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cude::Split: memPoolSize not configured for splitMode 'copyToAll'";
				return false;
			}
		}
		else if(splitModeStr == "roundRobin")
		{
			m_splitMode = detail::SplitMode::roundRobin;
		}
		else if(splitModeStr == "splitByPlane")
		{
			m_splitMode = detail::SplitMode::splitByPlaneIdx;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::Split: SplitMode '" << splitModeStr
									 << "' is not supported.";
			return false;
		}
		return true;
	}
	return false;
	
}
}
}
