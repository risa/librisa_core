// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Transform/IFFT2D.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

IFFT2D::IFFT2D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(numberOfInputs != 2)
	{
		BOOST_LOG_TRIVIAL(error) << "risa::cuda::IFFT2D: IFFT2D expects 2 intput streams. Got: "
								 << numberOfInputs;
		throw std::runtime_error("risa::cuda::IFFT2D: Wrong number of input streams.");
	}

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::IFFT2D: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// cuFFT library is initialized for each device
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, (m_imageWidth - 1) * 2 * m_imageHeight));
		initCuFFT(i);
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&IFFT2D::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D: Running " << m_numberOfDevices << " thread(s).";
}

IFFT2D::~IFFT2D()
{
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
		CHECK_CUFFT(cufftDestroy(m_plansInv[i]));
	}
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D: Destroyed.";
}

auto IFFT2D::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D:       (Device " << image.device() << " |  Input "
								 << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D: Finished.";
	}
}

auto IFFT2D::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto IFFT2D::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	const int outputWidth = (m_imageWidth - 1) * 2;
	auto sinoFreq = glados::cuda::make_device_ptr<cufftComplex, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight);
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGridPreIFFT(
		(int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));
	dim3 dimGridPostIFFT(
		(int)ceil(outputWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));
	BOOST_LOG_TRIVIAL(debug) << "recoLib::cuda::IFFT2D: Running thread for device " << deviceID;

	while(true)
	{
		auto imageReal = m_images[deviceID][0].take();
		if(!imageReal.valid())
			break;

		auto imageImag = m_images[deviceID][1].take();
		if(!imageImag.valid())
			break;

		auto image =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

		// take input streams and combine into cufftComplex format
		combineStreamsIntoCufftComplex<<<dimGridPreIFFT, dimBlock, 0, m_streams[deviceID]>>>(
			m_imageWidth, m_imageHeight, sinoFreq.get(), imageReal.data(), imageImag.data());

		// reverse transformation
		CHECK_CUFFT(cufftExecC2R(m_plansInv[deviceID], sinoFreq.get(), (cufftReal*)image.data()));

		// scale after inverse transform
		scaleAfterIFFT<<<dimGridPostIFFT, dimBlock, 0, m_streams[deviceID]>>>(
			outputWidth, m_imageHeight, image.data());

		image.setDevice(deviceID);
		image.setProperties(imageReal.properties());

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::IFFT2D:       (Device " << deviceID << " | Output " << 0
								 << "): Image " << image.index() << " processed.";
		m_results[0].push(std::move(image));
	}
}

auto IFFT2D::initCuFFT(const int deviceID) -> void
{

	CHECK(cudaSetDevice(deviceID));

	cudaStream_t stream;
	cufftHandle planInv;

	CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
	m_streams[deviceID] = stream;

	CHECK_CUFFT(cufftPlan2d(&planInv, m_imageHeight, (m_imageWidth - 1) * 2, CUFFT_C2R));

	CHECK_CUFFT(cufftSetStream(planInv, stream));

	m_plansInv[deviceID] = planInv;
}

auto IFFT2D::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D))
	{
		return true;
	}
	return false;
}

__global__ void scaleAfterIFFT(const int imageWidth, const int imageHeight, float* __restrict__ data)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();
	if(x < imageWidth && y < imageHeight)
	{
		const auto idx = y * imageWidth + x;
		const auto factor = imageWidth * imageHeight;
		data[idx] /= (float)factor;
	}
}

__global__ void combineStreamsIntoCufftComplex(const int imageWidth, const int imageHeight,
	cufftComplex* data, const float* const __restrict__ dataReal, const float* const __restrict__ dataImag)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();
	const auto idx = y * imageWidth + x;

	// remove scaling before inverse fft
	const float fact = imageWidth * imageHeight;

	if(idx < imageWidth * imageHeight)
	{
		data[idx].x = dataReal[idx] * fact;
		data[idx].y = dataImag[idx] * fact;
	}
}

}
}
