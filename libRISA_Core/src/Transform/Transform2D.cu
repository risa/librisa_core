/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 16. April 2024
 * Authors: Dominic Windisch <d.windisch@hzdr.de>
 * Date: 23. April 2024
 * Co-Authors: Stephan Boden <s.boden@hzdr.de
 */

#include "cuda_kernels_Transform2D.h"

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Transform/Transform2D.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <vector>

namespace risa
{
namespace cuda
{

Transform2D::Transform2D(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::Transform2D: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 4));
		m_streams[i] = stream;
	}

	// allocate memory for hash table on each device
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		allocateDeviceStorage(i);
		configureTransform2D(i); // calculate hash table and store on device
	}

	m_doConfig = false;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// allocate memory on all available devices
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Transform2D::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: Running " << m_numberOfDevices << " thread(s).";
}

Transform2D::~Transform2D()
{
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(m_memoryPoolIdxs[i]);
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto Transform2D::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D:       (Device " << image.device()
								 << " |  Input " << inputIdx << "): Image " << image.index()
								 << " arrived. (valid: " << image.valid() << ")";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: Finished.";
	}
}

auto Transform2D::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Transform2D::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	dim3 blocks2D(m_blockSize2D, m_blockSize2D);
	dim3 grids2D(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: Running thread for device " << deviceID;
	while(true)
	{
		if(m_doConfig)
		{
			configureTransform2D(deviceID);
			m_doConfig = false;
		}

		// take next image from input 0
		auto image = m_images[deviceID][0].take();

		if(!image.valid())
		{
			break;
		}

		auto result =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

		interp2<<<grids2D, blocks2D, 0, m_streams[deviceID]>>>(
			image.data(), result.data(), m_imageWidth, m_imageHeight, indices_d_[deviceID].get(), weights_d_[deviceID].get());
		CHECK(cudaPeekAtLastError());

		result.setDevice(deviceID);
		result.setProperties(image.properties());
		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(int i = 1; i < m_numberOfOutputs; i++)
		{
			auto imageSplit =
				glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

			cudaMemcpyAsync(imageSplit.data(), result.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			imageSplit.setDevice(deviceID);
			imageSplit.setProperties(result.properties());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D:       (Device " << deviceID << " | Output "
									 << i << "): Image " << imageSplit.index() << " processed.";
			m_results[i].push(std::move(imageSplit));
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D:       (Device " << deviceID << " | Output " << 0
								 << "): Image " << result.index() << " processed.";
		m_results[0].push(std::move(result));
	}
}

auto Transform2D::allocateDeviceStorage(int deviceID) -> void
{
	indices_d_[deviceID] = std::move(glados::cuda::make_device_ptr<uint32_t, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * 4));

	weights_d_[deviceID] = std::move(glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * 4));
}

auto Transform2D::configureTransform2D(int deviceID) -> void
{

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: Computing Hash Table for 2D transformation.";

	/* setup 2d transform information */
	// todo: any setup of tilt etc needed?

	// setup lookup tables on GPU
	dim3 blocks2D(m_blockSize2D, m_blockSize2D);
	dim3 grids2D(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	// todo: fill values

	const float distSrcDetInPx = m_distSrcDet / m_lPx;

	const float rollInRad = m_rollInDeg / 180.0f * M_PI;
	const float pitchInRad = m_pitchInDeg / 180.0f * M_PI;
	const float yawInRad = m_yawInDeg / 180.0f * M_PI;

	createLookupTables<<<grids2D, blocks2D, 0, m_streams[deviceID]>>>(m_imageWidth, m_imageHeight, rollInRad,
		yawInRad, pitchInRad, m_offsetX, m_offsetY, distSrcDetInPx, indices_d_[deviceID].get(), weights_d_[deviceID].get());
	CHECK(cudaPeekAtLastError());

	CHECK(cudaStreamSynchronize(m_streams[deviceID]));
}

auto Transform2D::update(glados::Subject* s) -> void
{
	// subject may call this function to update stage parameters during runtime
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Transform2D: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			// add tilt, roll, yaw, ...
			// clang-format off
			hasValidSetting = configReader.lookupValue("rollInDeg", m_rollInDeg) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("pitchInDeg", m_pitchInDeg) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("yawInDeg", m_yawInDeg) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("distSrcDet", m_distSrcDet) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("offsetX", m_offsetX) || hasValidSetting;
			hasValidSetting = configReader.lookupValue("offsetY", m_offsetY) || hasValidSetting;
			// clang-format on
			if(hasValidSetting)
			{
				std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Transform2D: Dynamic reconfiguration triggered.";
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Transform2D: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto Transform2D::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	std::string loadSourcePathMode;
	ConfigReader configReader = ConfigReader(configFile.data());
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".rollInDeg", m_rollInDeg)
		&& configReader.lookupValue(parameterSet + ".pitchInDeg", m_pitchInDeg)
		&& configReader.lookupValue(parameterSet + ".yawInDeg", m_yawInDeg)
		&& configReader.lookupValue(parameterSet + ".distSrcDet", m_distSrcDet)
		&& configReader.lookupValue(parameterSet + ".offsetX", m_offsetX)
		&& configReader.lookupValue(parameterSet + ".offsetY", m_offsetY))
	{

		return true;
	}

	return false;
}

__global__ void createLookupTables(const size_t imageWidth, const size_t imageHeight, const float rollInRad,
	const float yawInRad, const float pitchInRad, const float offsetX, const float offsetY,
	const float distSrcDetInPx, uint32_t* indices_d_, float* weights_d_)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
	{
		return;
	}

	const auto pixelIdx = y * imageWidth + x;
	const auto indicesIdx = pixelIdx * 4;
	const auto weightsIdx = pixelIdx * 4;

	/*
	* alpha = -10; % roll
	beta = -20; % yaw
	gamma = 10; % pitch

	dpx = d/lpx;
	alpharad = alpha/180*pi;
	betarad = beta/180*pi;
	gammarad = gamma/180*pi;
	*/
	float Inew = (float)y - (float)imageHeight / 2.0f - 0.5f;
	const float Jnew = (float)x - (float)imageWidth / 2.0f - 0.5f;

	if(Inew == 0.0f)
	{
		Inew = 1E-3; // Inew == 0 causes undefined indexes
	}

	// Ii = dpx. / ( cos(gammarad) * dpx./ Inew -     sin(betarad) / cos(betarad) * (cos(gammarad) * Jnew./
	// Inew + sin(gammarad) * sin(betarad))        - sin(gammarad) * cos(betarad) );
	const float term1 = cos(pitchInRad) * distSrcDetInPx / Inew;
	const float term2 = tan(yawInRad) * (cos(pitchInRad) * Jnew / Inew + sin(pitchInRad) * sin(yawInRad));
	const float term3 = sin(pitchInRad) * cos(yawInRad);

	const float Ii = distSrcDetInPx / (term1 - term2 - term3);

	// Ji = Ii.*(cos(gammarad) * Jnew./ Inew + sin(gammarad) * sin(betarad)) / cos(betarad);
	const float Ji = Ii * (cos(pitchInRad) * Jnew / Inew + sin(pitchInRad) * sin(yawInRad)) / cos(yawInRad);


	//  Iir = sin(alpharad) * Ji + cos(alpharad) * Ii;
	const float Iir = Ji * sin(rollInRad) + Ii * cos(rollInRad);
	
	//  Jir = cos(alpharad) * Ji - sin(alpharad) * Ii;
	const float Jir = Ji * cos(rollInRad) - Ii * sin(rollInRad);

	const float Iout = Iir + offsetY + (float)imageHeight / 2.0f + 0.5f;
	const float Jout = Jir + offsetX + (float)imageWidth / 2.0f + 0.5f;

	// Iout/Jout are floting point pixel coordinates and are valid in range 0...imageWidth-1,
	// 0...imageHeight-1 determine closest neighbours with respective weights

	if(Iout >= 0 && Iout < imageHeight - 1 && Jout >= 0 && Jout < imageWidth - 1)
	{
		const float wx = Jout - floor(Jout);
		const float wy = Iout - floor(Iout);
		const int32_t xi = floor(Jout);
		const int32_t yi = floor(Iout);

		indices_d_[indicesIdx + 0] = yi * imageWidth + xi;
		indices_d_[indicesIdx + 1] = yi * imageWidth + xi + 1;
		indices_d_[indicesIdx + 2] = (yi + 1) * imageWidth + xi;
		indices_d_[indicesIdx + 3] = (yi + 1) * imageWidth + xi + 1;
		weights_d_[weightsIdx + 0] = (1.0f - wx) * (1.0f - wy);
		weights_d_[weightsIdx + 1] = wx * (1.0f - wy);
		weights_d_[weightsIdx + 2] = (1.0f - wx) * wy;
		weights_d_[weightsIdx + 3] = wx * wy;
	}
	else
	{
		indices_d_[indicesIdx + 0] = 0;
		indices_d_[indicesIdx + 1] = 0;
		indices_d_[indicesIdx + 2] = 0;
		indices_d_[indicesIdx + 3] = 0;
		weights_d_[weightsIdx + 0] = 0.0f;
		weights_d_[weightsIdx + 1] = 0.0f;
		weights_d_[weightsIdx + 2] = 0.0f;
		weights_d_[weightsIdx + 3] = 0.0f;
	}
}

__global__ void interp2(const float* inputImage, float* outputImage, const size_t imageWidth,
	const size_t imageHeight, const uint32_t* indices_d_, const float* weights_d_)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
	{
		return;
	}
	const auto pixelIdx = y * imageWidth + x;
	const auto indicesIdx = pixelIdx * 4;
	const auto weightsIdx = pixelIdx * 4;
	// clang-format off
	outputImage[pixelIdx] = 
		inputImage[indices_d_[indicesIdx + 0]] * weights_d_[weightsIdx + 0] +
		inputImage[indices_d_[indicesIdx + 1]] * weights_d_[weightsIdx + 1] +
		inputImage[indices_d_[indicesIdx + 2]] * weights_d_[weightsIdx + 2] +
		inputImage[indices_d_[indicesIdx + 3]] * weights_d_[weightsIdx + 3];
	// clang-format on
}

} // namespace cuda
} // namespace risa
