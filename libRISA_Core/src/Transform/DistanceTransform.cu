/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 18. January 2025
 * Authors: Dominic Windisch <d.windisch@hzdr.de>
 */

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Transform/DistanceTransform.h>

#include <glados/cuda/Coordinates.h>
#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <vector>

#include <risaCore/Basics/UtilityKernels.h>

namespace risa
{
namespace cuda
{

DistanceTransform::DistanceTransform(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::DistanceTransform: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 4));
		m_streams[i] = stream;
	}

	size_t hpBufferSize;
	NppStatus status =
		nppiSignedDistanceTransformPBAGetBufferSize(NppiSize{m_imageWidth, m_imageHeight}, &hpBufferSize);
	if(status != NPP_SUCCESS)
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::DistanceTransform: Unable to get distance transform buffer size";
		throw std::runtime_error(
			"risa::cuda::DistanceTransform: Unable to get distance transform buffer size");
	}
	m_scratchMemory = glados::cuda::make_device_ptr<Npp8u, glados::cuda::async_copy_policy>(hpBufferSize);
	m_doConfig = true;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// allocate memory on all available devices
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&DistanceTransform::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform: Running " << m_numberOfDevices
							 << " thread(s).";
}

DistanceTransform::~DistanceTransform()
{
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(m_memoryPoolIdxs[i]);
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto DistanceTransform::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform:       (Device " << image.device()
								 << " |  Input " << inputIdx << "): Image " << image.index()
								 << " arrived. (valid: " << image.valid() << ")";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform: Finished.";
	}
}

auto DistanceTransform::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto DistanceTransform::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	NppStreamContext streamCtx;
	streamCtx.hStream = m_streams[deviceID];

	const size_t m_blockSize2D = 32;
	auto m_numThreads = m_blockSize2D * m_blockSize2D;
	
	// div by 2 because each block reads data from an area twice its size
	auto m_numBlocks = GET_MAX((m_imageWidth * m_imageHeight + m_numThreads - 1) / m_numThreads / 2, 1);
	auto m_minMaxVals_h =
		std::vector<float>(risa::cuda::detail::ReduceProperty::getReducePropertyArraySize(m_numBlocks));
	auto m_minMaxVals_d = glados::cuda::make_device_ptr<float>(m_minMaxVals_h.size());

	auto m_sharedMemBytes = risa::cuda::detail::ReduceProperty::getSharedMemSize(m_blockSize2D);

	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid(
		(int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	auto m_offsBuffer = glados::cuda::make_device_ptr<float>(m_imageWidth * m_imageHeight);

	initDistanceTransformForUniqueVals<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
		m_offsBuffer.get(), m_imageWidth, m_imageHeight);

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform: Running thread for device " << deviceID;
	while(true)
	{
		if(m_doConfig)
		{
			m_doConfig = false;
		}

		// take next image from input 0
		auto image = m_images[deviceID][0].take();

		if(!image.valid())
		{
			break;
		}

		auto result =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

		// ensure that the image is not empty, else disttransform will crash!
		reduceKernel<1024, detail::ReduceProperty::Maximum>
			<<<m_numBlocks, m_numThreads, m_sharedMemBytes, m_streams[deviceID]>>>(
				image.data(), m_imageHeight * m_imageWidth, m_minMaxVals_d.get());

		CHECK(cudaPeekAtLastError());
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		cudaMemcpyAsync(m_minMaxVals_h.data(), m_minMaxVals_d.get(), m_minMaxVals_h.size() * sizeof(float),
			cudaMemcpyDeviceToHost, m_streams[deviceID]);
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
		float imageMaxVal = std::numeric_limits<float>::lowest();
		for(auto i = 0; i < m_numBlocks; i++)
		{
			const auto maxIdx = detail::ReduceProperty::getNthMaxIdx(m_numBlocks, i);
			imageMaxVal = GET_MAX(imageMaxVal, m_minMaxVals_h[maxIdx]);
		}

		if(imageMaxVal > 0.0f)
		{
			NppStatus status = nppiSignedDistanceTransformPBA_32f_C1R_Ctx(image.data(),
				m_imageWidth * sizeof(float), 0.0f, 0.0f, 0.0f, nullptr, 0, nullptr, 0, nullptr, 0,
				result.data(), m_imageWidth * sizeof(float), NppiSize{m_imageWidth, m_imageHeight},
				m_scratchMemory.get(), streamCtx);
			if(m_applyUniqueOffset)
			{
				applyDistanceTransformForUniqueVals<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
					result.data(), m_offsBuffer.get(), m_imageWidth, m_imageHeight);
			}

		}
		else
		{
			cudaMemcpyAsync(result.data(), image.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);
		}

		CHECK(cudaPeekAtLastError());

		result.setDevice(deviceID);
		result.setProperties(image.properties());
		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(int i = 1; i < m_numberOfOutputs; i++)
		{
			auto imageSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
				m_memoryPoolIdxs[deviceID]);

			cudaMemcpyAsync(imageSplit.data(), result.data(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			imageSplit.setDevice(deviceID);
			imageSplit.setProperties(result.properties());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform:       (Device " << deviceID
									 << " | Output " << i << "): Image " << imageSplit.index()
									 << " processed.";
			m_results[i].push(std::move(imageSplit));
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DistanceTransform:       (Device " << deviceID
								 << " | Output " << 0 << "): Image " << result.index() << " processed.";
		m_results[0].push(std::move(result));
	}
}

auto DistanceTransform::update(glados::Subject*) -> void
{
	; // nothing to update
}

auto DistanceTransform::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	std::string loadSourcePathMode;
	ConfigReader configReader = ConfigReader(configFile.data());
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize))
	{
		if(m_imageWidth < 64 || m_imageWidth > 32767)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::DistanceTransform: Supported image width is 64...32767px, image is "
				<< m_imageWidth << "px wide.";
			return false;
		}

		if(m_imageHeight < 64 || m_imageHeight > 32767)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::DistanceTransform: Supported image height is 64...32767px, image is "
				<< m_imageHeight << "px high.";
			return false;
		}
		return true;

		configReader.setAllowedToFail(true);
		configReader.lookupValue(parameterSet + ".applyUniqueOffset", m_applyUniqueOffset); // not mandatory, defaults to false
	}

	return false;
}

__global__ void initDistanceTransformForUniqueVals(float* offs, size_t width, size_t height)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= width || y >= height)
		return;

	const float totalOffs = 0.4f; // offset at most by less than the minimum distance between two adjacent
								  // pixels (which is sqrt(2)/2 - 0.5)
	const auto idx = y * width + x;

	offs[idx] = -0.5f * totalOffs + static_cast<float>(idx) / static_cast<float>(width * height) * totalOffs;
}

__global__ void applyDistanceTransformForUniqueVals(float* in, const float* offs, size_t width, size_t height)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= width || y >= height)
		return;

	const auto idx = y * width + x;

	in[idx] += offs[idx];
}


} // namespace cuda
} // namespace risa
