/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 16. April 2024
 * Authors: Dominic Windisch <d.windisch@hzdr.de>
 * Date: 23. April 2024
 * Co-Authors: Stephan Boden <s.boden@hzdr.de
 */

#ifndef CUDA_KERNELS_TRANSFORM2D_H_
#define CUDA_KERNELS_TRANSFORM2D_H_

#include <risaCore/Transform/Transform2D.h>

#include <glados/cuda/Coordinates.h>

#define _USE_MATH_DEFINES
#include <cmath>
#include <stdio.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433
#endif
#ifndef M_PI_2
#define M_PI_2 1.57079632679489661923
#endif

namespace risa
{
namespace cuda
{

__global__ void createLookupTables(
	const int imageWidth, 
	const int imageHeight,
	const float imageRoll_deg,
	const float imageTilt_deg,
	const float imageYaw_deg,
	int* __restrict__ indices, 
	float* __restrict__ weights /* add/remove parameters as needed */)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	// for each output pixel, calculate the 4 neighbour pixels involved and their respective weight
	const int numberOfNeighbours = 4; // 2: nearest neighbour, 4: linear interp
	const auto weightsIndex = (y * imageWidth + x) * numberOfNeighbours;
	const auto indicesIndex = (y * imageWidth + x) * numberOfNeighbours * 2;

	// do the magic
	
	bool neighIsValid[4]; // some of the pixels may be outside the input image range, set them to false

	// setup output
	for(auto neighIdx = 0; neighIdx < 4; neighIdx++)
	{
		if(neighIsValid[neighIdx])
		{
			weights[weightsIndex + neighIdx] = 0.5f; // some weight
			indices[indicesIndex + 2 * neighIdx] = 17; // calc'd input index x
			indices[indicesIndex + 2 * neighIdx + 1] = 53; // calc'd input index y
		}
		else
		{
			weights[weightsIndex + neighIdx] = 0.0f;
			indices[indicesIndex + 2 * neighIdx] = 0;
			indices[indicesIndex + 2 * neighIdx + 1] = 0;
		}
	}
}

__global__ void transform2DKernel(
	const float* __restrict__ inputImage, 
	float* __restrict__ result,
	const int imageWidth, 
	const int imageHeight,
	const int* __restrict__ indices, 
	const float* __restrict__ weights)
{

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	auto numberOfNeighbours = 4; // 2: nearest neighbour, 4: linear interp
	auto baseIndex = imageWidth * y + x;
	auto weightsIndex = baseIndex * numberOfNeighbours;
	auto indicesIndex = baseIndex * numberOfNeighbours * 2;

	// TODO rework everything towards one dimensional indices

	const int neigh0X = indices[indicesIndex];
	const int neigh0Y = indices[indicesIndex + 1];
	float neigh0Weight = weights[weightsIndex];

	int neigh1X = indices[indicesIndex + 2];
	int neigh1Y = indices[indicesIndex + 3];
	float neigh1Weight = weights[weightsIndex + 1];

	int neigh2X = indices[indicesIndex + 4];
	int neigh2Y = indices[indicesIndex + 5];
	float neigh2Weight = weights[weightsIndex + 2];

	int neigh3X = indices[indicesIndex + 6];
	int neigh3Y = indices[indicesIndex + 7];
	float neigh3Weight = weights[weightsIndex + 3];

	result[baseIndex] = inputImage[neigh0Y * imageWidth + neigh0X] * neigh0Weight
		+ inputImage[neigh1Y * imageWidth + neigh1X] * neigh1Weight
		+ inputImage[neigh2Y * imageWidth + neigh2X] * neigh2Weight
		+ inputImage[neigh3Y * imageWidth + neigh3X] * neigh3Weight;
}

}
}

#endif // CUDA_KERNELS_TRANSFORM2D_H_
