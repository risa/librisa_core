// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Transform/FFT2D.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

FFT2D::FFT2D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(numberOfOutputs != 2)
	{
		BOOST_LOG_TRIVIAL(error) << "risa::cuda::FFT2D: FFT2D expects 2 output streams. Got: "
								 << numberOfOutputs;
		throw std::runtime_error("risa::cuda::FFT2D: Wrong number of output streams.");
	}

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::FFT2D: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// cuFFT library is initialized for each device
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, (m_imageWidth / 2 + 1) * m_imageHeight));
		initCuFFT(i);
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&FFT2D::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D: Running " << m_numberOfDevices << " thread(s).";
}

FFT2D::~FFT2D()
{
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
		CHECK_CUFFT(cufftDestroy(m_plansFwd[i]));
	}
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D: Destroyed.";
}

auto FFT2D::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D:       (Device " << image.device() << " |  Input "
								 << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D: Finished.";
	}
}

auto FFT2D::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto FFT2D::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	const int outputWidth = m_imageWidth / 2 + 1;
	auto sinoFreq = glados::cuda::make_device_ptr<cufftComplex, glados::cuda::async_copy_policy>(
		outputWidth * m_imageHeight);
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid((int)ceil(outputWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));
	BOOST_LOG_TRIVIAL(debug) << "recoLib::cuda::FFT2D: Running thread for device " << deviceID;

	while(true)
	{
		auto image = m_images[deviceID][0].take();
		if(!image.valid())
			break;

		// forward transformation
		CHECK_CUFFT(cufftExecR2C(m_plansFwd[deviceID], (cufftReal*)image.data(), sinoFreq.get()));

		auto dataReal =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);
		auto dataImag =
			glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(m_memoryPoolIdxs[deviceID]);

		// extract float streams
		extractStreamsFromCufftComplex<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
			outputWidth, m_imageHeight, sinoFreq.get(), dataReal.data(), dataImag.data());

		CHECK(cudaPeekAtLastError());

		dataReal.setDevice(deviceID);
		dataImag.setDevice(deviceID);
		dataReal.setProperties(image.properties());
		dataImag.setProperties(image.properties());

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
		
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FFT2D:       (Device " << deviceID << " | Output " << 0
								 << "): Image " << image.index() << " processed.";

		m_results[0].push(std::move(dataReal));
		m_results[1].push(std::move(dataImag));
	}
}

auto FFT2D::initCuFFT(const int deviceID) -> void
{

	CHECK(cudaSetDevice(deviceID));

	cudaStream_t stream;
	cufftHandle planFwd;

	CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
	m_streams[deviceID] = stream;

	CHECK_CUFFT(cufftPlan2d(&planFwd, m_imageHeight, m_imageWidth, CUFFT_R2C));

	CHECK_CUFFT(cufftSetStream(planFwd, stream));

	m_plansFwd[deviceID] = planFwd;
}

auto FFT2D::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D))
	{
		return true;
	}
	return false;
}

__global__ void extractStreamsFromCufftComplex(const int imageWidth, const int imageHeight,
	const cufftComplex* data, float* __restrict__ dataReal, float* __restrict__ dataImag)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	const auto idx = y * imageWidth + x;

	// scale output for more sensible processing inbetween stages
	const float fact = 1.0f / (imageWidth * imageHeight);

	if(idx < imageWidth * imageHeight)
	{
		dataReal[idx] = (float)data[idx].x * fact;
		dataImag[idx] = (float)data[idx].y * fact;
	}
}

}
}
