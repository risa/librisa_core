/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 30 November 2016
 * Authors: Tobias Frust (FWCC) <t.frust@hzdr.de>
 * Date: 15. February 2021
 * Co-Authors: Dominic Windisch <d.windisch@hzdr.de>, Andr� Bieberle <a.bieberle@hzdr.de>
 *
 */

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Loader/ImageSequenceLoader.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <chrono>
#include <exception>
#include <filesystem>
#include <glados/MemoryPool.h>
#include <set>

namespace risa
{

namespace cuda
{

ImageSequenceLoader::ImageSequenceLoader(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::ImageSequenceLoader: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	if(m_deviceId >= m_numberOfDevices)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Requested device id " << m_deviceId
								 << " is out of range. " << m_numberOfDevices - 1
								 << " is the last available device.";
		throw std::runtime_error("risa::cuda::ImageSequenceLoader: Requested device id is out of range.");
	}
	CHECK(cudaSetDevice(m_deviceId));
	CHECK(cudaStreamCreateWithPriority(&m_stream, cudaStreamNonBlocking, 5));

	switch(m_stackLoadMode)
	{
	case detail::StackLoadMode::BufferStack:
	{
		if(!loadImageStack())
		{
			throw std::runtime_error(
				"risa::cuda::ImageSequenceLoader: Unable to load benchmark data set with given parameters.");
		}

		m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);

		if(!copyImageStacktoDeviceBuffer(m_deviceId))
		{
			throw std::runtime_error(
				"risa::cuda::BenchmarkLodaer: Unable to copy benchmark dataset into device memory.");
		}
		break;
	}
	case detail::StackLoadMode::SingleFrame:
	{
		if(!validateSettings())
		{
			throw std::runtime_error(
				"risa::cuda::ImageSequenceLoader: Error in settings for SingleFrame load mode.");
		}
		m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight);
		break;
	}
	}

	m_totalImages = (m_lastIdx - m_firstIdx + 1);

	BOOST_LOG_TRIVIAL(info) << "risa::cuda::ImageSequenceLoader<" << m_parameterSet
							<< ">: Initialization done.";
}

ImageSequenceLoader::~ImageSequenceLoader()
{
	glados::MemoryPool<manager_type>::getInstance().freeMemory(m_memoryPoolIndex);

	CHECK(cudaSetDevice(m_deviceId));
	CHECK(cudaStreamDestroy(m_stream));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader<" << m_parameterSet << ">: Destroyed.";
}

auto ImageSequenceLoader::loadImage() -> glados::Image<manager_type>
{
	CHECK(cudaSetDevice(m_deviceId));
	// break conditions:
	if(m_triggerStop)
	{
		BOOST_LOG_TRIVIAL(info) << "##############################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::ImageSequenceLoader:: Received stop trigger. ##";
		BOOST_LOG_TRIVIAL(info) << "##############################################################";
		return glados::Image<manager_type>();
	}
	else if(m_stopMode == detail::loadControlMode::automatic && m_index >= m_stopFrame)
	{
		return glados::Image<manager_type>();
	}

	auto sino = glados::MemoryPool<manager_type>::getInstance().requestMemory(m_memoryPoolIndex);

	if(m_startMode == detail::loadControlMode::triggered)
	{
		// wait for start trigger
		BOOST_LOG_TRIVIAL(info) << "####################################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::ImageSequenceLoader:: Waiting for start trigger... ##";
		BOOST_LOG_TRIVIAL(info) << "####################################################################";

		std::unique_lock<std::mutex> lck(m_mutex);
		m_condVar.wait(lck, [&] { return m_triggerStart; });

		BOOST_LOG_TRIVIAL(info) << "risa::cuda::ImageSequenceLoader:: Received start trigger.";
		m_startMode = detail::loadControlMode::automatic;
	}

	switch(m_stackLoadMode)
	{

	case detail::StackLoadMode::BufferStack:
	{
		// copy from stack buffer into image buffer
		const auto offset = m_imageWidth * m_imageHeight * (m_index % m_rawImages.size());
		CHECK(cudaMemcpyAsync(sino.data(), &imagesBuffer_d.get()[offset],
			sizeof(float) * m_imageWidth * m_imageHeight, cudaMemcpyDeviceToDevice, m_stream));
		break;
	}
	case detail::StackLoadMode::SingleFrame:
	{
		// read from file into image buffer
		std::vector<unsigned char> rawImage_h;
		if(loadSliceFromFile(rawImage_h, (m_firstIdx + m_index % m_totalImages)))
		{
			if(m_needsToBeConverted)
			{
				CHECK(cudaMemcpyAsync(rawImageBuffer_d.get(), rawImage_h.data(), rawImageBuffer_d.size(),
					cudaMemcpyHostToDevice, m_stream));
				convertImageToFloat<<<m_grids, m_blocks, 0, m_stream>>>(rawImageBuffer_d.get(), sino.data(),
					m_imageWidth, m_imageHeight, m_dataChannels, m_activeChannels,
					m_fileFormat == detail::FileFormat::PNG);
				CHECK(cudaPeekAtLastError());
			}
			else
			{
				CHECK(cudaMemcpyAsync(sino.data(), rawImage_h.data(), rawImageBuffer_d.size(),
					cudaMemcpyHostToDevice, m_stream));
			}
		}
		else
		{
			return glados::Image<manager_type>();
		}
		break;
	}
	}

	if(m_index == 0 && m_stopMode == detail::loadControlMode::triggered)
	{
		BOOST_LOG_TRIVIAL(info) << "###################################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::ImageSequenceLoader:: Waiting for stop trigger... ##";
		BOOST_LOG_TRIVIAL(info) << "###################################################################";
	}

	if(m_index > 0 && sendInterval_us != 0)
	{
		auto nextSinoSendTime = m_lastSinoSendTime + std::chrono::microseconds(sendInterval_us);
		std::this_thread::sleep_until(nextSinoSendTime);
	}

	sino.setDevice(m_deviceId);
	sino.setIdx(m_index);
	sino.setPlane(m_index % m_numberOfPlanes);
	m_index++;
	sino.setStart(m_clock::now());
	CHECK(cudaStreamSynchronize(m_stream));
	m_lastSinoSendTime = m_clock::now();
	return sino;
}

auto ImageSequenceLoader::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader:  ###### UPDATE ###### -> " << fo->m_fileName
							 << ": " << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader:  ###### CONTENT ##### -> "
							 << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			hasValidSetting =
				configReader.lookupValue("frameRateLimit", m_targetFrameRate) || hasValidSetting;

			bool tmp = false;
			if(m_startMode == detail::loadControlMode::triggered
				&& configReader.lookupValue("triggerStart", tmp))
			{
				if(tmp)
				{
					{
						std::lock_guard<std::mutex> lck(m_mutex);
						m_triggerStart = true;
					}
					m_condVar.notify_one();
				}
				// return early because a start and a stop trigger should not happen in the same update. After
				// starting by trigger, m_startMode will be set to automatic so the start trigger won't be
				// checked again.
				return;
			}

			if(configReader.lookupValue("triggerStop", tmp))
			{
				std::lock_guard<std::mutex> lck(m_mutex);
				m_triggerStop = tmp;
				return;
			}

			if(hasValidSetting)
			{
				std::lock_guard<std::mutex> lck(m_mutex);
				sendInterval_us = m_targetFrameRate > 0 ? 1000000 / m_targetFrameRate : 0;
			}
		}
	}
}

auto ImageSequenceLoader::loadSliceFromFile(std::vector<unsigned char>& rawImage, size_t idx) -> bool
{
	// load a slice from the configured file path and the given index
	// in the process, validate that the size matches the previously configured one (if any)

	unsigned int width, height;
	std::string fullPath;

	auto idxStr = std::to_string(idx);
	auto paddedIdxStr =
		std::string(m_idxTotalDigits - std::min(m_idxTotalDigits, idxStr.length()), '0') + idxStr;

	switch(m_fileFormat)
	{
	case detail::FileFormat::PNG:
	{
		if(m_loadAllInDirectory)
		{
			fullPath = m_fileNames[idx - m_firstIdx];
		}
		else
		{
			fullPath = m_filePath + m_fileBaseName + paddedIdxStr + m_fileEnding;
		}

		std::vector<unsigned char> imageBuffer;
		lodepng::load_file(imageBuffer, fullPath);
		lodepng::State state;
		unsigned int error = lodepng::decode(rawImage, width, height, state, imageBuffer);
		if(error)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Error " << error
									 << " when trying to read '" << fullPath
									 << "': " << lodepng_error_text(error);
			return false;
		}
		m_activeChannels = lodepng_get_channels(&(state.info_png.color));
		break;
	}
	case detail::FileFormat::JPEG:
	{
		if(m_loadAllInDirectory)
		{
			fullPath = m_fileNames[idx];
		}
		else
		{
			fullPath = m_filePath + m_fileBaseName + paddedIdxStr + m_fileEnding;
		}

		/* read into file */
		FILE* f = fopen(fullPath.c_str(), "rb");
		if(!f)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::ImageSequenceLoader: File '" << fullPath
									 << "' not found.";

			return false;
		}
		fseek(f, 0, SEEK_END);
		size_t size = ftell(f);
		unsigned char* buf = (unsigned char*)malloc(size);
		fseek(f, 0, SEEK_SET);
		size_t read = fread(buf, 1, size, f);
		fclose(f);

		Jpeg::Decoder decoder(buf, size);
		if(decoder.GetResult() != Jpeg::Decoder::OK)
		{
			static const std::vector<std::string> error_msgs = {"OK", "NotAJpeg", "Unsupported",
				"OutOfMemory", "InternalError", "SyntaxError", "Internal_Finished"};
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::ImageSequenceLoader: Error decoding the input file "
									 << error_msgs[decoder.GetResult()];
			return false;
		}
		if(decoder.IsColor())
		{
			m_dataChannels = 3;
			m_activeChannels = 3;
		}
		else
		{
			m_dataChannels = 1;
			m_activeChannels = 1;
		}
		width = decoder.GetWidth();
		height = decoder.GetHeight();
		rawImage.resize(width * height * m_dataChannels);
		std::memcpy(rawImage.data(), decoder.GetImage(), rawImage.size());
		break;
	}
	case detail::FileFormat::HIS:
	{
		if(m_loadAllInDirectory)
		{
			fullPath = m_fileNames[idx];
		}
		else
		{
			fullPath = m_filePath + m_fileBaseName + paddedIdxStr + m_fileEnding;
		}

		his::HISHeader header{};

		auto&& fstream = std::ifstream{fullPath.c_str(), std::ios_base::binary};
		if(fstream.fail())
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoder:: Failed to open file at "
									 << fullPath;
			return false;
		}

		// set width and height from header
		if(!his::parseHeader(fstream, header))
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoder:: HIS header failed to validate";
			return false;
		}
		width = static_cast<unsigned int>(header.brx - header.ulx + 1u);
		height = static_cast<unsigned int>(header.bry - header.uly + 1u);

		// copy images bytes into rawImage
		// fstream points to the end of the header, i.e. the start of the image data
		using num_type = decltype(header.numberType);
		switch(header.numberType)
		{
		case static_cast<num_type>(his::dataType::uint8_t):
		{
			m_dataChannels = 1;
			break;
		}
		case static_cast<num_type>(his::dataType::uint16_t):
		{
			m_dataChannels = 2;
			break;
		}
		case static_cast<num_type>(his::dataType::uint32_t):
		{
			m_dataChannels = 4;
			break;
		}
		case static_cast<num_type>(his::dataType::float_t):
		{
			m_needsToBeConverted = false;
			m_dataChannels = 4;
			break;
		}
		case static_cast<num_type>(his::dataType::double_t):
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Converting HIS file with double "
										"data not implemented yet.";
			return false;
		}
		case static_cast<num_type>(his::dataType::notImplemented):
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::ImageSequenceLoader: Cannot handle HIS file with unsupported data type.";
			return false;
		}

		rawImage.resize(m_imageWidth * m_imageHeight * m_dataChannels);
		fstream.seekg(100, fstream.beg);
		fstream.read(reinterpret_cast<char*>(rawImage.data()), rawImage.size());
		break;
	}
	case detail::FileFormat::TIFF:
	{
		break;
	}
	case detail::FileFormat::BMP:
	{
		break;
	}
	}

	if(m_imageWidth == 0 && m_imageHeight == 0)
	{
		m_imageWidth = width;
		m_imageHeight = height;
		BOOST_LOG_TRIVIAL(warning)
			<< "risa::cuda::ImageSequenceLoader: Setting image size based on the first image to "
			<< m_imageWidth << "x" << m_imageHeight << " px.";
	}
	else if((m_imageWidth != width) || (m_imageHeight != height))
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::ImageSequenceLoader:: Changing image sizes are not supported. "
			   "Previous images where of size "
			<< m_imageWidth << "x" << m_imageHeight << "px, but " << fullPath << " is of size " << width
			<< "x" << height << "px.";
		return false;
	}
	return true;
}

auto ImageSequenceLoader::validateSettings() -> bool
{
	if(m_firstIdx > m_lastIdx)
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::ImageSequenceLoader: lastIdx must not be less than firstIdx.";
		return false;
	}
	std::vector<unsigned char> tmp;
	if(!loadSliceFromFile(
		   tmp, m_firstIdx)) // this will try to load the first image and check its size in the process
	{
		return false;
	}
	rawImageBuffer_d = glados::cuda::make_device_ptr<unsigned char, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * m_dataChannels);
	m_blocks = dim3(16, 16);
	m_grids = dim3(std::ceil(m_imageWidth / 16.0), std::ceil(m_imageHeight / 16.0));

	return true;
}

auto ImageSequenceLoader::loadImageStack() -> bool
{
	if(!validateSettings())
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Error in settings.";
		return false;
	}

	BOOST_LOG_TRIVIAL(info) << "risa:cuda::ImageSequenceLoader: Buffering " << m_lastIdx - m_firstIdx + 1
							<< " images...";
	// open image file(s)
	for(auto idx = m_firstIdx; idx <= m_lastIdx; idx++)
	{
		std::vector<unsigned char> rawImage;
		if(!loadSliceFromFile(rawImage, idx))
		{
			return false;
		}
		m_rawImages.push_back(rawImage);
	}

	return true;
}

auto ImageSequenceLoader::copyImageStacktoDeviceBuffer(int deviceId) -> bool
{
	CHECK(cudaSetDevice(deviceId));
	// allocate
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader: Allocate imagesBuffer in device memory ("
							 << m_imageWidth * m_imageHeight * m_dataChannels * m_rawImages.size() / 1000000
							 << "MB).";
	imagesBuffer_d = glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * m_rawImages.size());

	// copy unsigend_char buffers on device
	for(auto index = 0; index < m_rawImages.size(); index++)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader: Copy image " << index
								 << " to device memory.";
		if(m_needsToBeConverted)
		{
			CHECK(cudaMemcpyAsync(rawImageBuffer_d.get(), m_rawImages[index].data(),
				sizeof(unsigned char) * m_rawImages[index].size(), cudaMemcpyHostToDevice, m_stream));

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::ImageSequenceLoader: Convert image " << index << ".";
			convertImageToFloat<<<m_grids, m_blocks, 0, m_stream>>>(rawImageBuffer_d.get(),
				&(imagesBuffer_d.get()[index * m_imageWidth * m_imageHeight]), m_imageWidth, m_imageHeight,
				m_dataChannels, m_activeChannels, m_fileFormat == detail::FileFormat::PNG);
			CHECK(cudaPeekAtLastError());
		}
		else
		{
			CHECK(cudaMemcpyAsync(&(imagesBuffer_d.get()[index * m_imageWidth * m_imageHeight]),
				m_rawImages[index].data(), m_rawImages[index].size(), cudaMemcpyHostToDevice, m_stream));
		}
		CHECK(cudaPeekAtLastError());
	}

	return true;
}

auto ImageSequenceLoader::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string fileFormat;
	if(configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".frameRateLimit", m_targetFrameRate)
		&& configReader.lookupValue(parameterSet + ".filePath", m_filePath)
		&& configReader.lookupValue(parameterSet + ".fileBaseName", m_fileBaseName)
		&& configReader.lookupValue(parameterSet + ".firstIdx", m_firstIdx)
		&& configReader.lookupValue(parameterSet + ".lastIdx", m_lastIdx)
		&& configReader.lookupValue(parameterSet + ".loadAllInDirectory", m_loadAllInDirectory)
		&& configReader.lookupValue(parameterSet + ".fileFormat", m_fileEnding))
	{
		if(m_filePath.back() != '/')
			m_filePath.push_back('/');

		sendInterval_us = m_targetFrameRate > 0 ? 1000000 / m_targetFrameRate : 0;

		if(m_fileEnding.empty())
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::ImageSequenceLoader: Invalid empty file format";
			return false;
		}

		if(m_fileEnding[0] != '.')
		{
			m_fileEnding = "." + m_fileEnding;
		}

		if(m_fileEnding == ".PNG" || m_fileEnding == ".png")
		{
			m_fileFormat = detail::FileFormat::PNG;
			m_dataChannels = 4; // lodepng always decodes into RGBA format, i.e. 4 bytes per pixel
		}
		else if(m_fileEnding == ".JPG" || m_fileEnding == ".jpg" || m_fileEnding == ".JPEG"
			|| m_fileEnding == ".jpeg")
		{
			m_fileFormat = detail::FileFormat::JPEG;
		}
		else if(m_fileEnding == ".his" || m_fileEnding == ".HIS")
		{
			m_fileFormat = detail::FileFormat::HIS;
		}
		else if(m_fileEnding == ".BMP" || m_fileEnding == ".bmp")
		{
			m_fileFormat = detail::FileFormat::BMP;
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::ImageSequenceLoader: BMP is not yet implemened";
			return false;
		}
		else if(m_fileEnding == ".TIF" || m_fileEnding == ".tif" || m_fileEnding == ".tiff"
			|| m_fileEnding == ".TIFF")
		{
			m_fileFormat = detail::FileFormat::TIFF;
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::ImageSequenceLoader: TIFF is not yet implemened";
			return false;
		}
		else
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::ImageSequenceLoader: Unknown file format '"
									 << m_fileEnding << "'.";
			return false;
		}

		std::string s;
		if(!configReader.lookupValue(parameterSet + ".deviceId", m_deviceId))
		{
			m_deviceId = 0;
		}

		if(!configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth))
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "risa::cuda::ImageSequenceLoader: No image width provided, using the "
				   "width of the first image.";
			m_imageWidth = 0;
		}

		if(!configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight))
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "risa::cuda::ImageSequenceLoader: No image height provided, using the "
				   "height of the first image.";
			m_imageHeight = 0;
		}

		if(configReader.lookupValue(parameterSet + ".startMode", s))
		{
			if(s == "triggered")
			{
				m_startMode = detail::loadControlMode::triggered;
				m_triggerStart = false;
			}
			else if(s == "automatic")
			{
				m_startMode = detail::loadControlMode::automatic;
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::ImageSequenceLoader: Start mode '" << s << "' not supported";
				return false;
			}
		}
		else
		{
			return false;
		}

		if(configReader.lookupValue(parameterSet + ".stopMode", s))
		{
			if(s == "triggered")
			{
				m_stopMode = detail::loadControlMode::triggered;
				m_triggerStop = false;
			}
			else if(s == "automatic")
			{
				m_stopMode = detail::loadControlMode::automatic;
				configReader.lookupValue(parameterSet + ".numberOfFramesToSend", m_stopFrame);
				if(m_stopFrame == 0)
				{
					m_stopFrame = m_lastIdx - m_firstIdx + 1;
				}
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::ImageSequenceLoader: Stop mode '" << s << "' not supported";
				return false;
			}
		}
		else
		{
			return false;
		}

		if(!configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
		{
			m_numberOfPlanes = 1;
		}

		if(configReader.lookupValue(parameterSet + ".stackLoadMode", s))
		{
			if(s == "SingleFrame")
			{
				m_stackLoadMode = detail::StackLoadMode::SingleFrame;
			}
			else if(s == "BufferStack")
			{
				m_stackLoadMode = detail::StackLoadMode::BufferStack;
			}
			else
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Stack load mode '" << s
										 << "' not supported. Use 'BufferStack' or 'SingleFrame'";
				return false;
			}
		}
		else
		{
			return false;
		}

		if(!m_loadAllInDirectory)
		{
			if(!configReader.lookupValue(parameterSet + ".idxTotalDigits", m_idxTotalDigits))
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::ImageSequenceLoader: Configured not to load all "
											"files but idxTotalDigits not provided.";
				return false;
			}
		}
		else
		{
			// scan directory for fitting files
			std::set<std::string> fileNamesBuf;
			for(const auto& entry : std::filesystem::directory_iterator(m_filePath))
			{
				std::filesystem::path fname = entry.path();
				if(fname.extension().string().compare(m_fileEnding) == 0)
				{
					if(fname.filename().string().rfind(m_fileBaseName, 0) == 0)
					{
						// found a file of type m_fileEnding that starts with m_fileBaseName
						fileNamesBuf.insert(fname.string());
					}
				}
			}
			for(auto& fname : fileNamesBuf)
			{
				m_fileNames.push_back(fname);
			}
			BOOST_LOG_TRIVIAL(info) << "risa::cuda::ImageSequenceLoader: Found " << m_fileNames.size()
									<< " fitting files.";
		}

		return true;
	}

	return false;
}

__global__ void convertImageToFloat(unsigned char* __restrict__ rawImage, float* __restrict__ floatImage,
	const unsigned int imageWidth, const unsigned int imageHeight, const long dataChannels,
	const long activeChannels, bool isPNG)
{
	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;
	float pxVal = 0.0f;
	size_t pxIntVal = 0;
	if(!isPNG)
	{
		for(auto byteIdx = 0; byteIdx < dataChannels; byteIdx++)
		{
			pxIntVal += (rawImage[dataChannels * idx + byteIdx] << (8 * byteIdx));
		}
	}
	else
	{
		// PNG from lodepng is always RGBA, ignore A for now
		for(auto byteIdx = 0; byteIdx < activeChannels; byteIdx++)
		{
			pxIntVal += rawImage[dataChannels * idx + byteIdx];
		}
	}

	for(auto channel = 0; channel < activeChannels; channel++)
	{
		pxVal += (float)pxIntVal;
	}
	floatImage[idx] = pxVal / (float)activeChannels; // convert to grayscale
}
}
}
