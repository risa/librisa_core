/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 30 November 2016
 * Authors: Tobias Frust (FWCC) <t.frust@hzdr.de>
 * Date: 15. February 2021
 * Co-Authors: Dominic Windisch <d.windisch@hzdr.de>, Andr� Bieberle <a.bieberle@hzdr.de>
 *
 */

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Loader/HDF5Loader.h>

#include <risaCore/Basics/enums.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <chrono>
#include <exception>
#include <glados/MemoryPool.h>

#ifdef __linux__
#define PATH_SEP '/'
#elif _WIN32
#define PATH_SEP '\\'
#endif

namespace risa
{

namespace cuda
{

HDF5Loader::HDF5Loader(const std::string& configFile, const std::string& parameterSet)
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::HDF5Loader: Configuration file could not be loaded successfully.");
	}
	m_parameterSet = parameterSet;

	if(!initializeHDF5())
	{
		throw std::runtime_error(
			"risa::cuda::HDF5Loader: HDF5 configuration could not be initialized successfully.");
	}

	allocateBuffer();

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	if(m_deviceId >= m_numberOfDevices)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader: Requested device id " << m_deviceId
								 << " is out of range. " << m_numberOfDevices - 1
								 << " is the last available device.";
		throw std::runtime_error("risa::cuda::HDF5Loader: Requested device id is out of range.");
	}

	CHECK(cudaSetDevice(m_deviceId));

	m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
		m_memPoolSize, m_imageWidth * m_imageHeight);
	m_index = 0;

	CHECK(cudaStreamCreateWithPriority(&m_stream, cudaStreamNonBlocking, 5));

	BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Determined size of "
							<< m_imageWidth << "x" << m_imageHeight << "x" << m_imageCount;
	BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Initialization done.";
}

HDF5Loader::~HDF5Loader()
{

	// close all possibly still open hdf5 handles
	if(m_type_data != H5I_INVALID_HID)
		H5Tclose(m_type_data);
	for(const auto t : m_type_meta)
	{
		H5Tclose(t);
	}

	if(m_ds_data != H5I_INVALID_HID)
	{
		H5Dclose(m_ds_data);
	}
	for(const auto d : m_ds_meta)
	{
		H5Dclose(d);
	}

	if(m_msp_data != H5I_INVALID_HID)
	{
		H5Sclose(m_msp_data);
	}
	for(const auto s : m_msp_meta)
	{
		H5Sclose(s);
	}

	if(m_sp_data != H5I_INVALID_HID)
	{
		H5Sclose(m_sp_data);
	}
	for(const auto s : m_sp_meta)
	{
		H5Sclose(s);
	}

	if(m_HDF5File != H5I_INVALID_HID)
	{
		H5Fclose(m_HDF5File);
	}

	free(m_dataBuffer);
	for(const auto b : m_meta_bufs)
	{
		free(b);
	}

	glados::MemoryPool<manager_type>::getInstance().freeMemory(m_memoryPoolIndex);

	CHECK(cudaSetDevice(m_deviceId));
	CHECK(cudaStreamDestroy(m_stream));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Destroyed.";
}

auto HDF5Loader::loadImage() -> glados::Image<manager_type>
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: loadImage()";
	CHECK(cudaSetDevice(m_deviceId));
	// break conditions:
	if(m_triggerStop)
	{
		BOOST_LOG_TRIVIAL(info) << "#####################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::HDF5Loader:: Received stop trigger. ##";
		BOOST_LOG_TRIVIAL(info) << "#####################################################";
		return glados::Image<manager_type>();
	}
	else if(m_stopMode == detail::loadControlMode::automatic && m_index >= m_stopFrame)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: automatic stop";
		return glados::Image<manager_type>();
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: request memory";
	auto sino = glados::MemoryPool<manager_type>::getInstance().requestMemory(m_memoryPoolIndex);

	if(m_startMode == detail::loadControlMode::triggered)
	{
		// wait for start trigger
		BOOST_LOG_TRIVIAL(info) << "###########################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::HDF5Loader:: Waiting for start trigger... ##";
		BOOST_LOG_TRIVIAL(info) << "###########################################################";

		std::unique_lock<std::mutex> lck(m_mutex);
		m_condVar.wait(lck, [&] { return m_triggerStart; });

		BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader:: Received start trigger.";
		m_startMode = detail::loadControlMode::automatic;
	}

	// load from hdf5
	// => load data
	if(m_index == 0)
	{
		BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader: Initializing memory...(this may take a while)";
	}
	m_offset_data[detail::H5DIM::SLICES] = (m_firstIdx + m_index) % m_imageCount;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: Preparing to read "
							 << m_count_data[detail::H5DIM::WIDTH] << "x"
							 << m_count_data[detail::H5DIM::HEIGHT] << "x"
							 << m_count_data[detail::H5DIM::SLICES] << " elements starting from "
							 << m_offset_data[detail::H5DIM::WIDTH] << ","
							 << m_offset_data[detail::H5DIM::HEIGHT] << ","
							 << m_offset_data[detail::H5DIM::SLICES];
	if(H5Sselect_hyperslab(m_sp_data, H5S_SELECT_SET, m_offset_data, NULL, m_count_data, NULL) < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Error when setting up data space to read from '" << m_datasetName
								 << "' at index " << m_offset_data[detail::H5DIM::SLICES];
		throw std::runtime_error("risa::cuda::HDF5Loader: Error setting up data space for dataset.");
	}
	if(H5Sselect_hyperslab(m_msp_data, H5S_SELECT_SET, m_offset_msp, NULL, m_count_data, NULL) < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Error when setting up memory space to read from '" << m_datasetName
								 << "' at index " << m_offset_data[detail::H5DIM::SLICES];
		throw std::runtime_error("risa::cuda::HDF5Loader: Error setting up memory space for dataset.");
	}
	if(H5Dread(m_ds_data, m_type_data, m_msp_data, m_sp_data, H5P_DEFAULT, m_dataBuffer) < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Error when reading from '" << m_datasetName << "' at index "
								 << m_offset_data[detail::H5DIM::SLICES];
		throw std::runtime_error("risa::cuda::HDF5Loader: Error when reading data from dataset.");
	}

	CHECK(cudaMemcpyAsync(dataBuffer_d.get(), m_dataBuffer, m_elemBytes * m_imageWidth * m_imageHeight,
		cudaMemcpyHostToDevice, m_stream));

	// => load optional data
	m_offset_meta[0] = m_offset_data[detail::H5DIM::SLICES];
	if(m_metaNames.size() > 0 && m_index == 0)
	{
		BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader: Initializing meta data...(this may take a while)";
	}
	for(auto i = 0; i < m_metaNames.size(); i++)
	{
		H5Sselect_hyperslab(m_sp_meta[i], H5S_SELECT_SET, m_offset_meta, NULL, m_count_meta, NULL);
		if(H5Dread(m_ds_meta[i], m_type_meta[i], m_msp_meta[i], m_sp_meta[i], H5P_DEFAULT, m_meta_bufs[i])
			< 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Error when reading from '" << m_metaNames[i] << "' at index "
									 << m_offset_meta[0];
			throw std::runtime_error(
				"risa::cuda::HDF5Loader: Error when reading data from meta data dataset.");
		}

		setPropertyFromBufValue(&sino, m_metaNames[i], m_meta_bufs[i], m_native_type_meta[i]);

		if(m_native_type_meta[i] == detail::dataType::t_string)
		{
			free(m_meta_bufs[i]);
			m_meta_bufs[i] = nullptr;
		}
	}

	for(auto i = 0; i < m_attrNames.size(); i++)
	{
		setPropertyFromAny(&sino, m_attrNames[i], m_val_attr[i], m_native_type_attr[i]);
	}

	if(m_index == 0)
	{
		BOOST_LOG_TRIVIAL(info) << "risa::cuda::HDF5Loader: Initialization complete.";
	}

	if(m_index == 0 && m_stopMode == detail::loadControlMode::triggered)
	{
		BOOST_LOG_TRIVIAL(info) << "##########################################################";
		BOOST_LOG_TRIVIAL(info) << "## risa::cuda::HDF5Loader:: Waiting for stop trigger... ##";
		BOOST_LOG_TRIVIAL(info) << "##########################################################";
	}

	if(m_index > 0 && sendInterval_us != 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: waiting until next send time";
		auto nextSendTime = m_lastSendTime + std::chrono::microseconds(sendInterval_us);
		std::this_thread::sleep_until(nextSendTime);
	}

	sino.setDevice(m_deviceId);
	sino.setIdx(m_index);
	if(!sino.hasProperty("plane"))
	{
		sino.setPlane(m_index % m_numberOfPlanes);
	}
	sino.setStart(m_clock::now());
	m_lastSendTime = m_clock::now();

	switch(m_native_type_data)
	{
	case detail::dataType::t_bool:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(bool*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_int8:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(int8_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_int16:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(int16_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_int32:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(int32_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_int64:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(int64_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_uint8:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(uint8_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_uint16:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(uint16_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_uint32:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(uint32_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_uint64:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(uint64_t*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_float:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(float*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	case detail::dataType::t_double:
		convertArrayToFloat<<<grids, blocks, 0, m_stream>>>(
			(double*)dataBuffer_d.get(), sino.data(), m_imageWidth, m_imageHeight);
		break;
	}

	CHECK(cudaStreamSynchronize(m_stream));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader: process done for index " << m_index;

	m_index += m_incrementFrames;

	return sino;
}

auto HDF5Loader::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader:  ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Loader:  ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			hasValidSetting =
				configReader.lookupValue("frameRateLimit", m_targetFrameRate) || hasValidSetting;

			bool tmp = false;
			if(m_startMode == detail::loadControlMode::triggered
				&& configReader.lookupValue("triggerStart", tmp))
			{
				if(tmp)
				{
					{
						std::lock_guard<std::mutex> lck(m_mutex);
						m_triggerStart = true;
					}
					m_condVar.notify_one();
				}
				// return early because a start and a stop trigger should not happen in the same update. After
				// starting by trigger, m_startMode will be set to automatic so the start trigger won't be
				// checked again.
				return;
			}

			if(configReader.lookupValue("triggerStop", tmp))
			{
				std::lock_guard<std::mutex> lck(m_mutex);
				m_triggerStop = tmp;
				return;
			}

			if(hasValidSetting)
			{
				std::lock_guard<std::mutex> lck(m_mutex);
				sendInterval_us = m_targetFrameRate > 0 ? 1000000 / m_targetFrameRate : 0;
			}
		}
	}
}

auto HDF5Loader::allocateBuffer() -> void
{
	m_elemBytes = detail::getBytesPerElem(m_native_type_data);
	if(m_elemBytes > 0)
	{
		m_dataBuffer = (char*)malloc(m_imageWidth * m_imageHeight * m_elemBytes);
		dataBuffer_d = glados::cuda::make_device_ptr<char, glados::cuda::async_copy_policy>(
			m_imageWidth * m_imageHeight * m_elemBytes);
	}
	else
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Cannot get number of bytes per element in dataset'" << m_datasetName
								 << "'.";
		throw std::runtime_error("risa::cuda::HDF5Loader: Cannot get bytes per element in dataset.");
	}
}

auto HDF5Loader::setPropertyFromBufValue(glados::Image<risa::cuda::HDF5Loader::manager_type>* img,
	std::string key, char* buf, detail::dataType type) -> void
{
	switch(type)
	{
	case detail::dataType::t_bool:
	{
		auto val = reinterpret_cast<bool*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_uint8:
	{
		auto val = reinterpret_cast<uint8_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_uint16:
	{
		auto val = reinterpret_cast<uint16_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_uint32:
	{
		auto val = reinterpret_cast<uint32_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_uint64:
	{
		auto val = reinterpret_cast<uint64_t*>(buf);
		std::ignore = img->setProperty(
			key, (size_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_int8:
	{
		auto val = reinterpret_cast<int8_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_int16:
	{
		auto val = reinterpret_cast<int16_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_int32:
	{
		auto val = reinterpret_cast<int32_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_int64:
	{
		auto val = reinterpret_cast<int64_t*>(buf);
		std::ignore = img->setProperty(
			key, (ssize_t)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_float:
	{
		auto val = reinterpret_cast<float*>(buf);
		std::ignore = img->setProperty(
			key, (float)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}

	case detail::dataType::t_double:
	{
		auto val = reinterpret_cast<double*>(buf);
		std::ignore = img->setProperty(
			key, (float)(*val), glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_string:
	{
		std::string str(buf);
		std::ignore =
			img->setProperty(key, str, glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	}
	case detail::dataType::t_unknown:
	{
		; // cannot be reached
	}
	};
}

auto HDF5Loader::setPropertyFromAny(glados::Image<risa::cuda::HDF5Loader::manager_type>* img, std::string key,
	const std::any val, detail::dataType type) -> void
{
	switch(type)
	{
	case detail::dataType::t_bool:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<bool>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_uint8:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<uint8_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_uint16:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<uint16_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_uint32:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<uint32_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_uint64:
		std::ignore = img->setProperty(key, (size_t)std::any_cast<uint64_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_int8:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<int8_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_int16:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<int16_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_int32:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<int32_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_int64:
		std::ignore = img->setProperty(key, (ssize_t)std::any_cast<int64_t>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_float:
		std::ignore = img->setProperty(key, (float)std::any_cast<float>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_double:
		std::ignore = img->setProperty(key, (float)std::any_cast<double>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_string:
		std::ignore = img->setProperty(key, std::any_cast<std::string>(val),
			glados::details::PropertyOverwritePolicy::OverwriteValueAndType);
		break;
	case detail::dataType::t_unknown:
	{
		; // cannot be reached
	}
	};
}

auto HDF5Loader::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	if(configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".frameRateLimit", m_targetFrameRate)
		&& configReader.lookupValue(parameterSet + ".filePath", m_filePath)
		&& configReader.lookupValue(parameterSet + ".fileName", m_fileName)
		&& configReader.lookupValue(parameterSet + ".fileEnding", m_fileEnding)
		&& configReader.lookupValue(parameterSet + ".datasetName", m_datasetName))
	{

		sendInterval_us = m_targetFrameRate > 0 ? 1000000 / m_targetFrameRate : 0;
		if(m_filePath.back() != PATH_SEP)
		{
			m_filePath.push_back(PATH_SEP);
		}

		std::string s;

		if(!configReader.lookupValue(parameterSet + ".deviceId", m_deviceId))
		{
			m_deviceId = 0;
		}

		if(!configReader.lookupValue(parameterSet + ".incrementFrames", m_incrementFrames))
		{
			m_incrementFrames = 1;
		}

		configReader.setAllowedToFail(true); // user may skip defining a roi
		int definedRoiElems = 0;
		definedRoiElems += configReader.lookupValue(parameterSet + ".roiX", roi_x_);
		definedRoiElems += configReader.lookupValue(parameterSet + ".roiY", roi_y_);
		definedRoiElems += configReader.lookupValue(parameterSet + ".roiWidth", roi_w_);
		definedRoiElems += configReader.lookupValue(parameterSet + ".roiHeight", roi_h_);
		configReader.setAllowedToFail(false);

		if(definedRoiElems > 0 && definedRoiElems < 4)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader: Region of interest was not fully defined. "
										"Requires x, y, width and height.";
			return false;
		}
		m_roiDefined = (definedRoiElems == 4);

		// does not matter if these remain emtpy
		// disable warnings for parsing possibly defined but empty arguments
		configReader.setAllowedToFail(true);
		configReader.lookupValue(parameterSet + ".metaDataNames", m_metaNames);
		configReader.lookupValue(parameterSet + ".attributeNames", m_attrNames);
		configReader.setAllowedToFail(false);

		if(!configReader.lookupValue(parameterSet + ".firstIdx", m_firstIdx))
		{
			m_firstIdx = 0;
		}
		if(!configReader.lookupValue(parameterSet + ".lastIdx", m_lastIdx))
		{
			m_lastIdx = -1;
		}

		if(configReader.lookupValue(parameterSet + ".startMode", s))
		{
			if(s == "triggered")
			{
				m_startMode = detail::loadControlMode::triggered;
				m_triggerStart = false;
			}
			else
				m_startMode = detail::loadControlMode::automatic;
		}

		if(configReader.lookupValue(parameterSet + ".stopMode", s))
		{
			if(s == "triggered")
			{
				m_stopMode = detail::loadControlMode::triggered;
				m_triggerStop = false;
			}
			else
			{
				m_stopMode = detail::loadControlMode::automatic;
				configReader.lookupValue(parameterSet + ".numberOfFramesToSend", m_stopFrame);
			}
		}

		if(!configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
		{
			m_numberOfPlanes = 1;
		}
		return true;
	}

	return false;
}

bool HDF5Loader::initializeMainDataset()
{
	m_ds_data = H5Dopen(m_HDF5File, m_datasetName.c_str(), H5P_DEFAULT);
	if(m_ds_data == H5I_INVALID_HID)
	{
		printErrorCannotOpen("dataset", m_datasetName, m_datasetName[0] != '/');
		return false;
	}
	m_sp_data = H5Dget_space(m_ds_data);
	if(m_sp_data == H5I_INVALID_HID)
	{
		printErrorCannotOpen("dataspace for dataset", m_datasetName);
		return false;
	}
	// get dimensions
	auto ndims_data = H5Sget_simple_extent_ndims(m_sp_data);
	if(ndims_data != 3)
	{
		printErrorNumDims(m_datasetName, ndims_data, 3);
		return false;
	}
	hsize_t dims_data[3], maxdims_data[3];
	if(ndims_data != H5Sget_simple_extent_dims(m_sp_data, dims_data, maxdims_data))
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Unable to get dataset dimensions.";
		return false;
	}

	m_imageWidth = dims_data[detail::H5DIM::WIDTH];
	m_imageHeight = dims_data[detail::H5DIM::HEIGHT];
	m_imageCount = dims_data[detail::H5DIM::SLICES];

	if(m_roiDefined)
	{
		// sanity check ROI with now known image dimensions
		if(roi_x_ < 0 || roi_x_ >= m_imageWidth)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::HDF5Loader: Expected region of interest x in range [0 ... "
				<< m_imageWidth - 1 << "], got " << roi_x_;
			return false;
		}
		if(roi_y_ < 0 || roi_y_ >= m_imageHeight)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::HDF5Loader: Expected region of interest y in range [0 ... "
				<< m_imageHeight - 1 << "] got " << roi_y_;
			return false;
		}
		if(roi_w_ < 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader: Expected region of interest width > 0, got "
									 << roi_w_;
			return false;
		}
		if(roi_h_ < 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader: Expected region of interest height > 0, got "
									 << roi_h_;
			return false;
		}
		auto roi_horiz = roi_x_ + roi_w_ - 1;
		if(roi_horiz < 0 || roi_horiz >= m_imageWidth)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::HDF5Loader: Expected region of interest (x + width) in range [1 ... "
				<< m_imageWidth << "], got " << roi_horiz + 1;
			return false;
		}
		auto roi_vert = roi_y_ + roi_h_ - 1;
		if(roi_vert < 0 || roi_vert >= m_imageHeight)
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::HDF5Loader: Expected region of interest (y + height) in range [1 ... "
				<< m_imageHeight << "], got " << roi_vert + 1;
			return false;
		}
	}
	else
	{
		roi_x_ = 0;
		roi_y_ = 0;
		roi_w_ = m_imageWidth;
		roi_h_ = m_imageHeight;
	}

	m_imageWidth = roi_w_;
	m_imageHeight = roi_h_;

	if(m_lastIdx < 0)
	{
		m_stopFrame = m_imageCount;
	}
	else
	{
		m_stopFrame = m_lastIdx;
	}

	blocks = dim3(m_blockSize2D, m_blockSize2D);
	grids =
		dim3(std::ceil(m_imageWidth / (float)m_blockSize2D), std::ceil(m_imageHeight / (float)m_blockSize2D));

	if(m_firstIdx >= m_imageCount)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Trying to start loading at " << m_firstIdx
								 << ", but the file only contains " << m_imageCount << " images.";
		return false;
	}

	hsize_t dims_msp[3];
	dims_msp[detail::H5DIM::WIDTH] = m_imageWidth;
	dims_msp[detail::H5DIM::HEIGHT] = m_imageHeight;
	dims_msp[detail::H5DIM::SLICES] = 1; // set such that the memory space will always hold one frame
	m_msp_data = H5Screate_simple(ndims_data, dims_msp, NULL);
	if(m_msp_data == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Cannot get create memory space for dataset '" << m_datasetName
								 << "'.";
		return false;
	}

	// check data type
	m_type_data = H5Dget_type(m_ds_data);
	if(m_type_data == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: Cannot get type of dataset '" << m_datasetName << "'.";
		return false;
	}

	m_native_type_data = toNativeDatatype(m_type_data);
	if(m_native_type_data == detail::dataType::t_unknown)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Dataset '"
								 << m_datasetName << "' has an unknown data type.";
		return false;
	}
	if(m_native_type_data == detail::dataType::t_string)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
								 << ">: String data sets are not supported as frame data.";
		return false;
	}
	if(m_native_type_data != detail::dataType::t_float)
	{
		BOOST_LOG_TRIVIAL(warning) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Data in '"
								   << m_datasetName << "' will be converted from "
								   << printNativeDataType(m_native_type_data)
								   << " to float. This may lead to precission loss.";
	}

	m_offset_data[detail::H5DIM::WIDTH] = roi_x_;
	m_offset_data[detail::H5DIM::HEIGHT] = roi_y_;
	m_offset_data[detail::H5DIM::SLICES] = m_firstIdx;
	m_count_data[detail::H5DIM::WIDTH] = roi_w_;
	m_count_data[detail::H5DIM::HEIGHT] = roi_h_;
	m_count_data[detail::H5DIM::SLICES] = 1;
	// memory space was created as large as the roi, hence we do not require an offset in the msp
	m_offset_msp[detail::H5DIM::WIDTH] = 0;
	m_offset_msp[detail::H5DIM::HEIGHT] = 0;
	m_offset_msp[detail::H5DIM::SLICES] = 0;

	return true;
}

bool HDF5Loader::initializeOptionalDatasets()
{
	for(const auto metaName : m_metaNames)
	{
		m_ds_meta.emplace_back(H5Dopen(m_HDF5File, metaName.c_str(), H5P_DEFAULT));
		if(m_ds_meta.back() == H5I_INVALID_HID)
		{
			printErrorCannotOpen("dataset", metaName, metaName[0] != '/');
			return false;
		}

		m_sp_meta.emplace_back(H5Dget_space(m_ds_meta.back()));
		if(m_sp_meta.back() == H5I_INVALID_HID)
		{
			printErrorCannotOpen("dataspace for dataset", metaName);
			return false;
		}

		auto ndims_meta = H5Sget_simple_extent_ndims(m_sp_meta.back());
		if(ndims_meta != 1)
		{
			printErrorNumDims(metaName, ndims_meta, 1);
			return false;
		}
		hsize_t dims_meta[1], maxdims_meta[1];
		if(ndims_meta != H5Sget_simple_extent_dims(m_sp_meta.back(), dims_meta, maxdims_meta))
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Unable to get dataset dimensions.";
			return false;
		}

		if(dims_meta[0] != m_imageCount)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Dataset '"
									 << metaName << "' has not the same length (=" << dims_meta[0]
									 << ") as the number of images (=" << m_imageCount << "). ";
			return false;
		}

		dims_meta[0] = 1; // set such that the memory space will always hold one element
		m_msp_meta.emplace_back(H5Screate_simple(ndims_meta, dims_meta, NULL));
		if(m_msp_meta.back() == H5I_INVALID_HID)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Cannot create memory space for dataset '" << metaName << "'.";
			return false;
		}

		m_type_meta.emplace_back(H5Dget_type(m_ds_meta.back()));
		if(m_type_meta.back() == H5I_INVALID_HID)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Cannot get type of dataset '" << metaName << "'.";
			return false;
		}

		m_native_type_meta.emplace_back(toNativeDatatype(m_type_meta.back()));
		if(m_native_type_meta.back() == detail::dataType::t_unknown)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Dataset '"
									 << metaName << "' has an unknown data type.";
			return false;
		}

		if(m_native_type_meta.back() == detail::dataType::t_double)
		{
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::HDF5loader<" << m_parameterSet << ">: Meta data '"
									   << metaName << "' of type double will be converted to float.";
		}

		auto bytesPerElem = detail::getBytesPerElem(m_native_type_meta.back());
		if(bytesPerElem > 0)
		{
			char* buf = (char*)malloc(bytesPerElem);
			m_meta_bufs.push_back(buf);
		}
		else if(m_native_type_meta.back() == detail::dataType::t_string)
		{
			// HDF5 lib will handle memory management for string data
			m_meta_bufs.push_back(nullptr);
		}
	}
	m_offset_meta[0] = m_firstIdx;
	m_count_meta[0] = 1;
	return true;
}

bool HDF5Loader::initializeAttributes()
{
	for(const auto attrName : m_attrNames)
	{
		if(H5Aexists_by_name(m_HDF5File, m_datasetName.c_str(), attrName.c_str(), H5P_DEFAULT) <= 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Atribute with name '" << attrName
									 << "' does not exist in dataset '" << m_datasetName << "'.";
		}
		hid_t id_attr =
			H5Aopen_by_name(m_HDF5File, m_datasetName.c_str(), attrName.c_str(), H5P_DEFAULT, H5P_DEFAULT);
		if(id_attr < 0)
		{
			printErrorCannotOpen("attribute", attrName);
			return false;
		}

		hid_t type_attr = H5Aget_type(id_attr);
		if(type_attr == H5I_INVALID_HID)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet
									 << ">: Cannot get type of attribute '" << attrName << "'.";
			return false;
		}

		// read and store attribute value once
		m_native_type_attr.emplace_back(toNativeDatatype(type_attr));
		if(m_native_type_attr.back() == detail::dataType::t_unknown)
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Attribute '"
									 << attrName << "' has an unknown data type.";
			return false;
		}
		switch(m_native_type_attr.back())
		{
		case detail::dataType::t_bool:
		{
			if(!readAttribute<bool>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_uint8:
		{
			if(!readAttribute<uint8_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_uint16:
		{
			if(!readAttribute<uint16_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_uint32:
		{
			if(!readAttribute<uint32_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_uint64:
		{
			if(!readAttribute<uint64_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_int8:
		{
			if(!readAttribute<int8_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_int16:
		{
			if(!readAttribute<int16_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_int32:
		{
			if(!readAttribute<int32_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_int64:
		{
			if(!readAttribute<int64_t>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_float:
		{
			if(!readAttribute<float>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_double:
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "risa::cuda::HDF5Loader<" << m_parameterSet
				<< ">: Narrowing conversion from double to float for attribute '" << attrName << "'.";
			if(!readAttribute<double>(attrName, id_attr, type_attr))
				return false;
			break;
		}
		case detail::dataType::t_string:
		{
			if(!readAttributeStr(attrName, id_attr, type_attr))
				return false;
			break;
		}
		}

		H5Tclose(type_attr);
		H5Aclose(id_attr);
	}
	return true;
}

auto HDF5Loader::readAttributeStr(std::string name, hid_t id_attr, hid_t type_attr) -> bool
{
	hsize_t strSize = H5Aget_storage_size(id_attr);
	char* str = (char*)malloc(sizeof(char) * strSize);
	if(H5Aread(id_attr, type_attr, str) < 0)
	{
		printErrorReadVal(name);
		H5Tclose(type_attr);
		H5Aclose(id_attr);
		return false;
	}
	m_val_attr.emplace_back(std::string(str));
	free(str);
	return true;
}

template <typename T>
auto HDF5Loader::readAttribute(std::string name, hid_t id_attr, hid_t type_attr) -> bool
{
	T val;
	if(H5Aread(id_attr, type_attr, &val) < 0)
	{
		printErrorReadVal(name);
		H5Tclose(type_attr);
		H5Aclose(id_attr);
		return false;
	}
	m_val_attr.emplace_back(val);
	return true;
}

std::vector<std::string> HDF5Loader::getHDF5FileDatasetNames()
{

	if(m_HDF5File == H5I_INVALID_HID)
	{
		throw std::runtime_error("Cannot get HDF5File dataset names on invalid file");
	}

	std::vector<std::string> datasetNames;
	herr_t status;

	BOOST_LOG_TRIVIAL(debug) << "Datasets in the file are ";

	auto storeDatasetName = [](hid_t loc_id, const char* name, const H5O_info_t* h5info,
						 void* operator_data) -> herr_t
	{
		if(name[0] == '.')
		{
			return 0;
		}

		switch(h5info->type)
		{
		case H5O_TYPE_DATASET:
		{
			std::vector<std::string>* datasetNames = static_cast<std::vector<std::string>*>(operator_data);
			datasetNames->emplace_back(name);
			BOOST_LOG_TRIVIAL(debug) << "  /" << name << "(" << h5info->num_attrs << " attrs)";
			break;
		}
		default:; // do nothing
		}
		return 0;
	};

	status =
		H5Ovisit(m_HDF5File, H5_INDEX_NAME, H5_ITER_NATIVE, storeDatasetName, &datasetNames, H5O_INFO_ALL);
	if(status < 0)
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::HDF5Laoder: Unable to get the dataset names in the HDF5 file.";
		throw std::runtime_error("Unable to get dataset names in HDF5 file.");
	}
	return datasetNames;
}

bool HDF5Loader::initializeHDF5()
{

	// open the file
	std::string fullPath = m_filePath + m_fileName + m_fileEnding;
	m_HDF5File = H5Fopen(fullPath.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
	if(m_HDF5File == H5I_INVALID_HID)
	{
		printErrorCannotOpen("HDF5 file", fullPath);
		return false;
	}

	if(m_metaNames.empty())
	{
		auto availableDatasets = getHDF5FileDatasetNames();
		for(const auto& dataset : availableDatasets)
		{
			if(dataset != m_datasetName)
			{
				m_metaNames.push_back(dataset);
			}
		}
	}
	
	if(!initializeMainDataset())
	{
		return false;
	}

	if(!initializeOptionalDatasets())
	{
		return false;
	}

	if(!initializeAttributes())
	{
		return false;
	}

	return true;
}

template <typename T>
__global__ void convertArrayToFloat(
	T* __restrict__ input, float* __restrict__ output, const unsigned int width, const unsigned int height)
{
	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();
	if(x >= width || y >= height)
		return;

	const auto idx = y * width + x;
	output[idx] = (float)input[idx];
}
}
}
