#include <risaCore/Basics/NvjpegEncoder.h>

#include <glados/cuda/Check.h>

namespace risa
{
namespace cuda
{

auto NvjpegEncoder::init(int quality) -> void
{
	std::unique_lock<std::mutex> lck(m_mtxInit);
	if(!m_initOk)
	{
		m_jpegQuality = quality;
		m_encoderThread = std::thread(&NvjpegEncoder::initAndRun, this);
		m_cvInit.wait(lck, [&] { return m_initOk; });
	}
}

NvjpegEncoder::~NvjpegEncoder()
{
	m_stopFlag = true;
	m_encoderThread.join();
}

auto NvjpegEncoder::encodeImage(const image_props* image, std::vector<unsigned char>& jpeg) -> void
{

	std::unique_lock<std::mutex> lck(m_mtxEncode);
	m_cvEncode.wait(lck, [&] { return !m_workAvailable; });

	jpeg.clear();
	m_workImage = image;
	m_workData = &jpeg;
	{
		std::unique_lock<std::mutex> lckWork(m_mtxWork);
		m_workDone = false;
		m_workAvailable = true;
		m_cvWork.wait(lckWork, [&] { return m_workDone; });
		m_workAvailable = false;
	}
}

auto NvjpegEncoder::initAndRun() -> void
{
	if(!m_nv_handle)
	{
		m_nv_handle = new nvjpegHandle_t;
		nvjpegCreateSimple(m_nv_handle);
	}

	if(!m_nv_image)
	{
		m_nv_image = new nvjpegImage_t;
	}

	{
		std::lock_guard<std::mutex> lck(m_mtxInit);
		m_initOk = true;
	}
	m_cvInit.notify_all();

	while(true)
	{
		if(m_workAvailable)
		{
			// do work
			std::lock_guard<std::mutex> lock{m_mtxWork};

			// setup data layout
			m_nv_image->channel[0] = m_workImage->buffer;
			m_nv_image->pitch[0] = m_workImage->width * 3;

			if(m_nv_enc_params)
			{
				nvjpegEncoderParamsDestroy(*m_nv_enc_params);
				delete(m_nv_enc_params);
			}

			m_nv_enc_params = new nvjpegEncoderParams_t;
			nvjpegEncoderParamsCreate(*m_nv_handle, m_nv_enc_params, *m_workImage->stream);
			nvjpegEncoderParamsSetQuality(*m_nv_enc_params, m_jpegQuality, *m_workImage->stream);
			nvjpegEncoderParamsSetSamplingFactors(
				*m_nv_enc_params, NVJPEG_CSS_GRAY, *m_workImage->stream); // only return gray channel

			if(m_nv_enc_state)
			{
				nvjpegEncoderStateDestroy(*m_nv_enc_state);
				delete(m_nv_enc_state);
			}

			m_nv_enc_state = new nvjpegEncoderState_t;
			nvjpegEncoderStateCreate(*m_nv_handle, m_nv_enc_state, *m_workImage->stream);

			CHECK_NVJPEG(nvjpegEncodeImage(*m_nv_handle, *m_nv_enc_state, *m_nv_enc_params, m_nv_image,
				NVJPEG_INPUT_RGBI, m_workImage->width, m_workImage->height, *m_workImage->stream));
			cudaStreamSynchronize(*m_workImage->stream);

			size_t length = 0;
			// calling with NULL for data will just return the length
			CHECK_NVJPEG(nvjpegEncodeRetrieveBitstream(
				*m_nv_handle, *m_nv_enc_state, NULL, &length, *m_workImage->stream));
			cudaStreamSynchronize(*m_workImage->stream);

			// with known length, resize data array and retrieve actual data
			if(length > 0)
			{
				m_workData->resize(length);
				CHECK_NVJPEG(nvjpegEncodeRetrieveBitstream(
					*m_nv_handle, *m_nv_enc_state, m_workData->data(), &length, *m_workImage->stream));
				cudaStreamSynchronize(*m_workImage->stream);
			}

			m_workDone = true;
			m_workAvailable = false;
			m_cvWork.notify_one();
		}

		if(m_stopFlag)
		{
			break;
		}
	}
}

}
}