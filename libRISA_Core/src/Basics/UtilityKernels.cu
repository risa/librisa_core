#include <glados/cuda/Coordinates.h>
#include <risaCore/Basics/UtilityKernels.h>

#include <vector>

namespace risa
{

namespace cuda
{

__global__ void normalizeAndConvertTo8BitRGB(float* __restrict__ img, unsigned char* img8bit,
	const int imageWidth, const int imageHeight, const float minValue, const float maxValue)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;

	const float inputDynamic = maxValue - minValue;

	float val = 255.0 * (img[idx] - minValue) / inputDynamic;

	// auto-clip for normalization values outside normal range
	val = val > 255.0 ? 255 : val;
	val = val < 0 ? 0 : val;

	img8bit[3 * idx + 0] = val; // implicit convert float to uint8_t
	img8bit[3 * idx + 1] = val;
	img8bit[3 * idx + 2] = val;
}

__global__ void normalizeAndConvertTo8BitRGBA(float* __restrict__ img, unsigned char* img8bit,
	const int imageWidth, const int imageHeight, const float minValue, const float maxValue)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;

	const float inputDynamic = maxValue - minValue;

	float val = 255.0 * (img[idx] - minValue) / inputDynamic;

	// auto-clip for normalization values outside normal range
	val = val > 255.0 ? 255 : val;
	val = val < 0 ? 0 : val;

	img8bit[4 * idx + 0] = val; // implicit convert float to uint8_t
	img8bit[4 * idx + 1] = val;
	img8bit[4 * idx + 2] = val;
	img8bit[4 * idx + 3] = 255; // fully opaque alpha channel
}

// REDUCE KERNELS

#define REDUCE_GET_MAX(OFFS) \
	{ \
		if(useMask) \
		{ \
			smemMax[tid] = \
				GET_MAX_MASKED(smemMax[tid], smemMask[tid], smemMax[tid + OFFS], smemMask[tid + OFFS]); \
		} \
		else \
		{ \
			smemMax[tid] = GET_MAX(smemMax[tid], smemMax[tid + OFFS]); \
		} \
	}

#define REDUCE_GET_MIN(OFFS) \
	{ \
		if(useMask) \
		{ \
			smemMin[tid] = \
				GET_MIN_MASKED(smemMin[tid], smemMask[tid], smemMin[tid + OFFS], smemMask[tid + OFFS]); \
		} \
		else \
		{ \
			smemMin[tid] = GET_MIN(smemMin[tid], smemMin[tid + OFFS]); \
		} \
	}

#define REDUCE_GET_PROD(OFFS) \
	{ \
		if(useMask) \
		{ \
			smemProd[tid] = \
				GET_PROD_MASKED(smemProd[tid], smemMask[tid], smemProd[tid + OFFS], smemMask[tid + OFFS]); \
		} \
		else \
		{ \
			smemProd[tid] = GET_PROD(smemProd[tid], smemProd[tid + OFFS]); \
		} \
	}

#define REDUCE_GET_SUM(OFFS) \
	{ \
		if(useMask) \
		{ \
			smemSum[tid] = \
				GET_SUM_MASKED(smemSum[tid], smemMask[tid], smemSum[tid + OFFS], smemMask[tid + OFFS]); \
		} \
		else \
		{ \
			smemSum[tid] = GET_SUM(smemSum[tid], smemSum[tid + OFFS]); \
		} \
	}

#define REDUCE_MASK(OFFS) \
	{ \
		smemMask[tid] = smemMask[tid] && smemMask[tid + OFFS]; \
	}

#define IF_REDUCE_MAX(A) \
	if(props & detail::ReduceProperty::Maximum) \
	{ \
		A \
	}

#define IF_REDUCE_MIN(A) \
	if(props & detail::ReduceProperty::Minimum) \
	{ \
		A \
	}

#define IF_REDUCE_PROD(A) \
	if(props & detail::ReduceProperty::Product) \
	{ \
		A \
	}

#define IF_REDUCE_SUM(A) \
	if(props & detail::ReduceProperty::Sum) \
	{ \
		A \
	}

#define IF_USE_MASK(A) \
	if(useMask) \
	{ \
		A \
	}

namespace detail
{
namespace ReduceProperty
{
static const size_t NUM_PROPS = 4; // total number of properties
auto getReducePropertyArraySize(size_t numBlocks) -> size_t { return NUM_PROPS * numBlocks; }

auto getSharedMemSize(unsigned int blockSize2D) -> size_t
{
	return (blockSize2D * blockSize2D * NUM_PROPS * sizeof(float) * 2)
		+ (blockSize2D * blockSize2D * sizeof(bool)); // + size for boolean mask shared mem
};
auto getNthMaxIdx(size_t numBlocks, unsigned int n) -> size_t { return (0 * numBlocks + n); };

auto getNthMinIdx(size_t numBlocks, unsigned int n) -> size_t { return (1 * numBlocks + n); };

auto getNthProdIdx(size_t numBlocks, unsigned int n) -> size_t { return (2 * numBlocks + n); };

auto getNthSumIdx(size_t numBlocks, unsigned int n) -> size_t { return (3 * numBlocks + n); };
}
}

template <unsigned int blockSize, detail::ReducePropertyType props, bool useMask>
__device__ void warpReduce(volatile float* smemMax, volatile float* smemMin, volatile float* smemProd,
	volatile float* smemSum, volatile bool* smemMask, size_t tid, const bool* mask)
{
	// clang-format off
	if(blockSize >= 64)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX(32))
		IF_REDUCE_MIN(REDUCE_GET_MIN(32))
		IF_REDUCE_PROD(REDUCE_GET_PROD(32))
		IF_REDUCE_SUM(REDUCE_GET_SUM(32))
		IF_USE_MASK(REDUCE_MASK(32))
	}
	if(blockSize >= 32)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX(16))
		IF_REDUCE_MIN(REDUCE_GET_MIN(16))
		IF_REDUCE_PROD(REDUCE_GET_PROD(16))
		IF_REDUCE_SUM(REDUCE_GET_SUM(16))
		IF_USE_MASK(REDUCE_MASK(16))
	}
	if(blockSize >= 16)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX( 8))
		IF_REDUCE_MIN(REDUCE_GET_MIN( 8))
		IF_REDUCE_PROD(REDUCE_GET_PROD( 8))
		IF_REDUCE_SUM(REDUCE_GET_SUM( 8))
		IF_USE_MASK(REDUCE_MASK( 8))
	}
	if(blockSize >=  8)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX( 4))
		IF_REDUCE_MIN(REDUCE_GET_MIN( 4))
		IF_REDUCE_PROD(REDUCE_GET_PROD( 4))
		IF_REDUCE_SUM(REDUCE_GET_SUM( 4))
		IF_USE_MASK(REDUCE_MASK( 4))
	}
	if(blockSize >=  4)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX( 2))
		IF_REDUCE_MIN(REDUCE_GET_MIN( 2))
		IF_REDUCE_PROD(REDUCE_GET_PROD( 2))
		IF_REDUCE_SUM(REDUCE_GET_SUM( 2))
		IF_USE_MASK(REDUCE_MASK( 2))
	}
	if(blockSize >=  2)	{
		IF_REDUCE_MAX(REDUCE_GET_MAX( 1))
		IF_REDUCE_MIN(REDUCE_GET_MIN( 1))
		IF_REDUCE_PROD(REDUCE_GET_PROD( 1))
		IF_REDUCE_SUM(REDUCE_GET_SUM( 1))
		IF_USE_MASK(REDUCE_MASK( 1))
	}
	// clang-format on
}

template <unsigned int blockSize, detail::ReducePropertyType props, bool useMask>
__global__ void reduceKernel(
	const float* __restrict__ in, const size_t size, float* reducedProps, const bool* mask)
{
	extern __shared__ float smem[];
	float* smemMax = &smem[0 * blockSize];
	float* smemMin = &smem[1 * blockSize];
	float* smemProd = &smem[2 * blockSize];
	float* smemSum = &smem[3 * blockSize];
	bool* smemMask = (bool*)&smem[4 * blockSize];

	const size_t tid = threadIdx.x;
	size_t idx = (blockSize * 2) * blockIdx.x + tid;
	const size_t gridSize = (blockSize * 2) * gridDim.x;

	IF_REDUCE_MAX(smemMax[tid] = -FLT_MAX;)
	IF_REDUCE_MIN(smemMin[tid] = FLT_MAX;)
	IF_REDUCE_PROD(smemProd[tid] = 1;)
	IF_REDUCE_SUM(smemSum[tid] = 0;)
	IF_USE_MASK(smemMask[tid] = true;)

	while(idx < size)
	{
		if(useMask)
		{
			IF_REDUCE_MAX(smemMax[tid] = GET_MAX_MASKED(smemMax[tid], smemMask[tid], in[idx], mask[idx]);)
			IF_REDUCE_MIN(smemMin[tid] = GET_MIN_MASKED(smemMin[tid], smemMask[tid], in[idx], mask[idx]);)
			IF_REDUCE_PROD(smemProd[tid] = GET_PROD_MASKED(smemProd[tid], smemMask[tid], in[idx], mask[idx]);)
			IF_REDUCE_SUM(smemSum[tid] = GET_SUM_MASKED(smemSum[tid], smemMask[tid], in[idx], mask[idx]);)
			smemMask[tid] = smemMask[tid] && mask[idx];
		}
		else
		{
			IF_REDUCE_MAX(smemMax[tid] = GET_MAX(smemMax[tid], in[idx]);)
			IF_REDUCE_MIN(smemMin[tid] = GET_MIN(smemMin[tid], in[idx]);)
			IF_REDUCE_PROD(smemProd[tid] = GET_PROD(smemProd[tid], in[idx]);)
			IF_REDUCE_SUM(smemSum[tid] = GET_SUM(smemSum[tid], in[idx]);)
		}

		if(idx + blockSize < size)
		{
			if(useMask)
			{
				IF_REDUCE_MAX(smemMax[tid] = GET_MAX_MASKED(
								  smemMax[tid], smemMask[tid], in[idx + blockSize], mask[idx + blockSize]);)
				IF_REDUCE_MIN(smemMin[tid] = GET_MIN_MASKED(
								  smemMin[tid], smemMask[tid], in[idx + blockSize], mask[idx + blockSize]);)
				IF_REDUCE_PROD(smemProd[tid] = GET_PROD_MASKED(
								   smemProd[tid], smemMask[tid], in[idx + blockSize], mask[idx + blockSize]);)
				IF_REDUCE_SUM(smemSum[tid] = GET_SUM_MASKED(
								  smemSum[tid], smemMask[tid], in[idx + blockSize], mask[idx + blockSize]);)
				smemMask[tid] = smemMask[tid] && mask[idx + blockSize];
			}
			else
			{
				IF_REDUCE_MAX(smemMax[tid] = GET_MAX(smemMax[tid], in[idx + blockSize]);)
				IF_REDUCE_MIN(smemMin[tid] = GET_MIN(smemMin[tid], in[idx + blockSize]);)
				IF_REDUCE_PROD(smemProd[tid] = GET_PROD(smemProd[tid], in[idx + blockSize]);)
				IF_REDUCE_SUM(smemSum[tid] = GET_SUM(smemSum[tid], in[idx + blockSize]);)
			}
		}
		idx += gridSize;
	}
	__syncthreads();

	if(blockSize >= 1024)
	{
		if(tid < 512)
		{
			IF_REDUCE_MAX(REDUCE_GET_MAX(512))
			IF_REDUCE_MIN(REDUCE_GET_MIN(512))
			IF_REDUCE_PROD(REDUCE_GET_PROD(512))
			IF_REDUCE_SUM(REDUCE_GET_SUM(512))
			IF_USE_MASK(REDUCE_MASK(512))
		}
		__syncthreads();
	}
	if(blockSize >= 512)
	{
		if(tid < 256)
		{
			IF_REDUCE_MAX(REDUCE_GET_MAX(256))
			IF_REDUCE_MIN(REDUCE_GET_MIN(256))
			IF_REDUCE_PROD(REDUCE_GET_PROD(256))
			IF_REDUCE_SUM(REDUCE_GET_SUM(256))
			IF_USE_MASK(REDUCE_MASK(256))
		}
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if(tid < 128)
		{
			IF_REDUCE_MAX(REDUCE_GET_MAX(128))
			IF_REDUCE_MIN(REDUCE_GET_MIN(128))
			IF_REDUCE_PROD(REDUCE_GET_PROD(128))
			IF_REDUCE_SUM(REDUCE_GET_SUM(128))
			IF_USE_MASK(REDUCE_MASK(128))
		}
		__syncthreads();
	}
	if(blockSize >= 128)
	{
		if(tid < 64)
		{
			IF_REDUCE_MAX(REDUCE_GET_MAX(64))
			IF_REDUCE_MIN(REDUCE_GET_MIN(64))
			IF_REDUCE_PROD(REDUCE_GET_PROD(64))
			IF_REDUCE_SUM(REDUCE_GET_SUM(64))
			IF_USE_MASK(REDUCE_MASK(64))
		}
		__syncthreads();
	}

	if(tid < 32)
	{
		warpReduce<blockSize, props, useMask>(smemMax, smemMin, smemProd, smemSum, smemMask, tid, mask);
	}

	if(tid == 0)
	{
		reducedProps[blockIdx.x + 0 * gridDim.x] = smemMax[0];
		reducedProps[blockIdx.x + 1 * gridDim.x] = smemMin[0];
		reducedProps[blockIdx.x + 2 * gridDim.x] = smemProd[0];
		reducedProps[blockIdx.x + 3 * gridDim.x] = smemSum[0];
	}
}

template <typename T_from, typename T_to>
__global__ void convertType(const T_from* __restrict__ in, T_to* out, size_t imageWidth, size_t imageHeight)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	const auto idx = y * imageWidth + x;

	out[idx] = static_cast<T_to>(in[idx]);
}

auto instantiateTemplatedKernels = []()
{
	// list all kernel template instantiations here such that they can be found by the linker

	// clang-format off
	// we, unfortunately, have to list all combinations of options here explicitly
	using namespace detail::ReduceProperty;
	// single opt
	reduceKernel<256, Maximum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Minimum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Sum	 ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// dual opt
	reduceKernel<256, Maximum | Minimum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Maximum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Maximum | Sum	   ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Minimum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Minimum | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Product | Sum	   ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// single opt missing
	reduceKernel<256, Maximum | Minimum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Maximum | Minimum | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Maximum | Product | Sum	 ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<256, Minimum | Product | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// all opts
	reduceKernel<256, Maximum | Minimum | Product | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);

	// single opt
	reduceKernel<1024, Maximum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Minimum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Sum	  ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// dual opt
	reduceKernel<1024, Maximum | Minimum><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Maximum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Maximum | Sum	><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Minimum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Minimum | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Product | Sum	><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// single opt missing
	reduceKernel<1024, Maximum | Minimum | Product><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Maximum | Minimum | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Maximum | Product | Sum	  ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Minimum | Product | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	// all opts
	reduceKernel<1024, Maximum | Minimum | Product | Sum    ><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);

	// min/max in masked image
	reduceKernel<256, Maximum | Minimum, true><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);
	reduceKernel<1024, Maximum | Minimum, true><<<0, 0, 0, 0>>>(nullptr, 0, nullptr);

	// allowed type converts
	convertType<unsigned short, float><<<0,0,0,0>>>(nullptr, nullptr, 0, 0);
	convertType<float, unsigned short><<<0,0,0,0>>>(nullptr, nullptr, 0, 0); // will cast negative number to 65535

	// clang-format on
};

}

}