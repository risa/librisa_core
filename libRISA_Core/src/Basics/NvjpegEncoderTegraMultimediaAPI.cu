#include <risaCore/Basics/NvjpegEncoderTegraMultimediaAPI.h>

#include <NvUtils.h> // dump_dmabuf
#include <cudaEGL.h>

#include <glados/cuda/Check.h>

namespace risa
{
namespace cuda
{

auto NvjpegEncoder::init(int quality) -> void
{
	std::unique_lock<std::mutex> lck(m_mtxInit);
	if(!m_initOk)
	{
		m_jpegQuality = quality;
		m_encoderThread = std::thread(&NvjpegEncoder::initAndRun, this);
		m_cvInit.wait(lck, [&] { return m_initOk; });
	}
}

NvjpegEncoder::~NvjpegEncoder()
{
	m_stopFlag = true;
	if(m_encoderThread.joinable())
	{
		m_encoderThread.join();
	}
}

auto NvjpegEncoder::encodeImage(const image_props* image, std::vector<unsigned char>& jpeg) -> void
{

	std::unique_lock<std::mutex> lck(m_mtxEncode);
	m_cvEncode.wait(lck, [&] { return !m_workAvailable; });

	jpeg.clear();
	m_workImage = image;
	m_workData = &jpeg;
	{
		std::unique_lock<std::mutex> lckWork(m_mtxWork);
		m_workDone = false;
		m_workAvailable = true;
		m_cvWork.wait(lckWork, [&] { return m_workDone; });
		m_workAvailable = false;
	}
}

auto NvjpegEncoder::initAndRun() -> void
{
	auto checkJetsonAPI = [](CUresult status, std::string userMsg)
	{
		if(status == CUDA_SUCCESS)
			return;

		const char* errMsg = nullptr;
		cuGetErrorName(status, &errMsg);
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::NvjpegEncoder (Tegra Multimedia API): " << userMsg << ": "
								 << errMsg;
		throw std::runtime_error("NvjpegEncoderTegraMultimediaAPI: jetson_multimedia_api error");
	};

	auto checkNvBufAPI = [](NvBufSurfTransform_Error status, std::string userMsg)
	{
		if(status == NvBufSurfTransformError_Success)
			return;

		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::NvjpegEncoder (Tegra Multimedia API): " << userMsg << ": "
								 << status;
		throw std::runtime_error("NvjpegEncoderTegraMultimediaAPI: jetson_multimedia_api error");
	};

	cudaSetDevice(0);

	{
		std::lock_guard<std::mutex> lck(m_mtxInit);
		m_initOk = true;
	}
	m_cvInit.notify_all();

	while(true)
	{
		if(m_workAvailable)
		{
			// do work
			std::lock_guard<std::mutex> lock{m_mtxWork};

			NvBufSurface* m_nvBufSurfaceRGBA{nullptr};
			NvBufSurface* m_nvBufSurfaceYUV{nullptr};
			NvBufSurfaceAllocateParams m_params = {{0}};

			// setup data layout
			m_params.params.width = m_workImage->width;
			m_params.params.height = m_workImage->height;
			m_params.params.memType = NVBUF_MEM_SURFACE_ARRAY;
			m_params.params.layout = NVBUF_LAYOUT_PITCH;
			m_params.params.colorFormat =
				NVBUF_COLOR_FORMAT_YUV420; // unfortunatelly, the only supported format
			m_params.memtag = NvBufSurfaceTag_JPEG;
			if(cudaSuccess != NvBufSurfaceAllocate(&m_nvBufSurfaceYUV, 1, &m_params))
			{
				throw std::runtime_error(
					"NvjpegEncoderTegraMultimediaAPI: Unable to allocate surface YUV buffer.");
			}
			m_nvBufSurfaceYUV->numFilled = 1;

			m_params.params.colorFormat = NVBUF_COLOR_FORMAT_RGBA;
			if(cudaSuccess != NvBufSurfaceAllocate(&m_nvBufSurfaceRGBA, 1, &m_params))
			{
				throw std::runtime_error(
					"NvjpegEncoderTegraMultimediaAPI: Unable to allocate surface RGBA buffer.");
			}
			m_nvBufSurfaceRGBA->numFilled = 1;

			if(-1 == NvBufSurfaceMapEglImage(m_nvBufSurfaceRGBA, 0))
			{
				throw std::runtime_error(
					"NvjpegEncoderTegraMultimediaAPI: Unable to surface map RGBA buffer.");
			}
			CUgraphicsResource pResource{nullptr};
			checkJetsonAPI(
				cuGraphicsEGLRegisterImage(&pResource, m_nvBufSurfaceRGBA->surfaceList[0].mappedAddr.eglImage,
					CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD),
				" Register EGL image failed");

			CUeglFrame eglFrame;
			checkJetsonAPI(cuGraphicsResourceGetMappedEglFrame(&eglFrame, pResource, 0, 0),
				" Get mapped EGL frame failed");

			if(m_workImage->width * 4 == eglFrame.pitch)
			{
				CHECK(cudaMemcpyAsync(eglFrame.frame.pPitch[0], m_workImage->buffer,
					m_workImage->width * m_workImage->height * 4, cudaMemcpyDeviceToDevice,
					*m_workImage->stream)); // *4 because 4 channel RGBA expected
			}
			else
			{
				// need to copy data line-by-line :(
				auto* base = (char*) eglFrame.frame.pPitch[0]; // for pointer arithmetics (not allowed on void*)
				for(auto line = 0; line < m_workImage->height; line++)
				{
					CHECK(cudaMemcpyAsync(base + line * eglFrame.pitch,
						m_workImage->buffer + line * m_workImage->width * 4, m_workImage->width * 4,
						cudaMemcpyDeviceToDevice,
						*m_workImage->stream)); // *4 because 4 channel RGBA expected
				}
			}
			
			// make sure copy and nvbuf context are ready
			cudaStreamSynchronize(*m_workImage->stream);
			checkJetsonAPI(cuCtxSynchronize(), "Error during context sync");

			// convert to YUV color format
			NvBufSurfTransformConfigParams transformConfigParams;
			transformConfigParams.gpu_id = 0;
			transformConfigParams.compute_mode =
				NvBufSurfTransformCompute_VIC; // uses Jetson internals, no need to use cudaStream
			transformConfigParams.cuda_stream = *(m_workImage->stream); // propably ignored on Jetson

			checkNvBufAPI(NvBufSurfTransformSetSessionParams(&transformConfigParams),
				" Error during NvBufSurfTransformSetSessionParams");
			NvBufSurfTransformRect srcRect;
			srcRect.top = 0;
			srcRect.left = 0;
			srcRect.width = m_workImage->width;
			srcRect.height = m_workImage->height;

			NvBufSurfTransformRect dstRect;
			dstRect.top = 0;
			dstRect.left = 0;
			dstRect.width = m_workImage->width;
			dstRect.height = m_workImage->height;

			NvBufSurfTransformParams transformParams;
			transformParams.src_rect = &srcRect;
			transformParams.dst_rect = &dstRect;
			transformParams.transform_flag = NVBUFSURF_TRANSFORM_FILTER;
			transformParams.transform_flip = NvBufSurfTransform_None;
			transformParams.transform_filter =
				NvBufSurfTransformInter_Default; // GPU-nearest, VIC-nearest on Jetson
			checkNvBufAPI(NvBufSurfTransform(m_nvBufSurfaceRGBA, m_nvBufSurfaceYUV, &transformParams),
				"Error during RGBA to YUV mapping");

			checkJetsonAPI(cuCtxSynchronize(), "Error during context sync");

			NvBufSurfaceDestroy(m_nvBufSurfaceRGBA);
			checkJetsonAPI(cuGraphicsUnregisterResource(pResource), " Failed to unregister resource");

			int fd = m_nvBufSurfaceYUV->surfaceList[0].bufferDesc;

			// possibly dump YUV results here to debug

			NvJPEGEncoder* jpegenc = NvJPEGEncoder::createJPEGEncoder("jpenenc");
			unsigned long out_buf_size = m_workImage->width * m_workImage->height * 3 / 2;
			unsigned char* out_buf = new unsigned char[out_buf_size];
			auto ret = jpegenc->encodeFromFd(fd, JCS_YCbCr, &out_buf, out_buf_size, m_jpegQuality);
			if(ret < 0)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::NvjpegEncoder (Tegra Multimedia API): JPEG encoding failed";
				throw std::runtime_error("NvjpegEncoderTegraMultimediaAPI: jetson_multimedia_api error");
			}
			m_workData->resize(out_buf_size);
			memcpy(m_workData->data(), out_buf, out_buf_size);

			NvBufSurfaceDestroy(m_nvBufSurfaceYUV);
			delete(jpegenc);
			m_workDone = true;
			m_workAvailable = false;
			m_cvWork.notify_one();
		}

		if(m_stopFlag)
		{
			break;
		}
	}
}

}
}