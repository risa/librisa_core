#include <risaCore/Basics/enums.h>
namespace risa::cuda::detail
{
auto getBytesPerElem(dataType type) -> int
{
	constexpr const int NUM_BYTES_1{1};
	constexpr const int NUM_BYTES_2{2};
	constexpr const int NUM_BYTES_4{4};
	constexpr const int NUM_BYTES_8{8};
	constexpr const int NUM_BYTES_VAR{-1};

	switch(type)
	{
	case detail::dataType::t_bool:
	case detail::dataType::t_uint8:
	case detail::dataType::t_int8:
	{
		return NUM_BYTES_1;
	}
	case detail::dataType::t_int16:
	case detail::dataType::t_uint16:
	{
		return NUM_BYTES_2;
	}
	case detail::dataType::t_int32:
	case detail::dataType::t_uint32:
	case detail::dataType::t_float:
	{
		return NUM_BYTES_4;
	}
	case detail::dataType::t_int64:
	case detail::dataType::t_uint64:
	case detail::dataType::t_double:
	{
		return NUM_BYTES_8;
	}
	case detail::dataType::t_unknown:
	case detail::dataType::t_string:
	{
		return NUM_BYTES_VAR;
	}
	}
	// in C++23: std::unreachable();
	return NUM_BYTES_VAR;
	
}
} // namespace risa::cuda::detail
