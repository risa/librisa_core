// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/UtilityKernels.h>
#include <risaCore/Basics/NvjpegEncoder.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/WebCompatibleSaver.h>

#include <glados/Image.h>
#include <glados/MemoryPool.h>
#include <glados/observer/FileObserver.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <iostream>
#include <string>

namespace risa
{

namespace cuda
{

WebCompatibleSaver::WebCompatibleSaver(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::WebCompatibleSaver: Configuration file could not be loaded successfully.");
	}

	bool created = glados::utils::FileSystemHandle::createDirectory(m_outputPath);
	if(!created)
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::WebCompatibleSaver: Could not create target directory at "
								 << m_outputPath;

	if(!glados::utils::FileSystemHandle::setFilePermissionsWriteAll(m_outputPath))
		BOOST_LOG_TRIVIAL(error)
			<< "risa::cuda::WebCompatibleSaver:: Cannot grant write access to output path'" << m_outputPath
			<< "'.";

	for(auto i = 0; i < m_numberOfPlanes; i++)
	{
		m_tmrs.emplace_back();
		m_tmrs[i].start();
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 1));
		m_streams[i] = stream;
	}

	// intialize data structures for image processing before write
	m_sharedMemBytes = detail::ReduceProperty::getSharedMemSize(m_blockSize2D);
	m_numThreads = m_blockSize2D * m_blockSize2D;

	// div by 2 because each block reads data from an area twice its size
	m_numBlocks = GET_MAX((m_imageWidth * m_imageHeight + m_numThreads - 1) / m_numThreads / 2, 1);

	dimBlock = dim3(m_blockSize2D, m_blockSize2D);
	dimGrid =
		dim3((int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	auto reducePropertyArraySize = detail::ReduceProperty::getReducePropertyArraySize(m_numBlocks);
	m_minMaxVals_d =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(reducePropertyArraySize);
	m_minMaxVals_h = std::vector<float>(reducePropertyArraySize, 0.0);

	// initialize 8-bit image buffers
#ifdef LIBRISACORE_HAS_NVJPEG
	m_image8bit_d = glados::cuda::make_device_ptr<unsigned char, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * 3); // space for 3-channel image (RGB)
#elif defined(LIBRISACORE_HAS_JETSON_MULTIMEDIA_API)
	m_image8bit_d = glados::cuda::make_device_ptr<unsigned char, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * 4); // space for 4-channel image (RGBA)
#endif

	NvjpegEncoder::getInstance().init(m_jpegQuality);

	for(auto i = 0; i < m_numberOfPlanes; i++)
	{
		// accumulating min/max values (until reconfig)
		m_imageMinMaxVals.push_back(FLT_MAX); // intialize min value to FLT_MAX
		m_imageMinMaxVals.push_back(FLT_MIN); // intialize max value to FLT_MIN

		// init timers to limit output rate
		m_tmrs.emplace_back();
		m_tmrs[i].start();
		m_firstImageWritten.push_back(false);

		// check file handle for writing
		std::string fullPath = m_outputPath + m_fileName + "_plane" + std::to_string(i)
			+ (m_encodeResultsInBase64 ? ".txt" : ".jpg");
		auto ofStream = std::ofstream(fullPath, std::ios::out | std::ios::binary | std::ios::trunc);

		if(ofStream.fail())
		{
			BOOST_LOG_TRIVIAL(error) << "risa::cuda::WebCompatibleSaver::  File '" << fullPath
									 << "'could not be created.";
			throw std::runtime_error("File could not be created. Please check!");
		}
		ofStream.close();
		m_ofstreams.emplace_back(std::move(ofStream));
	}
}

WebCompatibleSaver::~WebCompatibleSaver()
{
	for(auto i = 0; i < m_numberOfPlanes; ++i)
	{
		m_ofstreams[i].close();
	}

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStreamDestroy(m_streams[i]);
	}

}

auto WebCompatibleSaver::saveImage(glados::Image<manager_type> image) -> void
{
	if(!image.valid())
		return;

	auto const deviceID = image.device();
	auto const plane = image.plane() < m_numberOfPlanes ? image.plane() : 0UL;

	m_tmrs[plane].stop();

	if(m_firstImageWritten[plane] && m_tmrs[plane].elapsed() < 0.1)
		return;

	// Strategy:
	// 1. find min/max in given float image
	// 2. normalize and convert image with these min/max values into 8-bit unsigned char image
	// 3. encode jpeg with nvjpeg
	// 4. retrieve bitstream and save
	// 5. optionally encode results in base64

	CHECK(cudaSetDevice(deviceID));

	if(!m_setManualMinMaxVals)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: Frame [" << image.index()
								 << "] [auto]: Determine min/max...";

		// 1. find min/max in given image:
		switch(m_blockSize2D * m_blockSize2D)
		{
		case 256:
			reduceKernel<256, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum>
				<<<m_numBlocks, m_numThreads, m_sharedMemBytes, m_streams[deviceID]>>>(
					image.data(), m_imageHeight * m_imageWidth, m_minMaxVals_d.get());
			break;
		case 1024:
			reduceKernel<1024, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum>
				<<<m_numBlocks, m_numThreads, m_sharedMemBytes, m_streams[deviceID]>>>(
					image.data(), m_imageHeight * m_imageWidth, m_minMaxVals_d.get());
			break;
		}
		CHECK(cudaPeekAtLastError());
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		cudaMemcpyAsync(m_minMaxVals_h.data(), m_minMaxVals_d.get(), m_minMaxVals_h.size() * sizeof(float),
			cudaMemcpyDeviceToHost, m_streams[deviceID]);
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(auto i = 0; i < m_numBlocks; i++)
		{
			const auto minIdx = detail::ReduceProperty::getNthMinIdx(m_numBlocks, i);
			m_imageMinMaxVals[2 * plane] = GET_MIN(m_imageMinMaxVals[2 * plane], m_minMaxVals_h[minIdx]);

			const auto maxIdx = detail::ReduceProperty::getNthMaxIdx(m_numBlocks, i);
			m_imageMinMaxVals[2 * plane + 1] =
				GET_MAX(m_imageMinMaxVals[2 * plane + 1], m_minMaxVals_h[maxIdx]);
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: Frame [" << image.index()
								 << "] [auto]:" << m_imageMinMaxVals[2 * plane] << " ... "
								 << m_imageMinMaxVals[2 * plane + 1];

		/* if(m_storeAutomaticValues)
		{
			m_manualMinMaxVals = m_imageMinMaxVals;
		}*/
	}

	// 2. normalize and convert into 8-bit single channel image
	auto normalizeMinMaxVals = m_setManualMinMaxVals ? m_manualMinMaxVals : m_imageMinMaxVals;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: Frame [" << image.index()
							 << "]:" << normalizeMinMaxVals[2 * plane] << " ... "
							 << normalizeMinMaxVals[2 * plane + 1];
	
	// TODO: WebCompatibleSaver shouldn't need to care about whether the jpeg encoder wants RBG or RGBA. Ideally, it should always take either
#ifdef LIBRISACORE_HAS_NVJPEG
	normalizeAndConvertTo8BitRGB<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(image.data(),
		m_image8bit_d.get(), m_imageWidth, m_imageHeight, normalizeMinMaxVals[2 * plane],
		normalizeMinMaxVals[2 * plane + 1]);
#elif defined(LIBRISACORE_HAS_JETSON_MULTIMEDIA_API)
	normalizeAndConvertTo8BitRGBA<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(image.data(),
		m_image8bit_d.get(), m_imageWidth, m_imageHeight, normalizeMinMaxVals[2 * plane],
		normalizeMinMaxVals[2 * plane + 1]);
#endif
	CHECK(cudaPeekAtLastError());

	// 3 encode to JPEG
	CHECK(cudaStreamSynchronize(m_streams[deviceID]));

	image_props imageStruct;
	imageStruct.height = m_imageHeight;
	imageStruct.width = m_imageWidth;
	imageStruct.plane = plane;
	imageStruct.buffer = m_image8bit_d.get();
	imageStruct.stream = &m_streams[deviceID];
	
	NvjpegEncoder::getInstance().encodeImage(&imageStruct, m_jpeg);

	// 4. retrieve and save
	const auto uncompressedSize = m_imageWidth * m_imageHeight * sizeof(float);
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: Frame [" << image.index()
							 << "]: Encoded image size: " << m_jpeg.size() << "B (input was "
							 << uncompressedSize << "B -> -" << 100 - 100 * m_jpeg.size() / uncompressedSize
							 << "% )";

	// 5. optionally encode in base64
	if(m_encodeResultsInBase64)
	{
		encodeInBase64(m_jpeg, m_includeMinMaxInBase64, normalizeMinMaxVals[2 * plane],
			normalizeMinMaxVals[2 * plane + 1], image.index());
	}

	std::string fullPath = m_outputPath + m_fileName + "_plane" + std::to_string(plane)
		+ (m_encodeResultsInBase64 ? ".txt" : ".jpg");

	m_ofstreams[plane].open(fullPath, std::ios::out | std::ios::binary | std::ios::trunc);
	if(m_ofstreams[plane].good())
	{
		m_ofstreams[plane].write((char*)m_jpeg.data(), m_jpeg.size());
	}
	else
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::WebCompatibleSaver: Unable to write image data. Is the file '" << fullPath
			<< "' open in another program?";
		throw std::runtime_error("risa::cuda::WebCompatibleSaver: Unable to write image data.");
	}

	m_ofstreams[plane].close();

	m_firstImageWritten[plane] = true;
	m_tmrs[plane].start();
}

auto WebCompatibleSaver::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: ###### UPDATE ###### -> " << fo->m_fileName
							 << ": " << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::WebCompatibleSaver: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool wantsManualMinMaxVals;
			if(!configReader.lookupValue("setManualMinMaxValues", wantsManualMinMaxVals))
				wantsManualMinMaxVals = m_setManualMinMaxVals;

			// pre-emptively tell the saveImage function to no longer overwrite the manual manuals
			// in case we're going to update them. Don't lock though, because it really doesn't
			// matter if we don't update the min/max's each frame anyways - in only matters that
			// we do not accidently overwrite the new value we've got from the update function.
			m_storeAutomaticValues = !wantsManualMinMaxVals;

			std::vector<float> minVals, maxVals;
			configReader.lookupValue("manualMinValues", minVals);
			configReader.lookupValue("manualMaxValues", maxVals);

			if(m_manualMinMaxVals.size() == m_numberOfPlanes * 2)
			{
				// we allready have valid manual values, update as many as available
				for(auto i = 0; i < minVals.size(); ++i)
					m_manualMinMaxVals[2 * i] = minVals[i];
				for(auto i = 0; i < maxVals.size(); ++i)
					m_manualMinMaxVals[2 * i + 1] = maxVals[i];
			}
			else
			{
				// no valid values yet, we need a valid number of values to build the vector
				if(minVals.size() == m_numberOfPlanes && maxVals.size() == m_numberOfPlanes)
				{
					for(auto i = 0; i < m_numberOfPlanes; ++i)
					{
						m_manualMinMaxVals.push_back(minVals[i]);
						m_manualMinMaxVals.push_back(maxVals[i]);
					}
				}
			}

			if(wantsManualMinMaxVals && m_manualMinMaxVals.size() != m_numberOfPlanes * 2)
			{
				// user wants to set manual values but has no values provided
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::WebCompatibleSaver: Requested manual min/max "
											"values without providing values";
				wantsManualMinMaxVals = false;
			}
			m_setManualMinMaxVals = wantsManualMinMaxVals;
			m_storeAutomaticValues = !wantsManualMinMaxVals;
		}
	}
}

auto WebCompatibleSaver::encodeInBase64(std::vector<unsigned char>& data, bool includeMinMax, float minVal,
	float maxVal, size_t imageIndex) -> void
{
	auto b64 = base64_encode(data.data(), data.size());
	size_t pos0 = 0;

	if(includeMinMax)
	{
		// we dump the raw bits for the min and max value float into the base64 encoded string
		// Caution: to properly interpret the results, encoding and decoding must happen on similar
		// machines (i.e. same endianess etc).
		// we also have to encode this header such that it can be transferred in char stream (e.g. text via
		// websocket)
		float metaData[3] = {(float)imageIndex, minVal, maxVal};
		auto metaDataB64 = base64_encode((unsigned char*)metaData, sizeof(float) * 3);
		std::copy(metaDataB64.begin(), metaDataB64.end(), data.begin());
		pos0 = metaDataB64.size();
	}
	data.resize(pos0 + b64.size());
	std::copy(b64.begin(), b64.end(), data.begin() + pos0);
}

auto WebCompatibleSaver::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".filePath", m_outputPath)
		&& configReader.lookupValue(parameterSet + ".fileName", m_fileName)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes)
		&& configReader.lookupValue(parameterSet + ".jpegQuality", m_jpegQuality))
	{
		if(m_outputPath.back() != '/')
			m_outputPath.push_back('/');

		if(configReader.lookupValue(parameterSet + ".encodeResultsInBase64", m_encodeResultsInBase64))
		{
			configReader.lookupValue(parameterSet + ".includeMinMaxInBase64", m_includeMinMaxInBase64);
		}
		else
		{
			m_encodeResultsInBase64 = false;
		}

		if(!configReader.lookupValue(parameterSet + ".setManualMinMaxValues", m_setManualMinMaxVals))
		{
			m_setManualMinMaxVals = false;
		}

		if(m_setManualMinMaxVals)
		{
			std::vector<float> manualMinVals, manualMaxVals;
			if(!configReader.lookupValue(parameterSet + ".manualMinValues", manualMinVals)
				|| !configReader.lookupValue(parameterSet + ".manualMaxValues", manualMaxVals))
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::WebCompatibleSaver: Manual min/max values active "
											"but no values provided. Using automatic values.";
				m_setManualMinMaxVals = false;
			}
			else if(manualMinVals.size() < m_numberOfPlanes)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::WebCompatibleSaver: Manual min/max values active "
					   "but number of provided min values does not cover number of planes. Expected "
					<< m_numberOfPlanes << ", got " << manualMinVals.size() << ".";
				return false;
			}
			else if(manualMaxVals.size() < m_numberOfPlanes)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::WebCompatibleSaver: Manual min/max values active "
					   "but number of provided max values does not cover number of planes. Expected "
					<< m_numberOfPlanes << ", got " << manualMaxVals.size() << ".";
				return false;
			}
			for(auto i = 0; i < m_numberOfPlanes; ++i)
			{
				m_manualMinMaxVals.push_back(manualMinVals[i]);
				m_manualMinMaxVals.push_back(manualMaxVals[i]);
			}
			m_storeAutomaticValues = false;
		}
		else
		{
			m_storeAutomaticValues = true;
		}

		return true;
	}

	return false;
}
}
}
