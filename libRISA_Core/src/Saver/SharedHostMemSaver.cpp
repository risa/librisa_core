// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/SharedHostMemSaver.h>

#include <glados/Image.h>
#include <glados/MemoryPool.h>
#include <glados/Utils.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <unistd.h>

#ifdef __cpp_lib_span
#include <span>
#endif

namespace risa
{

SharedHostMemSaver::SharedHostMemSaver(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::SharedHostMemSaver: Configuration file could not be loaded successfully.");
	}

	m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
		(int)m_numberOfPlanes, m_imageWidth * m_imageHeight);

	/* initialize shared memory for interprocess communication */
	if(m_outputPath.back() != '/')
	{
		m_outputPath.append("/");
	}
	m_backingFile = m_fileName;

	m_fileDescriptor = shm_open(m_backingFile.c_str(), O_RDWR | O_CREAT,
		S_IRWXU | S_IRWXG | S_IRWXO); // request write access, all others may read

	if(m_fileDescriptor == -1)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::SharedHostMemSaver: Cannot create shared memory segment: "
								 << strerror(errno);
		throw std::runtime_error("risa::SharedHostMemSaver: Cannot create shared memory segment.");
	}

	m_smemBytes = m_imageWidth * m_imageHeight * m_numberOfPlanes * sizeof(float);
	int fallocateErr = posix_fallocate(m_fileDescriptor, 0, (long)m_smemBytes); // allocate shared mem size
	if(fallocateErr != 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::SharedHostMemSaver: Cannot allocate shared memory segment: "
								 << strerror(fallocateErr);
		throw std::runtime_error("risa::SharedHostMemSaver: Cannot allocate shared memory segment.");
	}

	glados::utils::FileSystemHandle::setFilePermissionsAllAll("/dev/shm/" + m_fileName);

	m_memptr = static_cast<float*>(mmap(nullptr,
		m_smemBytes, // size
		PROT_READ | PROT_WRITE, // req read and write access
		MAP_SHARED, // make visibile to other processes
		m_fileDescriptor,
		0)); // offset 0

	close(m_fileDescriptor); // may be closed after mmap operation

	if(m_memptr == MAP_FAILED)
	{
		throw std::runtime_error("risa::SharedHostMemSaver: Cannot map shared memory segment.");
	}

	BOOST_LOG_TRIVIAL(debug) << ">>>>> backingfile: shared mem address: " << m_memptr << ", sz:" << m_smemBytes;
	BOOST_LOG_TRIVIAL(debug) << ">>>>> backingfile: backing file:       /dev/shm/" << m_backingFile;

	m_semptr = sem_open(parameterSet.c_str(), O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO,
		0); // NOLINT(cppcoreguidelines-pro-type-vararg)
	if(m_semptr == SEM_FAILED)
	{
		throw std::runtime_error("risa::SharedHostMemSaver: Cannot open semaphore.");
	}
	glados::utils::FileSystemHandle::setFilePermissionsAllAll("/dev/shm/sem." + parameterSet);

	for(size_t i = 0; i < m_numberOfPlanes; i++)
	{
		m_tmrs.emplace_back();
		m_tmrs[i].start();
	}

	BOOST_LOG_TRIVIAL(info) << "risa::SharedHostMemSaver<" << parameterSet << ">: Initialization done.";
}

// NOLINTBEGIN(bugprone-exception-escape)
SharedHostMemSaver::~SharedHostMemSaver()
{
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": unmap memory";
	munmap(m_memptr, m_smemBytes); /* unmap the storage */
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": sem_close";
	sem_close(m_semptr);
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": sem_unlink";
	sem_unlink(m_parameterSet.c_str());
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": shm_unlink";
	shm_unlink(m_backingFile.c_str());
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": free glados";
	glados::MemoryPool<manager_type>::getInstance().freeMemory(m_memoryPoolIndex);
	BOOST_LOG_TRIVIAL(debug) << m_parameterSet << ": unmap memory";
}
// NOLINTEND(bugprone-exception-escape)

auto SharedHostMemSaver::saveImage(glados::Image<manager_type> image) -> void
{
	if(!image.valid())
	{
		return;
	}

	m_tmrs[image.plane()].stop();
	double diff = m_tmrs[image.plane()].elapsed();
	constexpr const double MIN_DURATION_BETWEEN_WRITES{0.1};
	if(diff < MIN_DURATION_BETWEEN_WRITES)
	{
		return;
	}

	int semValue{0};
	if((sem_getvalue(m_semptr, &semValue) == 0))
	{
		// check if reading process has already consumed the data and decremented the semaphore to zero
		if(semValue == 0)
		{
			// copy img to shared memory -> GUI will access this later
#ifdef __cpp_lib_span
			std::span imgData(m_memptr, m_smemBytes / sizeof(float));
			memcpy(&imgData[image.plane() * m_imageWidth * m_imageHeight], image.data(),
				m_imageWidth * m_imageHeight * sizeof(float));
#else
			memcpy(m_memptr + image.plane() * m_imageWidth * m_imageHeight, image.data(),
				m_imageWidth * m_imageHeight * sizeof(float));
#endif

			// indicate memory ready with semaphore -> increment semaphore only once per set of images
			if(image.plane() == 0)
			{
				if(sem_post(m_semptr) < 0)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::SharedHostMemSaver: Unable to increment semaphore"; /* throw?? */
				};
			}
		}
	}
	else
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::SharedHostMemSaver: Unable to get semaphore value"; /* throw?? */
	}
	m_tmrs[image.plane()].start();
}

// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
auto SharedHostMemSaver::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string mode;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".filePath", m_outputPath)
		&& configReader.lookupValue(parameterSet + ".fileName", m_fileName)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
	{
		return true;
	}

	return false;
}

}
