// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/HDF5Saver.h>

#include <risaCore/Basics/enums.h>

#include <glados/Image.h>
#include <glados/MemoryPool.h>
#include <glados/observer/FileObserver.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <filesystem>
#include <iostream>
#include <string>

namespace risa
{

namespace cuda
{

HDF5Saver::HDF5Saver(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::HDF5Saver: Configuration file could not be loaded successfully.");
	}

	bool created = glados::utils::FileSystemHandle::createDirectory(m_outputPath);
	if(!created)
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Could not create target directory at " << m_outputPath;

	if(!glados::utils::FileSystemHandle::setFilePermissionsWriteAll(m_outputPath))
		BOOST_LOG_TRIVIAL(error) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">:: Cannot grant write access to output path'" << m_outputPath << "'.";

	if(!initializeHDF5())
	{
		throw std::runtime_error(
			"risa::cuda::HDF5Loader: HDF5 configuration could not be initialized successfully.");
	}

	m_bytesPerFrame = m_imageWidth * m_imageHeight * sizeof(float);
	m_dataBuffer = (float*)malloc(m_bytesPerFrame);

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 1));
		m_streams[i] = stream;
	}
}

HDF5Saver::~HDF5Saver()
{

	free(m_dataBuffer);
	for(auto b : m_buf_meta)
	{
		free(b);
	}

	H5Dclose(m_ds_data);
	H5Sclose(m_sp_data);

	for(auto ds : m_ds_meta)
	{
		H5Dclose(ds);
	}
	for(auto sp : m_sp_meta)
	{
		H5Sclose(sp);
	}

	H5Fclose(m_HDF5File);
}

auto HDF5Saver::saveImage(glados::Image<manager_type> image) -> void
{
	if(image.valid())
	{
		auto const deviceID = image.device();
		CHECK(cudaSetDevice(deviceID));

		// per default, each buffer will contain one slice.
		// If the property is set, write more than one
		size_t slicesPerBuffer = 1;
		std::ignore = image.getProperty("slicesPerBuffer", slicesPerBuffer);

		for(auto sliceInBuffer = 0; sliceInBuffer < slicesPerBuffer; sliceInBuffer++)
		{
			CHECK(cudaMemcpyAsync(m_dataBuffer, &image.data()[sliceInBuffer * m_bytesPerFrame],
				m_bytesPerFrame, cudaMemcpyDeviceToHost, m_streams[deviceID]));

			if(!m_firstFrameParsed)
			{
				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Populating descriptor list.";
				if(!populatePropDescrList(&image))
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Requested properties missing on the image.";
					throw std::runtime_error(
						"risa::cuda::HDF5Saver: Requested properties missing on the image.");
				}

				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Initializing metadata datasets.";
				if(!initializeMetaDataDatasets())
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Cannot initialize datasets for meta data.";
					throw std::runtime_error(
						"risa::cuda::HDF5Saver: Cannot initialize data sets for meta data.");
				}

				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Writing attributes.";
				if(!writeAttributes(&image))
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Cannot write attributes.";
					throw std::runtime_error("risa::cuda::HDF5Saver: Cannot write attributes.");
				}
				m_firstFrameParsed = true;
			}

			// extend metadata datasets and write data to file
			for(auto i = 0; i < m_ds_meta.size(); i++)
			{
				if(H5Dset_extent(m_ds_meta[i], m_count_meta) < 0)
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Cannot extend dataset '" << m_metaNames[i] << "'.";
					throw std::runtime_error("risa::cuda::HDF5Saver: Cannot extend dataset.");
				}
				if(H5Sselect_hyperslab(
					   m_sp_meta[i], H5S_SELECT_SET, m_offset_meta, NULL, m_chunk_sz_meta, NULL)
					< 0)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Cannot select hyperslab in dataset '" << m_metaNames[i] << "'.";
					throw std::runtime_error("risa::cuda::HDF5Saver: Cannot set hyperslab.");
				}
				// get value from prop and write
				if(!readPropertyIntoBuffer(&image, &m_sz_meta[i], &m_buf_meta[i], m_propDescriptors[i]))
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Cannot read value of property '"
						<< m_metaNames[i] << "'.";
					throw std::runtime_error("risa::cuda::HDF5Saver: Cannot write data.");
				}
				if(H5Dwrite_chunk(
					   m_ds_meta[i], H5P_DEFAULT, H5P_DEFAULT, m_offset_meta, m_sz_meta[i], m_buf_meta[i])
					< 0)
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Cannot write to dataset '" << m_metaNames[i] << "'.";
					throw std::runtime_error("risa::cuda::HDF5Saver: Cannot write data.");
				}
			}

			// sync stream
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			// extend dataset and write data to file
			if(H5Dset_extent(m_ds_data, m_count_data) < 0)
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
										 << ">: Cannot extend dataset '" << m_datasetName << "'.";
				throw std::runtime_error("risa::cuda::HDF5Saver: Cannot extend dataset.");
			}
			if(H5Sselect_hyperslab(m_sp_data, H5S_SELECT_SET, m_offset_data, NULL, m_chunk_sz_data, NULL) < 0)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Cannot select hyperslab in dataset '"
					<< m_datasetName << "'.";
				throw std::runtime_error("risa::cuda::HDF5Saver: Cannot set hyperslab.");
			}
			if(H5Dwrite_chunk(
				   m_ds_data, H5P_DEFAULT, H5P_DEFAULT, m_offset_data, m_bytesPerFrame, m_dataBuffer))
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
										 << ">: Cannot write to dataset '" << m_datasetName << "'.";
				throw std::runtime_error("risa::cuda::HDF5Saver: Cannot write data.");
			}
			// update counts
			m_count_data[detail::H5DIM::SLICES]++;
			m_count_meta[0]++;

			m_offset_data[detail::H5DIM::SLICES]++;
			m_offset_meta[0]++;
		}
	}
	else
	{
		// finalize hdf5 file (?)
		// handles will be closed in destructor anyways
	}
}

auto HDF5Saver::update(glados::Subject* s) -> void {}

auto HDF5Saver::readPropertyIntoBuffer(
	glados::Image<manager_type>* image, size_t* sz, void** buf, detail::GetPropertyDescriptor pd) -> bool
{
	switch(pd.m_propType)
	{
	case detail::GetPropertyType::floatingPoint:
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Reading '"
								 << pd.m_propKey << "' as type 'float'";
		float val;
		if(image->getProperty(pd.m_propKey, val) != glados::details::PropertyReturnCode::Success)
		{
			return false;
		}
		*sz = sizeof(val);
		memcpy(*buf, &val, sizeof(val));
		return true;
	}
	case detail::GetPropertyType::integer:
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Reading '"
								 << pd.m_propKey << "' as type 'size_t'";
		size_t val;
		if(image->getProperty(pd.m_propKey, val) != glados::details::PropertyReturnCode::Success)
		{
			return false;
		}
		*sz = sizeof(val);
		memcpy(*buf, &val, sizeof(val));
		return true;
	}
	case detail::GetPropertyType::signed_integer:
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Reading '"
								 << pd.m_propKey << "' as type 'ssize_t'";
		ssize_t val;
		if(image->getProperty(pd.m_propKey, val) != glados::details::PropertyReturnCode::Success)
		{
			return false;
		}
		*sz = sizeof(val);
		memcpy(*buf, &val, sizeof(val));
		return true;
	}
	case detail::GetPropertyType::timestamp:
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Reading '"
								 << pd.m_propKey << "' as type 'chrono::timestamp'";
		typedef std::chrono::high_resolution_clock m_clock;
		std::chrono::time_point<m_clock> val_t;
		if(image->getProperty(pd.m_propKey, val_t) != glados::details::PropertyReturnCode::Success)
		{
			return false;
		}
		size_t val = val_t.time_since_epoch().count();
		*sz = sizeof(val);
		memcpy(*buf, &val, sizeof(val));
		return true;
	}
	case detail::GetPropertyType::text:
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Reading '"
								 << pd.m_propKey << "' as type 'string'";
		std::string val;
		if(image->getProperty(pd.m_propKey, val) != glados::details::PropertyReturnCode::Success)
		{
			return false;
		}
		*sz = val.size();
		free(*buf);
		*buf = (void*)malloc(*sz);
		memcpy(*buf, val.c_str(), val.size());
		return true;
	}
	}

	return false;
}

auto HDF5Saver::writeAttributes(glados::Image<manager_type>* image) -> bool
{
	for(const auto& attrName : m_attrNames)
	{
		auto pd = setupPropertyDescriptor(image, attrName);

		// check if it exists
		if(H5Aexists_by_name(m_HDF5File, m_datasetName.c_str(), attrName.c_str(), H5P_DEFAULT) > 0)
		{
			if(m_overwritePolicy == detail::dataExistsPolicy::append)
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Existing attribute '" << attrName
					<< "' will be overwritten in append mode.";
			}
			H5Adelete_by_name(m_HDF5File, m_datasetName.c_str(), attrName.c_str(), H5P_DEFAULT);
		}

		// get value from image and write to attribute
		if(!writeAttributeValueFromProperty(image, pd))
		{
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::HDF5Saver<" << m_parameterSet
									   << ">: Error when writing attribute '" << pd.m_propKey << "'.";
			return false;
		}
	}

	return true;
}

auto HDF5Saver::writeAttributeValueFromProperty(
	glados::Image<manager_type>* image, detail::GetPropertyDescriptor pd) -> bool
{
	std::string key = pd.m_propKey;
	hid_t type_attr = getTypeIdFromPropType(pd.m_propType);
	if(type_attr == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Unable to determine data type for attribute '" << key << "'.";
		return false;
	}

	void* val = nullptr;
	// get value
	switch(pd.m_propType)
	{
	case detail::GetPropertyType::floatingPoint:
	{
		float val_f;
		if(image->getProperty(key, val_f) == glados::details::PropertyReturnCode::Success)
		{
			val = new float(val_f);
		}
	}
	case detail::GetPropertyType::integer:
	{
		size_t val_i;
		if(image->getProperty(key, val_i) == glados::details::PropertyReturnCode::Success)
		{
			val = new size_t(val_i);
		}
	}
	case detail::GetPropertyType::signed_integer:
	{
		ssize_t val_i;
		if(image->getProperty(key, val_i) == glados::details::PropertyReturnCode::Success)
		{
			val = new ssize_t(val_i);
		}
	}
	case detail::GetPropertyType::text:
	{
		std::string val_s;
		if(image->getProperty(key, val_s) == glados::details::PropertyReturnCode::Success)
		{
			val = new std::string(val_s);
		}
	}
	case detail::GetPropertyType::timestamp:
	{

		typedef std::chrono::high_resolution_clock m_clock;
		std::chrono::time_point<m_clock> val_t;
		if(image->getProperty(key, val_t) == glados::details::PropertyReturnCode::Success)
		{
			// write as unsigned long
			val = new size_t(val_t.time_since_epoch().count());
		}
	}
	}

	if(H5Tget_class(type_attr) == H5T_STRING)
	{
		H5Tset_size(type_attr, ((std::string*)val)->size());
	}

	// setup attribute
	hid_t sp_attr = H5Screate(H5S_SCALAR);
	if(sp_attr == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Unable to create data space for attribute '" << key << "'.";
		return false;
	}

	hid_t id_attr = H5Acreate_by_name(m_HDF5File, m_datasetName.c_str(), key.c_str(), type_attr, sp_attr,
		H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if(id_attr == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Unable to create attribute '" << key << "'.";
		return false;
	}

	// write data and close handles
	if(H5Tget_class(type_attr) == H5T_STRING)
	{
		H5Awrite(id_attr, type_attr, ((std::string*)val)->c_str());
		H5Tclose(type_attr);
	}
	else
	{
		H5Awrite(id_attr, type_attr, val);
	}

	H5Aclose(id_attr);
	H5Sclose(sp_attr);
	free(val);
	return true;
}

auto HDF5Saver::initializeMetaDataDatasets() -> bool
{

	// iterate meta data keys
	for(const auto& pd : m_propDescriptors)
	{
		// open or create respective dataset
		hid_t ds_meta, sp_meta, type_meta;
		std::string metaName = "/" + pd.m_propKey;
		if(H5Lexists(m_HDF5File, metaName.c_str(), H5P_DEFAULT) <= 0)
		{
			// does not exist
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet
									 << ">: Creating new metadata dataset '" << metaName << "'.";
			if(!createMetaDataset(metaName, pd, sp_meta, ds_meta, type_meta))
			{
				return false;
			}
		}
		else
		{
			// exist, check it
			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet
									 << ">: Checking existing metadata dataset '" << metaName << "'.";
			if(!checkMetaDataset(metaName, pd, sp_meta, ds_meta, type_meta))
			{
				return false;
			}
		}

		// store in hid_t vector
		m_type_meta.push_back(type_meta);
		m_ds_meta.push_back(ds_meta);
		m_sp_meta.push_back(sp_meta);

		// allocate buffer space
		switch(pd.m_propType)
		{
		case detail::GetPropertyType::floatingPoint:
		{
			m_sz_meta.push_back(sizeof(float));
			m_buf_meta.push_back(new float);
			break;
		}
		case detail::GetPropertyType::text:
		{
			m_sz_meta.push_back(0);
			m_buf_meta.push_back(nullptr);
			break;
		}
		case detail::GetPropertyType::integer:
		{
			m_sz_meta.push_back(sizeof(size_t));
			m_buf_meta.push_back(new size_t);
			break;
		}
		case detail::GetPropertyType::signed_integer:
		{
			m_sz_meta.push_back(sizeof(ssize_t));
			m_buf_meta.push_back(new ssize_t);
			break;
		}
		case detail::GetPropertyType::timestamp:
		{
			m_sz_meta.push_back(sizeof(size_t));
			m_buf_meta.push_back(new size_t);
			break;
		}
		}
	}
	return true;
}

auto HDF5Saver::setupPropertyDescriptor(glados::Image<manager_type>* image, std::string key)
	-> detail::GetPropertyDescriptor
{
	std::string val_s;
	if(image->getProperty(key, val_s) == glados::details::PropertyReturnCode::Success)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Property '" << key
								 << "' is of type 'string' (text)";
		return detail::GetPropertyDescriptor{detail::GetPropertyType::text, key};
	}

	float val_f;
	if(image->getProperty(key, val_f) == glados::details::PropertyReturnCode::Success)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Property '" << key
								 << "' is of type 'float' (floatingPoint)";
		return detail::GetPropertyDescriptor{detail::GetPropertyType::floatingPoint, key};
	}

	ssize_t val_i;
	if(image->getProperty(key, val_i) == glados::details::PropertyReturnCode::Success)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Property '" << key
								 << "' is of type 'ssize_t' (signed_integer)";
		return detail::GetPropertyDescriptor{detail::GetPropertyType::signed_integer, key};
	}

	size_t val_l;
	if(image->getProperty(key, val_l) == glados::details::PropertyReturnCode::Success)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Property '" << key
								 << "' is of type 'size_t' (integer)";
		return detail::GetPropertyDescriptor{detail::GetPropertyType::integer, key};
	}

	typedef std::chrono::high_resolution_clock m_clock;
	std::chrono::time_point<m_clock> val_t;
	if(image->getProperty(key, val_t) == glados::details::PropertyReturnCode::Success)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Property '" << key
								 << "' is of type 'chrono' (timestamp)";
		return detail::GetPropertyDescriptor{detail::GetPropertyType::timestamp, key};
	}

	throw std::runtime_error("risa::cuda::HDF5Saver: Cannot setup property descriptor.");
};

auto HDF5Saver::populatePropDescrList(glados::Image<manager_type>* image) -> bool
{
	auto keys = image->getPropertyKeys();

	auto hasAllRequestedKeys = [&](std::vector<std::string>& available, std::vector<std::string>& requested)
	{
		for(const auto& requestedKey : requested)
		{
			bool found = false;
			for(const auto& k : available)
			{
				if(k == requestedKey)
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Requested property '" << requestedKey
					<< "' is not available on the image.";
				return false;
			}
		}
		return true;
	};

	if(!m_metaNames.empty())
	{
		// check keys if all requested properties are available
		if(!hasAllRequestedKeys(keys, m_metaNames))
		{
			return false;
		}
	}
	else
	{
		m_metaNames = keys;
	}
	if(!m_attrNames.empty())
	{
		if(!hasAllRequestedKeys(keys, m_attrNames))
		{
			return false;
		}
	}

	// at this point, all requested keys are available. Store their respective type in the propDescr list
	for(const auto& k : m_metaNames)
	{
		auto pd = setupPropertyDescriptor(image, k);
		if(pd.m_propType == detail::GetPropertyType::text)
		{
			// cannot store strings in a dataset, append as attribute instead
			m_attrNames.push_back(k);
		}
		else
		{
			m_propDescriptors.push_back(pd);
		}
	}
	return true;
}

auto HDF5Saver::initializeHDF5() -> bool
{
	// check if file exists and act according to policy
	std::string fullPath = m_outputPath + m_fileName;
	std::filesystem::path path{fullPath};
	if(std::filesystem::exists(path))
	{
		// target HDF5 file already exists
		switch(m_overwritePolicy)
		{
		case detail::dataExistsPolicy::abort:
		{
			BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Target file '"
									 << m_fileName
									 << "' already exists. Aborting. Set "
										"policy to 'overwrite' or 'append' to commence.";
			return false;
		}
		case detail::dataExistsPolicy::append:
		{
			// check if the file contents meet the requirements to append data

			m_HDF5File = H5Fopen(fullPath.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
			if(m_HDF5File == H5I_INVALID_HID)
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Target file '"
										 << m_fileName << "' could not be opened as HDF5 file.";
				return false;
			}

			if(H5Lexists(m_HDF5File, m_datasetName.c_str(), H5P_DEFAULT) <= 0)
			{
				// dataset does not exist yet, create a new one
				return createDataset();
			}
			else
			{
				// dataset does exists, open
				m_ds_data = H5Dopen2(m_HDF5File, m_datasetName.c_str(), H5P_DEFAULT);
				if(m_ds_data == H5I_INVALID_HID)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Target dataset '"
						<< m_datasetName
						<< "' exists, but could not be opened. Possible name collision with an attribute.";
					return false;
				}
				m_sp_data = H5Dget_space(m_ds_data);
				if(m_sp_data == H5I_INVALID_HID)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Could not get dataspace for dataset '" << m_datasetName << "'.";
					return false;
				}
				// check dims
				auto ndims_data = H5Sget_simple_extent_ndims(m_sp_data);
				if(ndims_data != 3)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Expected dataset '"
						<< m_datasetName << "' to have 3 dimensions, got " << ndims_data << ".";
					return false;
				}
				hsize_t dims_data[3], maxdims_data[3];
				if(ndims_data != H5Sget_simple_extent_dims(m_sp_data, dims_data, maxdims_data))
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Unable to get dimensions of dataset '" << m_datasetName << "'.";
					return false;
				}
				if(m_imageHeight != dims_data[detail::H5DIM::HEIGHT]
					|| m_imageWidth != dims_data[detail::H5DIM::WIDTH])
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Expected data size with frame dimensions [" << m_imageWidth << "x"
						<< m_imageHeight << "], got [" << dims_data[detail::H5DIM::WIDTH] << "x"
						<< dims_data[detail::H5DIM::HEIGHT] << "]";
					return false;
				}
				if(maxdims_data[detail::H5DIM::SLICES] != H5S_UNLIMITED)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Dataset '" << m_datasetName
						<< "' is not extendable (max dims = current dims)";
					return false;
				}

				// check type
				hid_t type_data = H5Dget_type(m_ds_data);
				if(type_data == H5I_INVALID_HID)
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Cannot get type of dataset '" << m_datasetName << "'.";
					return false;
				}
				if(!H5Tequal(type_data, H5T_IEEE_F32LE))
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Dataset '" << m_datasetName
						<< "' has the wrong data type. Expected float (H5T_IEEE_F32LE).";
					return false;
				}

				// check chunk size
				hid_t cparms = H5Dget_create_plist(m_ds_data);
				if(cparms == H5I_INVALID_HID)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Cannot get parameters of dataset '" << m_datasetName << "'.";
					return false;
				}

				hid_t layout = H5Pget_layout(cparms);
				if(layout < 0)
				{
					BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
											 << ">: Cannot get layout of dataset '" << m_datasetName << "'.";
					return false;
				}

				if(layout != H5D_CHUNKED)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Layout of existing dataset '"
						<< m_datasetName << "' is not chunked.";
					return false;
				}

				int rank = H5Pget_chunk(cparms, 3, m_chunk_sz_data);
				if(rank < 0)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Cannot get chunk size of dataset '" << m_datasetName << "'.";
					return false;
				}

				if(rank != 3)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Expected chunk size with 3 dimensions, got " << rank << ".";
					return false;
				}

				if(m_chunk_sz_data[detail::H5DIM::HEIGHT] != m_imageHeight
					|| m_chunk_sz_data[detail::H5DIM::WIDTH] != m_imageWidth
					|| m_chunk_sz_data[detail::H5DIM::SLICES] != 1)
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "risa::cuda::HDF5Saver<" << m_parameterSet
						<< ">: Expected chunk size with dimensions [1x" << m_imageHeight << "x"
						<< m_imageWidth << "], got [" << m_chunk_sz_data[detail::H5DIM::SLICES] << "x"
						<< m_chunk_sz_data[detail::H5DIM::WIDTH] << "x"
						<< m_chunk_sz_data[detail::H5DIM::HEIGHT] << "]";
					return false;
				}

				m_count_data[detail::H5DIM::HEIGHT] = m_imageHeight;
				m_count_data[detail::H5DIM::WIDTH] = m_imageWidth;
				m_count_data[detail::H5DIM::SLICES] = dims_data[detail::H5DIM::SLICES] + 1;
				m_chunk_sz_data[detail::H5DIM::HEIGHT] = m_imageHeight;
				m_chunk_sz_data[detail::H5DIM::WIDTH] = m_imageWidth;
				m_chunk_sz_data[detail::H5DIM::SLICES] = 1;
				// initialize offset to last frame
				m_offset_data[detail::H5DIM::WIDTH] = 0;
				m_offset_data[detail::H5DIM::HEIGHT] = 0;
				m_offset_data[detail::H5DIM::SLICES] = dims_data[detail::H5DIM::SLICES];
			}
			return true;
		}
		case detail::dataExistsPolicy::overwrite:
		{
			// creating the file will truncate its contents automatically
			if(createHDF5File())
			{
				return createDataset();
			}
			return false;
		}
		}
	}
	else
	{
		// target HDF5 does not exist yet
		if(createHDF5File())
		{
			return createDataset();
		}
		return false;
	}

	return true;
}

auto HDF5Saver::getTypeIdFromPropType(detail::GetPropertyType propType) -> hid_t
{
	switch(propType)
	{
	case detail::GetPropertyType::integer:
		return H5T_STD_U64LE;
	case detail::GetPropertyType::signed_integer:
		return H5T_STD_I64LE;
	case detail::GetPropertyType::floatingPoint:
		return H5T_IEEE_F32LE;
	case detail::GetPropertyType::text:
	{
		// this may lead to ressource leakage! return value must be closed with H5Tclose()
		hid_t strType = H5Tcopy(H5T_C_S1);
		H5Tset_cset(strType, H5T_CSET_UTF8);
		return strType;
	}
	case detail::GetPropertyType::timestamp:
		return H5T_STD_U64LE;
	}
	return H5I_INVALID_HID;
}

auto HDF5Saver::checkMetaDataset(std::string metaName, detail::GetPropertyDescriptor pd, hid_t& sp_meta,
	hid_t& ds_meta, hid_t& type_meta) -> bool
{

	// dataset does exists, open
	ds_meta = H5Dopen2(m_HDF5File, metaName.c_str(), H5P_DEFAULT);
	if(ds_meta == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal)
			<< "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Target dataset '" << metaName
			<< "' exists, but could not be opened. Possible name collision with an attribute.";
		return false;
	}
	sp_meta = H5Dget_space(ds_meta);
	if(sp_meta == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Could not get dataspace for dataset '" << metaName << "'.";
		return false;
	}
	// check dims
	auto ndims_meta = H5Sget_simple_extent_ndims(sp_meta);
	if(ndims_meta != 1)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Expected meta dataset '"
								 << metaName << "' to have 1 dimension, got " << ndims_meta << ".";
		return false;
	}
	hsize_t dims_meta[1], maxdims_meta[1];
	if(ndims_meta != H5Sget_simple_extent_dims(sp_meta, dims_meta, maxdims_meta))
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Unable to get dimensions of dataset '" << metaName << "'.";
		return false;
	}
	if(m_offset_data[detail::H5DIM::SLICES] != dims_meta[0])
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Expected meta data with " << m_offset_data[2] << " entries, got "
								 << dims_meta[0]
								 << ". This will lead to inconsistent data. Delete file and retry.";
		return false;
	}
	if(maxdims_meta[0] != H5S_UNLIMITED)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Dataset '" << metaName
								 << "' is not extendable (max dims = current dims)";
		return false;
	}

	// check type
	type_meta = H5Dget_type(ds_meta);
	if(type_meta == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot get type of dataset '" << metaName << "'.";
		return false;
	}
	hid_t expectedType = getTypeIdFromPropType(pd.m_propType);
	std::string expectedTypeStr;
	switch(pd.m_propType)
	{
	case detail::GetPropertyType::integer:
		expectedTypeStr = "uint64_t (H5T_STD_U64LE)";
		break;
	case detail::GetPropertyType::signed_integer:
		expectedTypeStr = "int64_t (H5T_STD_I64LE)";
		break;
	case detail::GetPropertyType::floatingPoint:
		expectedTypeStr = "float (H5T_IEEE_F32LE)";
		break;
	case detail::GetPropertyType::text:
		expectedTypeStr = "string (H5T_C_S1)";
		break;
	case detail::GetPropertyType::timestamp:
		expectedTypeStr = "uint64_t (H5T_STD_U64LE)";
		break;
	}

	if(!H5Tequal(type_meta, expectedType))
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: Dataset '"
								 << m_datasetName << "' has the wrong data type. Expected " << expectedTypeStr
								 << ".";
		return false;
	}

	// check chunk size
	hid_t cparms = H5Dget_create_plist(ds_meta);
	if(cparms == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot get parameters of dataset '" << metaName << "'.";
		return false;
	}

	hid_t layout = H5Pget_layout(cparms);
	if(layout < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot get layout of dataset '" << metaName << "'.";
		return false;
	}

	if(layout != H5D_CHUNKED)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Layout of existing dataset '" << metaName << "' is not chunked.";
		return false;
	}

	int rank = H5Pget_chunk(cparms, 1, m_chunk_sz_meta);
	if(rank < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot get chunk size of dataset '" << metaName << "'.";
		return false;
	}

	if(rank != 1)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Expected chunk size with 1 dimension, got " << rank << ".";
		return false;
	}

	if(m_chunk_sz_meta[0] != 1)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Expected chunk dimensions of [1], got '" << m_chunk_sz_meta[0]
								 << "'.";
		return false;
	}

	// initialize offset to last frame
	m_offset_meta[0] = m_offset_data[detail::H5DIM::SLICES];
	m_count_meta[0] = dims_meta[0] + 1;
	m_chunk_sz_meta[0] = 1;
	return true;
}

auto HDF5Saver::createMetaDataset(std::string metaName, detail::GetPropertyDescriptor pd, hid_t& sp_meta,
	hid_t& ds_meta, hid_t& type_meta) -> bool
{
	if(m_HDF5File == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create dataset with invalid file handle.";
		return false;
	}

	if(m_offset_data[2] > 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Dataset will be appended to, but metaData does not exist yet. This "
									"will lead to inconsistent data. Delete file and retry.";
		return false;
	}
	m_count_meta[0] = 1;
	m_chunk_sz_meta[0] = 1;
	hsize_t max_count_meta[1]{H5S_UNLIMITED};

	hid_t cparms = H5Pcreate(H5P_DATASET_CREATE);
	if(H5Pset_chunk(cparms, 1, m_chunk_sz_meta) < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot set chunk size for '" << metaName << "'.";
		return false;
	}

	sp_meta = H5Screate_simple(1, m_count_meta, max_count_meta);
	if(sp_meta == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create data space for '" << metaName << "'.";
		return false;
	}

	type_meta = getTypeIdFromPropType(pd.m_propType);
	ds_meta = H5Dcreate2(m_HDF5File, metaName.c_str(), type_meta, sp_meta, H5P_DEFAULT, cparms, H5P_DEFAULT);
	if(ds_meta == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create data set '" << metaName << "'.";
		return false;
	}

	m_offset_meta[0] = 0;
	return true;
}

auto HDF5Saver::createDataset() -> bool
{
	if(m_HDF5File == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create dataset with invalid file handle.";
		return false;
	}
	m_count_data[detail::H5DIM::WIDTH] = m_imageWidth;
	m_count_data[detail::H5DIM::HEIGHT] = m_imageHeight;
	m_count_data[detail::H5DIM::SLICES] = 1;
	m_chunk_sz_data[detail::H5DIM::WIDTH] = m_count_data[detail::H5DIM::WIDTH];
	m_chunk_sz_data[detail::H5DIM::HEIGHT] = m_count_data[detail::H5DIM::HEIGHT];
	m_chunk_sz_data[detail::H5DIM::SLICES] = 1;
	hsize_t max_count_data[3];
	max_count_data[detail::H5DIM::WIDTH] = m_count_data[detail::H5DIM::WIDTH];
	max_count_data[detail::H5DIM::HEIGHT] = m_count_data[detail::H5DIM::HEIGHT];
	max_count_data[detail::H5DIM::SLICES] = H5S_UNLIMITED;

	m_sp_data = H5Screate_simple(3, m_count_data, max_count_data);
	if(m_sp_data == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create data space for '" << m_datasetName << "'.";
		return false;
	}

	hid_t cparms = H5Pcreate(H5P_DATASET_CREATE);
	if(H5Pset_chunk(cparms, 3, m_chunk_sz_data) < 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot set chunk size for '" << m_datasetName << "'.";
		return false;
	}

	m_ds_data = H5Dcreate2(
		m_HDF5File, m_datasetName.c_str(), H5T_IEEE_F32LE, m_sp_data, H5P_DEFAULT, cparms, H5P_DEFAULT);
	if(m_ds_data == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet
								 << ">: Cannot create data set '" << m_datasetName << "'.";
		return false;
	}

	m_offset_data[detail::H5DIM::WIDTH] = 0;
	m_offset_data[detail::H5DIM::HEIGHT] = 0;
	m_offset_data[detail::H5DIM::SLICES] = 0;
	return true;
}

auto HDF5Saver::createHDF5File() -> bool
{
	std::string fullPath = m_outputPath + m_fileName;
	m_HDF5File = H5Fcreate(fullPath.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	if(m_HDF5File == H5I_INVALID_HID)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Saver<" << m_parameterSet << ">: HDF5 File '"
								 << m_fileName << "' could not be created.";
		return false;
	}

	return true;
}

auto HDF5Saver::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string mode;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".outputPath", m_outputPath)
		&& configReader.lookupValue(parameterSet + ".fileName", m_fileName)
		&& configReader.lookupValue(parameterSet + ".datasetName", m_datasetName))
	{
		auto endsWith = [](std::string const& fullString, std::string const& ending) -> bool
		{
			if(fullString.length() >= ending.length())
			{
				return (
					0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
			}
			else
			{
				return false;
			}
		};

		if(!endsWith(m_fileName, ".h5") && !endsWith(m_fileName, ".hdf5"))
		{
			m_fileName.append(".hdf5");
		}

		if(!(m_datasetName[0] == '/'))
		{
			m_datasetName = "/" + m_datasetName;
		}

		if(!(m_outputPath.back() == '/'))
		{
			m_outputPath.push_back('/');
		}

		// optional:
		configReader.setAllowedToFail(true);
		// get a list of metadata per frame => prepare a dataset for each one
		configReader.lookupValue(parameterSet + ".metaDataNames", m_metaNames);
		// get a list of dataset attributes => these are written only once per dataset as attribute
		configReader.lookupValue(parameterSet + ".attributeNames", m_attrNames);
		configReader.setAllowedToFail(false);

		// set flag as to whether attrs are written
		m_firstFrameParsed = false;

		// get overwrite policy
		std::string policy("abort");
		if(!configReader.lookupValue(parameterSet + ".dataExistsPolicy", policy))
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "risa::cuda::HDF5Saver<" << m_parameterSet
				<< ">: No policy defined on how to handle existing data. Set to 'abort'.";
		}
		if(policy == "overwrite")
		{
			m_overwritePolicy = detail::dataExistsPolicy::overwrite;
		}
		else if(policy == "append")
		{
			m_overwritePolicy = detail::dataExistsPolicy::append;
		}
		else
		{
			m_overwritePolicy = detail::dataExistsPolicy::abort;
		}

		return true;
	}

	return false;
}
}
}
