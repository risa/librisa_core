// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/OpenCVViewer.h>

#include <risaCore/Basics/UtilityKernels.h>

#include <glados/Image.h>
#include <glados/MemoryPool.h>
#include <glados/observer/FileObserver.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace risa
{

namespace cuda
{

OpenCVViewer::OpenCVViewer(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::OpenCVViewer: Configuration file could not be loaded successfully.");
	}

	for(auto i = 0; i < m_numberOfPlanes; i++)
	{
		m_tmrs.emplace_back();
		m_tmrs[i].start();
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 1));
		m_streams[i] = stream;
	}

	// intialize data structures for image processing before write
	m_sharedMemBytes = detail::ReduceProperty::getSharedMemSize(m_blockSize2D);
	m_numThreads = m_blockSize2D * m_blockSize2D;

	// div by 2 because each block reads data from an area twice its size
	m_numBlocks = GET_MAX((m_imageWidth * m_imageHeight + m_numThreads - 1) / m_numThreads / 2, 1);

	dimBlock = dim3(m_blockSize2D, m_blockSize2D);
	dimGrid =
		dim3((int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	minMaxVals_d_ = glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(2 * m_numBlocks);
	minMaxVals_h_ = std::vector<float>(2 * m_numBlocks, 0.0);

	// init buffers
	image8bit_d_ = glados::cuda::make_device_ptr<unsigned char, glados::cuda::async_copy_policy>(
		m_imageWidth * m_imageHeight * 3); // space for 3-channel image
	image8bit_h_.reserve(m_imageWidth * m_imageHeight * 3); // reserve space for 3 channels (?)

	for(auto i = 0; i < m_numberOfPlanes; i++)
	{
		// accumulating min/max values (until reconfig)
		m_imageMinMaxVals.push_back(FLT_MAX); // intialize min value to FLT_MAX
		m_imageMinMaxVals.push_back(FLT_MIN); // intialize max value to FLT_MIN

		// init timers to limit output rate
		m_tmrs.emplace_back();
		m_tmrs[i].start();
		m_firstImageWritten.push_back(false);
	}
}

OpenCVViewer::~OpenCVViewer()
{ 
	; // anything necessary?
}

auto OpenCVViewer::saveImage(glados::Image<manager_type> image) -> void
{
	if(!image.valid())
	{
		return;
	}

	m_tmrs[image.plane()].stop();

	if(m_firstImageWritten[image.plane()] && m_tmrs[image.plane()].elapsed() < 0.04) // i.e. 25Hz
		return;

	// Strategy:
	// 1. find min/max in given float image
	// 2. normalize and convert image with these min/max values into 8-bit unsigned char image
	// 3. retrieve image and display

	auto const deviceID = image.device();
	auto const plane =
		[&]() -> size_t
	{
		if(image.plane() < m_numberOfPlanes)
		{
			return image.plane();
		};
		return 0UL;
	}();

	CHECK(cudaSetDevice(deviceID));

	if(!m_setManualMinMaxVals)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::OpenCVViewer: Frame [" << image.index()
								 << "] [auto]: Determine min/max...";

		// 1. find min/max in given image:
		switch(m_blockSize2D * m_blockSize2D)
		{
		case 256:
			reduceKernel<256, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum>
				<<<m_numBlocks, m_numThreads, m_sharedMemBytes, m_streams[deviceID]>>>(
					image.data(), m_imageHeight * m_imageWidth, minMaxVals_d_.get());
			break;
		case 1024:
			reduceKernel<1024, detail::ReduceProperty::Maximum | detail::ReduceProperty::Minimum>
				<<<m_numBlocks, m_numThreads, m_sharedMemBytes, m_streams[deviceID]>>>(
					image.data(), m_imageHeight * m_imageWidth, minMaxVals_d_.get());
			break;
		}
		CHECK(cudaPeekAtLastError());

		cudaMemcpyAsync(minMaxVals_h_.data(), minMaxVals_d_.get(), 2 * m_numBlocks * sizeof(float),
			cudaMemcpyDeviceToHost, m_streams[deviceID]);
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		for(auto i = 0; i < m_numBlocks; i++)
		{
			const auto minIdx = detail::ReduceProperty::getNthMinIdx(m_numBlocks, i);
			m_imageMinMaxVals[2 * plane] = GET_MIN(m_imageMinMaxVals[2 * plane], minMaxVals_h_[minIdx]);

			const auto maxIdx = detail::ReduceProperty::getNthMaxIdx(m_numBlocks, i);
			m_imageMinMaxVals[2 * plane + 1] = GET_MAX(m_imageMinMaxVals[2 * plane + 1], minMaxVals_h_[maxIdx]);
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::OpenCVViewer: Frame [" << image.index()
								<< "] [auto]:" << m_imageMinMaxVals[2 * image.plane()] << " ... "
								<< m_imageMinMaxVals[2 * image.plane() + 1];

		if(m_storeAutomaticValues)
		{
			m_manualMinMaxVals = m_imageMinMaxVals;
		}
	}

	// 2. normalize and convert into 8-bit single channel image
	auto normalizeMinMaxVals = m_manualMinMaxVals;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::OpenCVViewer: Frame [" << image.index()
							<< "]:" << normalizeMinMaxVals[2 * image.plane()] << " ... "
							<< normalizeMinMaxVals[2 * image.plane() + 1];

	normalizeAndConvertTo8BitRGB<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(image.data(),
		image8bit_d_.get(),
		m_imageWidth, m_imageHeight, normalizeMinMaxVals[2 * plane], normalizeMinMaxVals[2 * plane + 1]);
	CHECK(cudaPeekAtLastError());

	// 3. retrieve from GPU
	CHECK(cudaMemcpyAsync(image8bit_h_.data(), image8bit_d_.get(), m_imageWidth * m_imageHeight * 3,
		cudaMemcpyDeviceToHost, m_streams[deviceID]));
	CHECK(cudaStreamSynchronize(m_streams[deviceID]));

	// display image using OpenCV
	const std::string windowIdentifier = m_parameterSet + "(plane " + std::to_string(image.plane()) + ")";
	const std::string windowTitle = m_parameterSet + " (plane " + std::to_string(image.plane()) + ", index "
		+ std::to_string(image.index()) + ")";
	cv::Mat cvImage(m_imageHeight, m_imageWidth, CV_8UC3, image8bit_h_.data());
	cv::namedWindow(windowIdentifier, cv::WINDOW_NORMAL | cv::WINDOW_FREERATIO);
	cv::imshow(windowIdentifier, cvImage);
	cv::setWindowTitle(windowIdentifier, windowTitle);
	cv::waitKey(10);

	m_tmrs[image.plane()].start();
}

auto OpenCVViewer::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::OpenCVViewer: ###### UPDATE ###### -> " << fo->m_fileName
							 << ": " << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::OpenCVViewer: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool wantsManualMinMaxVals;
			if(!configReader.lookupValue("setManualMinMaxValues", wantsManualMinMaxVals))
				wantsManualMinMaxVals = m_setManualMinMaxVals;

			// pre-emptively tell the saveImage function to no longer overwrite the manual manuals
			// in case we're going to update them. Don't lock though, because it really doesn't
			// matter if we don't update the min/max's each frame anyways - in only matters that
			// we do not accidently overwrite the new value we've got from the update function.
			m_storeAutomaticValues = !wantsManualMinMaxVals;

			std::vector<float> minVals, maxVals;
			configReader.lookupValue("manualMinValues", minVals);
			configReader.lookupValue("manualMaxValues", maxVals);

			if(m_manualMinMaxVals.size() == m_numberOfPlanes * 2)
			{
				// we allready have valid manual values, update as many as available
				for(auto i = 0; i < minVals.size(); ++i)
					m_manualMinMaxVals[2 * i] = minVals[i];
				for(auto i = 0; i < maxVals.size(); ++i)
					m_manualMinMaxVals[2 * i + 1] = maxVals[i];
			}
			else
			{
				// no valid values yet, we need a valid number of values to build the vector
				if(minVals.size() == m_numberOfPlanes && maxVals.size() == m_numberOfPlanes)
				{
					for(auto i = 0; i < m_numberOfPlanes; ++i)
					{
						m_manualMinMaxVals.push_back(minVals[i]);
						m_manualMinMaxVals.push_back(maxVals[i]);
					}
				}
			}

			if(wantsManualMinMaxVals && m_manualMinMaxVals.size() != m_numberOfPlanes * 2)
			{
				// user wants to set manual values but has no values provided
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::OpenCVViewer: Requested manual min/max "
											"values without providing values";
				wantsManualMinMaxVals = false;
			}
			m_setManualMinMaxVals = wantsManualMinMaxVals;
			m_storeAutomaticValues = !wantsManualMinMaxVals;
		}
	}
}

auto OpenCVViewer::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes))
	{
		if(!configReader.lookupValue(parameterSet + ".setManualMinMaxValues", m_setManualMinMaxVals))
		{
			m_setManualMinMaxVals = false;
		}

		if(m_setManualMinMaxVals)
		{
			std::vector<float> manualMinVals, manualMaxVals;
			if(!configReader.lookupValue(parameterSet + ".manualMinValues", manualMinVals)
				|| !configReader.lookupValue(parameterSet + ".manualMaxValues", manualMaxVals))
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::OpenCVViewer: Manual min/max values active "
											"but no values provided. Using automatic values.";
				m_setManualMinMaxVals = false;
			}
			else if(manualMinVals.size() < m_numberOfPlanes)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::OpenCVViewer: Manual min/max values active "
					   "but number of provided min values does not cover number of planes. Expected "
					<< m_numberOfPlanes << ", got " << manualMinVals.size() << ".";
				return false;
			}
			else if(manualMaxVals.size() < m_numberOfPlanes)
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "risa::cuda::OpenCVViewer: Manual min/max values active "
					   "but number of provided max values does not cover number of planes. Expected "
					<< m_numberOfPlanes << ", got " << manualMaxVals.size() << ".";
				return false;
			}
			for(auto i = 0; i < m_numberOfPlanes; ++i)
			{
				m_manualMinMaxVals.push_back(manualMinVals[i]);
				m_manualMinMaxVals.push_back(manualMaxVals[i]);
			}
			m_storeAutomaticValues = false;
		}
		else {
			m_storeAutomaticValues = true;
		}

		return true;
	}

	return false;
}

}
}
