// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/DeviceDiscarder.h>

#include <glados/Image.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <iostream>
#include <string>

namespace risa
{

namespace cuda
{

DeviceDiscarder::DeviceDiscarder(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::DeviceDiscarder: Configuration file could not be loaded successfully.");
	}
}

DeviceDiscarder::~DeviceDiscarder() {
	
}

auto DeviceDiscarder::saveImage(glados::Image<manager_type> image) -> void
{
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::DeviceDiscarder: Discarding image "<<image.index();
	// We just discard the image, that's it. `image` goes out of scope and is released back to the memory mamanger.
	return;
}

auto DeviceDiscarder::readConfig(const std::string&, const std::string&) -> bool
{
	// There are no parameters necessary.
	return true;
}

}
}
