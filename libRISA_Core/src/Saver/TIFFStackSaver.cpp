// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Saver/TIFFStackSaver.h>

#include <glados/Image.h>
#include <glados/MemoryPool.h>

#include <boost/log/trivial.hpp>

#include <tiffio.h>

#include <exception>
#include <iostream>
#include <string>

#ifdef __cpp_lib_span
#include <span>
#endif

namespace risa
{

namespace detail
{
template <class T, bool = std::is_integral<T>::value, bool = std::is_unsigned<T>::value>
struct SampleFormat
{
};
template <class T>
struct SampleFormat<T, true, true>
{
	static constexpr auto value = SAMPLEFORMAT_UINT;
};
template <class T>
struct SampleFormat<T, true, false>
{
	static constexpr auto value = SAMPLEFORMAT_INT;
};
template <>
struct SampleFormat<float>
{
	static constexpr auto value = SAMPLEFORMAT_IEEEFP;
};
template <>
struct SampleFormat<double>
{
	static constexpr auto value = SAMPLEFORMAT_IEEEFP;
};

template <class T>
struct BitsPerSample
{
	static constexpr auto value = sizeof(T) << 3;
};

struct TIFFDeleter
{
	auto operator()(::TIFF* ptr) -> void { TIFFClose(ptr); }
};
}

TIFFStackSaver::TIFFStackSaver(const std::string& configFile, const std::string& parameterSet)
	: m_parameterSet{parameterSet}
{
	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::TIFFStackSaver: Configuration file could not be loaded successfully.");
	}

	bool created = glados::utils::FileSystemHandle::createDirectory(m_outputPath);
	if(!created)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::TIFFStackSaver: Could not create target directory at "
								 << m_outputPath;
		throw std::runtime_error("risa::TIFFStackSaver: Unable to create output directory.");
	}

	if(!glados::utils::FileSystemHandle::setFilePermissionsWriteAll(m_outputPath))
	{
		BOOST_LOG_TRIVIAL(error) << "risa::TIFFStackSaver:: Cannot grant write access to output path'"
								 << m_outputPath << "'.";
		throw std::runtime_error("risa::TIFFStackSaver: Unable to set output permissions.");
	}

	if(m_outputPath.back() != '/')
	{
		m_outputPath.append("/");
	}

	for(auto i = 0; i < m_numberOfPlanes; i++)
	{
		m_outputBuffers.emplace_back(m_circularBufferSize);
		m_fileIndex.push_back(0U);
	}

	m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
		m_numberOfPlanes * (m_circularBufferSize + 1), m_imageWidth * m_imageHeight);

	//   m_memoryPoolIndex = glados::MemoryPool<manager_type>::getInstance().registerStage(
	//         m_numberOfPlanes * (m_circularBufferSize+1),
	//         256*1024);

	for(size_t i = 0; i < (size_t)m_numberOfPlanes; i++)
	{
		m_tmrs.emplace_back();
		m_tmrs[i].start();
	}
}

// NOLINTNEXTLINE(bugprone-exception-escape)
TIFFStackSaver::~TIFFStackSaver()
{
	if(m_mode == detail::RecoMode::offline)
	{
		for(size_t i = 0; i < (size_t)m_numberOfPlanes; i++)
		{
			try
			{
				writeTiffSequence(i);
			}
			catch(...)
			{
				BOOST_LOG_TRIVIAL(error) << "risa::TIFFStackSaver: Error when trying to dump buffer.";
			}
		}
	}
	if(m_displayStatistics)
	{
		BOOST_LOG_TRIVIAL(info) << "risa::TIFFStackSaver<" << m_parameterSet
								<< ">:Maximum latency: " << m_maxLatency
								<< " ms; minimum latency: " << m_minLatency << " ms";
	}
	glados::MemoryPool<manager_type>::getInstance().freeMemory(m_memoryPoolIndex);
}

auto TIFFStackSaver::saveImage(glados::Image<manager_type> image) -> void
{
	if(m_mode == detail::RecoMode::discardAll)
	{
		return;
	}

	auto img = glados::MemoryPool<manager_type>::getInstance().requestMemory(m_memoryPoolIndex);
#ifdef __cpp_lib_span
	std::span span(image.data(), image.size());
	std::copy(span.begin(), span.end(), img.data());
#else
	std::copy(image.data(), image.data() + image.size(), img.data());
#endif
	img.setIdx(image.index());
	img.setPlane(image.plane());
	img.setStart(image.start());
	auto latency = img.duration();
	if(latency < m_minLatency)
	{
		m_minLatency = latency;
	}
	else if(latency > m_maxLatency)
	{
		m_maxLatency = latency;
	}
	m_outputBuffers[image.plane()].push_back(std::move(img));
	if(m_mode == detail::RecoMode::offline)
	{
		if(m_outputBuffers[image.plane()].full())
		{
			writeTiffSequence(image.plane());
		}
	}
	else if(m_mode == detail::RecoMode::online)
	{
		m_tmrs[image.plane()].stop();
		double diff = m_tmrs[image.plane()].elapsed();
		constexpr const double MIN_DURATION_BETWEEN_WRITES{0.02};
		if(diff < MIN_DURATION_BETWEEN_WRITES)
		{
			return;
		}
		std::string path =
			m_outputPath + m_fileName + "_plane" + std::to_string(image.plane()) + "_" + "0" + ".tif";
		auto tif = std::unique_ptr<::TIFF, detail::TIFFDeleter>{TIFFOpen(path.c_str(), "wb")};
		if(tif == nullptr)
		{
			throw std::runtime_error{"risa::TIFFStackSaver: Could not open file " + path + " for writing."};
		}
		glados::utils::FileSystemHandle::setFilePermissionsWriteAll(path);
		writeToTiff(tif.get(), image);

		m_tmrs[image.plane()].start();
	}
}

auto TIFFStackSaver::writeTiffSequence(size_t planeID) -> void
{
	if(m_outputBuffers[planeID].count() == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::TIFFStackSaver::writeTiffSequence: Empty buffer for plane "
								 << planeID;
		return;
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::TIFFStackSaver::writeTiffSequence: Write buffer for plane " << planeID;
	std::string path = m_outputPath + m_fileName + "_plane" + std::to_string(planeID) + "_"
		+ std::to_string(m_fileIndex[planeID]) + "seq.tif";

	std::string bufferPath;

	if(!m_bufferOutputBeforeWrite)
	{
		bufferPath = path; // directly write to final path
	}
	else
	{
		bufferPath = m_bufferPath + m_parameterSet + "m__buffer" + std::to_string(planeID) + ".tif";
		BOOST_LOG_TRIVIAL(debug) << "Buffering output to " << bufferPath;
	}

	auto tif = std::unique_ptr<::TIFF, detail::TIFFDeleter>{TIFFOpen(bufferPath.c_str(), "w")};

	if(tif == nullptr)
	{
		throw std::runtime_error{"risa::TIFFStackSaver: Could not open file " + path + " for writing."};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::TIFFStackSaver:         Writing out: " << path;
	for(auto i = 0U; i < m_outputBuffers[planeID].count(); i++)
	{
		writeToTiff(tif.get(), m_outputBuffers[planeID].at(i)); // do not std::move here to use copy elision
		if(TIFFWriteDirectory(tif.get()) != 1)
		{
			throw std::runtime_error{"risa::TIFFStackSaver: tiffio error while writing to tiff buffer"};
		}
	}
	++m_fileIndex[planeID];
	m_outputBuffers[planeID].clear();

	if(m_bufferOutputBeforeWrite)
	{
		// copy buffer file to final location
		std::filesystem::copy(bufferPath, path, std::filesystem::copy_options::overwrite_existing);
	}
}

auto TIFFStackSaver::writeToTiff(::TIFF* tif, const glados::Image<manager_type>& img) const -> void
{
	// NOLINTBEGIN
	TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, m_imageWidth);
	TIFFSetField(tif, TIFFTAG_IMAGELENGTH, m_imageHeight);
	TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);
	TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
	TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	TIFFSetField(tif, TIFFTAG_THRESHHOLDING, THRESHHOLD_BILEVEL);
	TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
	TIFFSetField(tif, TIFFTAG_SOFTWARE, "RISA");
	TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
	// NOLINTEND

	size_t offs = 0;
#ifdef __cpp_lib_span
	std::span imgData{img.data(), img.size()};
#endif
	for(uint32_t row = 0; row < m_imageHeight; ++row)
	{
#ifdef __cpp_lib_span
		TIFFWriteScanline(tif, &imgData[offs], row);
#else
		TIFFWriteScanline(tif, &img.data()[offs], row);
#endif
		offs += m_imageWidth;
	}
}

// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
auto TIFFStackSaver::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string mode;
	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".filePath", m_outputPath)
		&& configReader.lookupValue(parameterSet + ".fileName", m_fileName)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes)
		&& configReader.lookupValue(parameterSet + ".outputBufferSize", m_circularBufferSize)
		&& configReader.lookupValue(parameterSet + ".mode", mode))
	{
		if(mode == "offline")
		{
			m_mode = detail::RecoMode::offline;
		}
		else if(mode == "online")
		{
			m_mode = detail::RecoMode::online;
		}
		else if(mode == "discardAll")
		{
			m_mode = detail::RecoMode::discardAll;
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "TIFFStackSaver:: Requested mode \"" << mode << "\" not supported.";
			return false;
		}

		if(!configReader.lookupValue(parameterSet + ".displayStatistics", m_displayStatistics))
		{
			m_displayStatistics = false;
		}

		if(configReader.lookupValue(parameterSet + ".bufferPath", m_bufferPath))
		{
			m_bufferOutputBeforeWrite = true;
		}

		return true;
	}

	return false;
}

}
