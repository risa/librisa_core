// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// Andr� Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Segmentation/Watershed.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Watershed::Watershed(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::Watershed: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	// populate results vector with an empty queue for each output
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 1));
		m_streams[i] = stream;
	}

	m_doConfig = true;

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Watershed::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: Running " << m_numberOfDevices << " thread(s).";
}

Watershed::~Watershed()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: Destroyed.";
}

auto Watershed::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed:        (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: Finished.";
	}
}

auto Watershed::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto Watershed::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid(
		(int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: Running thread for device " << deviceID;

	auto directionsBuffer_d = glados::cuda::make_device_ptr<size_t>(m_imageWidth * m_imageHeight);
	auto doneFlag_d = glados::cuda::make_device_ptr<bool>(1);
	bool doneFlag_h = false;

	while(m_doConfig)
	{
		// setup one-time stuff

		// afterwards, reset flag
		m_doConfig = false;

		while(true)
		{
			auto img = m_imgs[deviceID][0].take();
			if(!img.valid())
				break;

			// initialize watershed direction array
			watershedKernel_setDirection<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(img.data(),
				directionsBuffer_d.get(), m_imageWidth, m_imageHeight, m_thresholdStop);
			CHECK(cudaPeekAtLastError());
			int iters = 0;
			doneFlag_h = false;
			while(!doneFlag_h)
			{
				iters++;
				// reset done flag on device
				CHECK(cudaMemsetAsync(doneFlag_d.get(), true, 1, m_streams[deviceID]));

				// propagate directions once
				watershedKernel_propagateDirection<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
					directionsBuffer_d.get(), m_imageWidth, m_imageHeight, doneFlag_d.get());
				CHECK(cudaPeekAtLastError());

				// get done flag
				CHECK(cudaMemcpyAsync(
					&doneFlag_h, doneFlag_d.get(), 1, cudaMemcpyDeviceToHost, m_streams[deviceID]));

				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			}
			BOOST_LOG_TRIVIAL(debug) << "Took " << iters << " iters";

			// now, all directions in the directionsBuffer should point to their respective seed pixel
			// finally, copy the regions back into the input buffer
			doneFlag_h = false;
			while(!doneFlag_h)
			{
				// reset done flag on device
				CHECK(cudaMemsetAsync(doneFlag_d.get(), true, 1, m_streams[deviceID]));

				// propagate directions once
				watershedKernel_splitConnectedAndFillOutput<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
					img.data(), directionsBuffer_d.get(), m_imageWidth, m_imageHeight, doneFlag_d.get());
				CHECK(cudaPeekAtLastError());

				// get done flag
				CHECK(cudaMemcpyAsync(
					&doneFlag_h, doneFlag_d.get(), 1, cudaMemcpyDeviceToHost, m_streams[deviceID]));

				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
			}

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(img.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::Watershed:        (Device " << deviceID << " | Output " << i
					<< "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			// wait until work on device is finished

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed:        (Device " << deviceID << " | Output "
									 << 0 << "): Image " << img.index() << " processed.";
			m_results[0].push(std::move(img));

			if(m_doConfig)
			{ // trigger for runtime reconfiguration
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Watershed:        Reconfiguration triggered.";
				break; // breaks inner while, outer will keep running after reconfiguration
			}
		}
	}
}

auto Watershed::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Watershed: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = configReader.lookupValue("thresholdStop", m_thresholdStop);
			if(hasValidSetting)
			{

				// std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Watershed: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto Watershed::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string WatershedType;

	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".thresholdStop", m_thresholdStop)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize))
	{
		return true;
	}
	return false;
}

__global__ void watershedKernel_setDirection(float* const input, size_t* directions, const size_t width,
	const size_t height, const float thresholdStop)
{
	// set direction of each pixel in directions array.
	// if > seed threshold:
	//		check if heighest value in neighbourhood -> if yes, point to self
	//												 -> if no, point to highest neighbour value
	// else if > stop threshold:
	//		point to highest neighbour
	// else
	//		point to NONE

	auto x = glados::cuda::getX();
	auto y = glados::cuda::getY();

	if(y >= height || x >= width)
	{
		return;
	}

	const auto idx = y * width + x;
	const auto val = input[idx];
	if(val <= thresholdStop)
	{
		// can never be a valid pixel
		directions[idx] = risa::cuda::INVALID_ADDR;
		return;
	}

	static const float MIN_FLOAT = -3.40281E38; // result of std::numeric_limits<float>::lowest();

	// find highest value neighbour pixel (TODO refactor with low-value brim to remove checks)
	size_t highestValIdx = idx;
	float highestVal = val;

	const auto valUp = y > 0 ? input[idx - width] : MIN_FLOAT;
	if(valUp > highestVal)
	{
		highestValIdx = idx - width;
		highestVal = valUp;
	}

	const auto valUpLeft = y > 0 && x > 0 ? input[idx - width - 1] : MIN_FLOAT;
	if(valUpLeft > highestVal)
	{
		highestValIdx = idx - width - 1;
		highestVal = valUpLeft;
	}

	const auto valLeft = x > 0 ? input[idx - 1] : MIN_FLOAT;
	if(valLeft > highestVal)
	{
		highestValIdx = idx - 1;
		highestVal = valLeft;
	}

	const auto valDownLeft = (x > 0 && y < height - 1) ? input[idx + width - 1] : MIN_FLOAT;
	if(valDownLeft > highestVal)
	{
		highestValIdx = idx + width - 1;
		highestVal = valDownLeft;
	}	

	const auto valDown = y < height - 1 ? input[idx + width] : MIN_FLOAT;
	if(valDown > highestVal)
	{
		highestValIdx = idx + width;
		highestVal = valDown;
	}

	const auto valDownRight = (y < height - 1 && x < width - 1) ? input[idx + width + 1] : MIN_FLOAT;
	if(valDownRight > highestVal)
	{
		highestValIdx = idx + width + 1;
		highestVal = valDownRight;
	}

	const auto valRight = (x < width - 1 )? input[idx + 1] : MIN_FLOAT;
	if(valRight > highestVal)
	{
		highestValIdx = idx + 1;
		highestVal = valRight;
	}

	const auto valUpRight = (x < width - 1 && y > 0) ? input[idx - width + 1] : MIN_FLOAT;
	if(valUpRight > highestVal)
	{
		highestValIdx = idx - width + 1;
		highestVal = valUpRight;
	}

	directions[idx] = highestValIdx;
}

// check pointed-to direction and apply its
__global__ void watershedKernel_propagateDirection(
	size_t* directions, const size_t width, const size_t height, bool* done)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= width || y >= height)
	{
		return;
	}

	const auto idx = y * width + x;
	if(directions[idx] == idx)
	{
		// its a seed pixel
		return;
	}

	if(directions[idx] == risa::cuda::INVALID_ADDR)
	{
		// its an invalid pixel
		return;
	}

	// neighbour must be valid at this point
	const auto neighbourIdx = directions[idx];
	const auto neighbourPointsTo = directions[neighbourIdx];

	if(neighbourPointsTo == neighbourIdx)
	{
		// neighbour is a seed pixel
		return;
	}

	// neighbour may be pointing to some intermediate pixel or an invalid one at this point
	// either way, we should also point to wherever the neighbour is pointing
	directions[idx] = neighbourPointsTo;
	*done = false;
}

// split connecting regions
__global__ void watershedKernel_splitConnectedAndFillOutput(
	float* output, size_t* directions, const size_t width, const size_t height, bool* done)
{
	// if any (4-connected) neighbour has different value (other than NONE)
	//		if self value is lower than neighbours
	//			set output to 0.0f
	//			return
	// set output to 1.0f;

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= width || y >= height)
	{
		return;
	}

	const auto idx = y * width + x;
	const auto val = directions[idx];
	if(val == risa::cuda::INVALID_ADDR)
	{
		// pixel is invalid, nothing to be done
		output[idx] = 0.0f;
		return;
	}

	size_t neighVals[8];
	neighVals[0] = (x > 0) ? directions[idx - 1] : risa::cuda::INVALID_ADDR; // left
	neighVals[1] = (y > 0) ? directions[idx - width] : risa::cuda::INVALID_ADDR; // up
	neighVals[2] = (y < height -1) ? directions[idx + width] : risa::cuda::INVALID_ADDR; // down
	neighVals[3] = (x < width - 1) ? directions[idx + 1] : risa::cuda::INVALID_ADDR; // right
	neighVals[4] = (x > 0 && y > 0) ? directions[idx - width - 1] : risa::cuda::INVALID_ADDR; // up left
	neighVals[5] = (x > 0 && y < height - 1) ? directions[idx + width - 1] : risa::cuda::INVALID_ADDR; // down left
	neighVals[6] = (x < width - 1 && y < height - 1) ? directions[idx + width + 1] : risa::cuda::INVALID_ADDR; // down right
	neighVals[7] = (x < width - 1 && y > 0) ? directions[idx - width + 1] : risa::cuda::INVALID_ADDR; // up right

	for(auto neigh = 0; neigh < 8; neigh++)
	{
		if(neighVals[neigh] != risa::cuda::INVALID_ADDR && neighVals[neigh] > val)
		{
			// own label is lower than a neighbour -> delete self
			directions[idx] = risa::cuda::INVALID_ADDR;
			output[idx] = 0.0f;
			*done = false;
			return;
		}
	}

	output[idx] = static_cast<float>(val);
}

}
}
