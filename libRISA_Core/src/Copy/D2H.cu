// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Copy/D2H.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>
#include <sstream>

namespace risa
{
namespace cuda
{

D2H::D2H(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs)
	, m_numberOfInputs(numberOfInputs)
	, m_reconstructionRate{0u}
	, m_parameterSet{parameterSet}
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error("risa::cuda::D2H: Configuration file could not be loaded successfully.");
	}

	for(auto i = 0; i < 64; i++)
		m_latencyHistogram.push_back(0ul);

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	m_memoryPoolIdx = glados::MemoryPool<hostManagerType>::getInstance().registerStage(
		m_memPoolSize, m_imageWidth * m_imageHeight);

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 0));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&D2H::processor, this, i};
	}

	m_enableCopy = true;

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H: Running " << m_numberOfDevices << " thread(s).";
	m_tmr.start();
}

D2H::~D2H()
{
	if(m_displayStatistics)
	{
		BOOST_LOG_TRIVIAL(info) << "Processed " << (double)m_count / m_tmr.elapsed() << " images/s in average.";
		std::stringstream latencyHist;
		latencyHist << "Latency: [";
		for(auto i = 0; i < m_latencyHistogram.size() - 1; i++)
		{
			latencyHist << (double)i * 0.1 << ": " << m_latencyHistogram[i] << ", ";
		}
		latencyHist << ">6.3: " << m_latencyHistogram.back() << "]";
		BOOST_LOG_TRIVIAL(info) << "Latency: " << latencyHist.str();
	}

	glados::MemoryPool<hostManagerType>::getInstance().freeMemory(m_memoryPoolIdx);
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
}

auto D2H::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		if(!m_enableCopy)
			return;

		if(img.index() == 0)
			m_tmr.start();
		if(m_count == 9999)
		{
			m_tmr.stop();
			m_count = 0;
			if(m_displayStatistics)
				BOOST_LOG_TRIVIAL(info)
					<< "Currently processing " << 10000.0 / (m_tmr.elapsed()) << " images/second.";
			m_tmr.start();
		}
		m_count++;
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H:            (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else
	{
		m_tmr.stop();
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage

		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H: Finished.";
	}
}

auto D2H::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto D2H::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H: Running thread for device " << deviceID;
	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			break;
		}

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H: Download image " << img.index();

		// copy image from device to host
		auto ret = glados::MemoryPool<hostManagerType>::getInstance().requestMemory(m_memoryPoolIdx);
		
		CHECK(cudaMemcpyAsync(
			ret.data(), img.data(), m_imageWidth * m_imageHeight * sizeof(float), cudaMemcpyDeviceToHost, m_streams[deviceID]));
		
		ret.setProperties(img.properties());
		
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		if(img.index() > 1000)
		{
			// first frames will very likely be blocked by averaging reference and dark and will therefore
			// have enourmous latency
			// => let all buffers drain before measuring latency
			auto latency = img.duration();
			auto latencyIdx = floor(latency / 0.1);
			latencyIdx = latencyIdx < 0 ? 0 : latencyIdx;
			latencyIdx = latencyIdx > 63 ? 63 : latencyIdx;

			m_latencyHistogram[latencyIdx]++;
		}

		// wait until work on device is finished
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::D2H:            (Device " << deviceID << " | Output " << 0
								 << "): Image " << ret.index() << " processed.";
		m_results[0].push(std::move(ret));
	}
}

auto D2H::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::D2H:  ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::D2H:  ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		const std::string fullPath = fo->m_filePath + fo->m_fileName;
		ConfigReader configReader = ConfigReader(fullPath.c_str(), true);

		bool enableOutput = false;
		configReader.lookupValue("enable", enableOutput);
		if(enableOutput)
		{
			BOOST_LOG_TRIVIAL(info) << "risa::D2H<" << m_parameterSet << ">: Enable copying.";
			m_enableCopy = true;
		}
		else
		{
			BOOST_LOG_TRIVIAL(info) << "risa::D2H<" << m_parameterSet << ">: Disable copying.";
			m_enableCopy = false;
		}
	}
}

auto D2H::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());

	if(configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize))
	{

		if(!configReader.lookupValue(
			   parameterSet + ".displayStatistics", m_displayStatistics))
			m_displayStatistics = false;

		return true;
	}

	else
		return false;
}

}
}
