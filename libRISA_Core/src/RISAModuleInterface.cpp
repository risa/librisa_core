// SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de)
#include <risaCore/RISAModuleInterface.h>

auto get_str(char* ptr, std::string str) -> uint32_t
{
	if(ptr != nullptr)
	{
		memcpy(ptr, str.data(), str.size()); // NOLINT(bugprone-not-null-terminated-result)
	}
	return (uint32_t)str.size();
}

auto get_str(char* ptr, int32_t idx, int32_t part, std::vector<std::vector<std::string>> strs) -> uint32_t
{
	if(idx < 0)
	{
		return (uint32_t)strs.size();
	}
	if(part < 0)
	{
		return (uint32_t)strs[(size_t)idx].size();
	}

	if(ptr != nullptr)
	{
		// NOLINTNEXTLINE(bugprone-not-null-terminated-result)
		memcpy(ptr, strs[(size_t)idx][(size_t)part].data(),
			strs[(size_t)idx][(size_t)part].size()); 
	}
	return (uint32_t)strs[(size_t)idx][(size_t)part].size();
}
