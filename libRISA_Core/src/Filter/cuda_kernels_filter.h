// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef CUDA_KERNELS_FILTER_H_
#define CUDA_KERNELS_FILTER_H_
#ifdef __linux__
#define _USE_MATH_DEFINES
#elif _WIN32
#define M_PI 3.14159265358979323846
#endif
#include <cmath>

namespace risa
{
namespace cuda
{

//!	computes the value of the Shepp-Logan-filter function
/**
 *	@param[in]	w	the coordinate at the frequency axis
 *	@param[in]	d	the cutoff-fraction
 *
 *
 */
template <typename T>
auto inline sheppLogan(const T w) -> T
{
	const T ret = std::sin(w * M_PI) / (w * M_PI);
	return ret;
}

//!	computes the value of the Cosine-filter function
/**
 *	@param[in]	w	the coordinate at the frequency axis
 *	@param[in]	d	the cutoff-fraction
 *
 *
 */
template <typename T>
auto inline cosine(const T w) -> T
{
	const T ret = std::cos(w * 2.0 * M_PI);
	return ret;
}

//!	computes the value of the Hamming-filter function
/**
 *	@param[in]	w	the coordinate at the frequency axis
 *	@param[in]	d	the cutoff-fraction
 *
 *
 */
template <typename T>
auto inline hamming(const T w) -> T
{
	const T ret = 0.54 + 0.46 * std::cos(w * 2.0 * M_PI);
	return ret;
}

//!	computes the value of the Hanning-filter function
/**
 *	@param[in]	w	the coordinate at the frequency axis
 *	@param[in]	d	the cutoff-fraction
 *
 *
 */
template <typename T>
auto inline hanning(const T w) -> T
{
	const T ret = 0.5 + 0.5 * std::cos(w * 2.0 * M_PI);
	return ret;
}

template <typename T>
auto inline flattop(const T w, const int filterSize) -> T
{
	const float fA0 = 1.0f;
	const float fA1 = 1.93f;
	const float fA2 = 1.29f;
	const float fA3 = 0.388f;
	const float fA4 = 0.032f;
	const float fNMinusOne = (float)(filterSize - 1.0f);

	const float fBaseCosInput = M_PI * w / fNMinusOne;
	const float fFirstTerm = fA1 * std::cos(2.0f * fBaseCosInput);
	const float fSecondTerm = fA2 * std::cos(4.0f * fBaseCosInput);
	const float fThridTerm = fA3 * std::cos(6.0f * fBaseCosInput);
	const float fForthTerm = fA4 * std::cos(8.0f * fBaseCosInput);

	const T ret = fA0 - fFirstTerm + fSecondTerm - fThridTerm + fForthTerm;
	return ret;
}

template <typename T>
auto inline gaussian(const T w, const float fSigma) -> T
{
	const float fQuotient = (w - 1.0f) / 2.0f;
	const float fEnum = w - fQuotient;
	const float fDenom = fSigma * fQuotient;
	const float fPower = -0.5f * (fEnum / fDenom) * (fEnum / fDenom);

	const T ret = std::exp(fPower);
	return ret;
}

template <typename T>
auto inline lanczos(const T w, const int filterSize) -> T
{
	float fDenum = (float)(filterSize - 1);
	float fX = 2.0f * w / fDenum - 1.0f;
	float fSinInput = M_PI * fX;
	const T ret = (std::abs(fSinInput) > 0.001f ? std::sin(fSinInput) / fSinInput : 1.0f);
	return ret;
}

template <typename T>
auto inline parzen(const T w, const int filterSize) -> T
{
	const float fQ = w / (float)(filterSize - 1.0f);
	const float f1mQ = 1.0f - fQ;

	const T ret = fQ >= 0.5f ? 1.0f - 6.0f * fQ * fQ * f1mQ : 2.0f * f1mQ * f1mQ * f1mQ;
	return ret;
}

template <typename T>
auto inline triangular(const T w, const int filterSize) -> T
{
	const float fNMinusOne = (float)(filterSize - 1);
	const float fAbsInput = w - fNMinusOne / 2.0f;
	const float fParenInput = fNMinusOne / 2.0f - std::abs(fAbsInput);
	const T ret = 2.0f / fNMinusOne * fParenInput;
	return ret;
}

}
}

#endif /* CUDA_KERNELS_FILTER_H */
