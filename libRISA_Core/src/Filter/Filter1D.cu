// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Filter/Filter1D.h>

#include "cuda_kernels_filter.h" // must be included AFTER Filter1D.h

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Filter1D::Filter1D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::Filter1D: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// cuFFT library is initialized for each device
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		initCuFFT(i);
	}

	m_doConfig = true;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Filter1D::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D: Running " << m_numberOfDevices << " thread(s).";
}

Filter1D::~Filter1D()
{
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
		CHECK_CUFFT(cufftDestroy(m_plansFwd[i]));
		CHECK_CUFFT(cufftDestroy(m_plansInv[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D: Destroyed.";
}

auto Filter1D::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D:       (Device " << image.device() << " |  Input "
								 << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D: Finished.";
	}
}

auto Filter1D::wait(int outputIdx = 0) -> output_type { return m_results[outputIdx].take(); }

auto Filter1D::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	auto sinoFreq = glados::cuda::make_device_ptr<cufftComplex, glados::cuda::async_copy_policy>(
		m_imageHeight * ((m_imageWidth / 2.0) + 1));
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid((int)ceil((m_imageWidth / 2.0 + 1) / (float)m_blockSize2D),
		(int)ceil(m_imageHeight / (float)m_blockSize2D));
	dim3 dimFullGrid(
		(int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));
	auto filterFunction_d =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(m_imageWidth / 2 + 1);

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D: Running thread for device " << deviceID;

	while(true)
	{
		if(m_doConfig)
		{
			designFilter();
			CHECK(cudaMemcpyAsync(filterFunction_d.get(), m_filter.data(), sizeof(float) * m_filter.size(),
				cudaMemcpyHostToDevice, m_streams[deviceID]));
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			m_doConfig = false;
		}

		auto image = m_images[deviceID][0].take();
		if(!image.valid())
			break;

		// forward transformation
		CHECK_CUFFT(cufftExecR2C(m_plansFwd[deviceID], (cufftReal*)image.data(), sinoFreq.get()));

		// Filtering
		applyFilter<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(
			(m_imageWidth / 2) + 1, m_imageHeight, sinoFreq.get(), filterFunction_d.get());

		CHECK(cudaPeekAtLastError());

		// reverse transformation
		CHECK_CUFFT(cufftExecC2R(m_plansInv[deviceID], sinoFreq.get(), (cufftReal*)image.data()));

		// scale after inverse fft
		scaleAfterIFFT1D<<<dimFullGrid, dimBlock, 0, m_streams[deviceID]>>>(
			m_imageWidth, m_imageHeight, image.data());

		// wait until work on device is finished
		CHECK(cudaStreamSynchronize(m_streams[deviceID]));
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter1D:       (Device " << deviceID << " | Output " << 0
								 << "): Image " << image.index() << " processed.";
		m_results[0].push(std::move(image));
	}
}

auto Filter1D::initCuFFT(const int deviceID) -> void
{

	CHECK(cudaSetDevice(deviceID));

	cudaStream_t stream;
	cufftHandle planFwd, planInv;

	CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
	m_streams[deviceID] = stream;

	// CHECK_CUFFT(cufftPlanMany(&planFwd, 1, &m_imageWidth, NULL, 0, 0, NULL, 0, 0, CUFFT_R2C,
	// m_imageHeight));
	CHECK_CUFFT(cufftPlan1d(&planFwd, m_imageWidth, CUFFT_R2C, m_imageHeight));

	CHECK_CUFFT(cufftSetStream(planFwd, stream));

	// CHECK_CUFFT(cufftPlanMany(&planInv, 1, &m_imageWidth, NULL, 0, 0, NULL, 0, 0, CUFFT_C2R,
	// m_imageHeight));
	CHECK_CUFFT(cufftPlan1d(&planInv, m_imageWidth, CUFFT_C2R, m_imageHeight));

	CHECK_CUFFT(cufftSetStream(planInv, stream));

	m_plansFwd[deviceID] = planFwd;
	m_plansInv[deviceID] = planInv;
}

auto Filter1D::designFilter() -> void
{
	int filterSize = m_imageWidth / 2 + 1;
	m_filter.clear();
	m_filter.reserve(filterSize);

	if(m_filterType != detail::FilterType1D::ramp)
	{
		m_rampOffsetPercent = 0.0f;
	}

	m_filter.push_back(
		m_rampOffsetPercent / 100.0f); // make sure not to call filter generator functions with w = 0

	for(auto i = 1; i < filterSize; i++)
	{
		float relativeIndex = 2.0f * (float)(i) / (float)m_imageWidth; // range 0...1

		if(relativeIndex > m_cutoffFraction)
		{
			m_filter.push_back(0.0f);
			continue;
		}

		const float n = m_rampOffsetPercent / 100.0f;
		const float m = 1.0f - n; // - 2.0f * (1.0f - n) / (float)m_imageWidth;

		float filterValue = m * (float)i + n;
		;

		switch(m_filterType)
		{
		case detail::FilterType1D::ramp:
		{
			// scale ramp filters between initial offset and 100 percent
			break;
		}

		case detail::FilterType1D::hamming:
		{
			filterValue *= hamming(relativeIndex);
			break;
		}
		case detail::FilterType1D::hanning:
		{
			filterValue *= hanning(relativeIndex);
			break;
		}
		case detail::FilterType1D::sheppLogan:
		{
			filterValue *= sheppLogan(relativeIndex);
			break;
		}
		case detail::FilterType1D::cosine:
		{
			filterValue *= cosine(relativeIndex);
			break;
		}
		case detail::FilterType1D::flattop:
		{
			filterValue *= flattop(relativeIndex, filterSize);
			break;
		}
		case detail::FilterType1D::gaussian:
		{
			filterValue *= gaussian(relativeIndex, 0.4f); // fixed sigma of 0.4 for now
			break;
		}
		case detail::FilterType1D::lanczos:
		{
			filterValue *= lanczos(relativeIndex, filterSize);
			break;
		}
		case detail::FilterType1D::parzen:
		{
			filterValue *= parzen(relativeIndex, filterSize);
			break;
		}
		case detail::FilterType1D::triangular:
		{
			filterValue *= triangular(relativeIndex, filterSize);
			break;
		}
		case detail::FilterType1D::none:
		{
			filterValue = 1.0f;
			break;
		}
		}

		m_filter.push_back(filterValue);
	}
}

auto Filter1D::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Masking: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			hasValidSetting = configReader.lookupValue("cutoffFraction", m_cutoffFraction);
			hasValidSetting =
				configReader.lookupValue("rampOffsetPercent", m_rampOffsetPercent) || hasValidSetting;
			std::string filterType;
			hasValidSetting = configReader.lookupValue("filterType", filterType) || hasValidSetting;
			setFilterType1DFromString(filterType);

			if(hasValidSetting)
			{
				m_doConfig = true;
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Filter1D: Reconfiguration triggered.";
			}
		}
	}
}

auto Filter1D::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".filterType", filterType)
		&& configReader.lookupValue(parameterSet + ".cutoffFraction", m_cutoffFraction))
	{
		setFilterType1DFromString(filterType);

		configReader.setAllowedToFail(true);
		if(!configReader.lookupValue(parameterSet + ".rampOffsetPercent", m_rampOffsetPercent))
		{
			m_rampOffsetPercent = 0.0f;
		}

		return true;
	}
	return false;
}

__global__ void applyFilter(
	const int x, const int y, cufftComplex* data, const float* const __restrict__ filter)
{
	const int j = blockIdx.y * blockDim.y + threadIdx.y;
	const int i = blockIdx.x * blockDim.x + threadIdx.x;
	if(i < x && j < y)
	{
		// cufft performs an unnormalized transformation ifft(fft(A))=length(A)*A
		//->normalization needs to be performed
		// const float filterVal = filter_d[i] * normalization;
		data[i + j * x].x *= filter[i];
		data[i + j * x].y *= filter[i];
	}
}

__global__ void scaleAfterIFFT1D(const int imageWidth, const int imageHeight, float* __restrict__ data)
{
	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();
	if(x < imageWidth && y < imageHeight)
	{
		const auto idx = y * imageWidth + x;
		const auto factor = (imageWidth);
		data[idx] /= (float)factor;
	}
}

}
}
