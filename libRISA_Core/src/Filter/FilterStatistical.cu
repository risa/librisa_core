// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Filter/FilterStatistical.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

#define ELEM_SWAP(a, b) \
	{ \
		register float t = (a); \
		(a) = (b); \
		(b) = t; \
	}
#define SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE 64

namespace risa
{
namespace cuda
{

FilterStatistical::FilterStatistical(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::FilterStatistical: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	for(auto deviceID = 0; deviceID < m_numberOfDevices; deviceID++)
	{
		CHECK(cudaSetDevice(deviceID));

		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
		m_streams[deviceID] = stream;
	}

	m_doConfig = true;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&FilterStatistical::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: Running " << m_numberOfDevices
							 << " thread(s).";
}

FilterStatistical::~FilterStatistical()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: Destroyed.";
}

auto FilterStatistical::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical:  (Device " << image.device()
								 << " |  Input " << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: Finished.";
	}
}

auto FilterStatistical::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto FilterStatistical::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	auto filteredImage =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(m_imageHeight * m_imageWidth);

	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid((int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: Running thread for device " << deviceID;

	while(m_doConfig)
	{

		m_doConfig = false;
		int filterKernelWidth = m_filterKernelWidth;
		int filterKernelHeight = m_filterKernelHeight;
		int selectIndex = m_selectIndex;

		while(true)
		{
			auto image = m_images[deviceID][0].take();
			if(!image.valid())
				break;

			// Filtering
			if(m_filterType == detail::FilterTypeSpecial::median
				|| m_filterType == detail::FilterTypeSpecial::selectIndex)
			{
				selectIndexFilter<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(m_imageWidth, m_imageHeight,
					image.data(), filteredImage.get(), filterKernelWidth, filterKernelHeight, selectIndex);

				CHECK(cudaPeekAtLastError());

				cudaMemcpyAsync(image.data(), filteredImage.get(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);
			}

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), image.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(image.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug)
					<< "risa::cuda::FilterStatistical:  (Device " << deviceID << " | Output " << i
					<< "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical:  (Device " << deviceID << " | Output "
									 << 0 << "): Image " << image.index() << " processed.";
			m_results[0].push(std::move(image));

			if(m_doConfig)
			{
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::FilterStatistical:  Reconfiguration triggered.";
				break;
			}
		}
	}
}

auto FilterStatistical::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: ###### UPDATE ###### -> " << fo->m_fileName
							 << ": " << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterStatistical: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			std::string filterType;
			if(configReader.lookupValue("filterType", filterType))
			{
				m_filterType = getFilterTypeSpecialFromString(filterType);
				hasValidSetting = true;
			}

			if(m_filterType == detail::FilterTypeSpecial::UNDEFINED
				&& filterType != "filterTypeSpecialNotDefined")
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::FilterStatistical: Dynamic reconfiguration has invalid filter type \""
					<< filterType << "\".";
				hasValidSetting = false;
			}

			if(m_filterType == detail::FilterTypeSpecial::median
				|| m_filterType == detail::FilterTypeSpecial::selectIndex)
			{
				int tmp = 1;
				if(configReader.lookupValue("filterKernelWidth", tmp) && (tmp % 2 == 1))
				{
					hasValidSetting =
						configReader.lookupValue("filterKernelWidth", m_filterKernelWidth) || hasValidSetting;
				}
				else if(tmp % 2 == 0)
				{
					BOOST_LOG_TRIVIAL(warning)
						<< "risa::cuda::FilterStatistical: Dynamic reconfiguration has "
						   "invalid filterKernelWidth ("
						<< tmp << "). Must be an odd value.";
				}

				tmp = 1;
				if(configReader.lookupValue("filterKernelHeight", tmp) && (tmp % 2 == 1))
				{
					hasValidSetting = configReader.lookupValue("filterKernelHeight", m_filterKernelHeight)
						|| hasValidSetting;
				}
				else if(tmp % 2 == 0)
				{
					BOOST_LOG_TRIVIAL(warning)
						<< "risa::cuda::FilterStatistical: Dynamic reconfiguration has "
						   "invalid filterKernelHeight ("
						<< tmp << "). Must be an odd value.";
				}
			}

			if(m_filterType == detail::FilterTypeSpecial::selectIndex)
			{
				int tmpIndex;
				if(configReader.lookupValue("filterSelectIndex", tmpIndex))
				{
					if(tmpIndex < 0 || tmpIndex >= m_filterKernelWidth * m_filterKernelHeight)
					{
						BOOST_LOG_TRIVIAL(error)
							<< "risa::cuda::FilterStatistical: Select index '"<< tmpIndex <<"' out of range. Valid: 0 ... "
							<< m_filterKernelHeight * m_filterKernelWidth - 1;
					}
					else if(m_filterKernelWidth * m_filterKernelHeight
						> SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE)
					{
						BOOST_LOG_TRIVIAL(error)
							<< "risa::cuda::FilterStatistical: Filter size is too large. "
							   "Maximum neighbourhood size for '"
							<< filterType << "' is " << SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE
							<< " elements.";
					}
					else
					{
						m_selectIndex = tmpIndex;
						hasValidSetting = true;
					}
				}
			}

			if(m_filterType == detail::FilterTypeSpecial::median)
				m_selectIndex = m_filterKernelWidth * m_filterKernelHeight / 2;

			if(hasValidSetting)
			{
				std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Filter2D: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto FilterStatistical::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;

	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".filterType", filterType))
	{

		m_filterType = getFilterTypeSpecialFromString(filterType);

		if(!configReader.lookupValue(parameterSet + ".filterKernelWidth", m_filterKernelWidth)
			|| !configReader.lookupValue(parameterSet + ".filterKernelHeight", m_filterKernelHeight))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "risa::cuda::FilterStatistical: No filter kernel size specified for filter type '"
				<< filterType << "'.";
			return false;
		}
		if(m_filterKernelWidth * m_filterKernelHeight > SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE)
		{
			BOOST_LOG_TRIVIAL(error)
				<< "risa::cuda::FilterStatistical: Filter size is too large. Maximum neighbourhood size "
				   "for '"
				<< filterType << "' is " << SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE << " elements.";
			return false;
		}

		switch(m_filterType)
		{
		case detail::FilterTypeSpecial::median:
		{
			m_selectIndex = (m_filterKernelHeight * m_filterKernelWidth) / 2;
			break;
		}
		case detail::FilterTypeSpecial::selectIndex:
		{
			if(!configReader.lookupValue(parameterSet + ".filterSelectIndex", m_selectIndex))
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::FilterStatistical: No select index defined for filter type '"
					<< filterType << "'.";
				return false;
			}
			if(m_selectIndex < 0 || m_selectIndex >= m_filterKernelWidth * m_filterKernelHeight)
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::FilterStatistical: Select index '" << m_selectIndex <<  "'out of range. Valid: 0 ... "
					<< m_filterKernelHeight * m_filterKernelWidth - 1;
				return false;
			}
			break;
		}
		case detail::FilterTypeSpecial::UNDEFINED:
		{
			if(filterType != "filterTypeSpecialNotDefined")
			{
				BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::FilterStatistical: Requested filter \"" << filterType
										 << "\"mode not supported.";
				return false;
			}
			m_filterKernelHeight = 1;
			m_filterKernelWidth = 1;
		}
		}

		return true;
	}
	return false;
}

__global__ void selectIndexFilter(const int imageWidth, const int imageHeight, float* dataIn, float* dataConv,
	const int filterWidth, const int filterHeight, const int selectIndex)
{

	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	int idy = threadIdx.y + blockIdx.y * blockDim.y;

	int size = filterWidth * filterHeight;

	if(idx >= imageWidth || idy >= imageHeight)
		return;

	float value;

	// create and fill neighbourhood to filter
	float neighbourhood[SELECT_INDEX_FILTER_MAX_NEIGHBOURHOOD_SIZE] = {0.0f};

	const int cx = filterWidth / 2;
	const int cy = filterHeight / 2;

	for(int i = 0; i < filterHeight; i++)
	{
		int y = idy + i - cy;
		if(y >= 0 && y < imageHeight)
		{

			for(int j = 0; j < filterWidth; j++)
			{
				int x = idx + j - cx;

				if(x >= 0 && x < imageWidth)
				{
					neighbourhood[i * filterWidth + j] = dataIn[y * imageWidth + x];
				}
			}
		}
	}

	// determine median of neighbourhood using quickselect algorithm
	// (https://en.wikipedia.org/wiki/Quickselect)
	int low = 0;
	int high = size - 1;
	int middle, ll, hh;

	while(true)
	{
		if(high <= low)
		{
			value = neighbourhood[selectIndex];
			break;
		}
		if(high == low + 1)
		{
			if(neighbourhood[low] > neighbourhood[high])
				ELEM_SWAP(neighbourhood[low], neighbourhood[high]);
			value = neighbourhood[selectIndex];
			break;
		}

		middle = (low + high) / 2;
		if(neighbourhood[middle] > neighbourhood[high])
			ELEM_SWAP(neighbourhood[middle], neighbourhood[high]);
		if(neighbourhood[low] > neighbourhood[high])
			ELEM_SWAP(neighbourhood[low], neighbourhood[high]);
		if(neighbourhood[middle] > neighbourhood[low])
			ELEM_SWAP(neighbourhood[middle], neighbourhood[low]);

		ELEM_SWAP(neighbourhood[middle], neighbourhood[low + 1]);

		ll = low + 1;
		hh = high;
		while(true)
		{
			do
				ll++;
			while(ll < size && neighbourhood[low] > neighbourhood[ll]);
			do
				hh--;
			while(hh >= 0 && neighbourhood[hh] > neighbourhood[low]);

			if(hh < ll)
				break;

			ELEM_SWAP(neighbourhood[ll], neighbourhood[hh]);
		}

		ELEM_SWAP(neighbourhood[low], neighbourhood[hh]);

		if(hh <= selectIndex)
			low = ll;
		if(hh >= selectIndex)
			high = hh - 1;
	}

	dataConv[idy * imageWidth + idx] = value;
}

}
}
