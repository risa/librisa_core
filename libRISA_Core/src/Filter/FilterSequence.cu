// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Filter/FilterSequence.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Coordinates.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

FilterSequence::FilterSequence(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::FilterSequence: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	for(auto deviceID = 0; deviceID < m_numberOfDevices; deviceID++)
	{
		CHECK(cudaSetDevice(deviceID));

		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
		m_streams[deviceID] = stream;
	}

	m_doConfig = true;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&FilterSequence::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: Running " << m_numberOfDevices << " thread(s).";
}

FilterSequence::~FilterSequence()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: Destroyed.";
}

auto FilterSequence::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence:       (Device " << image.device()
								 << " |  Input " << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: Finished.";
	}
}

auto FilterSequence::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto FilterSequence::processor(const int deviceID) -> void
{
	// nvtxNameOsThreadA(pthread_self(), "Filter");
	CHECK(cudaSetDevice(deviceID));
	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid((int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: Running thread for device " << deviceID;

	while(m_doConfig)
	{
		const int filterSequenceLength = m_filterSequenceLength;
		const int bufferSize = m_imageWidth * m_imageHeight * filterSequenceLength;

		auto weights_d =
			glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(m_filter.size());
		CHECK(cudaMemcpy(
			weights_d.get(), m_filter.data(), sizeof(float) * m_filter.size(), cudaMemcpyHostToDevice));

		std::vector<float> imageBuffer(bufferSize * m_numberOfPlanes, 0.0);
		auto buffer_d =
			glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(imageBuffer.size());
		CHECK(cudaMemcpy(
			buffer_d.get(), imageBuffer.data(), sizeof(float) * imageBuffer.size(), cudaMemcpyHostToDevice));

		std::vector<int> buffIdx;
		for(int i = 0; i < m_numberOfPlanes; i++)
			buffIdx.push_back(0);

		m_doConfig = false;

		while(true)
		{
			auto image = m_images[deviceID][0].take();
			if(!image.valid())
				break;

			// Filtering
			if(filterSequenceLength > 1)
			{
				applyFilterSequence<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(m_imageWidth, m_imageHeight,
					image.data(), &buffer_d.get()[image.plane() * bufferSize], weights_d.get(),
					filterSequenceLength, buffIdx[image.plane()]);

				buffIdx[image.plane()] = (buffIdx[image.plane()] + 1) % filterSequenceLength;
			}
			CHECK(cudaPeekAtLastError());

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), image.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(image.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: (Device " << deviceID << " | Output "
										 << i << "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: (Device " << deviceID << " | Output "
									 << 0 << "): Image " << image.index() << " processed.";
			m_results[0].push(std::move(image));

			if(m_doConfig)
			{ // trigger for runtime reconfiguration
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::FilterSequence: Reconfiguration triggered.";
				break; // breaks inner while, outer will keep running after reconfiguration
			}
		}
	}
}

auto FilterSequence::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: ###### UPDATE ###### -> " << fo->m_fileName
							 << ": " << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::FilterSequence: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = configReader.lookupValue("weights", m_filter);
			m_filterSequenceLength = m_filter.size();

			if(hasValidSetting)
			{
				const float sum = std::accumulate(m_filter.begin(), m_filter.end(), 0.0f);
				if(sum != 0.0f)
				{
					// do not normalize filters with sum zero (e.g. Laplace)
					const float normalizationFactor = 1.0 / sum;
					std::transform(m_filter.begin(), m_filter.end(), m_filter.begin(),
						[normalizationFactor](float v) -> float { return v * normalizationFactor; });
				}
				std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::FilterSequence: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto FilterSequence::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".numberOfPlanes", m_numberOfPlanes)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".weights", m_filter))
	{
		m_filterSequenceLength = m_filter.size();
		const float sum = std::accumulate(m_filter.begin(), m_filter.end(), 0.0f);
		if(sum != 0.0f)
		{
			// do not normalize filters with sum zero (e.g. Laplace)
			const float normalizationFactor = 1.0 / sum;
			std::transform(m_filter.begin(), m_filter.end(), m_filter.begin(),
				[normalizationFactor](float v) -> float { return v * normalizationFactor; });
		}
		return true;
	}
	return false;
}

__global__ void applyFilterSequence(const int imageWidth, const int imageHeight, float* currentImage,
	float* __restrict__ buffer, const float* weights, const int sequenceLen, const int currBufferIndex)
{

	const auto x = glados::cuda::getX();
	const auto y = glados::cuda::getY();

	if(x >= imageWidth || y >= imageHeight)
		return;

	// overwrite image at currBuffer index:
	const int bufferOffset = currBufferIndex * imageWidth * imageHeight;
	const int idx = y * imageWidth + x;
	buffer[bufferOffset + idx] = currentImage[idx];

	float tmp = 0.0;
	for(int i = 0; i < sequenceLen; i++)
	{
		int bufferIdx = (sequenceLen + currBufferIndex - i) % sequenceLen;
		tmp += buffer[bufferIdx * imageWidth * imageHeight + idx] * weights[i];
	}
	currentImage[idx] = tmp;
}
}
}
