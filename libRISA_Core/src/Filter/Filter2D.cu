// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaCore/Basics/performance.h>
#include <risaCore/ConfigReader/ConfigReader.h>
#include <risaCore/Filter/Filter2D.h>

#include <glados/cuda/Check.h>
#include <glados/cuda/Launch.h>
#include <glados/observer/FileObserver.h>

#include <boost/log/trivial.hpp>

#include <exception>

#ifdef __linux__
#define _USE_MATH_DEFINES
#elif _WIN32
#define M_PI 3.14159265358979323846
#endif

namespace risa
{
namespace cuda
{

Filter2D::Filter2D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
	const int numberOfInputs)
	: m_numberOfOutputs(numberOfOutputs), m_numberOfInputs(numberOfInputs)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::Filter2D: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		m_memoryPoolIdxs.push_back(glados::MemoryPool<deviceManagerType>::getInstance().registerStage(
			m_memPoolSize, m_imageWidth * m_imageHeight));
	}

	for(auto deviceID = 0; deviceID < m_numberOfDevices; deviceID++)
	{
		CHECK(cudaSetDevice(deviceID));

		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 3));
		m_streams[deviceID] = stream;
	}

	// load/generate filter kernel
	designFilter();
	m_doConfig = true;

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_images[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Filter2D::processor, this, i};
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: Running " << m_numberOfDevices << " thread(s).";
}

Filter2D::~Filter2D()
{
	for(auto idx : m_memoryPoolIdxs)
	{
		glados::MemoryPool<deviceManagerType>::getInstance().freeMemory(idx);
	}
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaStreamDestroy(m_streams[i]));
	}
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: Destroyed.";
}

auto Filter2D::process(input_type&& image, int inputIdx) -> void
{
	if(image.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D:       (Device " << image.device() << " |  Input "
								 << inputIdx << "): Image " << image.index() << " arrived.";
		m_images[image.device()][inputIdx].push(std::move(image));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: Received sentinel, finishing.";

		// send sentinel to processor thread and wait 'til it's finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_images[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}
		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: Finished.";
	}
}

auto Filter2D::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Filter2D::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	auto convolutedImage =
		glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(m_imageHeight * m_imageWidth);

	dim3 dimBlock(m_blockSize2D, m_blockSize2D);
	dim3 dimGrid((int)ceil(m_imageWidth / (float)m_blockSize2D), (int)ceil(m_imageHeight / (float)m_blockSize2D));

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: Running thread for device " << deviceID;

	while(m_doConfig)
	{
		auto filterFunction_d =
			glados::cuda::make_device_ptr<float, glados::cuda::async_copy_policy>(m_filter.size());
		CHECK(cudaMemcpy(
			filterFunction_d.get(), m_filter.data(), sizeof(float) * m_filter.size(), cudaMemcpyHostToDevice));
		m_doConfig = false;

		while(true)
		{
			auto image = m_images[deviceID][0].take();
			if(!image.valid())
				break;

			// Filtering
			applyConvolution2D<<<dimGrid, dimBlock, 0, m_streams[deviceID]>>>(m_imageWidth, m_imageHeight,
				image.data(), convolutedImage.get(), filterFunction_d.get(), m_filterKernelWidth,
				m_filterKernelHeight);

			CHECK(cudaPeekAtLastError());

			cudaMemcpyAsync(image.data(), convolutedImage.get(), m_imageWidth * m_imageHeight * sizeof(float),
				cudaMemcpyDeviceToDevice, m_streams[deviceID]);

			// wait until work on device is finished
			CHECK(cudaStreamSynchronize(m_streams[deviceID]));

			for(int i = 1; i < m_numberOfOutputs; i++)
			{
				auto imgSplit = glados::MemoryPool<deviceManagerType>::getInstance().requestMemory(
					m_memoryPoolIdxs[deviceID]);

				cudaMemcpyAsync(imgSplit.data(), image.data(), m_imageWidth * m_imageHeight * sizeof(float),
					cudaMemcpyDeviceToDevice, m_streams[deviceID]);

				imgSplit.setDevice(deviceID);
				imgSplit.setProperties(image.properties());

				// wait until work on device is finished
				CHECK(cudaStreamSynchronize(m_streams[deviceID]));
				BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D:       (Device " << deviceID << " | Output "
										 << i << "): Image " << imgSplit.index() << " processed.";
				m_results[i].push(std::move(imgSplit));
			}

			BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D:       (Device " << deviceID << " | Output "
									 << 0 << "): Image " << image.index() << " processed.";
			m_results[0].push(std::move(image));

			if(m_doConfig)
			{ // trigger for runtime reconfiguration
				BOOST_LOG_TRIVIAL(info) << "risa::cuda::Filter2D:      Reconfiguration triggered.";
				break; // breaks inner while, outer will keep running after reconfiguration
			}
		}
	}
}

auto Filter2D::designFilter() -> void
{

	m_filter.clear();

	if(m_filterType == detail::FilterType2D::custom)
	{
		m_filter = m_filterKernelCustomValues;
	}
	else
	{
		int filterNumElements = m_filterKernelWidth * m_filterKernelHeight;
		// generate predefined filter types
		if(m_filterType == detail::FilterType2D::sobelHorizontal)
		{
			m_filter.assign(m_filterValuesSobelHorizontal, m_filterValuesSobelHorizontal + filterNumElements);
		}
		else if(m_filterType == detail::FilterType2D::sobelVertical)
		{
			m_filter.assign(m_filterValuesSobelVertical, m_filterValuesSobelVertical + filterNumElements);
		}
		else if(m_filterType == detail::FilterType2D::laplace)
		{
			m_filter.assign(m_filterValuesLaplace, m_filterValuesLaplace + filterNumElements);
		}
		else if(m_filterType == detail::FilterType2D::laplaceDiagonalEdges)
		{
			m_filter.assign(
				m_filterValuesLaplaceDiagonalEdges, m_filterValuesLaplaceDiagonalEdges + filterNumElements);
		}
		else if(m_filterType == detail::FilterType2D::gaussian)
		{
			generateFilterGaussian(m_filterKernelWidth, m_filterKernelHeight, m_filterKernelGaussianSigma);
		}
		else if(m_filterType == detail::FilterType2D::mean)
		{
			const float meanVal = 1.0 / m_filterKernelWidth / m_filterKernelHeight;
			for(int i = 0; i < m_filterKernelWidth * m_filterKernelHeight; i++)
				m_filter.push_back(meanVal);
		}
		else
		{
			// no valid filter kernel definition -> use unfiltered
			m_filterKernelWidth = 1;
			m_filterKernelHeight = 1;
			m_filter.push_back(1.0);
		}
	}

	// normalize
	const float sum = std::accumulate(m_filter.begin(), m_filter.end(), 0.0f);
	if(sum != 0.0f)
	{
		// do not normalize filters with sum zero (e.g. Laplace)
		const float normalizationFactor = 1.0 / sum;
		std::transform(m_filter.begin(), m_filter.end(), m_filter.begin(),
			[normalizationFactor](float v) -> float { return v * normalizationFactor; });
	}
}

auto Filter2D::update(glados::Subject* s) -> void
{
	glados::FileObserver* fo = static_cast<glados::FileObserver*>(s);
	std::string fileContentStr(fo->m_fileContents.begin(), fo->m_fileContents.end());
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: ###### UPDATE ###### -> " << fo->m_fileName << ": "
							 << fo->m_message;
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Filter2D: ###### CONTENT ##### -> " << fileContentStr;
	if(fo->m_fileContentsValid)
	{
		ConfigReader configReader = ConfigReader(true); // true -> allowed to fail
		if(configReader.parseString(fileContentStr))
		{
			bool hasValidSetting = false;
			std::string filterType;
			if(configReader.lookupValue("filterType", filterType))
			{
				m_filterType = getFilterTypeFromString(filterType);
				hasValidSetting = true;
			}

			if(m_filterType == detail::FilterType2D::UNDEFINED && filterType != "filterType2DNotDefined")
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Filter2D: Dynamic reconfiguration has invalid filter type \""
					<< filterType << "\".";
				hasValidSetting = false;
			}

			if(m_filterType == detail::FilterType2D::gaussian)
			{
				hasValidSetting =
					configReader.lookupValue("filterKernelGaussianSigma", m_filterKernelGaussianSigma)
					|| hasValidSetting;
			}

			if(m_filterType == detail::FilterType2D::custom || m_filterType == detail::FilterType2D::mean
				|| m_filterType == detail::FilterType2D::gaussian)
			{
				int tmp = 1;
				if(configReader.lookupValue("filterKernelWidth", tmp) && (tmp % 2 == 1))
				{
					hasValidSetting =
						configReader.lookupValue("filterKernelWidth", m_filterKernelWidth) || hasValidSetting;
				}
				else if(tmp % 2 == 0)
				{
					BOOST_LOG_TRIVIAL(warning)
						<< "risa::cuda::Filter2D: Dynamic reconfiguration has invalid filterKernelWidth ("
						<< tmp << "). Must be an odd value.";
				}

				tmp = 1;
				if(configReader.lookupValue("filterKernelHeight", tmp) && (tmp % 2 == 1))
				{
					hasValidSetting = configReader.lookupValue("filterKernelHeight", m_filterKernelHeight)
						|| hasValidSetting;
				}
				else if(tmp % 2 == 0)
				{
					BOOST_LOG_TRIVIAL(warning)
						<< "risa::cuda::Filter2D: Dynamic reconfiguration has invalid filterKernelHeight ("
						<< tmp << "). Must be an odd value.";
				}
			}

			if(m_filterType == detail::FilterType2D::sobelHorizontal
				|| m_filterType == detail::FilterType2D::sobelVertical
				|| m_filterType == detail::FilterType2D::laplace
				|| m_filterType == detail::FilterType2D::laplaceDiagonalEdges)
			{
				m_filterKernelWidth = 3;
				m_filterKernelHeight = 3;
			}

			if(m_filterType == detail::FilterType2D::custom)
			{
				const int numElements = m_filterKernelHeight * m_filterKernelWidth;
				std::vector<float> tmp;
				if(configReader.lookupValue("filterKernelCustomValues", tmp))
				{ // read in temp to prevent overwriting with faulty values
					if(tmp.size() == numElements)
					{
						configReader.lookupValue("filterKernelCustomValues", m_filterKernelCustomValues);
						hasValidSetting = true;
					}
				}
			}

			if(hasValidSetting)
			{
				designFilter();
				std::unique_lock<std::mutex> lock(m_mutex);
				m_doConfig = true; // trigger reconfig
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "risa::cuda::Filter2D: Dynamic reconfiguration has no valid entry.";
			}
		}
	}
}

auto Filter2D::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	std::string filterType;
	if(configReader.lookupValue(parameterSet + ".imageHeight", m_imageHeight)
		&& configReader.lookupValue(parameterSet + ".imageWidth", m_imageWidth)
		&& configReader.lookupValue(parameterSet + ".blockSize2D", m_blockSize2D)
		&& configReader.lookupValue(parameterSet + ".memPoolSize", m_memPoolSize)
		&& configReader.lookupValue(parameterSet + ".filterType", filterType))
	{

		m_filterType = getFilterTypeFromString(filterType);

		switch(m_filterType)
		{
		case detail::FilterType2D::UNDEFINED:
		{
			if(filterType != "filterType2DNotDefined")
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Filter2D: Requested filter \"" << filterType << "\"mode not supported.";
				return false;
			}
			m_filterType = detail::FilterType2D::custom;
			m_filterKernelWidth = 1;
			m_filterKernelHeight = 1;
			m_filterKernelCustomValues = {1};
			break;
		}
		case detail::FilterType2D::gaussian:
		{
			if(!configReader.lookupValue(
				   parameterSet + ".filterKernelGaussianSigma", m_filterKernelGaussianSigma))
			{
				m_filterKernelGaussianSigma = 1.0; // default value when not set in config file
			}
			// deliberate fallthrough here
		}
		case detail::FilterType2D::mean:
		case detail::FilterType2D::custom:
		{
			if(!configReader.lookupValue(parameterSet + ".filterKernelWidth", m_filterKernelWidth)
				|| !configReader.lookupValue(parameterSet + ".filterKernelHeight", m_filterKernelHeight))
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Filter2D: No filter kernel size specified for filter type '" << filterType
					<< "'.";
				return false;
			}

			if(m_filterKernelWidth % 2 == 0 || m_filterKernelHeight % 2 == 0)
			{
				BOOST_LOG_TRIVIAL(error)
					<< "risa::cuda::Filter2D: Only odd filter kernel lengths are supported.";
				return false;
			}
			break;
		}
		case detail::FilterType2D::laplace:
		case detail::FilterType2D::laplaceDiagonalEdges:
		case detail::FilterType2D::sobelHorizontal:
		case detail::FilterType2D::sobelVertical:
		{
			m_filterKernelWidth = 3;
			m_filterKernelHeight = 3;
			break;
		}
		
		}

		if(m_filterType == detail::FilterType2D::custom)
		{
			const unsigned int numElements = m_filterKernelHeight * m_filterKernelWidth;
			m_filterKernelCustomValues.clear();
			if(!configReader.lookupValue(
				   parameterSet + ".filterKernelCustomValues", m_filterKernelCustomValues))
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Filter2D: Custom filter kernel values not provided.";
				return false;
			}
			if(m_filterKernelCustomValues.size() != numElements)
			{
				BOOST_LOG_TRIVIAL(error) << "risa::cuda::Filter2D: Number of custom filter kernel "
											"values does not match custom filter value size.";
				return false;
			}
		}

		return true;
	}
	return false;
}

__global__ void applyConvolution2D(const int imageWidth, const int imageHeight, float* dataIn,
	float* dataConv, const float* const __restrict__ filter, const int filterWidth, const int filterHeight)
{
	const int j = blockIdx.y * blockDim.y + threadIdx.y;
	const int i = blockIdx.x * blockDim.x + threadIdx.x;

	const int dx = filterWidth / 2;
	const int dy = filterHeight / 2;
	float convolutedValue = 0.0;

	if(i < imageWidth && j < imageHeight)
	{

		for(int xFilter = 0; xFilter < filterWidth; xFilter++)
		{
			int xImage = i - dx + xFilter;
			if(xImage >= 0 && xImage < imageWidth)
				for(int yFilter = 0; yFilter < filterHeight; yFilter++)
				{
					int yImage = j - dy + yFilter;
					if(yImage >= 0 && yImage < imageHeight)
					{
						convolutedValue +=
							dataIn[xImage + yImage * imageWidth] * filter[xFilter + yFilter * filterWidth];
					}
				}
		}

		dataConv[i + j * imageWidth] = convolutedValue;
	}
}

auto Filter2D::generateFilterGaussian(int filterWidth, int filterHeight, float sigma) -> void
{
	const float cx = filterWidth / 2.0;
	const float cy = filterHeight / 2.0;
	const float p = 1.0 / (2 * M_PI * sigma * sigma);
	const float q = -1.0 / (2.0 * sigma * sigma);

	m_filter.clear();

	for(auto y = 0; y < filterHeight; y++)
		for(auto x = 0; x < filterWidth; x++)
			m_filter.push_back(p * exp(q * ((x - cx) * (x - cx) + (y - cy) * (y - cy))));
}

}
}
