// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef RISA_CORE_H_
#define RISA_CORE_H_

#include <risaCore/Average/Average.h>
#include <risaCore/Combine/Combine.h>
#include <risaCore/Convert/ushortToFloat.h>
#include <risaCore/Copy/D2H.h>
#include <risaCore/Filter/Filter1D.h>
#include <risaCore/Filter/Filter2D.h>
#include <risaCore/Filter/FilterSequence.h>
#include <risaCore/Filter/FilterStatistical.h>
#include <risaCore/Flip/Flip.h>
#include <risaCore/Flow/Interleave.h>
#include <risaCore/Flow/Split.h>
#include <risaCore/Flow/StartAfterNImages.h>
#include <risaCore/Flow/Stitch.h>
#include <risaCore/Flow/StopAfterNImages.h>
#include <risaCore/Loader/ImageSequenceLoader.h>
#ifdef LIBRISACORE_HAS_HDF5
#include <risaCore/Loader/HDF5Loader.h>
#endif
#include <risaCore/Masking/Masking.h>
#include <risaCore/Property/GetProperty.h>
#include <risaCore/Property/SetProperty.h>
#include <risaCore/Reduce/Reduce.h>
#include <risaCore/Resize/Reframe.h>
#include <risaCore/Resize/Scale.h>
#include <risaCore/Rotate/Rotate.h>
#include <risaCore/Saver/DeviceDiscarder.h>
#ifdef LIBRISACORE_HAS_HDF5
#include <risaCore/Saver/HDF5Saver.h>
#endif
#ifdef LIBRISACORE_HAS_OPENCV
#include <risaCore/Saver/OpenCVViewer.h>
#endif
#ifdef __linux__
#include <risaCore/Saver/SharedHostMemSaver.h>
#endif
#ifdef LIBRISACORE_HAS_TIFF
#include <risaCore/Saver/TIFFStackSaver.h>
#endif
#ifdef LIBRISACORE_HAS_JPEG_ENCODER
#include <risaCore/Saver/WebCompatibleSaver.h>
#endif
#include <risaCore/Segmentation/Watershed.h>
#include <risaCore/Statistics/HistogramPerPixel.h>
#include <risaCore/Transform/DistanceTransform.h>
#include <risaCore/Transform/FFT2D.h>
#include <risaCore/Transform/IFFT2D.h>
#include <risaCore/Transform/Transform2D.h>

#endif // !RISA_CORE_H_
