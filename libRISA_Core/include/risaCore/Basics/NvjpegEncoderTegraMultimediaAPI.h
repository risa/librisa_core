#ifndef NVJPEGENCODERIMPL_H
#define NVJPEGENCODERIMPL_H

#include <glados/Singleton.h>

#include <condition_variable>
#include <map>
#include <string>
#include <thread>

#include "NvJpegEncoder.h" // from tegra-multimedia-API
#include "NvBufSurface.h"  // from tegra-multimedia-API

#include <boost/log/trivial.hpp>

namespace risa
{
namespace cuda
{

typedef struct
{
	cudaStream_t* stream;
	unsigned char* buffer;
	int width;
	int height;
	int plane;
} image_props;
class NvjpegEncoder : public Singleton<NvjpegEncoder>
{
	public:
	NvjpegEncoder() = default;
	~NvjpegEncoder();
	auto init(int quality) -> void;

	auto encodeImage(image_props const* image, std::vector<unsigned char>& jpeg) -> void;

	private:

	auto initAndRun() -> void;
	std::condition_variable m_cvInit, m_cvEncode, m_cvWork;
	std::mutex m_mtxInit, m_mtxEncode, m_mtxWork;
	bool m_initOk{false};
	bool m_workAvailable{false};
	bool m_workDone{false};
	const image_props* m_workImage;
	std::vector<unsigned char>* m_workData;
	bool m_stopFlag{false};
	std::thread m_encoderThread;

	int m_jpegQuality{70};
	
	

};
}
}

#endif /*NVJPEGENCODERIMPL_H*/