// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),

#ifndef ENUMS_H_
#define ENUMS_H_

#include <string>

namespace risa
{
namespace cuda
{

namespace detail
{
enum class loadControlMode
{
	automatic,
	triggered
};

enum class dataType
{
	t_bool,
	t_int8,
	t_int16,
	t_int32,
	t_int64,
	t_uint8,
	t_uint16,
	t_uint32,
	t_uint64,
	t_float,
	t_double,
	t_string,
	t_unknown
};

enum class GetPropertyType
{
	all, /* get all available properties and try to print */
	duration, /* float, duration from setting "start" in milliseconds */
	floatingPoint, /* float, static value*/
	signed_integer, /* ssize_t, static value */
	integer, /* size_t, static value */
	text, /* std::string, static value */
	timestamp /* std::chrono::time_point<std::chrono::high_resolution_clock> */
};

enum class GetPropertyOutput
{
	print,
	writeAllToFile,
	writeLatestToFile
};

enum class GetPropertyOutputFormat
{
	csv,
	keyValue
};

struct GetPropertyDescriptor
{
	GetPropertyType m_propType;
	std::string m_propKey;
};

enum H5DIM
{
	// the order of dimensions is based on the HDF5 documentation (https://docs.hdfgroup.org/hdf5/develop/_f_m_t11.html):
	// 'The first dimension stored in the list of dimensions is the slowest changing dimension and the last
	// dimension stored is the fastest changing dimension.'
	SLICES = 0,
	HEIGHT = 1,
	WIDTH = 2
};

auto getBytesPerElem(dataType type) -> int;

} // namespace detail
} // namespace cuda
} // namespace risa

#endif /* ENUMS_H_ */
