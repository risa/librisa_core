#ifndef NVJPEGENCODER_H
#define NVJPEGENCODER_H

#ifdef LIBRISACORE_HAS_NVJPEG
#include "NvjpegEncoderNvJPEG.h"
#elif defined(LIBRISACORE_HAS_JETSON_MULTIMEDIA_API)
#include "NvjpegEncoderTegraMultimediaAPI.h"
#endif

#endif /*NVJPEGENCODER_H*/