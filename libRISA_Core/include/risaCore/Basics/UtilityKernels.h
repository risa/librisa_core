#ifndef UTILITY_KERNELS_H
#define UTILITY_KERNELS_H

#include <float.h> // FLT_MAX

#ifdef _WIN32
#define __FLT_MAX__ FLT_MAX
#endif

#define GET_MAX(A, B) A > B ? A : B
#define GET_MIN(A, B) A < B ? A : B
#define GET_PROD(A, B) A * B
#define GET_SUM(A, B) A + B

// if both are masked, return +/- FLT_MAX. if only one value is masked, return the other one.  if none is
// masked, return GET_MAX(A,B):
#define GET_MAX_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? -FLT_MAX : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : (GET_MAX(A, B))))

#define GET_MIN_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? FLT_MAX : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : (GET_MIN(A, B))))

#define GET_PROD_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? 1 : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : GET_PROD(A, B)))

#define GET_SUM_MASKED(A, MASK_A, B, MASK_B) \
	(MASK_A && MASK_B) ? 0 : ((MASK_A && !MASK_B) ? B : ((!MASK_A && MASK_B) ? A : GET_SUM(A, B)))

namespace risa
{

namespace cuda
{

//! This cuda kernel normalizes a float image into an 8-bit RGB image (values between 0 and 255)
/**
 * @param[in]  img		       input array (image)
 * @param[out] img8bit		   output array (8bit RGB image)
 * @param[in]  imageWidth	   image width in pixels
 * @param[in]  imageHeight	   image height in pixels
 * @param[in]  minValue		   lowest pixel value in input image
 * @param[in]  maxValue		   highest pixel value in input image
 */
__global__ void normalizeAndConvertTo8BitRGB(float* __restrict__ img, unsigned char* img8bit,
	const int imageWidth, const int imageHeight, const float minValue, const float maxValue);

__global__ void normalizeAndConvertTo8BitRGBA(float* __restrict__ img, unsigned char* img8bit,
	const int imageWidth, const int imageHeight, const float minValue, const float maxValue);

namespace detail
{
using ReducePropertyType = int;
namespace ReduceProperty
{
// workaround instead of 'enum class' for easier bitwise ops
enum : ReducePropertyType
{
	Minimum = 1,
	Maximum = 2,
	Sum = 4,
	Product = 8
};

auto getReducePropertyArraySize(size_t numBlocks) -> size_t;

auto getSharedMemSize(unsigned int blockSize2D) -> size_t;

auto getNthMaxIdx(size_t numBlocks, unsigned int n) -> size_t;

auto getNthMinIdx(size_t numBlocks, unsigned int n) -> size_t;

auto getNthProdIdx(size_t numBlocks, unsigned int n) -> size_t;

auto getNthSumIdx(size_t numBlocks, unsigned int n) -> size_t;
}
}

//! This cuda kernel reduces a float array to the requested properties
/**
* The respective properties are enabled by compile-time flags.
* 
 * @tparam     blockSize	   number of threads per block
 * @tparam     props		   properties to reduce (flags: min, max, sum, product)
 * @param[in]  in	           input array (image)
 * @param[in]  size	           input array size (number of elements)
 * @param[out] reducedProps    min and max value of the array
 * @param[in]  mask			   bool mask array of image size with true = masked out value
 */
template <unsigned int blockSize, detail::ReducePropertyType props, bool useMask = false>
__global__ void reduceKernel(const float* __restrict__ in, const size_t size, float* reducedProps, const bool* mask = nullptr);


template <typename T_from, typename T_to>
__global__ void convertType(const T_from* __restrict__ in, T_to* out, size_t imageWidth, size_t imageHeight);
} // namespace risa
} // namespace cuda

#endif // UTILITY_KERNELS_H