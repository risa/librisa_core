#ifndef NVJPEGENCODERIMPL_H
#define NVJPEGENCODERIMPL_H

#include <glados/Singleton.h>

#include <condition_variable>
#include <map>
#include <string>
#include <thread>
#include <nvjpeg.h>

#include <boost/log/trivial.hpp>

namespace risa
{
namespace cuda
{
typedef struct
{
	cudaStream_t* stream;
	unsigned char* buffer;
	int width;
	int height;
	int plane;
} image_props;

class NvjpegEncoder : public Singleton<NvjpegEncoder>
{
	public:
	NvjpegEncoder() = default;
	~NvjpegEncoder();
	auto init(int quality) -> void;

	auto encodeImage(image_props const* image, std::vector<unsigned char>& jpeg) -> void;

	private:

	auto initAndRun() -> void;
	std::condition_variable m_cvInit, m_cvEncode, m_cvWork;
	std::mutex m_mtxInit, m_mtxEncode, m_mtxWork;
	bool m_initOk{false};
	bool m_workAvailable{false};
	bool m_workDone{false};
	const image_props* m_workImage;
	std::vector<unsigned char>* m_workData;
	bool m_stopFlag{false};
	std::thread m_encoderThread;

	int m_jpegQuality{70};
	nvjpegImage_t* m_nv_image = nullptr;
	nvjpegHandle_t* m_nv_handle = nullptr;
	nvjpegEncoderState_t* m_nv_enc_state = nullptr;
	nvjpegEncoderParams_t* m_nv_enc_params = nullptr;
};
}
}

#endif /*NVJPEGENCODERIMPL_H*/