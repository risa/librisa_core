// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef PERFORMANCE_H_
#define PERFORMANCE_H_

#include <chrono>

namespace risa
{

//! The Timer-class uses the std::chrono library and provides a high precision timer.
/**
 * This class provides a high precision timer based on the chrono-library of C++11.
 * It can be used, to measure the elapsed time.
 */
class Timer
{

	public:
	Timer() {}

	//! Start the time duration measurement.
	void start() { m_beg = m_clock::now(); }

	//! Stop the time duration measurement.
	void stop() { m_end = m_clock::now(); }

	//! Computes the elapsed time between #start() and #stop()
	/**
	 * @return  the elapsed time duration between #start() and #stop()
	 */
	double elapsed() const { return std::chrono::duration_cast<m_second>(m_end - m_beg).count(); }

	private:
	typedef std::chrono::steady_clock m_clock;
	typedef std::chrono::duration<double, std::ratio<1>> m_second;
	std::chrono::time_point<m_clock> m_beg; //!<  stores the beginning point
	std::chrono::time_point<m_clock> m_end; //!<  stores the end time point
};

}

#endif /* PERFORMANCE_H_ */
