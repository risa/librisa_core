// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef CONFIGREADER_H
#define CONFIGREADER_H
#pragma once

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // fix deprecation warning in boost 1.74 (latest version for
									   // ubuntu 22.04)
#include <boost/log/trivial.hpp>

#include "json.hpp"

#include <string>
#include <vector>
#include <sstream>

namespace risa
{

//!   Reads the configuration values from the input file.
/**
 * This class takes the file path to a JSON configuration file as input and reads it
 * using boost::property_tree::ptree.
 */
class ConfigReader
{

	public:
	//! Create the configuration reader from the given input file
	/**
	 *
	 * @param[in]  configFile  path to configuration file
	 * @param[in]  allowedToFail   whether the variable lookup is allowed to fail. Setting to `true`
	 * surpresses warnings for failed variable lookups.
	 *
	 */
	ConfigReader(const char* configFile, bool allowedToFail = false);

	//! Default construct a ConfigReader. Actual configuration needs to be read later using parseFile method
	//! or parseString method.
	/**
	 *
	 * @param[in]  allowedToFail   whether the variable lookup is allowed to fail. Setting to `true`
	 * surpresses warnings for failed variable lookups.
	 *
	 */
	ConfigReader(bool allowedToFail = false);

	//! Set the allowedToFail-flag to surpress error messages from missing parameters
	void setAllowedToFail(bool val) { m_allowedToFail = val; };

	//! Parse the configuration at the specified path.
	/**
	 * @param[in]  configPath the file path of the configuration file
	 * @retval true   the file was parsed successfully
	 * @retval false  the file could not be parsed successfully
	 */
	bool parseFile(const std::string& configPath);

	//! Parse the given configuration string.
	/**
	 * @param[in]  configString the configuration string to parse
	 * @retval true   the string was parsed successfully
	 * @retval false  the string could not be parsed successfully
	 */
	bool parseString(const std::string& configString);

	//! Reads the configuration value identified by the identifier-string
	/**
	 * @param[in]  identifier  the string used to identify the desired parameter in the configuration file
	 * @param[out] value       the value, that was passed in the configuration file
	 * @retval true   the parameter could be read successfully
	 * @retval false  the parameter could not be read successfully
	 */
	template <typename T>
	bool lookupValue(const std::string& identifier, T& value)
	{
		if(!m_isInitialized)
		{
			BOOST_LOG_TRIVIAL(fatal) << "ConfigReader was not initialized before use. Use `parseFile` or "
										"`parseString` to initialize.";
			return false;
		}

		bool ret = false;

		try
		{
			// separate identifier by dots
			std::vector<std::string> tokens;
			std::stringstream remainder(identifier);
			std::string curr;
			while(getline(remainder, curr, '.'))
			{
				tokens.push_back(curr);
			}
			// iterate down to the object
			auto obj = cfg[tokens[0]];
			for(size_t depth = 1; depth < tokens.size(); depth++)
			{
				obj = obj[tokens[depth]];
			}

			// read the actual value
			value = obj.get<T>();
			ret = true;
		}
		catch(...)
		{
			; // handle?
		}

		if(ret)
			BOOST_LOG_TRIVIAL(debug) << "Configuration value " << identifier << ": " << value;
		else if(!m_allowedToFail)
			BOOST_LOG_TRIVIAL(error) << "Configuration value " << identifier << ": NOT FOUND";

		return ret;
	}

	//! Reads the configuration value identified by the identifier-string
	/**
	 * @param[in]  identifier  the string used to identify the desired parameter in the configuration file
	 * @param[out] value       the vector of values, that was passed in the configuration file
	 * @retval true   the parameter could be read successfully
	 * @retval false  the parameter could not be read successfully
	 */
	template <typename T>
	bool lookupValue(const std::string& identifier, std::vector<T>& value)
	{
		if(!m_isInitialized)
		{
			BOOST_LOG_TRIVIAL(fatal) << "ConfigReader was not initialized before use. Use `parseFile` or "
										"`parseString` to initialize.";
			return false;
		}

		try
		{
			// separate identifier by dots
			std::vector<std::string> tokens;
			std::stringstream remainder(identifier);
			std::string curr;
			while(getline(remainder, curr, '.'))
			{
				tokens.push_back(curr);
			}

			// iterate down to the object
			auto obj = cfg[tokens[0]];
			for(size_t depth = 1; depth < tokens.size(); depth++)
			{
				obj = obj[tokens[depth]];
			}

			for(const auto& elem : obj)
			{
				value.push_back(elem.get<T>());
			}
			if(obj.is_array())
			{
				BOOST_LOG_TRIVIAL(debug)
					<< "Configuration value " << identifier << ": <vector of size " << value.size() << ">";
				return true;
			}
			else if(obj.is_object())
			{
				if(!m_allowedToFail)
					BOOST_LOG_TRIVIAL(error) << "Configuration value " << identifier << ": EXPECTED ARRAY";

				return false;
			}

			if(!m_allowedToFail)
				BOOST_LOG_TRIVIAL(error) << "Configuration value " << identifier << ": NOT FOUND";

			return false;
		}
		catch(...)
		{
			if(!m_allowedToFail)
				BOOST_LOG_TRIVIAL(error) << "Configuration value " << identifier << ": NOT FOUND";

			return false;
		}
	}

	nlohmann::json lookup(const std::string& identifier)
	{
		nlohmann::json ret;
		if(!m_isInitialized)
		{
			BOOST_LOG_TRIVIAL(fatal) << "ConfigReader was not initialized before use. Use `parseFile` or "
										"`parseString` to initialize.";
			return ret;
		}

		try
		{
			// separate identifier by dots
			std::vector<std::string> tokens;
			std::stringstream remainder(identifier);
			std::string curr;
			while(getline(remainder, curr, '.'))
			{
				tokens.push_back(curr);
			}

			// iterate down to the object
			ret = cfg[tokens[0]];
			for(size_t depth = 1; depth < tokens.size(); depth++)
			{
				ret = ret[tokens[depth]];
			}
		}
		catch(...)
		{
			if(!m_allowedToFail)
				BOOST_LOG_TRIVIAL(error) << "Configuration value " << identifier << ": NOT FOUND";
		}
		return ret;
	}

	private:
	nlohmann::json cfg;
	bool m_allowedToFail;
	bool m_isInitialized = false;
};
}

#endif /*CONFIGREADER_H*/
