// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef RISA_MODULE_INTERFACE
#define RISA_MODULE_INTERFACE

#pragma once

#include <glados/Image.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/HostMemoryManager.h>
#include <glados/observer/Subject.h>
#include <glados/pipeline/GenericStage.h>

#ifdef __linux__
#define EXPORT extern "C"
#elif _WIN32
#define EXPORT extern "C" __declspec(dllexport)
#endif

//******************************
// SUPPORTED INPUT/OUTPUT TYPES
//******************************
// float, double, char, int16_t, uint16_t, int32_t, uint32_t, size_t
template <typename T>
using cuda_array = glados::Image<glados::cuda::DeviceMemoryManager<T, glados::cuda::async_copy_policy>>;

template <typename T>
using host_array = glados::Image<glados::cuda::HostMemoryManager<T, glados::cuda::async_copy_policy>>;

//********************************
// INTERFACE TO PROCESSING STAGE
//********************************

template <typename in_t, typename out_t>
class RISAModule_Processor
{
	public:
	virtual ~RISAModule_Processor() {}

	typedef in_t input_type;
	typedef out_t output_type;

	//! Push an input image of type `input_type` into the processing stage
	virtual void process(input_type&& img, int inputIdx) = 0;

	//! Wait until processed image is returned
	virtual output_type wait(int outputIdx) = 0;

	//! Update class member variables asynchronously
	virtual void update(glados::Subject* s) = 0;
};

//***************************
// INTERFACE TO SOURCE STAGE
//***************************

template <typename out_t>
class RISAModule_Source
{
	public:
	virtual ~RISAModule_Source() {}

	typedef out_t output_type;

	//! Load an image
	virtual output_type loadImage() = 0;

	//! Update class member variables asynchronously
	virtual void update(glados::Subject* s) = 0;
};

//**************************
//* INTERFACE TO SINK STAGE*
//**************************

template <typename in_t>
class RISAModule_Sink
{
	public:
	virtual ~RISAModule_Sink() {}

	typedef in_t input_type;

	//! Load an image
	virtual void saveImage(input_type image) = 0;

	//! Update class member variables asynchronously
	virtual void update(glados::Subject* s) = 0;
};

//*****************************************
//* INTERFACE TO EXPOSE LIBRARY FUNCTIONS *
//*****************************************
auto get_str(char* ptr, std::string str) -> uint32_t;
auto get_str(char* ptr, int32_t idx, int32_t part, std::vector<std::vector<std::string>> strs) -> uint32_t;

#endif /*RISA_MODULE_INTERFACE*/
