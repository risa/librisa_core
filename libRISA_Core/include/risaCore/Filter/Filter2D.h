// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef FILTER2D_H_
#define FILTER2D_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <numeric>
#include <thread>

namespace risa
{
namespace cuda
{

namespace detail
{
/**
 *  This enum represents the filter type
 *  to be used during the filtering.
 */
enum class FilterType2D
{
	sobelHorizontal,
	sobelVertical,
	laplace,
	laplaceDiagonalEdges,
	gaussian,
	mean,
	custom,
	UNDEFINED
};
}

//! This stage filters an image with a precomputed filter kernel using direct convolution.
class Filter2D : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet the specific parameter set to use
	 *    @param[in]  numberOfOutputs  the number of output streams
	 *    @param[in]  numberOfInputs   the number of input streams
	 */
	Filter2D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Destroys the cudaStreams.
	 */
	~Filter2D();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  image input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& image, int inputIdx) -> void;

	//! Takes one image from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest reconstructed image in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void;

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_images; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs; //!<  number of input streams
	mutable std::mutex m_mutex; //!<  mutex to coordinate access to reconfiguration flag 'doConfig_'
	bool m_doConfig; //!<  triggers (re-)configuration

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads

	int m_imageHeight; //!<  the image height in pixels
	int m_imageWidth; //!<  the image widht in pixels

	detail::FilterType2D m_filterType; //!<  the filter type that shall be used
	detail::FilterType2D getFilterTypeFromString(std::string& str)
	{
		if(str == "sobelHorizontal")
			return detail::FilterType2D::sobelHorizontal;
		if(str == "sobelVertical")
			return detail::FilterType2D::sobelVertical;
		if(str == "laplace")
			return detail::FilterType2D::laplace;
		if(str == "laplaceDiagonalEdges")
			return detail::FilterType2D::laplaceDiagonalEdges;
		if(str == "gaussian")
			return detail::FilterType2D::gaussian;
		if(str == "mean")
			return detail::FilterType2D::mean;
		if(str == "custom")
			return detail::FilterType2D::custom;

		return detail::FilterType2D::UNDEFINED;
	};

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	// kernel execution coniguration
	int m_blockSize2D; //!<  the block size of the filter kernel
	int m_memPoolSize; //!< specifies, how many elements are allocated by memoryPool
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	std::vector<float> m_filter; //!<  stores the values of the filter function
	int m_filterKernelHeight; //!<  the height of the filter kernel in y direction
	int m_filterKernelWidth; //!<  the width of the filter kernel in x direction

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one sinogram from the queue. It calls the desired filter
	 * CUDA kernel in its own stream. After the computation of the filtered projections the
	 * filtered parallel sinogram is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(const int deviceID) -> void;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function. If an invalid filter function is requested, the ramp filter is used.
	 *
	 * @param[in] configFile path to config file
	 * @param[in] parameterSet  the specific parameter set to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	//!<  This function computes the requested filter function once on the host
	auto designFilter() -> void;

	// Predefined filter values:
	float m_filterValuesSobelHorizontal[9]{
		-1.0, 0.0, 1.0, -2.0, 0.0, 2.0, -1.0, 0.0, 1.0}; //!< Sobel horizontal edge detection
	int m_filterWidthSobelHorizontal = 3;
	int m_filterHeightSobelHorizontal = 3;

	float m_filterValuesSobelVertical[9]{
		-1.0, -2.0, -1.0, 0.0, 0.0, 0.0, 1.0, 2.0, 1.0}; //!< Sobel vertical edge detection
	int m_filterWidthSobelVertical = 3;
	int m_filterHeightSobelVertical = 3;

	float m_filterValuesLaplace[9]{
		0.0, 1.0, 0.0, 1.0, -4.0, 1.0, 0.0, 1.0, 0.0}; //!< Laplace 90° edge detection
	int m_filterWidthLaplace = 3;
	int m_filterHeightLaplace = 3;

	float m_filterValuesLaplaceDiagonalEdges[9]{
		1.0, 1.0, 1.0, 1.0, -8.0, 1.0, 1.0, 1.0, 1.0}; //!< Laplace 45° edge detection
	int m_filterWidthLaplaceDiagonalEdges = 3;
	int m_filterHeightLaplaceDiagonalEdges = 3;

	auto generateFilterGaussian(int filterWidth, int filterHeight, float sigma) -> void;
	float m_filterKernelGaussianSigma = 1.0;

	std::vector<float> m_filterKernelCustomValues; //!< Storage for filter values of a custom filter kernel
};

//!<  CUDA Kernel that convolutes each image pixel with the prefedined filter function
/**
 *    The variable i represents the column index in the image,
 *    the variable j represents the row index in the image.
 *
 *    @param[in]  imageWidth  the number of columns in the image
 *    @param[in]  imageHeight  the number of rows in the image
 *    @param[in]  dataIn    the input image data
 *    @param[out] dataConv the convoluted image data
 *    @param[in]  filter   pointer to the precomputed filter function
 *    @param[in]  filterWidth   the number of columns in the filter mask
 *    @param[in]  filterHeight  the number of rows in the filter mask
 */
__global__ void applyConvolution2D(const int imageWidth, const int imageHeight, float* dataIn,
	float* dataConv, const float* const __restrict__ filter, const int filterWidth, const int filterHeight);
}
}

#endif /* FILTER2D_H_ */
