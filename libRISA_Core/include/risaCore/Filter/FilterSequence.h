// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef FILTERSEQUENCE_H_
#define FILTERSEQUENCE_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <numeric>
#include <thread>

namespace risa
{
namespace cuda
{

//! This stage filters a sequence of images with a given set of weights.
class FilterSequence : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet the specific parameter set to use
	 *    @param[in]  numberOfOutputs  the number of output streams
	 *    @param[in]  numberOfInputs   the number of input streams
	 */
	FilterSequence(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Destroys the cudaStreams.
	 */
	~FilterSequence();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  image input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& image, int inputIdx) -> void;

	//! Takes one image from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest reconstructed image in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void;

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_images; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs; //!<  number of input streams
	mutable std::mutex m_mutex; //!<  mutex to coordinate access to reconfiguration flag 'doConfig_'
	bool m_doConfig; //!<  triggers (re-)configuration

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads

	int m_imageHeight; //!<  the image height in pixels
	int m_imageWidth; //!<  the image widht in pixels
	int m_numberOfPlanes; //!< the number of imaging planes

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	// kernel execution coniguration
	int m_blockSize2D; //!<  the block size of the filter kernel
	int m_memPoolSize; //!< specifies, how many elements are allocated by memoryPool
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	std::vector<float> m_filter; //!<  stores the values of the filter function
	int m_filterSequenceLength; //!<  the number of images to filter
	int m_outputImageIndexInSequence; //!< the index of the image to output in the sequence. 0 = latest image

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one sinogram from the queue. It calls the desired filter
	 * CUDA kernel in its own stream. After the computation of the filtered projections the
	 * filtered parallel sinogram is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(const int deviceID) -> void;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function. If an invalid filter function is requested, the ramp filter is used.
	 *
	 * @param[in] configFile path to config file
	 * @param[in] parameterSet  the specific parameter set to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;
};

//!<  CUDA Kernel that applies a temporal convolution
/**
 *    @param[in]  imageWidth  the number of columns in the image
 *    @param[in]  imageHeight  the number of rows in the image
 *    @param[in,out]  currentImage    latest input image, will be reused for output
 *    @param[in]  buffer        buffer over sequence len images
 *    @param[in]  weights    weight of filter functon for each respecitve image in the sequence
 *    @param[in]  sequenceLen   number of images in the sequence
 *    @param[in]  currBufferIndex    the index in which to store the latest image
 */
__global__ void applyFilterSequence(const int imageWidth, const int imageHeight, float* currentImage,
	float* __restrict__ buffer, const float* weights, const int sequenceLen, const int currBufferIndex);
}
}

#endif /* FILTERSEQUENCE_H_ */
