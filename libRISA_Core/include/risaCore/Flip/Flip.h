// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef FLIP_H_
#define FLIP_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/HostMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <thread>

namespace risa
{
namespace cuda
{

//!	This stage flips Images (either horizontally or vertically.)
/**
 * Accepts only one input port, multiple output ports are possible.
 * Each input image will be flipped and forwarded to the output port with index 0.
 * If there is more than one output port, copies of the flipped image are forwarded to the other output ports.
 */
class Flip : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system. Allocates memory using
	 * the MemoryPool.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet the specific paramter set to use
	 *    @param[in]  numberOfOutputs   number of ouput streams
	 *    @param[in]  numberOfInputs   number of input streams
	 */
	Flip(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Tells MemoryPool to free the allocated memory.
	 *    Destroys the cudaStreams.
	 */
	~Flip();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  img input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& img, int inputIdx) -> void;

	//! Takes one sinogram from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest sinogram in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void
	{
		BOOST_LOG_TRIVIAL(error)
			<< "risa::cuda::Flip:: Update function not implemented. Ignoring subject " << s;
	};

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_imgs; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs; //!<  number of input streams

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once
	std::map<int, unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	int m_blockSize2D; //!<  2D block size of the attenuation kernel
	int m_memPoolSize; //!<  specifies, how many elements are allocated by memory pool

	int m_imageWidth; //!< image width in pixels
	int m_imageHeight; //!< image height in pixels
	bool m_flipHorizontally; //!< flag whether to flip horizontally, else vertically

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile path to config file
	 * @param[in] parameterSet  identifier of specific parameter set options to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one image from the input queue #m_imgs. The image is transfered from device to host
	 * using the asynchronous cudaMemcpyAsync()-operation. The resulting host structure is pushed back into
	 * the output queue #m_results.
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(const int deviceID) -> void;
};

//! CUDA kernel to convert the data type of an input image
/**
 * Flips the image contained in data horizontally.
 */
__global__ void flipHorizontally(float* __restrict__ data, const int imageWidth, const int imageHeight);

//! CUDA kernel to convert the data type of an input image
/**
 * Flips the image contained in data vertically.
 */
__global__ void flipVertically(float* __restrict__ data, const int imageWidth, const int imageHeight);

}
}

#endif /* FLIP_H_ */
