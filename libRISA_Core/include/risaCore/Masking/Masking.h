// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef MASKING_H_
#define MASKING_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <thread>

namespace risa
{
namespace cuda
{

namespace detail
{
/**
 *  This enum represents the masking type
 *  to be used during the masking
 */
enum class MaskingType
{
	circleMaskOutside,
	circleMaskInside,
	rectangleMaskOutside,
	rectangleMaskInside,
	upperPart,
	lowerPart,
	leftPart,
	rightPart,
	maskingTypeNoMask,
	UNDEFINED
};
}
//! This stage multiplies a precomputed mask with the reconstructed image.
/**
 * This class represents a masking stage. It multiplies the reconstructed image with
 * a precomputed mask in a CUDA kernel, to hide irrelevant areas.
 */
class Masking : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{

	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet  identifier of specific parameter set to use
	 *    @param[in]  numberOfOutputs  the number of output streams
	 *    @param[in]  numberOfInputs  the number of input streams
	 */
	Masking(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *   Destroys the cudaStreams.
	 */
	~Masking();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  img input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& img, int inputIdx) -> void;

	//! Takes one image from the output queue outputIdx #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest reconstructed image in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void;

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_imgs; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output queues
	int m_numberOfInputs; //!<  number of input streams
	mutable std::mutex m_mutex; //!<  mutex to coordinate access to reconfiguration flag 'doConfig_'
	bool m_doConfig; //!<  triggers (re-)configuration
	bool m_doAutoNormalize{false}; //!< whether to automatically normalize based on the min/max image value
	float m_fixedMinValue{-0.0045f},
		m_fixedMaxValue{0.016f}; //!< fixed min/max image values which are to be scaled to 0 ... 1

	int m_blockSize2D; //!< the block size of the masking kernel
	int m_memPoolSize; //!< specifies, how many elements are allocated by memoryPool
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one reconstruced image from the queue. It calls the masking
	 * CUDA kernel in its own stream. After the multiplication of the mask with the image, the
	 * result is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(int deviceID) -> void;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	int m_imageWidth; //!<  the number of pixels per row
	int m_imageHeight; //!< the number of pixels per column
	unsigned int m_numberOfPlanes;

	detail::MaskingType m_maskingType; //!< enum of chosen masking type
	detail::MaskingType getMaskingTypeFromString(std::string& str)
	{
		if(str == "circleMaskOutside")
			return detail::MaskingType::circleMaskOutside;
		else if(str == "circleMaskInside")
			return detail::MaskingType::circleMaskInside;
		else if(str == "rectangleMaskOutside")
			return detail::MaskingType::rectangleMaskOutside;
		else if(str == "rectangleMaskInside")
			return detail::MaskingType::rectangleMaskInside;
		else if(str == "upperPart")
			return detail::MaskingType::upperPart;
		else if(str == "lowerPart")
			return detail::MaskingType::lowerPart;
		else if(str == "leftPart")
			return detail::MaskingType::leftPart;
		else if(str == "rightPart")
			return detail::MaskingType::rightPart;
		else if(str == "noMask")
			return detail::MaskingType::maskingTypeNoMask;
		else
			return detail::MaskingType::UNDEFINED;
	}
	int m_rectangleMaskLowerLeftX{0}; //!< x value of lower left corner for masking
	int m_rectangleMaskLowerLeftY{0}; //!< y value of lower left corner for masking
	int m_rectangleMaskWidth{0}; //!<  width of rectangluar mask
	int m_rectangleMaskHeight{0}; //!<  height of rectangular mask
	float m_circleMaskCenterX{0.0}; //!< x value of center of circular mask
	float m_circleMaskCenterY{0.0}; //!< y value of center of circular mask
	float m_circleMaskRadius{0.0}; //!< radius of circular mask

	//! fraction of image size to mask
	/**
	 * for top/bottom: fraction of m_imageHeight
	 * for left/right: fraction of m_imageWidth
	 */
	float m_maskingFraction{0};

	float m_maskingValue{0.0}; //!<  the value to which the masked area should be set

	std::vector<bool> m_imageMask; //!< precalculated image mask

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile path to config file
	 * @param[in] parameterSet indentifier of the specific parameter set to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	//!< This function precalculate the boolean filter mask
	auto createMask() -> bool;
	//!< This function creates a boolean circular filter mask
	/**
	 *    @param[in] maskOuterRegion whether to mask outer region. Set to false, to mask inner region instead
	 *
	 *    @retval true mask was created successfully
	 *    @retval false mask could not be created with given parameters
	 */
	auto createCircleMask(bool maskOuterRegion) -> bool;
	//!< This function creates a boolean rectangular filter mask
	/**
	 *    @param[in] maskOuterRegion whether to mask outer region. Set to false, to mask inner region instead
	 *
	 *    @retval true mask was created successfully
	 *    @retval false mask could not be created with given parameters
	 */
	auto createRectangularMask(bool maskOuterRegion) -> bool;
};

//!   This CUDA kernel multiplies the mask and the reconstructed image
/**
 * @param[in,out] img            the reconstructed image, that is multiplied with the mask in-place
 * @param[in]     value          the value, the pixels shall be replaced with
 * @param[in]     imageWidth     image width in pixels
 * @param[in]     imageHeight    image height in pixels
 * @param[in]     imageMask      precalculated boolean image mask of size `imageWidth` * `imageHeight`. Where
 * set to true, the image pixels will be replaced with `value`
 */
__global__ void applyImageMask(float* __restrict__ img, const float value, const int imageWidth,
	const int imageHeight, const bool* const __restrict__ imageMask);

//!   This CUDA kernel multiplies the mask and the reconstructed image
/**
 * @param[in,out] img            the reconstructed image, that is normalized in place
 * @param[in]     imageWidth     image width in pixels
 * @param[in]     imageHeight    image height in pixels
 * @param[in, out]     maxValue    maximum pixel value in the image
 * @param[in, out]     minValue    minimum pixel value in the image
 * @param[in]     normalizeToValueMax    lower value to map to during normalization, default 0.0
 * @param[in]     normalizeToValueMin    higher value to map to during normalization, default 1.0
 */
__global__ void normalizeImage(float* __restrict__ img, const int imageWidth, const int imageHeight,
	const float maxValue, const float minValue, const float normalizeToValueMax,
	const float normalizeToValueMin);

} // namespace cuda
} // namespace risa

#endif /* MASKING_H_ */
