// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef WEBCOMPATIBLESAVER_H_
#define WEBCOMPATIBLESAVER_H_

#include <risaCore/RISAModuleInterface.h>
#include <risaCore/Basics/performance.h>

#include <glados/CircularBuffer.h>
#include <glados/Image.h>
#include <glados/Utils.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/observer/Subject.h>

#include <glados/cuda/DeviceMemoryManager.h>

#include <glados/cuda/Memory.h>

#include <string>
#include <vector>

namespace risa
{

namespace cuda
{

//! This stage is suited for online inspection of streams on GPU.
/**
 * This stages generates data at up to 50 fps for online inspection. It takes images from a GPU buffer,
 * normalizes and conerts them in char arrays before encoding them in JPEG format. The results can then
 * be written to disk as JPEG files or in base64 encoded format. When using base64 encoding, the result
 * may further be prepended by the min and max image value before normalization to allow reconstruction
 * of the absolut input values.
 * Filestreams are closed after the each write such that observers of the files know when they are safe
 * to read.
 */
class WebCompatibleSaver : public RISAModule_Sink<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	WebCompatibleSaver(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* s) -> void;

	protected:
	~WebCompatibleSaver();

	private:
	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	int m_imageWidth; //!< image width in pixels
	int m_imageHeight; //!< image height in pixels
	int m_numberOfPlanes; //!< the number of planes
	std::string m_parameterSet; //!< internal storage for parameterizaton

	std::string m_outputPath, m_fileName; //!< the path and name of the output file

	std::vector<Timer> m_tmrs; //!< timers to keep track of when the last image per plane was written
	std::vector<bool> m_firstImageWritten; //!< flag whether at least one image was written for the plane

	int m_imageMask_d;
	int m_imageMaskData;
	int m_blockSize2D{32};
	dim3 dimBlock, dimGrid;
	size_t m_sharedMemBytes, m_numBlocks, m_numThreads;
	std::vector<float> m_minMaxVals_h, m_imageMinMaxVals;
	glados::cuda::device_ptr<float, glados::cuda::async_copy_policy> m_minMaxVals_d;
	glados::cuda::device_ptr<unsigned char, glados::cuda::async_copy_policy> m_image8bit_d;
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	std::vector<std::ofstream> m_ofstreams;
	std::vector<unsigned char> m_jpeg;
	int m_jpegQuality{70};

	bool m_storeAutomaticValues{true}; //!< one-time flag to store next automatic min/max values
	bool m_encodeResultsInBase64{false}; //!< flag whether to encode results in base64 and store as string
	bool m_includeMinMaxInBase64{true}; //!< for base64 encoded results, prepend two float values for min/max
									   //!< value before normalization
	bool m_setManualMinMaxVals{
		false}; //!< whether min/max values for normalization are to be calculated or are set manually
	std::vector<float> m_manualMinMaxVals; //!< the min/max values to use. Holds pairs (min, min) for each
										  //!< plane in sequential order
	auto encodeInBase64(std::vector<unsigned char>& data, bool includeMinMax, float minVal, float maxVal, size_t imageIndex)
		-> void;

	/**
	 * base64_encode - Base64 encode
	 * @src: Data to be encoded
	 * @len: Length of the data to be encoded
	 * @out_len: Pointer to output length variable, or %NULL if not used
	 * Returns: Allocated buffer of out_len bytes of encoded data,
	 * or empty string on failure
	 */
	static constexpr unsigned char base64_table[65] =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	std::vector<unsigned char> base64_encode(const unsigned char* src, size_t len)
	{
		unsigned char *out, *pos;
		const unsigned char *end, *in;

		size_t olen;

		olen = 4 * ((len + 2) / 3); /* 3-byte blocks to 4-byte */

		if(olen < len)
			return std::vector<unsigned char>(); /* integer overflow */

		std::vector<unsigned char> outVec;
		outVec.resize(olen);
		out = (unsigned char*)&outVec[0];

		end = src + len;
		in = src;
		pos = out;
		while(end - in >= 3)
		{
			*pos++ = base64_table[in[0] >> 2];
			*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
			*pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
			*pos++ = base64_table[in[2] & 0x3f];
			in += 3;
		}

		if(end - in)
		{
			*pos++ = base64_table[in[0] >> 2];
			if(end - in == 1)
			{
				*pos++ = base64_table[(in[0] & 0x03) << 4];
				*pos++ = '=';
			}
			else
			{
				*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
				*pos++ = base64_table[(in[1] & 0x0f) << 2];
			}
			*pos++ = '=';
		}

		return outVec;
	}
};
__global__ void WCSnormalizeAndConvert(float* __restrict__ img, unsigned char* img8bit, const int imageWidth,
	const int imageHeight, const float minValue, const float maxValue);

}
}
#endif /* WEBCOMPATIBLESAVER_H_ */
