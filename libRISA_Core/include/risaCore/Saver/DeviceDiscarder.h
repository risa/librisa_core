// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 

#ifndef DEVICEDISCARDER_H_
#define DEVICEDISCARDER_H_

#include "../RISAModuleInterface.h"

#include <glados/Image.h>
#include <glados/Utils.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/observer/Subject.h>

#include <glados/cuda/DeviceMemoryManager.h>

#include <glados/cuda/Memory.h>

namespace risa
{

namespace cuda
{

//! This stage simply discards frame from device memory.
/**
 * It can be used to effienctly discard data from device memory. Similar to using the OfflineSaver in DiscardAll mode.
 */
class DeviceDiscarder : public RISAModule_Sink<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	DeviceDiscarder(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* ) -> void{};

	protected:
	~DeviceDiscarder();

	private:
	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	std::string m_parameterSet; //!< internal storage for parameterizaton
};

}
}
#endif /* DEVICEDISCARDER_H_ */
