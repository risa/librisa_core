// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef SHARED_HOST_MEM_SAVER_H_
#define SHARED_HOST_MEM_SAVER_H_
#include "../Basics/performance.h"
#include "../RISAModuleInterface.h"

#include <glados/CircularBuffer.h>
#include <glados/Image.h>
#include <glados/cuda/HostMemoryManager.h>
#include <glados/observer/Subject.h>

#include <string>
#include <vector>

#include <semaphore.h>
#include <sys/mman.h>

namespace risa
{

//! This stage is suited for online and offline processing via configuration options.
/**
 * In offline mode it has numberOfPlanes output buffers of fixed size. If a buffer is full, it is written to
 * disk and cleared afterwars. In online mode, the output acts as a circular buffer. The oldest values are
 * overwritten, if the buffer is full. At program exit, the buffer is written to disk
 */
class SharedHostMemSaver : public RISAModule_Sink<host_array<float>>
{
	public:
	using manager_type = glados::cuda::HostMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	SharedHostMemSaver(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* s) -> void
	{
		BOOST_LOG_TRIVIAL(error)
			<< "risa::SharedHostMemSaver:: Update function not implemented. Ignoring subject " << s;
	};

	protected:
	~SharedHostMemSaver();

	private:
	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	size_t m_memoryPoolIndex;

	size_t m_imageWidth{0}; //!< image width in pixels
	size_t m_imageHeight{0}; //!< image height in pixels
	size_t m_numberOfPlanes{0}; //!< the number of planes

	std::string m_outputPath, m_fileName; //!< the path and name of the output file
	std::string m_backingFile;
	std::string m_parameterSet;

	double m_minLatency{std::numeric_limits<double>::max()};
	double m_maxLatency{0.0};

	std::vector<Timer> m_tmrs;

	// shared memory variables:
	int m_fileDescriptor; // file descriptor
	float* m_memptr; // address to shared memory
	sem_t* m_semptr; // sempahore
	size_t m_smemBytes;
};

}

#endif /* SHARED_HOST_MEM_SAVER_H_ */
