// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef OPENCVVIEWER_H_
#define OPENCVVIEWER_H_

#include "../Basics/performance.h"
#include "../RISAModuleInterface.h"

#include <glados/CircularBuffer.h>
#include <glados/Image.h>
#include <glados/Utils.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/observer/Subject.h>

#include <glados/cuda/DeviceMemoryManager.h>

#include <glados/cuda/Memory.h>

#include <string>
#include <vector>

namespace risa
{

namespace cuda
{

//! This stage is suited for online inspection of streams on GPU.
/**
 * Opens a preview window showing the image stream.
 */
class OpenCVViewer : public RISAModule_Sink<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	OpenCVViewer(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* s) -> void;

	protected:
	~OpenCVViewer();

	private:
	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	int m_imageWidth; //!< image width in pixels
	int m_imageHeight; //!< image height in pixels
	int m_numberOfPlanes; //!< the number of planes
	std::string m_parameterSet; //!< internal storage for parameterizaton

	std::vector<Timer> m_tmrs; //!< timers to keep track of when the last image per plane was written
	std::vector<bool> m_firstImageWritten; //!< flag whether at least one image was written for the plane

	int imageMask_d_;
	int m_imageMaskData;
	int m_blockSize2D{16};
	dim3 dimBlock, dimGrid;
	size_t m_sharedMemBytes, m_numBlocks, m_numThreads;
	std::vector<float> minMaxVals_h_, m_imageMinMaxVals;
	glados::cuda::device_ptr<float, glados::cuda::async_copy_policy> minMaxVals_d_;
	std::vector<unsigned char> image8bit_h_;
	glados::cuda::device_ptr<unsigned char, glados::cuda::async_copy_policy> image8bit_d_;
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	bool m_storeAutomaticValues{true}; //!< one-time flag to store next automatic min/max values
	bool m_setManualMinMaxVals{
		false}; //!< whether min/max values for normalization are to be calculated or are set manually
	std::vector<float> m_manualMinMaxVals; //!< the min/max values to use. Holds pairs (min, min) for each
										  //!< plane in sequential order
};

}
}
#endif /* OPENCVVIEWER_H_ */
