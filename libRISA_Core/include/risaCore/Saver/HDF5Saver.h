// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#ifndef HDF5SAVER_H_
#define HDF5SAVER_H_

#include <risaCore/Basics/enums.h>
#include <risaCore/Basics/performance.h>
#include <risaCore/RISAModuleInterface.h>

#include <glados/CircularBuffer.h>
#include <glados/Image.h>
#include <glados/Utils.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/observer/Subject.h>

#include <glados/cuda/DeviceMemoryManager.h>

#include <glados/cuda/Memory.h>

#include <string>
#include <vector>

#include <hdf5.h>

namespace risa
{

namespace cuda
{

namespace detail
{
enum class dataExistsPolicy
{
	overwrite, /* overwrite existing data */
	append, /* (try to) append data */
	abort /* cancel initialization */
};
}

//! This stage stores resulting images in an HDF5 file.
/**
 */
class HDF5Saver : public RISAModule_Sink<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	HDF5Saver(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* s) -> void;

	protected:
	~HDF5Saver();

	private:
	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	int m_imageWidth; //!< image width in pixels
	int m_imageHeight; //!< image height in pixels
	size_t m_bytesPerFrame; //!< number of bytes per frame
	std::string m_parameterSet; //!< internal storage for parameterizaton
	detail::dataExistsPolicy m_overwritePolicy; //! policy on how to handle existing data

	std::string m_outputPath, m_fileName, m_datasetName; //!< the path and name of the output file

	int m_blockSize2D{16};
	dim3 dimBlock, dimGrid;
	size_t m_sharedMemBytes, m_numBlocks, m_numThreads;
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once
	bool m_firstFrameParsed;

	// hdf5 handles
	hid_t m_HDF5File; //!< index of hdf5 file containing data array
	// main dataset
	float* m_dataBuffer; //! host buffer of initially unknown size
	hid_t m_ds_data; //!< dataset for data array
	hid_t m_sp_data; //!< file and memory spaces for data array
	hsize_t m_offset_data[3]; //!< offset in data array
	hsize_t m_count_data[3], m_chunk_sz_data[3]; //!< size of data array
	// additional datasets
	std::vector<hid_t> m_ds_meta; //! datasets for meta data per frame
	std::vector<hid_t> m_sp_meta;
	std::vector<hid_t> m_type_meta;
	std::vector<size_t> m_sz_meta; //! bytes per entry
	std::vector<void*> m_buf_meta; //! value buffers per entry
	hsize_t m_offset_meta[1], m_count_meta[1], m_chunk_sz_meta[1];

	std::vector<std::string> m_metaNames; //! names of meta data datasets per frame
	std::vector<std::string> m_attrNames; //! names of attributes to append to each frame
	std::vector<detail::GetPropertyDescriptor> m_propDescriptors; //! list of property descriptors
	auto getTypeIdFromPropType(detail::GetPropertyType propType) -> hid_t;
	[[nodiscard]] auto populatePropDescrList(glados::Image<manager_type>* image) -> bool;
	[[nodiscard]] auto initializeMetaDataDatasets() -> bool;
	[[nodiscard]] auto writeAttributes(glados::Image<manager_type>* image) -> bool;
	[[nodiscard]] auto writeAttributeValueFromProperty(
		glados::Image<manager_type>* image, detail::GetPropertyDescriptor pd) -> bool;
	[[nodiscard]] auto createDataset() -> bool;
	[[nodiscard]] auto createMetaDataset(std::string metaName, detail::GetPropertyDescriptor pd,
		hid_t& sp_meta, hid_t& ds_meta, hid_t& type_meta) -> bool;
	[[nodiscard]] auto checkMetaDataset(std::string metaName, detail::GetPropertyDescriptor pd,
		hid_t& sp_meta, hid_t& ds_meta, hid_t& type_meta) -> bool;
	[[nodiscard]] auto createHDF5File() -> bool;
	auto setupPropertyDescriptor(glados::Image<manager_type>* image, std::string key)
		-> detail::GetPropertyDescriptor;
	auto readPropertyIntoBuffer(
		glados::Image<manager_type>* image, size_t* sz, void** buf, detail::GetPropertyDescriptor pd) -> bool;
	auto initializeHDF5() -> bool;
};

}
}
#endif /* HDF5SAVER_H_ */
