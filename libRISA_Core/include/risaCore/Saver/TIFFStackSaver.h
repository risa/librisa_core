// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef TIFFSTACKSAVER_H_
#define TIFFSTACKSAVER_H_

#include "../Basics/performance.h"
#include "../RISAModuleInterface.h"

#include <glados/CircularBuffer.h>
#include <glados/Image.h>
#include <glados/Utils.h>
#include <glados/cuda/HostMemoryManager.h>
#include <glados/observer/Subject.h>

#include <tiffio.h>

#include <string>
#include <vector>
#include <limits>

namespace risa
{

namespace detail
{
enum class RecoMode
{
	offline,
	online,
	discardAll
};
}

//! This stage is suited for online and offline processing via configuration options.
/**
 * In offline mode it has numberOfPlanes output buffers of fixed size. If a buffer is full, it is written to
 * disk and cleared afterwars. In online mode, the output acts as a circular buffer. The oldest values are
 * overwritten, if the buffer is full. At program exit, the buffer is written to disk
 */
class TIFFStackSaver : public RISAModule_Sink<host_array<float>>
{
	public:
	using manager_type = glados::cuda::HostMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	TIFFStackSaver(const std::string& configFile, const std::string& parameterSet);

	//!< this function is called, when an image exits the software pipeline
	auto saveImage(glados::Image<manager_type> image) -> void;

	auto update(glados::Subject* s) -> void
	{
		BOOST_LOG_TRIVIAL(error) << "risa::TIFFStackSaver:: Update function not implemented. Ignoring subject "
								 << s;
	};

	protected:
	~TIFFStackSaver();

	private:
	//! Creates a Tiff Sequence to be stored on disk
	/**
	 * @param[in] planeID   specifies, which buffer is to be stored on hard disk
	 */
	auto writeTiffSequence(size_t planeID) -> void;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	//! writes a single image to the tiff sequence
	/**
	 * @param[in]  tif   pointer to the TIFF-sequence
	 * @param[in]  img   the image to be written into the tiff-file
	 */
	auto writeToTiff(::TIFF* tif, const glados::Image<manager_type>& img) const -> void;

	auto writeToPreviewTiff(::TIFF* tif, glados::Image<manager_type> img) const -> void;

	size_t m_memoryPoolIndex;

	size_t m_imageWidth{0}; //!< image width in pixels
	size_t m_imageHeight{0}; //!< image height in pixels
	int m_numberOfFrames{0}; //!< the number of frames
	int m_numberOfPlanes{0}; //!< the number of planes
	int m_framesPerFile{0}; //!< the number of frames per file
	
	std::string m_parameterSet; //!< internal storage for parameterizaton

	detail::RecoMode m_mode{detail::RecoMode::offline}; //!< the reconstruction mode. Options: online, offline

	int m_circularBufferSize{0}; //!< the size of the output buffers

	std::string m_outputPath, m_fileName; //!< the path and name of the output file
	std::string m_bufferPath; //!< path to fast buffer which is copied to final location after writing
	bool m_bufferOutputBeforeWrite{false}; //!< whether to buffer output before writing to final location

	bool m_displayStatistics{false};
	double m_minLatency{1.79769e+308 /* std ::numeric_limits<double>::max()*/};
	double m_maxLatency{0.0};

	std::vector<Timer> m_tmrs;

	std::vector<std::size_t> m_fileIndex;
	std::vector<glados::CircularBuffer<glados::Image<manager_type>>> m_outputBuffers;
};

}

#endif /* TIFFStackSaver_H_ */
