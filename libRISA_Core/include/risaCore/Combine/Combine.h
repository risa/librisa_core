// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef COMBINE_H_
#define COMBINE_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <string>
#include <thread>

namespace risa
{
namespace cuda
{

namespace detail
{

enum class combineMethod
{
	combineAdd,
	combineSubtract,
	combineDifference,
	combineMultiply,
	combineDivide,
	combineMax,
	combineMin,
	combineMean,
	combineWeighted,
	combineSoftThresholdGreaterThan,
	combineSoftThresholdLowerThan,
	combineHardThreshold,
	combineMethodNotDefined,
	combineRaiseToPower,
	doNothing
};

enum class combineSource
{
	combineSourceStream,		// combine image-by-image from two streams
	combineSourceStreamSingle,  // combine each image of stream 0 with the single image of stream 1
	combineSourceFile,			// combine each image of stream 0 with the content of the file
	combineSourceStaticValue,	// combine each image of stream 0 with a static scalar value
	combineSourceNotDefined
};

}
//! This stage combines two image streams using a specified operation
/**
 * This class represents the combine stage. It combines two streams using a specified
 * operation such as adding, substracting, difference.
 */
class Combine : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system. Allocates memory using
	 * the MemoryPool for all CUDA devices.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet  identifier of specific parameter set to use
	 *    @param[in]  numberOfOutputs   the number of output streams
	 *    @param[in]  numberOfInputs    the number of input streams
	 */
	Combine(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Tells MemoryPool to free the allocated memory.
	 *    Destroys the cudaStreams.
	 */
	~Combine();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  img input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& img, int inputIdx) -> void;

	//! Takes one sinogram from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest sinogram in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void;

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_images; //!<  one separate vector of input queues for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs; //!<  number of input streams
	int m_activeInputStreams; //!<  keep track of active input streams
	mutable std::mutex m_mutex; //!<  mutex to coordinate access to reconfiguration flag 'doConfig_'
	bool m_doConfig; //!<  triggers (re-)configuration

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one sinogram from the queue. It calls the attenuation
	 * CUDA kernel in its own stream. After the computation of the attenuation data, the
	 * fan beam sinogram is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(int deviceID) -> void;

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	// configuration values
	int m_imageWidth; //!<  the image width in pixels
	int m_imageHeight; //!<  the image height in pixels
	int m_numberOfPlanes; //!<  the number of detector planes

	// kernel execution coniguration
	int m_blockSize2D; //!<  2D block size of the attenuation kernel
	int m_memPoolSize; //!<  specifies, how many elements are allocated by memory pool
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	detail::combineMethod m_combineMethod;
	detail::combineMethod getCombineMethodFromString(const std::string& str)
	{
		if(str == "combineAdd")
			return detail::combineMethod::combineAdd;
		if(str == "combineSubtract")
			return detail::combineMethod::combineSubtract;
		if(str == "combineDifference")
			return detail::combineMethod::combineDifference;
		if(str == "combineMultiply")
			return detail::combineMethod::combineMultiply;
		if(str == "combineDivide")
			return detail::combineMethod::combineDivide;
		if(str == "combineMax")
			return detail::combineMethod::combineMax;
		if(str == "combineMin")
			return detail::combineMethod::combineMin;
		if(str == "combineMean")
			return detail::combineMethod::combineMean;
		if(str == "combineWeighted")
			return detail::combineMethod::combineWeighted;
		if(str == "combineSoftThresholdGreaterThan")
			return detail::combineMethod::combineSoftThresholdGreaterThan;
		if(str == "combineSoftThresholdLowerThan")
			return detail::combineMethod::combineSoftThresholdLowerThan;
		if(str == "combineHardThreshold")
			return detail::combineMethod::combineHardThreshold;
		if(str == "combineRaiseToPower")
			return detail::combineMethod::combineRaiseToPower;
		if(str == "combineNull")
			return detail::combineMethod::doNothing;

		return detail::combineMethod::combineMethodNotDefined;
	}

	detail::combineSource m_combineSource;
	detail::combineSource getCombineSourceFromString(const std::string& str)
	{
		if(str == "combineSourceStream")
			return detail::combineSource::combineSourceStream;
		if(str == "combineSourceStreamSingle")
			return detail::combineSource::combineSourceStreamSingle;
		if(str == "combineSourceFile")
			return detail::combineSource::combineSourceFile;
		if(str == "combineSourceStaticValue")
			return detail::combineSource::combineSourceStaticValue;

		return detail::combineSource::combineSourceNotDefined;
	}

	float m_combineWeightOfMainImage;
	std::string m_sourceFile;
	float m_sourceStaticValue;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile path to config file
	 * @param[in]  parameterSet  identifier of specific parameter set to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;
};

//!   CUDA kernel adds two image streams
/**
 * This CUDA kernel combines two image streams by adding the pixel values.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesAdd(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel subtracts two image streams
/**
 * This CUDA kernel combines two image streams by subtracting the pixel values of image_B from image_A.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesSubtract(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel calculates the difference between two image streams
/**
 * This CUDA kernel combines two image streams by setting the pixel values of image_A to the difference
 * between image_A and image_B.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesDifference(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel adds multiplies image streams
/**
 * This CUDA kernel combines two image streams by multiplying the pixel values.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesMultiply(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel raises input A to power of input B
/**
 * This CUDA kernel combines two image streams by raising to the pixel values of input B to power given in
 * input B.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesRaiseToPower(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel divides two image streams
/**
 * This CUDA kernel combines two image streams by dividing the pixel values of image_A by the pixel values of
 * image_B. If a pixel in image_B is zero, the corresponding pixel in image_A will be set to zero.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesDivide(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel calculates the maximum of two image streams
/**
 * This CUDA kernel combines two image streams by setting the pixel values of image_A to the maximum pixel
 * value of image_A and image_B.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesMax(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel calculates the minimum of two image streams
/**
 * This CUDA kernel combines two image streams by setting the pixel values of image_A to the minimum pixel
 * value of image_A and image_B
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 */
__global__ void combineImagesMin(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight);

//!   CUDA kernel calculates a weighted sum of two image streams
/**
 * This CUDA kernel combines two image streams by adding the pixel values of image_A x weight to the pixel
 * values of image_B x (1 - weight).
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 * @param[in] weight the weight of image_A
 */
__global__ void combineImagesWeighted(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight, const float weight);

//!   CUDA kernel thresholds input A based on input B
/**
 * This CUDA kernel combines two image streams by thresholding input A based on the values in input B.
 * When using a soft threshold, the non-thresholded values remain untouched.
 * When using a hard threshold, values larger than the threshold will be set to 1.0 and values lower than the
 * threshold will be set to 0.0.
 *
 * @param[in,out]  image_A pointer to the first image. Output will be written into image_A
 * @param[in]  image_B pointer to the second image
 * @param[in]  imageWidth the width of the images in pixels
 * @param[in]  imageHeight the height of the images in pixels
 * @param[in]  useSoftThreshold whether to use soft or hard threshold
 * @param[in]  useGreaterThan whether value is upper threshold
 */
__global__ void combineImagesThreshold(float* __restrict__ image_A, const float* __restrict__ image_B,
	const unsigned int imageWidth, const unsigned int imageHeight, const bool useSoftThreshold,
	const bool useGreaterThan);

}
}

#endif /* COMBINE_H_ */
