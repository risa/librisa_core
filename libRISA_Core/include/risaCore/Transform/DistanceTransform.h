/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 16. April 2024
 * Authors: Dominic Windisch <d.windisch@hzdr.de>
 * Date: 23. April 2024
 * Co-Authors: Stephan Boden <s.boden@hzdr.de
 */

#ifndef DISTANCETRANSFORM_H_
#define DISTANCETRANSFORM_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <array>
#include <cuda_runtime.h>
#include <fstream>
#include <map>
#include <thread>

#include <nppdefs.h>
#include <nppcore.h>
#include <nppi_filtering_functions.h>

namespace risa
{
namespace cuda
{

//!  This stage performs 2D inplane image manipulation (roll, yaw, pitch)
/**
 * This class represents an inplane image manipulation step. It computes a hash table durig configuration
 * which is used in a CUDA kernel to interpolate the data.
 */
class DistanceTransform : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system. Allocates memory using
	 * the MemoryPool for all CUDA devices.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  numberOfOutputs   the number of output streams
	 *    @param[in]  numberOfInputs    the number of input streams
	 */
	DistanceTransform(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Tells MemoryPool to free the allocated memory.
	 *    Destroys the cudaStreams.
	 */
	~DistanceTransform();

	//! Pushes the image to the processor-threads
	/**
	 * The scheduling for multi-GPU usage is done in this function.
	 *
	 * @param[in]  fanSinogram input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& fanSinogram, int inputIdx) -> void;

	//! Takes one image from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest reconstructed image in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void;

	protected:
	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_images; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs;
	mutable std::mutex m_mutex; //!<  mutex to coordinate access to reconfiguration flag 'doConfig_'
	bool m_doConfig; //!<  triggers (re-)configuration

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads
	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool
	int m_numberOfDevices; //!<  the number of available CUDA devices
	// configuration parameters
	bool m_applyUniqueOffset{true};
	int m_imageWidth;
	int m_imageHeight;
	int m_memPoolSize; //!<  the number of elements the memory pool allocates
	glados::cuda::device_ptr<Npp8u, glados::cuda::async_copy_policy> m_scratchMemory;

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one sinogram from the queue. It calls the fan to parallel beam interpolation
	 * CUDA kernel in its own stream. After the computation of the fan to parallel beam rebinning, the
	 * reconstructed image is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(const int deviceID) -> void;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile path to config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;
};

__global__ void initDistanceTransformForUniqueVals(float* offs, size_t width, size_t height);

__global__ void applyDistanceTransformForUniqueVals(
	float* in, const float* offs, size_t width, size_t height);


}
}

#endif /* DISTANCETRANSFORM */
