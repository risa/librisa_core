// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef IFFT2D_H_
#define IFFT2D_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <cufft.h>

#include <map>
#include <thread>

namespace risa
{
namespace cuda
{

//! This stage performs a 2d inverse fourier transform on the input image.
class IFFT2D : public RISAModule_Processor<cuda_array<float>, cuda_array<float>>
{
	public:
	using input_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The input data type that needs to fit the output type of the previous stage
	using output_type =
		glados::Image<glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>>;
	//!< The output data type that needs to fit the input type of the following stage
	using deviceManagerType = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;

	public:
	//!   Initializes everything, that needs to be done only once
	/**
	 *
	 *    Runs as many processor-thread as CUDA devices are available in the system.
	 *
	 *    @param[in]  configFile  path to configuration file
	 *    @param[in]  parameterSet the specific parameter set to use
	 *    @param[in]  numberOfOutputs  the number of output streams
	 *    @param[in]  numberOfInputs   the number of input streams
	 */
	IFFT2D(const std::string& configFile, const std::string& parameterSet, const int numberOfOutputs,
		const int numberOfInputs);

	//!   Destroys everything that is not destroyed automatically
	/**
	 *    Destroys the cudaStreams.
	 */
	~IFFT2D();

	//! Pushes the image to the processor-threads
	/**
	 * @param[in]  image input data that arrived from previous stage
	 * @param[in]  inputIdx  the index of the input stream on which the input data arrived
	 */
	auto process(input_type&& image, int inputIdx) -> void;

	//! Takes one image from the output queue #m_results and transfers it to the neighbored stage.
	/**
	 *    @return  the oldest reconstructed image in the output queue #m_results
	 */
	auto wait(int outputIdx) -> output_type;

	auto update(glados::Subject* s) -> void
	{
		BOOST_LOG_TRIVIAL(error) << "risa::cuda::FFT2D:: Update function not implemented. Ignoring subject "
								 << s;
	};

	private:
	std::map<int, std::vector<glados::Queue<input_type>>>
		m_images; //!<  one separate input queue for each available CUDA device
	std::vector<glados::Queue<output_type>>
		m_results; //!<  vector of output queues in which the processed sinograms are stored
	int m_numberOfOutputs; //!<  number of output streams
	int m_numberOfInputs; //!<  number of input streams

	std::map<int, std::thread> m_processorThreads; //!<  stores the processor()-threads

	int m_imageHeight; //!<  the number of projections in the parallel beam sinogramm over 180 degrees
	int m_imageWidth; //!<  the number of detectors in the parallel beam sinogramm over 180 degrees

	float m_cutoffFraction; //!<  the fraction at which the filter function is cropped and set to zero.

	int m_numberOfDevices; //!<  the number of available CUDA devices in the system

	// kernel execution coniguration
	int m_blockSize2D; //!<  the block size of the filter kernel
	int m_memPoolSize; //!<  specifies, how many elements are allocated by memory pool
	std::vector<unsigned int>
		m_memoryPoolIdxs; //!<  stores the indeces received when regisitering in MemoryPool

	std::map<int, cufftHandle>
		m_plansInv; //!<  the inverse plans for the cuFFT inverse tranformation;  for each device one;

	std::map<int, cudaStream_t> m_streams; //!<  stores the cudaStreams that are created once

	//! main data processing routine executed in its own thread for each CUDA device, that performs the data
	//! processing of this stage
	/**
	 * This method takes one sinogram from the queue. It calls the desired filter
	 * CUDA kernel in its own stream. After the computation of the filtered projections the
	 * filtered parallel sinogram is pushed into the output queue
	 *
	 * @param[in]  deviceID specifies on which CUDA device to execute the device functions
	 */
	auto processor(const int deviceID) -> void;

	//!   initializes the cuFFT and creates the forward and inverse plans once for each deviceID
	/**
	 *
	 * @param[in]  deviceID the ID of the device that shall be initialized for cuFFT
	 */
	auto initCuFFT(const int deviceID) -> void;

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function. If an invalid filter function is requested, the ramp filter is used.
	 *
	 * @param[in] configFile path to config file
	 * @param[in] parameterSet the specific parameter set to use
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;
};

//!<  CUDA Kernel that combines a float stream for the real and imaginary part into a complex fft result.
/**
 *    The variable x represents the column index in the image,
 *    the variable y represents the row index in the image.
 *
 *    @param[in]  imageWidth  the number of columns in the image
 *    @param[in]  imageHeight  the number of rows in the image
 *    @param[out] data  the fft transformed image
 *    @param[in]  dataReal  the real part of the transformed image
 *    @param[in]  dataImag  the imaginary part of the transformed image
 */
__global__ void combineStreamsIntoCufftComplex(const int imageWidth, const int imageHeight,
	cufftComplex* data, const float* const __restrict__ dataReal, const float* const __restrict__ dataImag);

//!<  CUDA Kernel that scales the IFFT result such that IFFT(FFT(x)) == x
/**
 *    @param[in]  imageWidth  the number of columns in the image
 *    @param[in]  imageHeight  the number of rows in the image
 *    @param[inout] data  the ifft transformed image
 */
__global__ void scaleAfterIFFT(const int imageWidth, const int imageHeight, float* __restrict__ data);

}
}

#endif /* IFFT2D_H_ */
