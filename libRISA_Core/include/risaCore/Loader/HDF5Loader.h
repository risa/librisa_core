/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: August 2023
 * Author: Dominic Windisch <d.windisch@hzdr.de>
 *
 */
#ifndef HDF5LOADER_H_
#define HDF5LOADER_H_

#include <fstream>
#include <map>
#include <memory>
#include <queue>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include "../Basics/enums.h"
#include "../Basics/performance.h"

#include "../RISAModuleInterface.h"

#include <boost/log/trivial.hpp>

#include "glados/Image.h"
#include "glados/MemoryPool.h"
#include "glados/cuda/DeviceMemoryManager.h"
#include "glados/observer/Subject.h"

#include <hdf5.h>

namespace risa
{
namespace cuda
{

//! The loader stage for loading HDF5 Data
/**
 *
 */

class HDF5Loader : public RISAModule_Source<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using output_type = glados::Image<manager_type>;

	public:
	HDF5Loader(const std::string& configFile, const std::string& parameterSet);

	//! #loadImage is called, when the software pipeline is able to process a new image
	/**
	 * @return  the image that is pushed through the software pipeline
	 */
	auto loadImage() -> output_type;

	auto update(glados::Subject* s) -> void;

	protected:
	~HDF5Loader();

	private:
	int m_memoryPoolIndex; //!<  stores the indeces received when registering in MemoryPool
	cudaStream_t m_stream; //!<  stores the cudaStreams that are created once
	dim3 blocks, grids;
	size_t m_blockSize2D; //! block dimenstion for CUDA kernel call
	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	unsigned int m_numberOfPlanes{2}; //!< we assume 2 planes for benchmarking, however, may be overwritten
	std::queue<output_type> m_buffer;
	std::mutex m_mutex;
	std::condition_variable m_condVar;
	detail::loadControlMode
		m_startMode; //!< how to control starting to load. Options: "automatic" (default): start loading
					 //!< instantly, "triggered": wait for start trigger to start loading
	detail::loadControlMode
		m_stopMode; //!< how to control stopping to load. Options: "automatic" (default): stop loading after
					//!< numberOfFrames were loaded, "triggered:" wait for stop trigger to stop loading
	bool m_triggerStart{false}, m_triggerStop{false};
	glados::cuda::device_ptr<float, glados::cuda::async_copy_policy> imagesBuffer_d;

	// configuration parameters
	std::string m_parameterSet; //!< convenience for better log output
	int m_memPoolSize; //!< specifies, how many elements are allocated by memory pool
	int m_deviceId; //!< device Id (GPU) to copy to

	unsigned int m_stopFrame, m_numberOfFramesToSend; //!< number of frames to send in benchmark mode
	std::string m_filePath, m_fileName, m_fileEnding, m_datasetName;
	std::vector<std::string> m_metaNames; //! names of meta data datasets per frame
	std::vector<std::string> m_attrNames; //! names of attributes to append to each frame
	ssize_t m_firstIdx, m_lastIdx, m_imageWidth, m_imageHeight, m_imageCount;
	ssize_t m_incrementFrames; //! number of frames to increment by (i.e. when set to 1 load every frame)
	ssize_t roi_x_, roi_y_, roi_w_,
		roi_h_; //! region of interest defined by lower-left corner (x,y) and width/height (w,h)
	bool m_roiDefined; //! whether region of interest is defined

	std::size_t m_index{0u};

#ifdef __linux__
	using m_clock = std::chrono::high_resolution_clock;
#elif _WIN32
	using m_clock = std::chrono::system_clock;
#endif
	std::chrono::time_point<m_clock> m_lastSendTime;
	unsigned int m_targetFrameRate;
	unsigned int sendInterval_us; //!< target interval between sending frames

	// hdf5 handles
	hid_t m_HDF5File{H5I_INVALID_HID}; //!< index of hdf5 file containing data array
	// main dataset
	char* m_dataBuffer; //! buffer of initially unknown size
	glados::cuda::device_ptr<char, glados::cuda::async_copy_policy>
		dataBuffer_d; //! ondevice buffer before conversion to float
	size_t m_elemBytes; //! bytes per element in the buffer
	hid_t m_type_data; //! data type in dataset
	detail::dataType m_native_type_data; //! native data type in dataset
	hid_t m_ds_data; //!< dataset for data array
	hid_t m_sp_data, m_msp_data; //!< file and memory spaces for data array
	hsize_t m_offset_data[3]; //!< offset in data array
	hsize_t m_offset_msp[3]; //!< offset in memory space
	hsize_t m_count_data[3]; //!< size of data array
	// additional datasets
	std::vector<hid_t> m_type_meta; //! data type in dataset
	std::vector<detail::dataType> m_native_type_meta; //! native data type in dataset
	std::vector<hid_t> m_ds_meta; //! datasets for meta data per frame
	std::vector<hid_t> m_sp_meta, m_msp_meta;
	hsize_t m_offset_meta[1], m_count_meta[1];
	std::vector<char*> m_meta_bufs; //! memory buffers for meta data values
	// additonal attributes
	std::vector<detail::dataType> m_native_type_attr; //! native data type of attributes
	std::vector<std::any> m_val_attr; //! value of attributes

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	private:
	auto allocateBuffer() -> void;
	[[nodiscard]] auto getHDF5FileDatasetNames() -> std::vector<std::string>;
	[[nodiscard]] auto initializeHDF5() -> bool;
	[[nodiscard]] auto initializeMainDataset() -> bool;
	[[nodiscard]] auto initializeOptionalDatasets() -> bool;
	[[nodiscard]] auto initializeAttributes() -> bool;
	template <typename T>
	[[nodiscard]] auto readAttribute(std::string name, hid_t id_attr, hid_t type_attr) -> bool;
	[[nodiscard]] auto readAttributeStr(std::string name, hid_t id_attr, hid_t type_attr) -> bool;
	auto setPropertyFromAny(glados::Image<risa::cuda::HDF5Loader::manager_type>* img, std::string key,
		const std::any val, detail::dataType type) -> void;
	auto setPropertyFromBufValue(glados::Image<risa::cuda::HDF5Loader::manager_type>* img, std::string key,
		char* val, detail::dataType type) -> void;

	auto printNativeDataType(detail::dataType type) -> std::string
	{
		std::string str;
		switch(type)
		{
		case detail::dataType::t_bool:
			str = "bool";
			break;
		case detail::dataType::t_int8:
			str = "int8";
			break;
		case detail::dataType::t_int16:
			str = "int16";
			break;
		case detail::dataType::t_int32:
			str = "int32";
			break;
		case detail::dataType::t_int64:
			str = "int64";
			break;
		case detail::dataType::t_uint8:
			str = "uint8";
			break;
		case detail::dataType::t_uint16:
			str = "uint16";
			break;
		case detail::dataType::t_uint32:
			str = "uint32";
			break;
		case detail::dataType::t_uint64:
			str = "uint64";
			break;
		case detail::dataType::t_float:
			str = "float";
			break;
		case detail::dataType::t_double:
			str = "double";
			break;
		case detail::dataType::t_string:
			str = "string";
			break;
		case detail::dataType::t_unknown:
			str = "unknown";
			break;
		}
		return str;
	};

	auto toNativeDatatype(hid_t type) -> detail::dataType
	{
		if(H5Tequal(type, H5T_STD_U8LE))
		{
			return detail::dataType::t_uint8;
		}
		if(H5Tequal(type, H5T_STD_U16LE))
		{
			return detail::dataType::t_uint16;
		}
		if(H5Tequal(type, H5T_STD_U32LE))
		{
			return detail::dataType::t_uint32;
		}
		if(H5Tequal(type, H5T_STD_U64LE))
		{
			return detail::dataType::t_uint64;
		}
		if(H5Tequal(type, H5T_STD_I8LE))
		{
			return detail::dataType::t_int8;
		}
		if(H5Tequal(type, H5T_STD_I16LE))
		{
			return detail::dataType::t_int16;
		}
		if(H5Tequal(type, H5T_STD_I32LE))
		{
			return detail::dataType::t_int32;
		}
		if(H5Tequal(type, H5T_STD_I64LE))
		{
			return detail::dataType::t_int64;
		}
		if(H5Tequal(type, H5T_IEEE_F32LE) || H5Tequal(type, H5T_NATIVE_FLOAT))
		{
			return detail::dataType::t_float;
		}
		if(H5Tequal(type, H5T_IEEE_F64LE) || H5Tequal(type, H5T_NATIVE_DOUBLE))
		{
			return detail::dataType::t_double;
		}
		if(H5Tequal(type, H5T_C_S1) || H5Tget_class(type) == H5T_STRING)
		{
			return detail::dataType::t_string;
		}
		return detail::dataType::t_unknown;
	}

	auto printErrorCannotOpen(std::string type, std::string val, bool hint = false)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5Loader<" << m_parameterSet << ">: Cannot open " << type
								 << " '" << val << "'";
		if(hint)
		{
			BOOST_LOG_TRIVIAL(warning) << "risa::cuda::HDF5Loader<" << m_parameterSet
									   << ">: Hint: Dataset names typically start with '/'.";
		}
	};

	auto printErrorNumDims(std::string name, int hasDims, int expectedDims)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5loader<" << m_parameterSet << ">: Expected " << name
								 << " to have " << expectedDims << " dimensions, but it has " << hasDims
								 << ".";
	};

	auto printErrorReadVal(std::string name)
	{
		BOOST_LOG_TRIVIAL(fatal) << "risa::cuda::HDF5loader<" << m_parameterSet
								 << ">: Could not read value of " << name << ".";
	}
};

template <typename T>
__global__ void convertArrayToFloat(
	T* __restrict__ input, float* __restrict__ output, const unsigned int width, const unsigned int height);
}
}

#endif /* HDF5LOADER_H_ */
