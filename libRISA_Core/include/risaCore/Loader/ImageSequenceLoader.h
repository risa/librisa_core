/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 30 November 2016
 * Date: Ocotber 2022
 * Author: Dominic Windisch <d.windisch@hzdr.de>
 *
 */
#ifndef IMAGESEQUENCELOADER_H_
#define IMAGESEQUENCELOADER_H_

#include <fstream>
#include <map>
#include <memory>
#include <queue>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include "../Basics/enums.h"
#include "../Basics/performance.h"
// image format include
#include <his/his.h>
#include <jpeg-decoder/jpeg-decoder.h>
#include <lodepng/lodepng.h>

#include "../RISAModuleInterface.h"

#include <boost/log/trivial.hpp>

#include "glados/Image.h"
#include "glados/MemoryPool.h"
#include "glados/cuda/DeviceMemoryManager.h"
#include "glados/observer/Subject.h"

namespace risa
{
namespace cuda
{

namespace detail
{
enum class StackLoadMode
{
	SingleFrame,
	BufferStack
};
enum class FileFormat
{
	BMP,
	HIS,
	JPEG,
	PNG,
	TIFF
};
}

//! Load images stacks
/**
 *
 */

class ImageSequenceLoader : public RISAModule_Source<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using output_type = glados::Image<manager_type>;

	public:
	ImageSequenceLoader(const std::string& configFile, const std::string& parameterSet);

	//! #loadImage is called, when the software pipeline is able to process a new image
	/**
	 * @return  the image that is pushed through the software pipeline
	 */
	auto loadImage() -> output_type;

	auto update(glados::Subject* s) -> void;

	protected:
	~ImageSequenceLoader();

	private:
	int m_memoryPoolIndex; //!<  stores the indeces received when registering in MemoryPool
	cudaStream_t m_stream; //!<  stores the cudaStreams that are created once
	int m_numberOfDevices; //!<  the number of available CUDA devices in the system
	unsigned int m_numberOfPlanes{2}; //!< we assume 2 planes for benchmarking, however, may be overwritten
	std::queue<output_type> m_buffer;
	std::mutex m_mutex;
	std::condition_variable m_condVar;
	detail::loadControlMode
		m_startMode; //!< how to control starting to load. Options: "automatic" (default): start loading
					 //!< instantly, "triggered": wait for start trigger to start loading
	detail::loadControlMode
		m_stopMode; //!< how to control stopping to load. Options: "automatic" (default): stop loading after
					//!< numberOfFrames were loaded, "triggered:" wait for stop trigger to stop loading
	bool m_triggerStart{false}, m_triggerStop{false};
	glados::cuda::device_ptr<float, glados::cuda::async_copy_policy> imagesBuffer_d;
	glados::cuda::device_ptr<unsigned char, glados::cuda::async_copy_policy> rawImageBuffer_d;
	size_t m_activeChannels{1}; //! active channels per pixel (i.e. how many bytes contain color information)
	size_t m_dataChannels{1}; //! data storage channels per pixel (i.e. bytes per pixel in memory)
	dim3 m_blocks, m_grids;
	bool m_needsToBeConverted{
		true}; // indicates whether current file format needs to be converted to floating point

	// configuration parameters
	std::string m_parameterSet; //!< convenience for better log output
	int m_memPoolSize; //!< specifies, how many elements are allocated by memory pool
	int m_deviceId; //!< device Id (GPU) to copy to

	unsigned int m_stopFrame, m_numberOfFramesToSend; //!< number of frames to send in benchmark mode
	std::string m_filePath, m_fileBaseName, m_fileEnding;
	std::size_t m_firstIdx, m_lastIdx, m_imageWidth, m_imageHeight, m_idxTotalDigits;
	std::vector<std::vector<unsigned char>> m_rawImages; //!< image buffer in host-memory
	size_t m_totalImages{0};
	detail::StackLoadMode m_stackLoadMode;
	detail::FileFormat m_fileFormat;
	bool m_loadAllInDirectory; //! whether to load all files in directory in order regardless of the index
	std::vector<std::string> m_fileNames;

	std::size_t m_index{0u};

#ifdef __linux__
	using m_clock = std::chrono::high_resolution_clock;
#elif _WIN32
	using m_clock = std::chrono::system_clock;
#endif
	std::chrono::time_point<m_clock> m_lastSinoSendTime;
	unsigned int m_targetFrameRate;
	unsigned int sendInterval_us; //!< target interval between sending frames

	//!  Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	private:
	//! Check for valid and sane settings
	auto validateSettings() -> bool;

	//! Loads single slice from file into rawImage
	auto loadSliceFromFile(std::vector<unsigned char>& rawImage, size_t idx) -> bool;

	//! Loads benchmark data into host-side memory
	auto loadImageStack() -> bool;

	//! Copies benchmark data into device memory
	auto copyImageStacktoDeviceBuffer(int deviceId) -> bool;
};

__global__ void convertImageToFloat(unsigned char* __restrict__ rawImage, float* __restrict__ floatImage,
	const unsigned int imageWidth, const unsigned int imageHeight, const long dataChannels,
	const long activeChannels, bool isPNG);
}
}

#endif /* IMAGESEQUENCELOADER_H_ */
