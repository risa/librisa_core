/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 30 November 2016
 * Date: Ocotber 2022
 * Author: Dominic Windisch <d.windisch@hzdr.de>
 *
 */

#include "his.h"

namespace risa
{

namespace his
{

template <typename T>
auto readEntry(std::ifstream& fstream, T& value) -> bool
{
	return (!fstream.read(reinterpret_cast<char*>(&value), sizeof(T)).fail());
}

// return true is header is ok, return false on error
auto parseHeader(std::ifstream& fstream, HISHeader& header) -> bool
{
	//clang-format off
	if(readEntry(fstream, header.fileType) // must read in this exact order!
		&& readEntry(fstream, header.headerSize) && readEntry(fstream, header.headerVersion)
		&& readEntry(fstream, header.fileSize) && readEntry(fstream, header.imageHeaderSize)
		&& readEntry(fstream, header.ulx) && readEntry(fstream, header.uly) && readEntry(fstream, header.brx)
		&& readEntry(fstream, header.bry) && readEntry(fstream, header.frameNumber)
		&& readEntry(fstream, header.correction) && readEntry(fstream, header.integrationTime)
		&& readEntry(fstream, header.numberType) && readEntry(fstream, header.x))
	{
		//clang-format on

		// check header validity
		if(header.fileType != fileId)
		{
			BOOST_LOG_TRIVIAL(error)
				<< "risa::his::parseHeader: HIS file header contains wrong file id. Did you "
				   "try to open a non-HIS file?";
			return false;
		}

		if(header.headerSize != fileHeaderSize)
		{
			BOOST_LOG_TRIVIAL(error) << "risa::his::parseHeader: HIS header has unexpected size of "
									 << header.headerSize << ", expected " << fileHeaderSize << ".";
			return false;
		}

		if(header.numberType == static_cast<std::uint16_t>(dataType::notImplemented))
		{
			BOOST_LOG_TRIVIAL(error) << "risa::his::parseHeader: HIS file contains unsupported data type.";
			return false;
		}

		if(header.frameNumber != 1)
		{
			BOOST_LOG_TRIVIAL(error)
				<< "risa::his::parseHeader: Only single-frame HIS files are supported. File contains "
				<< header.frameNumber << " frames.";
			return false;
		}

		return true;
	}

	BOOST_LOG_TRIVIAL(error) << "risa::his::parseHeader: Error when reading the HIS header.";
	return false;
}

} // namespace his
} // namespace risa