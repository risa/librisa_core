/*
 * This file is part of the RISA-library.
 *
 * Copyright (C) 2022 Helmholtz-Zentrum Dresden-Rossendorf
 *
 * RISA is free software: You can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RISA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RISA. If not, see <http://www.gnu.org/licenses/>.
 *
 * Date: 30 November 2016
 * Date: Ocotber 2022
 * Author: Dominic Windisch <d.windisch@hzdr.de>
 *
 */
#ifndef IMAGESEQUENCELOADER_HIS_FORMAT_H_
#define IMAGESEQUENCELOADER_HIS_FORMAT_H_

#include <algorithm>
#include <cerrno>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <memory>
#include <string>
#include <system_error>
#include <utility>
#include <vector>

#include <boost/log/trivial.hpp>


namespace risa
{

namespace his
{

constexpr auto fileHeaderSize = 68;
constexpr auto paddingBytes = 34;
constexpr auto hardwareHeaderSize = 32;
constexpr auto headerSize = fileHeaderSize + hardwareHeaderSize;
constexpr auto fileId = 0x7000;

struct HISHeader
{
	std::uint16_t fileType; // = file_id
	std::uint16_t headerSize; // size of this file header in bytes
	std::uint16_t headerVersion; // yy.y
	std::uint32_t fileSize; // size of the whole file in bytes
	std::uint16_t imageHeaderSize; // size of the image header in bytes
	std::uint16_t ulx, uly, brx, bry; // bounding rectangle of image
	std::uint16_t frameNumber; // number of frames in the current file
	std::uint16_t correction; // 0 = none, 1 = offset, 2 = gain, 4 = bad pixel, (ored)
	double integrationTime; // frame time in microseconds
	std::uint16_t numberType; /* short, long integer, float, signed/unsigned, inverted,
							   * fault map, offset/gain correction data, badpixel correction data
							   * */
	std::uint8_t x[paddingBytes]; // fill up to 68 bytes
};

enum class dataType
{
	notImplemented = -1,
	uint8_t = 2,
	uint16_t = 4,
	uint32_t = 32,
	double_t = 64,
	float_t = 128
};

template <typename T>
auto readEntry(std::ifstream& fstream, T& value) -> bool;

// return true is header is ok, return false on error
auto parseHeader(std::ifstream& fstream, HISHeader& header) -> bool;

} // namespace his
} // namespace risa

#endif // IMAGESEQUENCELOADER_HIS_FORMAT_H_