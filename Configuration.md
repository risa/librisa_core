<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf

SPDX-License-Identifier: CC-PDDC
-->

# Configuration reference for libRISA_Core

## Source stages
<details><summary><h4>HDF5Loader</h4></summary>

_Loads HDF5 datasets._  

Loads data from a HDF5 file. Specify the name of the 3D dataset (stack of images) within the file. Optionally, define one or more 1D datasets (one entry per image) to be loaded alongside the image data. Optionally, define attribute names which will be read from the HDF5 file and appended to each image as property. 

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| HDF5Loader| 0 | 1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `filePath` | Directory containing the HDF5 file | string |
| `fileName` | Name of the HDF5 file | string |
| `fileEnding` | File extension of the HDF5 file | string |
| `datasetName` | Name of the 3D dataset to load | string |
| `metaDataNames` | Name(s) of the 1D metadata dataset(s) to load | vector\<string\> |
| `attributeNames` | Name(s) of the attribute(s) to load | vector\<string\> |
| `firstIdx` | First image index to load | integer |
| `lastIdx` | Last image index to load | integer, use `-1` to load all images |
| `incrementFrames` | Increment between consecutive frames to load | integer. Use `1` to load every image, `2` to load every second image, and so on. |
| `roiX` | Region of interest to load, lowest x index | integer |
| `roiY` | Region of interest to load, lowest y index | integer |
| `roiWidth` | Region of interest to load, width | integer |
| `roiHeight` | Region of interest to load, height | integer |
| `frameRateLimit` | Maximum frames per second to send | integer, 0 for unlimited rate|
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `blockSize2D` | kernel launch argument | integer, recommended: 16 or 32 |
| `deviceId` | GPU ID to stream data to | integer |
| `startMode` | start mode to use | string,<br><br>Options:<br>`"automatic"` -> start loading instantly<br>`"triggered"` -> wait for start trigger before loading |
| `stopMode` | stop mode to use | string,<br><br>Options:<br>`"automatic"` -> stop loading when `numberOfFrames` were sent<br>`"triggered"` -> keep sending until stop trigger is received |
</details>

<details><summary><h4>ImageSequenceLoader</h4></summary>

_Loads a sequence of images._  

Based on the file name and a range of file indices, a sequence of images are loaded into memory and streamed to the connected stage. Image files may be buffered on device for performance tests or more efficient repeated sending of the same stack.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| ImageSequenceLoader | 0 | 1 x `cuda_array<float>` | `frameRateLimit` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer. Optional, will be set based on the first image if not set. |
| `imageWidth` | Image width in pixels | integer. Optional, will be set based on the first image if not set. |
| `frameRateLimit` | Maximum frames per second to send | integer, 0 for unlimited rate|
| `filePath` | Directory containing the PNG file(s) | string |
| `fileBaseName` | PNG base file name without index | string, the full filepath is constructed from `<filePath>/<fileBaseName><0-padded index>.png` |
| `firstIdx` | First index to load | integer |
| `lastIdx` | Last index to load | integer |
| `idxTotalDigits` | Total number of digits for the index string | integer |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `deviceId` | GPU ID to stream data to | integer |
| `numberOfFramesToSend` | number of frames to send | integer |
| `startMode` | start mode to use | string,<br><br>Options:<br>`"automatic"` -> start loading instantly<br>`"triggered"` -> wait for start trigger before loading |
| `stopMode` | stop mode to use | string,<br><br>Options:<br>`"automatic"` -> stop loading when `numberOfFrames` were sent<br>`"triggered"` -> keep sending until stop trigger is received |
| `numberOfPlanes` | number of 'planes' to distribute images to | integer |
| `fileFormat` | file extension of images to load | string,<br>Options: `PNG`, `JPEG`, `BMP` (not implemented yet), `TIFF`(not implemented yet)<br>Note: common alternative endings are supported (e.g. `jpg`, `jpeg`, `JPEG`, `JPG`) |
| `stackLoadMode` | enum of how to load sequence of images | string, <br><br>Options:<br> `SingleFrame` => load each frame from file and push into pipeline<br>`BufferStack` => first load whole stack of images into memory, then start streaming
</details>

## Processor stages
<details><summary><h4>Average</h4></summary>

_Average multiple images._  

You can either average each pixel individually (returning a same-size image), or also average the rows or columns to create an output vector. When provided images from multiple imaging planes, each image plane will be averaged individually. This stage will only return an output when the last input image has arrived, i.e. an `invalid` image. Then, one averaged result for each imaging plane is provided on each of the output ports.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Average | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `averagingType` | Averaging type | string, <br><br>Options: <br>`"averagePixelwise"` => average each pixel individually, <br>`"averageRows"` => average all rows into a resulting vector of size imageWidth, <br>`"averageColumns"` => average all columns into a resulting vector of size imageHeight|
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |

</details>

<details><summary><h4>Combine</h4></summary>

_Combine an image stream with another stream, a static value or a static file using pixel-wise operations (add, multiply, difference, ...)._  

When using `combineSource = combineSourceStream`, the number of inputs must be `2`; else `1`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Combine | 1(2) x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `combineSource`<br>`combineMethod`<br>`combineWeightOfImage`<br>`sourceFile`<br>`sourceStaticValue` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `combineMethod` | Method to use for combining sources | string, <br><br>Options: <br>`"combineAdd"` => add pixel values,<br> `"combineDifference"` => absolute difference between pixel values,<br> `"combineDivide"` => divide pixel values of image stream by combineSource, <br> `"combineHardThreshold"` => set pixels greater than combineSource to `1`, others to `0`,<br>`"combineMax"` => set to maximum of image stream and combineSource, <br>`"combineMean"` => set to mean value between image source and combineSource, <br>`"combineMin"` => set to minimum of image stream and combineSource,<br>`"combineMultiply"` => multiply pixel values from image stream with combineSource, <br>`"combineRaiseToPower"` => raise pixel values of image stream to power of combineSource,<br>`"combineSoftThresholdGreaterThan"` => set pixels greater than combineSource to `1`, leave rest unchanged, <br>`"combineSoftThresholdLowerThan"` => set pixels lower than combineSource to `0`, leave rest unchanged, <br>`"combineSourceSubtract"` => subtract combineSource from image stream pixel values,<br>`"combineWeighted"` => weighted sum of the image stream and the combine source |
| `combineSource` | Source to use for combining | string, <br><br>Options: <br>`"combineSourceStream"` => use second image stream as combineSource,<br> `"combineSourceStaticValue"` => use a static value as combineSource,<br> `"combineSourceFile"` => use a file as combineSource. Note that this file _must_ contain `imageWidth` x `imageHeight` x `numberOfPlanes` float values |
| `combineWeightOfImage` | Weight of main image stream for combine method combineWeighted | float, range `0.0 ... 1.0`  |
| `sourceFile` | File path when using combineSourceFile | string |
| `sourceStaticValue` | Static value when using combineSourceStaticValue | float |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |

</details>

<details><summary><h4>Convert</h4></summary>

_Convert data format from uint16 to float._

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Convert | 1 x `cuda_array<uint16_t>` | &ge;1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `normalize` | Whether to normalize data during conversion | bool |
| `normalizeMaxValue` | The maximum uint16_t value which will be mapped to float `1.0` in the output | integer |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |

</details>

<details><summary><h4>Device-to-Host</h4></summary>

_Download data from GPU._

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| D2H | 1 x `cuda_array<float>` | 1 x `host_array<float>` | `"enable"` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `displayStatistics` | Whether to display statistics (processed images per second) | bool |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| (__RUNTIME ONLY__) `enable` | enable/disable downloading | bool |

</details>

<details><summary><h4>Filter1D</h4></summary>

_Filter in one dimension using FFT._

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Filter1D | 1 x `cuda_array<float>` | 1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filterType` | Filter type to use | string, <br><br>Options:<br>`"cosine"` => cosine filter, <br>`"hamming"` => Hamming filter,<br>`"hanning"` => Hanning filter, <br>`"ramp"` => ramp filter,<br>`"sheppLogan"` => Shepp-Logan filter|
| `cutoffFraction` | cutoff fraction for filter kernel creation | float |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |

</details>

<details><summary><h4>Filter2D</h4></summary>

_Filter using 2D convolution kernel._

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Filter2D | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `filterType`<br>`filterKernelGaussianSigma`<br>`filterKernelWidth`<br>`filterKernelHeight`<br>`filterKernelCustomValues` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filterType` | Filter type to use | string, <br><br>Options:<br>`"custom"` => user-defined filter kernel, <br>`"gaussian"` => Gaussian filter,<br>`"laplace"` => Laplace edge filter (horizontal & vertical), <br>`"laplaceDiagonalEdges"` => Laplace edge filter (diagnoal),<br>`"mean"` => mean filter (box filter),<br>`"sobelHorizontal"` => Sobel edge filter (horizontal),<br>`"sobelVertical"` => Sobel edge filter (vertical)|
| `filterKernelHeight` | filter kernel height in pixels. _Must be odd._ | integer |
| `filterKernelWidth` | filter kernel width in pixels. _Must be odd._ | integer |
| `filterKernelCustomValues` | custom filter kernel values  | vector&lt;float&gt; of size `filterKernelHeight` x `filterKernelWidth`|
| `filterKernelGaussianSigma` | sigma value for Gaussian filter | float |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>FilterSequence</h4></summary>

_Filter a sequence of images using a pixel-based convolution kernel (along time axis)._

Returns the weighted sum of the image sequence based on the provided weights per image.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| FilterSequence | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `sequenceLength`<br>`weights` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `sequenceLength` | number of images to filter | integer |
| `weights` | weight per image | vector&lt;float&gt; of size `sequenceLength` |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>FilterStatistical</h4></summary>

_Filter using sorted neighbourhoods (e.g. median filter)._

For each pixel, its neighbourhood of size `filterKernelHeight` x `filterKernelWidth` is sorted by value. The pixel is then assigned the `filterSelectIndex`th value of the neighbourhood, e.g. the middle one when using the median filter.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| FilterStatistical | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `filterType`<br>`filterKernelWidth`<br>`filterKernelHeight`<br>`filterSelectIndex` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filterType` | Filter type to use | string, <br><br>Options:<br>`"median"` => use median value of sorted neighbourhood, <br>`"selectIndex"` => select a specific index from the sorted neighbourhood|
| `filterKernelHeight` | filter kernel height in pixels | integer |
| `filterKernelWidth` | filter kernel width in pixels | integer |
| `filterSelectIndex` | the index to select from the sorted neighbourhood  | integer |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>Flip</h4></summary>

_Flip image vertically or horizontally._

Flip image vertically or horizontally.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Flip| 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | None |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `flipMode` | Flip direction to use | string, <br><br>Options:<br>`"horizontal"` => flip image horizontally, i.e, along its central vertical axis, <br>`"vertical"` => flip image vertically, i.e, along its central horizontal axis|
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>


<details><summary><h4>Fourier-Transform 2D</h4></summary>

_Apply forward fast fourier transform in 2D._ 

Yields separate streams for real- and imaginary part. The output is _not_ scaled.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| FFT2D | 1 x `cuda_array<float>` | 2 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>GetProperty</h4></summary>

_Print or save properties to a file._  

Any image can have added a set or properties. This stage will extract the specified properties and print them to the console or save them in a text file. 

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| GetProperty | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `propertyDescriptors` | Object describing the type and name of the property to extract | Object with keys:<br>`propertyKey` => string with property key<br>`propertyType` => string with type of property (`all` => get all properties, `duration` => get duration from timestamp given in the key, `integer` => an integer value, `floatingPoint` => a floating point value, `text` => a string, `timestamp` => a timestamp)|
| `propertyOutput` | Output method for extracted properties | string,<br><br>Options: <br>`print` => print properties to std::out<br>`writeAllToFile` => write properties of each image to the file<br>`writeLatestToFile` => write only latest value to file |
| `outputOnStateChangeOnly` | Only invokes output method when state has changed | bool |
| `maxOutputRate` | Maximum number of outputs per second | integer |
| `outputFormat` | Output format to use | integer <br><br>Options: `csv` => write as csv<br>`keyValue` => write as key-value pairs|

</details>

<details><summary><h4>HistogramPerPixel</h4></summary>

_Create histogram for each pixel._  

For each pixel, a histogram over all images is created. Results are pushed as a stack of images with one image per bin of the histogram. Values outside the histogram range defined by `histogramMinValue` and `histogramMaxValue` will be added to the lowest or highest bin, respectively. Each slice of the histogram will have the center of its bin attached as floating point property `histogramBinCenter`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| HistogramPerPixel | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `numberOfHistogramBins` | Number of histogram bins | integer, <br><br>Maximum: 256|
| `histogramMinValue` | Lower bound of histogram | float |
| `histogramMaxValue` | Upper bound of histogram | float |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |

</details>

<details><summary><h4>Inverse Fourier-Transform 2D</h4></summary>

_Apply inverse fast fourier transform in 2D._  

Consumes real part on input `0` and imagingary part on input `1`. The output is scaled such that `IFFT2D(FFT2D(x)) == x`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| IFFT2D | 2 x `cuda_array<float>` | 1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>Interleave</h4></summary>

_Interleave multiple input streams into one output stream._

Consumes multiple input streams (round-robin) and combines them into a single output stream by appending them in-order. 

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Interleave| &ge; 1 x `cuda_array<float>` | 1 x `cuda_array<float>` | None |

#### Parameters
_No parameters to specify_

</details>

<details><summary><h4>Masking</h4></summary>

_Mask a region of the image with a given value._

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Masking | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `maskingType`<br>`maskingValue`<br>`fixedMinValue`<br>`fixedMaxValue`<br>`circleMaskCenterX`<br>`circleMaskCenterY`<br>`circleMaskRadius`<br>`rectangleMaskLowerLeftX`<br>`rectangleMaskLowerLeftY`<br>`rectangleMaskHeight`<br>`rectangleMaskWidth`<br>`maskingFraction` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `maskingType` | Masking type to use | string, <br><br>Options:<br>`"circleMaskOutside"` => circular mask (outside is masked), <br>`"circleMaskInside"` => circular mask (inside is masked),<br>`"rectangleMaskInside"` => rectangular mask (inside is masked), ,<br>`"rectangleMaskOutside"` => rectangular mask (outside is masked),<br>`"upperPart"` => mask upper part of the image,<br>`"lowerPart"` => mask lower part of the image,<br>`"leftPart"` => mask left part of the image,<br>`"rightPart"` => mask right part of the image,<br>`"noMask"` => do not apply a mask|
| `maskingValue` | pixel value to apply to all masked pixels | float |
| `circleMaskCenterX` | x coordinate of circular mask center | float |
| `circleMaskCenterY` | y coordinate of circular mask center | float |
| `circleMaskRadius` | radius of circular mask | float |
| `rectangleMaskLowerLeftX` | x coordinate of lower left corner of rectangular mask | float |
| `rectangleMaskLowerLeftY` | y coordinate of lower left corner of rectangular mask | float |
| `rectangleMaskHeight` | height of rectangular mask | float |
| `rectangleMaskWidth` | width of rectangular mask | float |
| `maskingFraction` | fraction of the image to mask when using `lowerPart`, `upperPart`, `leftPart` or `rightPart` | float in range `0.0 ... 1.0` |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>Reduce</h4></summary>

_Recude image to scalar value._  

Reduce the image to its minimum, mean, maximum, sum and/or product of each pixel value. The scalar result is then appended to the image as a property.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Reduce | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `reduceToProperties` | Which properties to reduce to | vector<string>, <br><br>All the listed options will the determined and appended as floatingPoint property.<br>Options: <br>`"maximum"` => maximum value, appended as `maxValue`, <br>`"mean"` => mean value, appended as `meanValue`, <br>`"minimum"` => minimum value, appended as `minValue`, <br>`"product"` => product value, appended as `productValue`, <br>`"sum"` => sum value, appended as `sumValue`, <br>`"all"` => all of the above |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>Reframe</h4></summary>

_Reframe image by cropping and/or padding._

Reframes the image by cropping and/or padding. Define the position on where to put the input image relative to the viewport from `(0,0)` to `(outputImageWidth, outputImageHeight)`. Any pixels in the viewport which are not covered by the input image will be filled with `fillerPixelValue`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Reframe| 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `coordinateBaseOfOriginalImageInFinalImageX`<br>`coordinateBaseOfOriginalImageInFinalImageY`|

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `inputImageHeight` | Input image height in pixels | integer |
| `inputImageWidth` | Input image width in pixels | integer |
| `outputImageHeight` | Output image height in pixels | integer |
| `outputImageWidth` | Output image width in pixels | integer |
| `coordinateBaseOfOriginalImageInFinalImageX` | Input image position in viewport, x coordinate | integer|
| `coordinateBaseOfOriginalImageInFinalImageY` | Input image position in viewport, y coordinate | integer|
| `fillePixelValue` | value to fill uncovered pixels with | float |
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>Rotate</h4></summary>

_Rotate image in 90° increments._

Rotate image in 90° increments (0°, 90°, 180° or 270°).

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Rotate| 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | None |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `rotationAngleDeg` | Rotation angle (degrees) | integer, must be a multiple of 90|
| `blockSize2D` | block size for kernel invocation | integer, recommended: 32 |
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 |

</details>

<details><summary><h4>SetProperty</h4></summary>

_Sets one or more properties (key-value pairs) on an image._  

Define a set or properties to be set on an image. These are described in a list of `propertyDescriptors`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| GetProperty | 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | `propertyDescriptors`<br>`clearProperties` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `propertyDescriptors` | Object describing the type and name of the property to extract | vector of objects with keys:<br>`propertyKey` => string with property name<br>`propertyType` => one of the following strings with type of property:<br>- `increment` => increment an existing `integer` property,<br>- `integer` => set an unsigned integer value,<br>- `signed_integer` => set a signed integer value,<br>- `floatingPoint` => set a floating point value,<br>- `text` => set a string,<br>- `timestamp` => append current timestamp<br>- `file` => read values from a text file. First line must hold the type (`integer`, `floatingPoint`, `singed_integer` or `text`), following lines are read as one value per line. Wraps around.|
| **(RUNTIME ONLY)** `clearProperties` | whether to clear all existing properties before updating from `propertyDescriptors`| bool |
</details>

<details><summary><h4>Split</h4></summary>

_Splits an input stream into mutliple output streams._  

Define a set or properties to be set on an image. These are described in a list of `propertyDescriptors`.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| Split| 1 x `cuda_array<float>` | &ge;1 x `cuda_array<float>` | _none_ |


#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `splitMode` | Split mode across defined outputs | string, <br><br>Options:<br>`"copyToAll"` => copy input stream to each output stream<br>`"roundRobin"` => distribute evenly in order of arrival<br>`"splitByPlane"` => split by property `plane` of each image into respective output stream |
| `imageHeight` | Image height in pixels | integer (only required in `copyToAll` mode)|
| `imageWidth` | Image width in pixels | integer (only required in `copyToAll` mode)|
| `memPoolSize` | number of images in stage buffer | integer, recommended: 10 (only required in `copyToAll` mode)|

</details>

## Sink Stages

<details><summary><h4>HDF5Saver</h4></summary>

_Saves image stream in HDF5 dataset(s)._  

Writes data to an HDF5 file. Specify the name of the 3D dataset (stack of images) within the file. Optionally, define one or more image property to be stored alongside the image data (one entry per image). Optionally, define one or more image property which will be written to the HDF5 file as attribute. 

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| HDF5Saver| 1 x `cuda_array<float>` | 0 | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageWidth` | Image width in pixels | integer |
| `imageHeight` | Image height in pixels | integer |
| `outputPath` | Directory containing the HDF5 file | string |
| `fileName` | Name of the HDF5 file | string. Will be appended with '.hdf5' if necessary. |
| `datasetName` | Name of the 3D dataset to write | string. Will be prepended with '/' if necessary. |
| `metaDataNames` | Name(s) of the 1D metadata dataset(s) to write (one entry per image)| vector\<string\> |
| `attributeNames` | Name(s) of the attribute(s) to save (one entry taken from first image)| vector\<string\> |
| `dataExistsPolicy` | Policy to handle existing data | string, <br><br>Options:<br>`"abort"` => cancel execution if file or dataset already exists<br>`"append"` => append to existing data, create if it does not exist<br>`"overwrite"` => overwrite existing data, create if it does not exist  |
</details>

<details><summary><h4>OpenCVViewer</h4></summary>

__Requires OpenCV__

_Preview latest image in a local window._

The latest image of a stream is displayed in a local window. For each imaging plane, a separate window is created. In the title of the window, the stage identifier and the image index is shown. The window updates with up to 25 Hz.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| OpenCVViewer | 1 x `cuda_array<float>` | 0 | `"setManualMinMaxValues"`<br>`"manualMinValues"`<br>`"manualMaxValues"` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `setManualMinMaxValues` | Whether to set min/max for scaling manually | bool |
| `manualMinValues` | manually defined minimum value to scale images | vector&lt;float&gt; of size `numberOfPlanes` |
| `manualMaxValues` | manually defined maximum value to scale images | vector&lt;float&gt; of size `numberOfPlanes` |

</details>

<details><summary><h4>SharedHostMemSaver</h4></summary>

__LINUX ONLY__

_Save latest image in shared memory region._

The shared memory section is created using `shm_open` with name `fileName`. The shared memory holds `imageHeight` x `imageWidth` x `numberOfPlanes` float values. A semaphore of name `parameterSet` is used to indicate when the observing process (e.g. a GUI) may safely read the shared memory contents. Whenever the semaphore value is larger than 0, the memory holds a finished image.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| SharedHostMemSaver | 1 x `host_array<float>` | 0 | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filePath` | Path to write the images to (ignored) | string |
| `fileName` | Base filename used for shared memory section naming | string |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |

</details>

<details><summary><h4>TIFFStackSaver</h4></summary>

_Save incoming images into 32-bit TIFF stacks._

For each imaging plane, a separate image stack is created. Stacks are split into files containing up to `outputBufferSize` images each. In `"online"` mode, a single preview image per imaging plane will be overwritten at up to 50 FPS.

Output TIFFs are grayscale, have 32 bits per sample in floating point encoding and no compression. They can be viewed, e.g., with ImageJ.

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| TIFFStackSaver | 1 x `host_array<float>` | 0 | _none_ |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filePath` | Path to write the images to | string |
| `fileName` | Base filename. Will be extended by information about respective imaging plane and part indicitator for split-up stacks. | string |
| `mode` | Mode of operation | string, <br><br>Options:<br>`"offline"` => store all images as described above,<br>`"online"` => use a single preview image per plane (overwritten at up to 50 FPS),<br>`"discardAll"` => discard the incoming images without storing  |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |
| `displayStatistics` | Whether to display statistics (latency) | bool |
| `outputBufferSize` | number of images to buffer before dumping into TIFF stack | integer, recommended: 1000 |

</details>

<details><summary><h4>WebCompatibleSaver</h4></summary>

_Save latest incoming image in a JPEG preview (optionally base64 encoded)._

For each imaging plane, a separate preview image is created. Using `nvjpeg`, the images are directly encoded from CUDA buffers without downloading from the GPU beforehand.

When using the base64 encoded output, the base64 string can directly be used in an HTML `<img>` tag as `src`.

Note, however, that when encoding the minimum and maximum values aswell, these are prepended to the image data. The valid image source url then starts at the 12th character (0-indexed).

#### Overview
| Name | Number & type of inputs | Number & type of outputs | Runtime configurable parameters |
| --- | --- | --- | :--- |
| WebCompatibleSaver | 1 x `cuda_array<float>` | 0 | `"setManualMinMaxValues"`<br>`"manualMinValues"`<br>`"manualMaxValues"` |

#### Parameters
| Parameter | Description | Value(s) |
| --- | --- | --- |
| `imageHeight` | Image height in pixels | integer |
| `imageWidth` | Image width in pixels | integer |
| `filePath` | Path to write the images to | string |
| `fileName` | Base filename. Will be extended by information about respective imaging plane | string |
| `encodeResultsInBase64` | Whether to encode results in base64. If `true` results will be stored as `.txt`, else as `.jpeg` | bool |
| `includeMinMaxInBase64` | Whether to include min/max values before scaling in base64 | bool |
| `setManualMinMaxValues` | Whether to set min/max for scaling manually | bool |
| `jpegQuality` | JPEG compression quality | integer (0 to 100), recommended: 90|
| `manualMinValues` | manually defined minimum value to scale images | vector&lt;float&gt; of size `numberOfPlanes` |
| `manualMaxValues` | manually defined maximum value to scale images | vector&lt;float&gt; of size `numberOfPlanes` |
| `numberOfPlanes` | the number of imaging planes in the dataset | integer |

</details>
